.PHONY: clean All

All:
	@echo ----------Building project:[ Sockets - Debug ]----------
	@cd "src/Sockets" && "$(MAKE)" -f "Sockets.mk" type=Debug 
	@echo ----------Building project:[ Resource - Debug ]----------
	@cd "src/Resource" && "$(MAKE)" -f "Resource.mk" type=Debug 
	@echo ----------Building project:[ DAL - Debug ]----------
	@cd "src/DAL" && "$(MAKE)" -f "DAL.mk" type=Debug 
	@echo ----------Building project:[ Generator - Debug ]----------
	@cd "src/Generator" && "$(MAKE)" -f "Generator.mk" type=Debug 
	@echo ----------Building project:[ DB - Debug ]----------
	@cd "src/DB" && "$(MAKE)" -f "DB.mk" type=Debug 
	@echo ----------Building project:[ Initializer - Debug ]----------
	@cd "src/Initializer" && "$(MAKE)" -f "Initializer.mk" type=Debug 
	@echo ----------Building project:[ Core - Debug ]----------
	@cd "src/Core" && "$(MAKE)" -f "Core.mk" type=Debug 
	@echo ----------Building project:[ Server - Debug ]----------
	@cd "src/Server" && "$(MAKE)" -f "Server.mk" type=Debug 
clean:
	@echo ----------Building project:[ Sockets - Debug ]----------
	@cd "src/Sockets" && "$(MAKE)" -f "Sockets.mk" type=Debug  clean
	@echo ----------Building project:[ Resource - Debug ]----------
	@cd "src/Resource" && "$(MAKE)" -f "Resource.mk" type=Debug  clean
	@echo ----------Building project:[ DAL - Debug ]----------
	@cd "src/DAL" && "$(MAKE)" -f "DAL.mk" type=Debug  clean
	@echo ----------Building project:[ Generator - Debug ]----------
	@cd "src/Generator" && "$(MAKE)" -f "Generator.mk" type=Debug  clean
	@echo ----------Building project:[ DB - Debug ]----------
	@cd "src/DB" && "$(MAKE)" -f "DB.mk" type=Debug  clean
	@echo ----------Building project:[ Initializer - Debug ]----------
	@cd "src/Initializer" && "$(MAKE)" -f "Initializer.mk" type=Debug  clean
	@echo ----------Building project:[ Core - Debug ]----------
	@cd "src/Core" && "$(MAKE)" -f "Core.mk" type=Debug  clean
	@echo ----------Building project:[ Server - Debug ]----------
	@cd "src/Server" && "$(MAKE)" -f "Server.mk" type=Debug  clean
