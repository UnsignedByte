/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Generator.h
 * This file contains the Generator class.
 *
 * @see Generator 
 */ 

#include <fstream>

#include "Types.h"

/**
 * This class will generate TableImpls and FieldImpls.
 * 
 * It will retreive it's data from the Tables singleton. 
 * 
 * @see Tables 
 */ 
class Generator
{
public:
	/** Create a Generator that puts all classes in the specified namespace. */
	explicit Generator(const std::string& name);
	
	/** Destructor, closes all files. */
	~Generator();
	
	/** Generates the definitions required by the DAL. */
	void GenerateDAL(TableDefVector::const_iterator begin, TableDefVector::const_iterator end);

private:
	/** Hide the copy constructor. */
	Generator(const Generator& rhs);
	
	/** Hide the assignment operator. */
	Generator operator=(const Generator& rhs);
	
	void AppendLicense(std::ofstream& file);
	void AppendGeneratorNotice(std::ofstream& file);
	
	void AppendHeaderIncludes();
	void AppendHeaderClass(TableDefPtr table);
	void AppendHeaderFooter();
	void CreateHeader();
	
	void AppendSourceIncludes();
	void AppendSourceClass(TableDefPtr table);
	void AppendSourceFooter();
	void CreateSource();
	
	void AppendHeaderTableImpls();
	void AppendSourceTableImpls();
	void CreateTI();
	
	std::string m_name; /**< The namespace to put all classes in. */
	std::string m_fieldname; /**< The name of the file containing the FieldImpls. */
	std::string m_tablename; /**< The name of the file containing the TableImpls. */
	std::string m_tabs; /**< The string to use as tabbification sequence. */
	std::ofstream m_headerfile; /**< An output file stream to the FieldImpls headerfile. */
	std::ofstream m_sourcefile; /**< An output file stream to the FieldImpls sourcefile. */
	std::ofstream m_tiheaderfile; /**< An output file stream to the TableImpls headerfile. */
	std::ofstream m_tisourcefile; /**< An output file stream to the TableImpls sourcefile. */
	
	TableDefVector::const_iterator m_begin; /**< The begin iterator to use to get the tables. */
	TableDefVector::const_iterator m_end; /**< The end iterator ot use to get the tables. */
};
