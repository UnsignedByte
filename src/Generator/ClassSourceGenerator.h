/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file ClassSourceGenerator.h
 * This file contains the ClassSourceGenerator class.
 * 
 * @see ClassSourceGenerator 
 */ 

#include <fstream>

#include "Types.h"

/**
 * This class generates the FieldImpls implementations for the specified table and appends it to the specified file.
 */ 
class ClassSourceGenerator
{
public:
	/** Construct a new Generator for the specified table and output to the specified file. */
	explicit ClassSourceGenerator(TableDefPtr table, std::ofstream* file);
	
	/** Destructor, a noop. */
	~ClassSourceGenerator();
	
	/** Generates the class containing the FieldImpls for the specified table. */
	void GenerateClass();

private:
	/** Hide the copy constructor. */
	ClassSourceGenerator(const ClassSourceGenerator& rhs);
	
	/** Hide the assignment operator. */
	ClassSourceGenerator operator=(const ClassSourceGenerator& rhs);
	
	std::string m_tabs; /**< The character to use as tabbification sequence. */
	std::string m_name; /**< The name of the table this ClassHeaderGenerator will generate the FieldImpls for. */
	TableDefPtr m_table; /**< The table this ClassHeaderGenerator will generate the FieldImpls for. */
	std::ofstream* m_file; /**< The file to append the FieldImpls to. */
};
