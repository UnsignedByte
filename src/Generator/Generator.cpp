/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>

#include "Assert.h"
#include "ClassHeaderGenerator.h"
#include "ClassSourceGenerator.h"
#include "Generator.h"
#include "Global.h"
#include "StringUtilities.h"
#include "TableDef.h"
#include "Tables.h"

using std::cout;
using std::endl;

Generator::Generator(const std::string& name) :
m_name(name),
m_fieldname("FieldImpls"),
m_tablename("TableImpls"),
m_tabs("\t")
{
	std::string headername;
	headername.append(m_fieldname);
	headername.append(".h");
	m_headerfile.open(headername.c_str());
	cout << "> " << headername << endl;
	
	std::string sourcename;
	sourcename.append(m_fieldname);
	sourcename.append(".cpp");
	m_sourcefile.open(sourcename.c_str());
	cout << "> " << sourcename << endl;
	
	std::string tiheadername;
	tiheadername.append(m_tablename);
	tiheadername.append(".h");
	m_tiheaderfile.open(tiheadername.c_str());
	cout << "> " << tiheadername << endl;
	
	std::string tisourcename;
	tisourcename.append(m_tablename);
	tisourcename.append(".cpp");
	m_tisourcefile.open(tisourcename.c_str());
	cout << "> " << tisourcename << endl;
}

Generator::~Generator()
{
	Assert(m_headerfile.is_open());
	Assert(m_sourcefile.is_open());
	Assert(m_tiheaderfile.is_open());
	Assert(m_tisourcefile.is_open());

	m_headerfile.close();
	m_sourcefile.close();
	m_tiheaderfile.close();
	m_tisourcefile.close();
}

void Generator::GenerateDAL(TableDefVector::const_iterator begin, TableDefVector::const_iterator end)
{
	m_begin = begin;
	m_end = end;
	
	CreateHeader();
	CreateSource();
	CreateTI();
}

void Generator::AppendLicense(std::ofstream& file)
{
	Assert(file);
		
	file << "/***************************************************************************" << endl;
	file << " *   Copyright (C) 2008 by Sverre Rabbelier                                *" << endl;
	file << " *   sverre@rabbelier.nl                                                   *" << endl;
	file << " *                                                                         *" << endl;
	file << " *   This program is free software; you can redistribute it and/or modify  *" << endl;
	file << " *   it under the terms of the GNU General Public License as published by  *" << endl;
	file << " *   the Free Software Foundation; either version 3 of the License, or     *" << endl;
	file << " *   (at your option) any later version.                                   *" << endl;
	file << " *                                                                         *" << endl;
	file << " *   This program is distributed in the hope that it will be useful,       *" << endl;
	file << " *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *" << endl;
	file << " *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *" << endl;
	file << " *   GNU General Public License for more details.                          *" << endl;
	file << " *                                                                         *" << endl;
	file << " *   You should have received a copy of the GNU General Public License     *" << endl;
	file << " *   along with this program; if not, write to the                         *" << endl;
	file << " *   Free Software Foundation, Inc.,                                       *" << endl;
	file << " *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *" << endl;
	file << " ***************************************************************************/" << endl;
	file << endl;
	
	return;
}

void Generator::AppendGeneratorNotice(std::ofstream& file)
{
	Assert(file);

	file << "/* NOTE: This file was generated automatically. Do not edit. */" << endl;
	file << endl;
}

void Generator::AppendHeaderIncludes()
{
	Assert(m_headerfile);
	
	m_headerfile << "#pragma once" << endl;
	m_headerfile << endl;
	m_headerfile << "/**" << endl;
	m_headerfile << " * @file " << m_fieldname << ".h" << endl;
	m_headerfile << " * This file contains the generated FieldImpl classes." << endl;
	m_headerfile << " *" << endl;
	m_headerfile << " * @see FieldImpl" << endl;
	m_headerfile << " */" << endl;
	m_headerfile << endl;	
	
	m_headerfile << "#include \"Types.h\"" << endl;	
	m_headerfile << "#include \"TableImpl.h\"" << endl;	
	m_headerfile << endl;
	
	m_headerfile << "namespace " << m_name << endl;
	m_headerfile << "{" << endl;
	
	return;
}

void Generator::AppendHeaderClass(TableDefPtr table)
{
	Assert(table);
	
	ClassHeaderGenerator gen(table, &m_headerfile, m_name);
	gen.GenerateClass();
	
	return;
}

void Generator::AppendHeaderFooter()
{
	Assert(m_headerfile);
	
	m_headerfile << "} // end of namespace" << endl;
	m_headerfile << endl;
	
	return;
}

void Generator::CreateHeader()
{
	AppendLicense(m_headerfile);
	AppendGeneratorNotice(m_headerfile);
	AppendHeaderIncludes();
	
	for(TableDefVector::const_iterator it = m_begin; it != m_end; it++)
		AppendHeaderClass(*it);
			
	AppendHeaderFooter();
	
	return;
}

void Generator::AppendSourceIncludes()
{
	Assert(m_sourcefile);
	
	m_sourcefile << "#include \"FieldImpls.h\"" << endl;
	m_sourcefile << "#include \"TableImpl.h\"" << endl;
	m_sourcefile << "#include \"TableImpls.h\"" << endl;
	m_sourcefile << "#include \"Tables.h\"" << endl;
	m_sourcefile << "#include \"FieldImpl.h\"" << endl;
	m_sourcefile << "#include \"KeyImpl.h\"" << endl;
	m_sourcefile << endl;
	
	m_sourcefile << "using namespace " << m_name << ";" << endl;
	m_sourcefile << endl;
	
	return;
}

void Generator::AppendSourceClass(TableDefPtr table)
{
	Assert(table);
	
	ClassSourceGenerator gen(table, &m_sourcefile);
	gen.GenerateClass();
	
	return;
}

void Generator::AppendSourceFooter()
{
	Assert(m_sourcefile);
	
	m_headerfile << endl;
	
	return;
}

void Generator::CreateSource()
{
	AppendLicense(m_sourcefile);
	AppendGeneratorNotice(m_sourcefile);
	AppendSourceIncludes();
		
	for(TableDefVector::const_iterator it = m_begin; it != m_end; it++)
		AppendSourceClass(*it);
			
	AppendSourceFooter();
	
	return;
}

void Generator::CreateTI()
{
	AppendLicense(m_tiheaderfile);
	AppendGeneratorNotice(m_tiheaderfile);
	AppendHeaderTableImpls();
		
	AppendLicense(m_tisourcefile);
	AppendGeneratorNotice(m_tisourcefile);
	AppendSourceTableImpls();
}

void Generator::AppendHeaderTableImpls()
{
	Assert(m_tiheaderfile);

	m_tiheaderfile << "#pragma once" << endl;
	m_tiheaderfile << endl;
		
	m_tiheaderfile << "/**" << endl;
	m_tiheaderfile << " * @file " << m_tablename << ".h" << endl;
	m_tiheaderfile << " * This file contains the generated " << m_name << "::TableImpls class." << endl;
	m_tiheaderfile << " *" << endl;
	m_tiheaderfile << " * @see " << m_name << "::TableImpls" << endl;
	m_tiheaderfile << " */" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << "#include \"Types.h\"" << endl;
	m_tiheaderfile << "#include \"FieldImpls.h\"" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << "namespace " << m_name << endl;
	m_tiheaderfile << "{" << endl;
	
	m_tiheaderfile << m_tabs << "/**" << endl;
	m_tiheaderfile << m_tabs << " * This is a generated class that contains all TableImpls." << endl;
	m_tiheaderfile << m_tabs << " *" << endl;
	m_tiheaderfile << m_tabs << " * Before using any tables, <code>Initialize</code> should be called." << endl;
	m_tiheaderfile << m_tabs << " *" << endl;
	m_tiheaderfile << m_tabs << " * @see Initialize" << endl;
	m_tiheaderfile << m_tabs << " */" << endl;
	m_tiheaderfile << m_tabs << "class TableImpls : public Singleton<db::TableImpls>" << endl;
	m_tiheaderfile << m_tabs << "{" << endl;
	m_tiheaderfile << m_tabs << "public:" << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** Initialize all tables. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "void Initialize();" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** Returns an iterator to the beginning of m_tables. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImplVector::const_iterator begin() const { return m_tables.begin(); }" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** Returns an iterator to the end of m_tables. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImplVector::const_iterator end() const { return m_tables.end(); }" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << endl;
	for(TableDefVector::const_iterator it = m_begin; it != m_end; it++)
	{
		TableDefPtr table = *it;
		m_tiheaderfile << m_tabs << m_tabs << "SmartPtr<" << table->getName() << "> " << String::Get()->toupper(table->getName()) << ";"; 
		m_tiheaderfile << " /**< TableImpl for the " << table->getName() << " class. */" << endl;
	}
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << "private:" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "/** This is a singleton class, use <code>Get</code> to get an instance. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImpls();" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** This is a singleton class, use <code>Free</code> to free the instance. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "~TableImpls() { }" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** Hide the copy constructor. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImpls(const TableImpls& rhs);" << endl;
	m_tiheaderfile << endl;
	
	m_tiheaderfile << m_tabs << m_tabs << "/** Hide the assignment operator. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImpls operator=(const TableImpls& rhs);" << endl;
	m_tiheaderfile << endl;	
	
	m_tiheaderfile << m_tabs << m_tabs << "friend class Singleton<TableImpls>;" << endl;
	m_tiheaderfile << endl;
	m_tiheaderfile << m_tabs << m_tabs << "TableImplVector m_tables; /**< All generated tables. */" << endl;
	m_tiheaderfile << m_tabs << m_tabs << "bool m_initialized; /**< Whether this manager is initialized yet. */" << endl;
	m_tiheaderfile << m_tabs << "};" << endl;
	m_tiheaderfile << "} // end of namespace" << endl;
	m_tiheaderfile << endl;
}

void Generator::AppendSourceTableImpls()
{
	Assert(m_tisourcefile);

	//m_tisourcefile << "#ifdef _WIN32" << endl;
	//m_tisourcefile << "#pragma warning (disable:4244)" << endl;
	//m_tisourcefile << "#pragma warning (disable:4267)" << endl;
	//m_tisourcefile << "#endif" << endl;
	//m_tisourcefile << endl;
	
	m_tisourcefile << "#include \"TableImpl.h\"" << endl;
	m_tisourcefile << "#include \"TableImpls.h\"" << endl;
	m_tisourcefile << "#include \"Tables.h\"" << endl;
	m_tisourcefile << "#include \"FieldImpl.h\"" << endl;
	m_tisourcefile << "#include \"FieldImpls.h\"" << endl;
	m_tisourcefile << "#include \"KeyImpl.h\"" << endl;
	m_tisourcefile << endl;
	
	m_tisourcefile << "using namespace " << m_name << ";" << endl;
	m_tisourcefile << endl;	
	
	m_tisourcefile << "TableImpls::TableImpls() :" << endl;
	for(TableDefVector::const_iterator it = m_begin; it != m_end; it++)
	{
		TableDefPtr table = *it;
		Assert(table);
		
		if(it != m_begin)
			m_tisourcefile << "," << endl;
		
		m_tisourcefile << String::Get()->toupper(table->getName()) << "( SmartPtr<db::" << table->getName() <<">(new db::" << table->getName() << "()) )";
	}
	
	m_tisourcefile << endl;
	m_tisourcefile << "{" << endl;
	for(TableDefVector::const_iterator it = m_begin; it != m_end; it++)
	{
		TableDefPtr table = *it;
		Assert(table);
		
		m_tisourcefile << m_tabs << "m_tables.push_back(" << String::Get()->toupper(table->getName()) << ");" << endl;
	}
	m_tisourcefile << "}" << endl;
	m_tisourcefile << endl;
	
	m_tisourcefile << "void TableImpls::Initialize()" << endl;
	m_tisourcefile << "{" << endl;
	m_tisourcefile << m_tabs << "Assert(!m_initialized);" << endl;
	m_tisourcefile << endl;
	
	m_tisourcefile << m_tabs << "m_initialized = true;" << endl;
	m_tisourcefile << endl;	
	
	m_tisourcefile << m_tabs << "for(TableImplVector::const_iterator it = m_tables.begin(); it != m_tables.end(); it++)" << endl;		
	m_tisourcefile << m_tabs << "{" << endl;
	m_tisourcefile << m_tabs << m_tabs << "TableImplPtr table = *it;" << endl;
	m_tisourcefile << m_tabs << m_tabs << "Assert(table);" << endl;
	m_tisourcefile << endl;
	m_tisourcefile << m_tabs << m_tabs << "table->Initialize();" << endl;
	m_tisourcefile << m_tabs << "}" << endl;
	m_tisourcefile << endl;

	m_tisourcefile << "}" << endl;
	m_tisourcefile << endl;
}
