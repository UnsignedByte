/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file ClassHeaderGenerator.h
 * This file contains the ClassHeaderGenerator class.
 *
 * @see ClassHeaderGenerator 
 */ 

#include <fstream>

#include "Types.h"

/**
 * This class generates the FieldImpls declarations for the specified table and appends it to the specified file.
 */ 
class ClassHeaderGenerator
{
public:
	/** Construct a new Generator for the specified table and output to the specified file. */
	explicit ClassHeaderGenerator(TableDefPtr table, std::ofstream* file, std::string ns);
	
	/** Destructor, a noop. */
	~ClassHeaderGenerator();
	
	
	/** Generates the class containing the FieldImpls for the specified table. */
	void GenerateClass();

private:
	/** Hide the copy constructor. */
	ClassHeaderGenerator(const ClassHeaderGenerator& rhs);
	
	/** Hide the assignment operator. */
	ClassHeaderGenerator operator=(const ClassHeaderGenerator& rhs);
	
	void AppendHeader();
	void AppendCtor();
	void AppendFields();
	void AppendBody();
	void AppendFooter();
		
	std::string m_tabs; /**< The character to use as tabbification sequence. */
	std::string m_name; /**< The name of the table this ClassHeaderGenerator will generate the FieldImpls for. */
	std::string m_namespace; /**< The namespace these classes are generated in. */
	TableDefPtr m_table; /**< The table this ClassHeaderGenerator will generate the FieldImpls for. */
	std::ofstream* m_file; /**< The file to append the FieldImpls to. */
};
