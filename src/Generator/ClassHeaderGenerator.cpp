/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "ClassHeaderGenerator.h"
#include "FieldDef.h"
#include "Global.h"
#include "StringUtilities.h"
#include "TableDef.h"
#include "Tables.h"

using std::endl;

ClassHeaderGenerator::ClassHeaderGenerator(TableDefPtr table, std::ofstream* file, std::string ns) :
m_tabs("\t"),
m_namespace(ns),
m_table(table),
m_file(file)
{
	m_name = m_table->getName();
}

ClassHeaderGenerator::~ClassHeaderGenerator()
{

}

void ClassHeaderGenerator::GenerateClass()
{
	Assert(m_table->primarykeysize() > 0);

	AppendHeader();
	AppendFields();
	AppendFooter();
}

void ClassHeaderGenerator::AppendHeader()
{
	Assert(m_file);
	
	(*m_file) << m_tabs << "/**" << endl;
	(*m_file) << m_tabs << " * This is a generated class that contains all FieldImpls for the " <<  m_name << " table." << endl;
	(*m_file) << m_tabs << " */" << endl;
	(*m_file) << m_tabs << "class " << m_name << " : public TableImpl" << endl;
	(*m_file) << m_tabs << "{" << endl;
	(*m_file) << m_tabs << "public:" << endl;
	
	return;
}

void ClassHeaderGenerator::AppendFields()
{
	for(FieldDefVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		(*m_file) << m_tabs << m_tabs;
		
		FieldDefPtr field = *it;
		if(field->isKey())
			(*m_file) << "KeyImplPtr ";
		else
			(*m_file) << "FieldImplPtr ";
			
		(*m_file) << String::Get()->toupper(field->getName()) << ";";
		(*m_file) << " /**< FieldImplPtr for the " << field->getName() << " field. */" << endl;
	}
	(*m_file) << endl;
}

void ClassHeaderGenerator::AppendFooter()
{
	Assert(m_file);
	
	(*m_file) << m_tabs << "private:" << endl;
	(*m_file) << m_tabs << m_tabs << "/** This is a managed class, use <code>TableImpls</code> to get an instance. */" << endl;
	(*m_file) << m_tabs << m_tabs << m_name << "(): TableImpl(\"" << m_name << "\") { }" << endl;
	(*m_file) << endl;
	
	(*m_file) << m_tabs << m_tabs << "/** This is a managed class, free <code>TableImpls</code> to free the instance. */" << endl;	
	(*m_file) << m_tabs << m_tabs << "~" << m_name << "() { }" << endl;
	(*m_file) << endl;
	
	(*m_file) << m_tabs << m_tabs << "/** Hide the copy constructor. */" << endl;
	(*m_file) << m_tabs << m_tabs << m_name << "(const " << m_name << "& rhs);" << endl;
	(*m_file) << endl;
	
	(*m_file) << m_tabs << m_tabs << "/** Hide the assignment operator. */" << endl;
	(*m_file) << m_tabs << m_tabs << m_name << " operator=(const " << m_name << "& rhs);" << endl;
	(*m_file) << endl;
	
	(*m_file) << m_tabs << m_tabs << "/** Initialize the table's fields. */" << endl;
	(*m_file) << m_tabs << m_tabs << "void Initialize();" << endl;
	(*m_file) << endl;
	
	(*m_file) << m_tabs << m_tabs << "friend class TableImpls;" << endl;
	(*m_file) << m_tabs << m_tabs << "friend SmartPtrDelete(" << m_name << ");" << endl;
	(*m_file) << m_tabs << "};" << endl;
	(*m_file) << endl;
	
	return;
}
