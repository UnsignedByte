/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "ClassSourceGenerator.h"
#include "FieldDef.h"
#include "Global.h"
#include "KeyDef.h"
#include "StringUtilities.h"
#include "TableDef.h"
#include "Tables.h"

using std::endl;

ClassSourceGenerator::ClassSourceGenerator(TableDefPtr table, std::ofstream* file) :
m_tabs("\t"),
m_table(table),
m_file(file)
{
	Assert(table);
	
	m_name = m_table->getName();	
}

ClassSourceGenerator::~ClassSourceGenerator()
{

}

void ClassSourceGenerator::GenerateClass()
{
	Assert(m_table->primarykeysize() > 0);
		
	Assert(m_file);			

	(*m_file) << "void " << m_name << "::Initialize()" << endl;
	(*m_file) << "{" << endl;
	for(FieldDefVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{		
		FieldDefPtr field = *it;
			
		(*m_file) << m_tabs << String::Get()->toupper((*it)->getName()) << " = ";
		
		// C++ -does- have reflection, it does now at least.
		if(field->isKey())
		{
			KeyDef* keydef = (KeyDef*)field.get();
			// KeyImpl(TableImplPtr <these ones>, TableImplPtr <these ones>, ...);
			(*m_file) << "KeyImplPtr(new KeyImpl";
			(*m_file) << "(TableImpls::Get()->" << String::Get()->toupper(m_table->getName());
			(*m_file) << ", TableImpls::Get()->" << String::Get()->toupper(keydef->getForeignTable()->getName());
		}
		else
		{
			// FieldImpl(TableImplPtr <this one>, ...);
			(*m_file) << "FieldImplPtr(new FieldImpl";
			(*m_file) << "(TableImpls::Get()->" << String::Get()->toupper(m_table->getName());
		}
		
		// ...Impl(TableImplPtr table, ..., cstring <this one>, ...);
		(*m_file) << ", \"" << (*it)->getName() << "\"";
		
		if(field->isKey())
		{
			if(field->isPrimaryKey()) 
			{				
				// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool <primary>, ...);
				(*m_file) << ", true";
			
				if(m_table->primarykeysize() == 1)
				{
					// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool <lookup>);
					(*m_file) << ", true";
				}
				else
				{
					// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool <lookup>);			
					(*m_file) << ", false";
				}
			}
			else
			{											
				// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool <primary>, ...);
				(*m_file) << ", false";
									
				if(field->isLookup())
				{
					// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool <lookup>);
					(*m_file) << ", true";
				}
				else
				{
					// KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool <lookup>);
					(*m_file) << ", false";
				}				
			}
		}
		else
		{
			if(field->isText())
			{
				// FieldImpl(TableImplPtr table, cstring name, bool <text>, ...);
				(*m_file) << ", true";
			}
			else
			{
				// FieldImpl(TableImplPtr table, cstring name, bool <text>, ...);
				(*m_file) << ", false";
			}
			
			std::string defaultvalue = field->getDefaultValue();
			if(defaultvalue.size() > 0)
			{
				// FieldImpl(TableImplPtr table, cstring name, bool text, const std::string& <defaultvalue>);
				(*m_file) << ", std::string(\"" << defaultvalue << "\")";
			}
			else
			{				
				if(field->isLookup())
				{
					// FieldImpl(TableImplPtr table, cstring name, bool text, bool <lookup>);
					(*m_file) << ", true";
				}
				else
				{
					// FieldImpl(TableImplPtr table, cstring name, bool text, bool <lookup>);
					(*m_file) << ", false";
				}
			}
		}
		
		(*m_file) << "));";
		(*m_file) << endl;
	}
	(*m_file) << endl;
		
	for(FieldDefVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{			
		FieldDefPtr field = *it;
		Assert(field);
		
		(*m_file) << m_tabs << "m_fields.push_back(" << String::Get()->toupper((*it)->getName()) << ");" << endl;
	}
	
	(*m_file) << endl;
	(*m_file) << m_tabs << "updateFieldCount();" << endl;

	(*m_file) << "}" << endl;
	(*m_file) << endl;
}
