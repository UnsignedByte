/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <fstream>

#ifdef _WIN32
#include <winsock2.h>
#endif

#include "FieldImpls.h"
#include "GameVersion.h"
#include "SavableHeaders.h"
#include "TableImpls.h"
#include "DatabaseMgr.h"
#include "Global.h"
#include "ListenSocket.h"
#include "SQLSocket.h"
#include "UBHandler.h"
#include "UBSocket.h"
#include "SqliteMgr.h"
#include "Managers.h"
#include "StringUtilities.h"

void exitfunc()
{
	printf("exiting...\n");
	std::cin.get();
	return;
}

std::string add(SocketHandler* h,int port)
{
	ListenSocket<UBSocket> *l = new ListenSocket<UBSocket>(*h);
	
	std::string msg = "Attempting bind on port";
	msg.append(String::Get()->fromInt(port));
	msg.append("... ");
		
	if (l -> Bind(port))
	{
		msg.append("Not successful\n");
		delete l;		
	}
	else
	{
		msg.append("OK\n");
		l -> SetDeleteByHandler();
		h->Add(l);
	}
	
	return msg;
}

std::string addSQL(SocketHandler* h, int port)
{
	ListenSocket<SQLSocket> *l = new ListenSocket<SQLSocket>(*h);
	
	std::string msg = "Attempting SQL bind on port ";
	msg.append(String::Get()->fromInt(port));
	msg.append("... ");
	
	
	if (l -> Bind(port))
	{
		msg.append("Not successful\n");
		delete l;		
	}
	else
	{
		msg.append("OK\n");
		l -> SetDeleteByHandler();
		h->Add(l);
	}
	
	return msg;
}

bool g_quit = false;
extern bool g_shutdown;
extern bool g_nocatch;
extern bool g_printsql;
extern bool g_printstatus;
extern bool g_printbugs;
extern bool g_printlogs;

static void logToFile(Global* global, const std::string& text)
{
	std::string output = text;
	output.append("\n");
	
	std::ofstream errorfile;
	errorfile.open("errorlog.txt", std::ios_base::app);
	
	if(!errorfile.is_open())
	{
		global->printexception("Could not open errorlog file.\n");
		return;
	}
	
	errorfile.write(output.c_str(), output.size());
	if(errorfile.bad())
	{
		global->printexception("Could not write to errorlog file.\n");
		return;
	}
	
	return;
}

static void printSyntaxHelp(Global* global, const std::string& name)
{
	std::string msg = "usage: ";
	
	msg.append(name);
	msg.append(" [-n|--no-catch] [-q|--quiet] [-s|--sql] [-t|--status] [-b|--bugs] [-l|--logs]");
	
	msg.append("\n");
	
	global->printraw(msg);
}

static bool parseArgs(Global* global, int argc, char** argv)
{
	bool quiet = false;	
	
	for(int i = 1; i < argc; i++)
	{	
		bool parsed = false;
		
		if(!strncmp("-n", argv[i], 2) || !strncmp("--no-catch", argv[i], 10))
		{			
			global->printmode("Not catching exceptions.\n");
			g_nocatch = true;
			parsed = true;
		}
		
		if(!strncmp("-q", argv[i], 2) || !strncmp("--quiet", argv[i], 5))
		{
			global->printmode("Quiet mode enabled.\n");
			g_printstatus = false;
			g_printbugs = false;
			g_printlogs = false;
			g_printsql = false;
			quiet = true;
			parsed = true;
		}
		
		if(!strncmp("-s", argv[i], 2) || !strncmp("--sql", argv[i], 5))
		{
			if(quiet)
				global->printmode("Overriding quiet, printing sql anyway.\n");
			else
				global->printmode("Printing sql.\n");
				
			g_printsql = true;
			parsed = true;
		}
		
		if(!strncmp("-t", argv[i], 2) || !strncmp("--status", argv[i], 8))
		{
			if(quiet)
				global->printmode("Overriding quiet, printing status msgs anyway.\n");
			else
				global->printmode("Printing status msgs.\n");
				
			g_printstatus = true;
			parsed = true;
		}
		
		if(!strncmp("-b", argv[i], 2) || !strncmp("--bugs", argv[i], 5))
		{
			if(quiet)
				global->printmode("Overriding quiet, printing bug msgs anyway.\n");
			else
				global->printmode("Printing bug msgs.\n");
				
			g_printbugs = true;
			parsed = true;
		}
		
		if(!strncmp("-l", argv[i], 2) || !strncmp("--log", argv[i], 5))
		{
			if(quiet)
				global->printmode("Overriding quiet, printing log msgs anyway.\n");				
			else
				global->printmode("Printing log msgs.\n");
				
			g_printlogs = true;
			parsed = true;
		}
		
		if(!parsed)
		{
			printSyntaxHelp(global, argv[0]);
			return false;
		}
	}
	
	return true;
}

int main(int argc, char** argv)
{	
	Global* global = Global::Get();
	
	if(argc > 1)
	{
		bool parsed = parseArgs(global, argc, argv);
		if(!parsed)
			return 1;
	}
	
	global->printstatus("Opening database...\n");
	std::string dbname = game::vname;
	dbname.append(".db");
	DatabaseMgr::Initialize(dbname);
	global->printstatus("Database opened.\n");
	
	global->printstatus("Initializing in-memory table definitions...\n");
	db::TableImpls::Get()->Initialize();
	global->printstatus("In-memory table definitions initialized.\n");

	global->printstatus("Binding ports...\n");
	std::string portstatus;
	
	portstatus = add(UBHandler::Get(), 4000);
	global->printstatus(portstatus);
	
	portstatus = add(UBHandler::Get(), 4040);
	global->printstatus(portstatus);
	
	portstatus = add(UBHandler::Get(), 5060);
	global->printstatus(portstatus);
	
	portstatus = addSQL(UBHandler::Get(), 9090);
	global->printstatus(portstatus);
	
	global->printstatus("Ports bound.\n");
	global->printstatus("Running!\n");

	while (!g_quit)
	{
		if(g_nocatch)
		{
			UBHandler::Get()->Select(0, 200000);
			UBHandler::Get()->SwitchEditors();
		}
		else
		{		
			try {
				UBHandler::Get()->Select(0, 200000);
				UBHandler::Get()->SwitchEditors();
			} catch(std::exception& e) {
				global->printexception("While in main loop, trying to quit gracefully", e.what());
				logToFile(global, e.what());
				g_quit = true;
			}
		}
	}

	try {
		UBHandler::Get()->Shutdown();
		UBHandler::Get()->Select();
	} catch(std::exception& e) {
		global->printexception("While closing sockets, trying to quit gracefully.", e.what());
		logToFile(global, e.what());
	}	
	
	try {
		UBHandler::Free(); 
	} catch(std::exception& e) {
		global->printexception("While freeing the UBHandler, trying to quit gracefully.", e.what());
		logToFile(global, e.what());
	}
	
	g_shutdown = true;
	
	try {
		mud::Managers::Free();
	} catch(std::exception& e) {
		global->printexception("While freeing mud::Managers, trying to quit gracefully.", e.what());
		logToFile(global, e.what());
	}
	
	try {
		SqliteMgr::Free();
	} catch(std::exception& e) {
		global->printexception("While freeing the SqliteMgr, trying to quit gracefully.", e.what());
		logToFile(global, e.what());
	}
	
	try {
		DatabaseMgr::Free();
	} catch(std::exception& e) {
		global->printexception("While freeing the DatabaseMgr, trying to quit gracefully.", e.what());
		logToFile(global, e.what());
	}

	global->printstatus("End of program.\n");
	exitfunc();
	return 0;
}
