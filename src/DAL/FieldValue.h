/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file FieldValue.h
 * This file contains the FieldValue class.
 * 
 * @see FieldValue
 */ 

#include "Types.h"

/**
 * This class contains the value of one cell in a table.
 *
 * A value belongs to a specific field.
 * Values are strongly typed, this is asserted at runtime.
 */ 
class FieldValue
{
public:
	/** Constructs a Value object with no value (e.g., a NULL cell). */
	FieldValue(FieldImplPtr field);
	
	/** Constructs a Value object with a string value (e.g., a Text cell). */
	FieldValue(FieldImplPtr field, cstring value);
	
	/** Constructs a Value object with a value_type valueu (e.g. an Integer cell). */
	FieldValue(FieldImplPtr field, value_type value);
	
	/** Value destructor, a noop. */
	virtual ~FieldValue();
	
	/** Returns what field this Value belongs to. */
	FieldImplPtr getField() const;
	
	/** Returns the table of the field this Value belongs to. */
	TableImplPtr getTable() const;
	
	/** Returns the name of the field this value belongs to. */
	cstring getName() const;

	/** Returns the string value of this Value, only valid if this value belongs to a Text cell. */
	const std::string& getStringValue() const;
	
	/** Returns the integer value of this Value, only valid if this value does not belong to a Text cell. */
	value_type getIntegerValue() const;
	
	/** Returns the boolean value of this Value, oly valid if this value does not belong to a Text cell. */
	bool getBoolValue() const;
	
	/** Returns wether this Value is 'dirty', e.g., if it has been changed without being saved. */
	bool isDirty() const;
	
	/** Whether this field is a key field. */
	virtual bool isKey() const { return false; }
	
	
	/** Sets the text value of this Value, only valid if this value belongs to a Text cell. */
	void setTextValue(const std::string& value);
	
	/** Sets the integer value of this Value, only valid if this value does not belong to a Text cell. */
	void setIntegerValue(value_type value);
	
	/** Sets the integer valueu of this Value to 0 or 1, only valid if this value does not belong to a Text cell. */
	void setBoolValue(bool value);
	
	/** Sets this Value's dirtyness, e.g., if it has been changed without being saved. */
	void setDirty(bool dirty);
	
private:
	FieldImplPtr m_field; /**< Member containing the field this value belongs to. */
	std::string m_textvalue; /**< Member containing the field's text value. */
	value_type m_integervalue; /**< Member containing the field's integer value. */
	bool m_dirty; /**< Wether the field is dirty or not. */
};
