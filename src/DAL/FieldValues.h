/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file FieldValues.h
 * This file contains the FieldValues bucket class.
 * 
 * @see FieldValues
 * @see FieldValue
 */

#include "Types.h"

/**
 * This class is a bucket for Value objects.
 * 
 * It will hold any Value objects for the table it was constructed for.
 * Value objects cann be added through the <code>addValue</code> function. 
 * Also, any  tables added with <code>addJoinedTable</code> are allowed.
 *
 * @see FieldValue
 * @see addJoinedTable
 * @see addValue
 */ 
class FieldValues
{
public:
	/** Constuct a new Values bucket for fields of this specific table. */
	FieldValues(TableImplPtr table);
	
	/** Destructor, a noop */
	~FieldValues();
	
	/** Returns the table this Values bucket is for. */
	TableImplPtr getTable() const;
	
	/** Add a Value, this value is run-time asserted to be of the proper table. */
	void addValue(FieldValuePtr key);
	
	/** Add a Value, this value is run-time asserted to be of the proper table. */
	void addValue(FieldImplPtr field, cstring value);
	
	/** Add a Value, this value is run-time asserted to be of the proper table. */
	void addValue(FieldImplPtr field, value_type value);
	
	/** Add a table from which Value's may be added. */
	void addJoinedTable(TableImplPtr joinedTable);
	
	
	/** Returns the number of added fields. */
	size_t size() const;
	
	/** Returns the first stored Value. */
	ValuePtr first() const;
	
	/** Returns an iterator to the first stored value. */
	ValueMap::const_iterator begin() const;
	
	/** Returns an iterator to the 'end' iterator. */
	ValueMap::const_iterator end() const;
	
	/** Returns an iterator to the result of 'find' with this specific field as argument. */
	ValueMap::const_iterator find(FieldImplPtr field) const;
	
	/** Returns a Value for this field, asserted not to be the 'end' iterator. */
	ValuePtr getField(FieldImplPtr field) const;

private:
	TableImplPtr m_table; /**< A member containing the table this Values bucket is for. */
	ValueMap m_fields; /**< A member containing the Value's that have been added. */
	TableImplVector m_joinedTables; /**< A member containing the joined tables. */
};
