/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldDef.h"
#include "FieldImpl.h"
#include "KeyImpl.h"
#include "SavableManager.h"
#include "SavableManagers.h"
#include "SelectionMask.h"
#include "SqliteMgr.h"
#include "StringUtilities.h"
#include "TableImpl.h"

TableImpl::TableImpl(cstring name) :
m_name(name),
m_primarykeysize(0)
{

}

cstring TableImpl::getName() const
{
	return m_name;
}

Strings TableImpl::tableList(SelectionMaskPtr mask)
{
	std::vector<Strings> result;
	
	SqliteMgr::Get()->doSelectMulti(mask.get());
	SavableManagersPtr managers = mask->getResult();
	
	if(!managers->size())
	{
		Strings emptyResult;
		emptyResult.push_back("None.");
		return emptyResult;
	}
	
	Strings headerfields = managers->getHeader();	
	
	for(SavableManagerVector::const_iterator it = managers->begin(); it != managers->end(); it++)
	{
		SavableManagerPtr manager = *it;					
		result.push_back(manager->fieldList());
	}
	
	return String::Get()->createTable(result, headerfields);	
}

KeyImplPtr TableImpl::firstkey() const
{
	Assert(m_primarykeysize == 1);
	KeyImplPtr key;
	
	for(FieldImplVector::const_iterator it = m_fields.begin(); it != m_fields.end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;
			
		key = boost::static_pointer_cast<KeyImpl>(field);
	}
	
	Assert(key);
	return key;
}

void TableImpl::updateFieldCount()
{
	for(FieldImplVector::const_iterator it = m_fields.begin(); it != m_fields.end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(field->isPrimaryKey())
			m_primarykeysize++;
	}
}
