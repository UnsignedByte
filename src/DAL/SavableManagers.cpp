/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "FieldValue.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "SavableManager.h"
#include "SavableManagers.h"
#include "TableImpl.h"

SavableManagers::SavableManagers(TableImplPtr table) : 
m_table(table)
{
	
}

SavableManagers::~SavableManagers()
{
	
}

void SavableManagers::addManager(SavableManagerPtr manager)
{
	Assert(manager->getTable() == m_table);
	
	m_managers.push_back(manager);
}

Strings SavableManagers::getHeader() const
{
	Strings result;
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr value = *it;
		if(!value->isKey() || value->isPrimaryKey())
			result.push_back(value->getName());
		else
			result.push_back(value->getName().substr(2)); // strip off the 'fk' prefix
	}
	
	return result;
}
