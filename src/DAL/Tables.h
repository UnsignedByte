/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Tables.h
 * This file contains the Tables class.
 * 
 * @see Tables
 */ 

#include "Types.h"

/**
 * This class serves as a storage for pointers to all defined Tables.
 *
 * Since C++ does not offer reflection this class was added as a form of 'manual' reflection.
 * By making use of the begin() and end() iterator the generator is able to iterate over all defined fiels. 
 * As such, when adding a table, you should add <code>m_tables.push_back(YOURTABLE)</code> at the bottom of the contructor.
 */ 
class Tables : public Singleton<Tables>
{
public:
	TableDefPtr ACCOUNTS; /**< The Accounts table. */
	TableDefPtr AREAS; /**< The Areas table, part of the Room system. */
	TableDefPtr BRANCHES; /**< The Branches table, part of the Skill system. */
	TableDefPtr CHANNELS; /**< The Channels table, the communication channels are stored here. */
	TableDefPtr CHANNELLOGS; /**< The logs of the channels, -not- a relational table! */
	TableDefPtr CHARACTERACCOUNT; /**< The Character - Accounts relational table. */
	TableDefPtr CHARACTERBRANCH; /**< The Character - Branch relational table. */
	TableDefPtr CHARACTERCLUSTER; /**< The Character - Cluster relational table. */
	TableDefPtr CHARACTERSKILL; /**< The Character - Skill relational table. */
	TableDefPtr CHARACTERSTAT; /**< The Character - Stat relational table. */
	TableDefPtr CHARACTERTREE; /**< The Character - Tree relational table. */
	TableDefPtr CHUNKS; /**< The Chunks table, part of the Room system. */
	TableDefPtr CLUSTERS; /**< The Clusters table, part of the Room system. */
	TableDefPtr COLOURS; /**< The Colours table, the colours are stored here. */
	TableDefPtr COMMANDS; /**< The Commands table, part of the Command system. */
	TableDefPtr DETAILS; /**< The Details table, part of the Room system. */
	TableDefPtr DETAILAREA; /**< The Detail - Area relational table, part of the Room system. */
	TableDefPtr DETAILROOM; /**< The Detail - Room relational table, part of the Room system. */
	TableDefPtr DETAILCHUNK; /**< The Detail - Chunk relational table, part of the Room system. */
	TableDefPtr DETAILCHARACTER; /**< The Detail - Character relational table, part of the Room system. */
	TableDefPtr DETAILDETAIL; /**< The Detail - Detail relational table, part of the Room system. */
	TableDefPtr ECHOS; /**< The Echos table, part of the Room system. */
	TableDefPtr ENTITIES; /**< The Entities table, all players, mobiles and objects are stored here. */
	TableDefPtr EXITS; /**< The Exits table, part of the Room system. */
	TableDefPtr GRANTGROUPS; /**< The Grantgroups table, part of the Command system. */
	TableDefPtr LOGS; /**< The Logs table, it contains all the logs. */
	TableDefPtr PERMISSIONS; /**< The Permissions table, part of the Command system. */
	TableDefPtr RACES; /**< The Races table, part of the Character system. */
	TableDefPtr ROOMS; /**< The Rooms table, part of the Room system. */
	TableDefPtr SECTORS; /**< The Sectors table, part of the Room system. */
	TableDefPtr SKILLS; /**< The Skills table, part of the Skill system. */
	TableDefPtr SOCIALS; /**< The Socials table. */
	TableDefPtr STATS; /**< The Skills table, part of the Skill system. */
	TableDefPtr TRACES; /**< The Traces table, part of the Trace system. */
	TableDefPtr TRACECHUNK; /**< The Trace - Chunk relational table, part of the Trace system. */
	TableDefPtr TRACEDETAIL; /**< The Trace - Detail relational table, part of the Trace system. */
	TableDefPtr TRACEENTITY; /**< The Trace - Entity relational table, part of the Trace system. */
	TableDefPtr TRACEROOM; /**< The Trace - Room relational table, part of the Trace system. */
	TableDefPtr TREES; /**< The Trees table, part of the Skill system. */
	TableDefPtr VERSION; /**< The Version table, it contains one row, the current version. */
	
	/** Returns an iterator to the beginning of m_tables. */
	TableDefVector::const_iterator begin() const { return m_tables.begin(); }
	
	/** Returns an iterator to the end of m_tables. */
	TableDefVector::const_iterator end() const { return m_tables.end(); }
	
private:
	friend class Singleton<Tables>;
	
	/** This is a singleton class, use <code>Get</code> to get an instance. */
	Tables();
	
	/** This is a singleton class, use <code>Free</code> to free the instance. */
	~Tables();
		
	TableDefVector m_tables; /**< A member containing al the tables. */
};
