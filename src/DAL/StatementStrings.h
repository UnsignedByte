/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file StatementStrings.h
 * This file contains the StatementStrings class.
 *
 * @see StatementStrings 
 */ 

#include "Types.h"

/**
 * This class is a bucket that contains the SQL statements for a certain action on a table.
 */ 
class StatementStrings
{
public:
	/** Creates an empty statements bucket. */
	StatementStrings() { }
	
	/** Destructor, a noop. */
	~StatementStrings() { }
	
	/** Returns the SQL to erase an entry from this table. */
	cstring getErase() const {return m_erase;}
	
	/** Returns the SQL to insert an entry into this table. */
	cstring getInsert() const {return m_insert;}
	
	/** Returns the SQL to update an entry from this table. */
	cstring getUpdate() const {return m_update;}
	
	/** Returns the SQL to select an entry from this table. */
	cstring getSelect() const {return m_select;}
	
	/** Returns the SQL to look up an entry from this table on the specified field. */
	cstring getLookup(FieldPtr field) {return m_lookup[field.get()];}
	
	/** Returns the SQL to list all entries in a table. */
	cstring getList() const {return m_list;}
	
	
	/** Sets the SQL to erase an entry from this table. */
	void setErase(cstring erase) { m_erase = erase; }
	
	/** Sets the SQL to insert an entry into this table. */
	void setInsert(cstring insert) { m_insert = insert; }
	
	/** Sets the SQL to update an entry in this table. */
	void setUpdate(cstring update) { m_update = update; }
	
	/** Sets the SQL to select an entry from this table. */
	void setSelect(cstring select) { m_select = select; }
	
	/** Sets the SQL to look up an entry from this table on the specified field. */
	void setLookup(FieldPtr field, cstring lookup) { Assert(field); m_lookup[field.get()] = lookup; }
	
	/** Sets the SQL to list all entries in a table. */
	void setList(cstring list) { m_list = list; }
	
private:
	typedef std::map<Field*, std::string> fieldmap; /**< A type of a map containing the SQL for each field's lookup*/
	
	std::string m_insert; /**< The SQL to insert an entry into this table. */
	std::string m_erase; /**< The SQL to erase an entry from this table. */
	std::string m_update; /**< The SQL to update an entry in this table. */
	std::string m_select; /**< The SQL to select an entry from this table. */
	fieldmap m_lookup; /**< The map containing the SQL for each field's lookup. */
	std::string m_list; /**< The SQL to list all entries in a table. */
};

typedef SmartPtr<StatementStrings> StatementStringsPtr; /**< The type of a pointer to a statements bucket. */
