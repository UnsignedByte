/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file KeyImpl.h
 * This file contains the KeyImpl class.
 *
 * @see KeyImpl 
 */ 

#include "FieldImpl.h"
#include "Types.h"

/**
 * This class represents a generated (concrete version of a) KeyDef.
 *
 * This class should not be instantiated manually.
 * Instead, the generated ones contained in TableImpls should be used.
 *
 * @see KeyDef
 * @see TableImpls 
 * @see FieldImpl
 */ 
class KeyImpl : public FieldImpl
{
public:
	/** Construct a foreign key belonging to the specified native table to the specified foreign table with the specified name. */
	KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool lookup = false);
	
	/** Destructor, a noop. */
	~KeyImpl() { }
	
	
	/** Returns a new FieldValue for this field. */
	FieldValuePtr newValue();
		
		
	/** Returns the foreign table this key points to, is equal to the native table in the case of a native key. */
	TableImplPtr getForeignTable() const ;
	
	/** Returns that this is a key field. */
	bool isKey() const { return true; }
	
	bool isPrimaryKey() const { return m_primary; }
	
	/** Returns whether this is a foreign key. */
	bool isForeignKey() const ;

private:	
	TableImplPtr m_foreignTable; /**< The table this key belongs to. */
	bool m_primary; /**< Whether this field is a primary key. */
};
