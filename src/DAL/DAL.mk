##
## Auto Generated makefile, please do not edit
##
ProjectName:=DAL

## Debug
ifeq ($(type),Debug)
ConfigurationName :=Debug
IntermediateDirectory :=./Debug
OutDir := $(IntermediateDirectory)
LinkerName:=g++
ArchiveTool :=ar rcu
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix :=.o
DebugSwitch :=-gstab
IncludeSwitch :=-I
LibrarySwitch :=-l
OutputSwitch :=-o 
LibraryPathSwitch :=-L
PreprocessorSwitch :=-D
SourceSwitch :=-c 
CompilerName :=g++
OutputFile :=../../lib/libubdal.a
Preprocessors :=
ObjectSwitch :=-o 
ArchiveOutputSwitch := 
CmpOptions :=-g -Wall $(Preprocessors)
LinkOptions := -O0
IncludePath :=  $(IncludeSwitch). $(IncludeSwitch)../Sockets $(IncludeSwitch)../Resource $(IncludeSwitch)../DAL 
RcIncludePath :=
Libs :=
LibPath := $(LibraryPathSwitch). 
endif

Objects=$(IntermediateDirectory)/Database$(ObjectSuffix) $(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix) $(IntermediateDirectory)/Field$(ObjectSuffix) $(IntermediateDirectory)/FieldDef$(ObjectSuffix) $(IntermediateDirectory)/FieldImpl$(ObjectSuffix) $(IntermediateDirectory)/Join$(ObjectSuffix) $(IntermediateDirectory)/Keys$(ObjectSuffix) $(IntermediateDirectory)/KeyValue$(ObjectSuffix) $(IntermediateDirectory)/Relation$(ObjectSuffix) $(IntermediateDirectory)/SavableManager$(ObjectSuffix) \
	$(IntermediateDirectory)/SavableManagers$(ObjectSuffix) $(IntermediateDirectory)/SelectionMask$(ObjectSuffix) $(IntermediateDirectory)/SqliteError$(ObjectSuffix) $(IntermediateDirectory)/SqliteMgr$(ObjectSuffix) $(IntermediateDirectory)/Statements$(ObjectSuffix) $(IntermediateDirectory)/Table$(ObjectSuffix) $(IntermediateDirectory)/TableDef$(ObjectSuffix) $(IntermediateDirectory)/TableImpl$(ObjectSuffix) $(IntermediateDirectory)/Tables$(ObjectSuffix) $(IntermediateDirectory)/FieldValue$(ObjectSuffix) \
	$(IntermediateDirectory)/FieldValues$(ObjectSuffix) $(IntermediateDirectory)/KeyImpl$(ObjectSuffix) $(IntermediateDirectory)/Query$(ObjectSuffix) 

##
## Main Build Tragets 
##
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile):  $(Objects)
	$(ArchiveTool) $(ArchiveOutputSwitch)$(OutputFile) $(Objects)

./Debug:
	@test -d ./Debug || mkdir ./Debug


PreBuild:


##
## Objects
##
$(IntermediateDirectory)/Database$(ObjectSuffix): Database.cpp $(IntermediateDirectory)/Database$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Database.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Database$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Database$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Database$(ObjectSuffix) -MF$(IntermediateDirectory)/Database$(ObjectSuffix).d -MM Database.cpp

$(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix): DatabaseMgr.cpp $(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)DatabaseMgr.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix) -MF$(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix).d -MM DatabaseMgr.cpp

$(IntermediateDirectory)/Field$(ObjectSuffix): Field.cpp $(IntermediateDirectory)/Field$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Field.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Field$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Field$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Field$(ObjectSuffix) -MF$(IntermediateDirectory)/Field$(ObjectSuffix).d -MM Field.cpp

$(IntermediateDirectory)/FieldDef$(ObjectSuffix): FieldDef.cpp $(IntermediateDirectory)/FieldDef$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)FieldDef.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/FieldDef$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/FieldDef$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/FieldDef$(ObjectSuffix) -MF$(IntermediateDirectory)/FieldDef$(ObjectSuffix).d -MM FieldDef.cpp

$(IntermediateDirectory)/FieldImpl$(ObjectSuffix): FieldImpl.cpp $(IntermediateDirectory)/FieldImpl$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)FieldImpl.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/FieldImpl$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/FieldImpl$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/FieldImpl$(ObjectSuffix) -MF$(IntermediateDirectory)/FieldImpl$(ObjectSuffix).d -MM FieldImpl.cpp

$(IntermediateDirectory)/Join$(ObjectSuffix): Join.cpp $(IntermediateDirectory)/Join$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Join.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Join$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Join$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Join$(ObjectSuffix) -MF$(IntermediateDirectory)/Join$(ObjectSuffix).d -MM Join.cpp

$(IntermediateDirectory)/Keys$(ObjectSuffix): Keys.cpp $(IntermediateDirectory)/Keys$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Keys.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Keys$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Keys$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Keys$(ObjectSuffix) -MF$(IntermediateDirectory)/Keys$(ObjectSuffix).d -MM Keys.cpp

$(IntermediateDirectory)/KeyValue$(ObjectSuffix): KeyValue.cpp $(IntermediateDirectory)/KeyValue$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)KeyValue.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/KeyValue$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/KeyValue$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/KeyValue$(ObjectSuffix) -MF$(IntermediateDirectory)/KeyValue$(ObjectSuffix).d -MM KeyValue.cpp

$(IntermediateDirectory)/Relation$(ObjectSuffix): Relation.cpp $(IntermediateDirectory)/Relation$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Relation.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Relation$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Relation$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Relation$(ObjectSuffix) -MF$(IntermediateDirectory)/Relation$(ObjectSuffix).d -MM Relation.cpp

$(IntermediateDirectory)/SavableManager$(ObjectSuffix): SavableManager.cpp $(IntermediateDirectory)/SavableManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SavableManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SavableManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SavableManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SavableManager$(ObjectSuffix) -MF$(IntermediateDirectory)/SavableManager$(ObjectSuffix).d -MM SavableManager.cpp

$(IntermediateDirectory)/SavableManagers$(ObjectSuffix): SavableManagers.cpp $(IntermediateDirectory)/SavableManagers$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SavableManagers.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SavableManagers$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SavableManagers$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SavableManagers$(ObjectSuffix) -MF$(IntermediateDirectory)/SavableManagers$(ObjectSuffix).d -MM SavableManagers.cpp

$(IntermediateDirectory)/SelectionMask$(ObjectSuffix): SelectionMask.cpp $(IntermediateDirectory)/SelectionMask$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SelectionMask.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SelectionMask$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SelectionMask$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SelectionMask$(ObjectSuffix) -MF$(IntermediateDirectory)/SelectionMask$(ObjectSuffix).d -MM SelectionMask.cpp

$(IntermediateDirectory)/SqliteError$(ObjectSuffix): SqliteError.cpp $(IntermediateDirectory)/SqliteError$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SqliteError.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SqliteError$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SqliteError$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SqliteError$(ObjectSuffix) -MF$(IntermediateDirectory)/SqliteError$(ObjectSuffix).d -MM SqliteError.cpp

$(IntermediateDirectory)/SqliteMgr$(ObjectSuffix): SqliteMgr.cpp $(IntermediateDirectory)/SqliteMgr$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SqliteMgr.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SqliteMgr$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SqliteMgr$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SqliteMgr$(ObjectSuffix) -MF$(IntermediateDirectory)/SqliteMgr$(ObjectSuffix).d -MM SqliteMgr.cpp

$(IntermediateDirectory)/Statements$(ObjectSuffix): Statements.cpp $(IntermediateDirectory)/Statements$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Statements.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Statements$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Statements$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Statements$(ObjectSuffix) -MF$(IntermediateDirectory)/Statements$(ObjectSuffix).d -MM Statements.cpp

$(IntermediateDirectory)/Table$(ObjectSuffix): Table.cpp $(IntermediateDirectory)/Table$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Table.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Table$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Table$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Table$(ObjectSuffix) -MF$(IntermediateDirectory)/Table$(ObjectSuffix).d -MM Table.cpp

$(IntermediateDirectory)/TableDef$(ObjectSuffix): TableDef.cpp $(IntermediateDirectory)/TableDef$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)TableDef.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/TableDef$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TableDef$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/TableDef$(ObjectSuffix) -MF$(IntermediateDirectory)/TableDef$(ObjectSuffix).d -MM TableDef.cpp

$(IntermediateDirectory)/TableImpl$(ObjectSuffix): TableImpl.cpp $(IntermediateDirectory)/TableImpl$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)TableImpl.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/TableImpl$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TableImpl$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/TableImpl$(ObjectSuffix) -MF$(IntermediateDirectory)/TableImpl$(ObjectSuffix).d -MM TableImpl.cpp

$(IntermediateDirectory)/Tables$(ObjectSuffix): Tables.cpp $(IntermediateDirectory)/Tables$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Tables.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Tables$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Tables$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Tables$(ObjectSuffix) -MF$(IntermediateDirectory)/Tables$(ObjectSuffix).d -MM Tables.cpp

$(IntermediateDirectory)/FieldValue$(ObjectSuffix): FieldValue.cpp $(IntermediateDirectory)/FieldValue$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)FieldValue.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/FieldValue$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/FieldValue$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/FieldValue$(ObjectSuffix) -MF$(IntermediateDirectory)/FieldValue$(ObjectSuffix).d -MM FieldValue.cpp

$(IntermediateDirectory)/FieldValues$(ObjectSuffix): FieldValues.cpp $(IntermediateDirectory)/FieldValues$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)FieldValues.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/FieldValues$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/FieldValues$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/FieldValues$(ObjectSuffix) -MF$(IntermediateDirectory)/FieldValues$(ObjectSuffix).d -MM FieldValues.cpp

$(IntermediateDirectory)/KeyImpl$(ObjectSuffix): KeyImpl.cpp $(IntermediateDirectory)/KeyImpl$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)KeyImpl.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/KeyImpl$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/KeyImpl$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/KeyImpl$(ObjectSuffix) -MF$(IntermediateDirectory)/KeyImpl$(ObjectSuffix).d -MM KeyImpl.cpp

$(IntermediateDirectory)/Query$(ObjectSuffix): Query.cpp $(IntermediateDirectory)/Query$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Query.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Query$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Query$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Query$(ObjectSuffix) -MF$(IntermediateDirectory)/Query$(ObjectSuffix).d -MM Query.cpp

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/Database$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Database$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/DatabaseMgr$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Field$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Field$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/FieldDef$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/FieldDef$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/FieldImpl$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/FieldImpl$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Join$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Join$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Keys$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Keys$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/KeyValue$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/KeyValue$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Relation$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Relation$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SavableManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SavableManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SavableManagers$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SavableManagers$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SelectionMask$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SelectionMask$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SqliteError$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SqliteError$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SqliteMgr$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SqliteMgr$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Statements$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Statements$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Table$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Table$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/TableDef$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/TableDef$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/TableImpl$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/TableImpl$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Tables$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Tables$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/FieldValue$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/FieldValue$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/FieldValues$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/FieldValues$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/KeyImpl$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/KeyImpl$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Query$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Query$(ObjectSuffix).d
	$(RM) $(OutputFile)

-include $(IntermediateDirectory)/*.d

