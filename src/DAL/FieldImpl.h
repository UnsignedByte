/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file FieldImpl.h
 * This file contains the FieldImpl class. 
 *
 * @see FieldImpl 
 */ 

#include "Field.h"
#include "Types.h"

/**
 * This class represents a generated (concrete version of a) FieldDef.
 *
 * This class should not be instantiated manually.
 * Instead, the generated ones contained in TableImpls should be used.
 *
 * @see FieldDef
 * @see TableImpls 
 * @see Field
 */ 
class FieldImpl : public Field, public boost::enable_shared_from_this<FieldImpl>
{
public:
	/** Constructs a new field belonging to the specified table with the specified name, default value and 'textness'.*/
	FieldImpl(TableImplPtr table, cstring name, bool text, cstring defaultvalue);
	
	/** Constructs a new field belonging to the specified table with the specified name 'textness' and lookup possibility.*/
	FieldImpl(TableImplPtr table, cstring name, bool text, bool lookup);
	
	/** Destructor, a noop. */
	~FieldImpl();
	
	
	/** Returns the table this field belongs to. */
	TableImplPtr getTable() const { return m_table; }
	
	/** Returns the default value for this field. */
	const std::string& getDefaultValue() const { return m_defaultvalue; }
	
	/** Whether this field is 'guaranteed' to be unique. */
	bool isLookup() const { return m_lookup; }
	
	
	/** Returns a new FieldValue for this field. */
	virtual FieldValuePtr newValue();
	
	
	/** Returns wether this field is a key. */
	virtual bool isKey() const { return false; }
	
	/** Returns wether this field is a primary key. */
	virtual bool isPrimaryKey() const { return false; }
	
private:
	TableImplPtr m_table; /**< The table this field belongs to. */
	std::string m_defaultvalue; /**< The defaultvalue for this field. */
	bool m_lookup; /**< Whether this field is a lookup field. */
};
