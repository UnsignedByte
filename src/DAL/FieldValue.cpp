/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "FieldValue.h"
#include "KeyImpl.h"
#include "TableImpl.h"

FieldValue::FieldValue(FieldImplPtr field) : 
m_field(field), 
m_textvalue(""), 
m_integervalue(0) ,
m_dirty(false)
{
	Assert(field);
}

FieldValue::FieldValue(FieldImplPtr field, cstring value) : 
m_field(field), 
m_textvalue(value), 
m_integervalue(0),
m_dirty(true)
{
	Assert(field);
}

FieldValue::FieldValue(FieldImplPtr field, value_type value) : 
m_field(field), 
m_textvalue(""), 
m_integervalue(value),
m_dirty(true)
{
	Assert(field);
}

FieldValue::~FieldValue()
{

}

FieldImplPtr FieldValue::getField() const
{ 
	return m_field; 
}

TableImplPtr FieldValue::getTable() const
{ 
	return m_field->getTable(); 
}

cstring FieldValue::getName() const
{ 
	return m_field->getName(); 
}

const std::string& FieldValue::getStringValue() const
{ 
	Assert(m_field->isText());
	return m_textvalue; 
}

value_type FieldValue::getIntegerValue() const
{ 
	Assert(!m_field->isText());
	return m_integervalue; 
}

bool FieldValue::getBoolValue() const
{ 
	Assert(!m_field->isText());
	return m_integervalue == 1 ? true : false; 
}

bool FieldValue::isDirty() const
{ 
	return m_dirty; 
}

void FieldValue::setTextValue(const std::string& value)
{ 
	Assert(m_field->isText());
	m_textvalue = value; 
}

void FieldValue::setIntegerValue(value_type value)
{ 
	Assert(!m_field->isText());
	m_integervalue = value; 
}

void FieldValue::setBoolValue(bool value)
{ 
	Assert(!m_field->isText());
	m_integervalue = (value ? 1 : 0); 
}

void FieldValue::setDirty(bool dirty)
{ 
	m_dirty = dirty; 
}
