/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "FieldValue.h"
#include "FieldValues.h"
#include "Join.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Keys.h"
#include "Relation.h"
#include "SavableManager.h"
#include "SavableManagers.h"
#include "SelectionMask.h"
#include "SqliteMgr.h"
#include "StringUtilities.h"
#include "TableImpl.h"

ByKeyCache SavableManager::ms_byKeyCache;
ByValueCache SavableManager::ms_byValueCache;

SavableManager::SavableManager(TableImplPtr table) :
m_table(table),
m_newentry(true),
m_locked(false)
{
	Assert(table);
	
	for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
				
		m_fields[it->get()] = field->newValue();
	}
}

SavableManager::~SavableManager()
{

}

SavableManagersPtr SavableManager::getmulti(FieldValuesPtr values) // static
{
	Assert(values);
	
	Joins joins;
	return getmulti(values, joins);
}

SavableManagersPtr SavableManager::getmulti(FieldValuesPtr values, const Joins& joins) // static
{
	Assert(values);
	
	TableImplPtr table = values->getTable();
	Assert(table);
	
	SelectionMaskPtr mask(new SelectionMask(table));
	
	for(ValueMap::const_iterator it = values->begin(); it != values->end(); it++)
	{
		ValuePtr value = it->second;
		mask->addField(value);
	}
	
	for(Joins::const_iterator it = joins.begin(); it != joins.end(); it++)
	{
		JoinPtr join = *it;
		mask->addJoin(join);
	}
	
	return getmulti(mask);
}

SavableManagersPtr SavableManager::getmulti(SelectionMaskPtr mask) // static
{
	Assert(mask);
	
	SqliteMgr::Get()->doSelectMulti(mask.get());	
	return mask->getResult();
}

SavableManagerPtr SavableManager::getnew(TableImplPtr table) // static
{
	Assert(table);
	
	SavableManagerPtr result(new SavableManager(table));
	return result;
}

SavableManagerPtr SavableManager::bykey(KeyValuePtr key) // static
{
	Assert(key);
	
	TableImplPtr table = key->getTable();
	KeysPtr keys(new Keys(table));
	keys->addKey(key);
	return bykeys(keys);
}

SavableManagerPtr SavableManager::bykeys(KeysPtr keys) // static
{
	Assert(keys);
	
	KeyFieldMap map = constructMap(keys);
	
	if(ms_byKeyCache.find(map) == ms_byKeyCache.end())
	{		
		SavableManagerPtr result = getnew(keys->getTable());
		
		bool locked = result->lock(); // new object, should always be unlocked
		Assert(locked);
		
		/// Begin locked section
		result->setKeys(keys);
		SqliteMgr::Get()->doSelect(result.get());
		doFullCache(result);
		result->cleanup();
		/// End locked section
		
		result->unlock();
	}
	
	SavableManagerPtr result = ms_byKeyCache[map];
	Assert(result);
	
	return result;
}

SavableManagerPtr SavableManager::byvalue(ValuePtr value) // static
{	
	Assert(value);
	
	FieldImplPtr field = value->getField();
	Assert(field);
	Assert(field->isLookup());
	
	TextFieldPair pair = constructPair(value);
	
	if(ms_byValueCache.find(pair) == ms_byValueCache.end())
	{
		SavableManagerPtr result = getnew(value->getTable());
		
		bool locked = result->lock(); // new object, should always be unlocked
		Assert(locked);
		
		/// Begin locked section
		result->m_lookupvalue = value;
		SqliteMgr::Get()->doLookup(result.get(), value->getField());
		doFullCache(result);
		result->cleanup();
		/// End locked section
		
		result->unlock();
	}
	
	SavableManagerPtr result = ms_byValueCache[pair];
	Assert(result);
	
	return result;
}

KeysPtr SavableManager::lookupvalue(ValuePtr value) // static
{
	Assert(value);
	
	SavableManagerPtr result = byvalue(value);
	KeysPtr keys = result->getKeys();
	return keys;
}


size_t SavableManager::count(SelectionMaskPtr mask)
{
	Assert(mask);
	
	SqliteMgr::Get()->doCount(mask.get());	
	return mask->getCount();
}

size_t SavableManager::count(KeysPtr keys)
{
	Assert(keys);
	Assert(keys->size() == keys->getTable()->primarykeysize());
	
	TableImplPtr table = keys->getTable();
	Assert(table);
	
	SelectionMaskPtr mask(new SelectionMask(table));
	
	for(KeyImplMap::const_iterator it = keys->begin(); it != keys->end(); it++)
	{
		KeyValuePtr key = it->second;
		Assert(key);
		
		mask->addField(key);
	}
	
	return count(mask);
}

void SavableManager::doFullCache(SavableManagerPtr manager) // static
{
	Assert(manager);
	
	Assert(manager->primaryKeySize() == manager->getTable()->primarykeysize());
	
	KeyFieldMap map = constructMap(manager);
	ms_byKeyCache[map] = manager;
	
	for(FieldImplVector::const_iterator it = manager->getTable()->begin(); it != manager->getTable()->end(); it++)
	{
		FieldImplPtr field = *it;		
		Assert(field);
		
		if(!field->isLookup())
			continue;
		
		ValuePtr value = manager->getValue(field);
		Assert(value);
		
		TextFieldPair pair = constructPair(value);
		
		// Don't allow overwriting a previous value, unless it pointed to us.
		Assert((ms_byValueCache.find(pair) == ms_byValueCache.end()) || (ms_byValueCache[pair] == manager));			
		ms_byValueCache[pair] = manager;
	}	
}

void SavableManager::doUncache(SavableManagerPtr manager) // static
{
	Assert(manager);
	
	for(FieldImplVector::const_iterator it = manager->getTable()->begin(); it != manager->getTable()->end(); it++)
	{
		FieldImplPtr field = *it;		
		Assert(field);
		
		if(!field->isLookup())
			continue;
		
		FieldImpl* fieldptr = field.get();
		Assert(fieldptr);
		
		FieldValuePtr value = manager->m_orig[fieldptr];
		
		// Check if it was a new value
		if(!value)
			continue;
		
		TextFieldPair pair = constructPair(value);
		
		// Assert that any previous values reference us		
		Assert((ms_byValueCache.find(pair) == ms_byValueCache.end()) || (ms_byValueCache[pair] == manager));			
		ms_byValueCache.erase(pair);
	}
}

void SavableManager::doRecache(SavableManagerPtr manager)
{
	doUncache(manager);
	doFullCache(manager);
}

KeyFieldMap SavableManager::constructMap(KeysPtr keys) // static
{
	KeyFieldMap result;
	for(KeyImplMap::const_iterator it = keys->begin(); it != keys->end(); it++)
		result[it->first] = it->second->getIntegerValue();	
	
	return result;
}

KeyFieldMap SavableManager::constructMap(SavableManagerPtr manager) // static
{
	KeyFieldMap result;
	
	for(FieldImplVector::const_iterator it = manager->getTable()->begin(); it != manager->getTable()->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;
			
		FieldValuePtr fieldvalue = manager->getValue(field);
		Assert(fieldvalue);
			
		KeyValuePtr key = boost::static_pointer_cast<KeyValue>(fieldvalue);
		Assert(key);
		
		KeyImplPtr keyimpl = key->getKeyImpl();
		Assert(keyimpl);
		
		result[keyimpl.get()] = key->getIntegerValue();	
	}
	
	return result;
}

TextFieldPair SavableManager::constructPair(ValuePtr value) // static
{
	TextFieldPair result;
	FieldImplPtr field = value->getField();
	
	result.first = field.get();
	if(field->isText())
		result.second = value->getStringValue();
	else
		result.second = String::Get()->fromInt(value->getIntegerValue());		
	
	return result;
}

std::string SavableManager::fromMap(const KeyFieldMap& map) // static
{			
	Strings result;
	std::string tablename;
	
	for(std::map<KeyImpl*, value_type>::const_iterator it = map.begin(); it != map.end(); it++)
	{
		KeyImpl* key = it->first;
		value_type value = it->second;
		
		TableImplPtr table = key->getTable();
		Assert(table);
		
		tablename = table->getName();
		
		std::string line = key->getName();
		line.append(": ");
		line.append(String::Get()->fromInt(value));
		
		result.push_back(line);
	}
	
	std::string name("Keys from table ");
	name.append(tablename);
	
	return String::Get()->box(result, name);
}

void SavableManager::erase()
{
	Assert(m_locked);
	
	if(m_newentry)
		return;
		
	SqliteMgr::Get()->doErase(this);
	doUncache(shared_from_this());	
}

void SavableManager::save()
{
	Assert(m_locked);
	
	if(m_newentry)
	{		
		SqliteMgr::Get()->doInsert(this);		
		doFullCache(shared_from_this());
		m_newentry = false;
	}

	if(m_orig.size() > 0)
	{		
		SqliteMgr::Get()->doUpdate(this);
		doRecache(shared_from_this());
		cleanup();
	}
}

void SavableManager::discard()
{
	Assert(m_locked);
	
	SqliteMgr::Get()->doSelect(this);
	cleanup();
}

bool SavableManager::exists() const
{
	if(m_newentry)
		return false;

	return true;
}

bool SavableManager::lock()
{
	if(m_locked)
		return false;
		
	m_locked = true;
	return true;
}

void SavableManager::unlock()
{
	Assert(m_locked);
	
	m_locked = false;
}

void SavableManager::bindKeys(sqlite3* db, sqlite3_stmt* stmt, const int startpos) const
{
	Assert(db);
	Assert(stmt);
	
	int pos = startpos;
	int rc = 0;
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;
			
		FieldValuePtr fieldvalue = getValue(field);
		Assert(fieldvalue);
		
		value_type value = fieldvalue->getIntegerValue();
		
		rc = sqlite3_bind_int64(stmt, pos, value);
		
		if(rc != SQLITE_OK)
			throw SqliteError(db);
			
		pos++;
	}
}

void SavableManager::bindFields(sqlite3* db, sqlite3_stmt* stmt, const int startpos) const
{
	Assert(db);
	Assert(stmt);
	
	int pos = startpos;
	int rc = 0;
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(field->isPrimaryKey())
			continue;
		
		if(field->isText())
		{
			FieldValuePtr value = getValue(field);
			std::string stringvalue = value->getStringValue();
			rc = sqlite3_bind_text(stmt, pos, stringvalue.c_str(), stringvalue.size(), SQLITE_TRANSIENT);
		}
		else
		{
			FieldValuePtr value = getValue(field);
			value_type integervalue = value->getIntegerValue();
			rc = sqlite3_bind_int64(stmt, pos, integervalue);
		}
			
		if(rc != SQLITE_OK)
			throw SqliteError(db);
		pos++;
	}
}

void SavableManager::bindUpdate(sqlite3* db, sqlite3_stmt* stmt) const
{
	Assert(db);
	Assert(stmt);
	
	bindFields(db, stmt);
	bindKeys(db, stmt, m_fields.size() - primaryKeySize() + 1);
}

void SavableManager::bindLookup(sqlite3* db, sqlite3_stmt* stmt) const
{
	Assert(db);
	Assert(stmt);
	
	int rc = 0;
	if(m_lookupvalue->getField()->isText())
		rc = sqlite3_bind_text(stmt, 1, m_lookupvalue->getStringValue().c_str(), m_lookupvalue->getStringValue().size(), SQLITE_TRANSIENT);
	else
		rc = sqlite3_bind_int64(stmt, 1, m_lookupvalue->getIntegerValue());
		
	if(rc != SQLITE_OK)
		throw SqliteError(db);
}

void SavableManager::parseInsert(sqlite3* db)
{	
	Assert(m_locked);
	Assert(db);
	Assert(m_table->primarykeysize() == 1);
	
	FieldImplPtr field = m_table->firstkey();
	Assert(field);
	
	value_type value = sqlite3_last_insert_rowid(db);
	
	FieldValuePtr key = field->newValue();
	Assert(key->isKey());
	
	key->setIntegerValue(value);
	
	setValue(key);
}

void SavableManager::parseSelect(sqlite3_stmt* stmt, const int startpos)
{
	Assert(m_locked);
	Assert(stmt);
	
	int pos = startpos;
	const unsigned char * text;
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(field->isPrimaryKey())
			continue;
		
		if(field->isText())
		{
			text = sqlite3_column_text(stmt, pos);
			if(text != 0)
			{				
				std::string value = std::string((const char *)text);
				
				FieldValuePtr fieldvalue = getValue(field);								
				getValue(field)->setTextValue(value);
			}
		}
		else
		{
			value_type value =  sqlite3_column_int64(stmt, pos);	
			FieldValuePtr fieldvalue = getValue(field);			
			getValue(field)->setIntegerValue(value);
		}
		
		pos++;
	}
	
	m_newentry = false;
}

void SavableManager::parseLookup(sqlite3_stmt* stmt)
{
	Assert(m_locked);
	Assert(stmt);
	
	int pos = 0;
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;
		
		KeyImplPtr key = boost::static_pointer_cast<KeyImpl>(field);
		Assert(key);
		
		setValue(key, sqlite3_column_int64(stmt, pos));
		pos++;
	}
}

TableImplPtr SavableManager::getTable() const
{
	return m_table;
}

size_t SavableManager::primaryKeySize() const
{
	size_t keysize = 0;
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;		
		
		keysize++;
	}
	
	return keysize;
}

KeysPtr SavableManager::getKeys() const
{
	KeysPtr result(new Keys(m_table));
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		if(!field->isPrimaryKey())
			continue;		
		
		FieldValuePtr value = getValue(field);
		Assert(value);
		
		KeyValuePtr key = boost::static_pointer_cast<KeyValue>(value);
		Assert(key);
		
		Assert(key->isKey());		
		result->addKey(key);		
	}
	
	return result;
}

ValuePtr SavableManager::getValue(FieldImplPtr field) const
{
	Assert(field);
	
	FieldValueMap::const_iterator it = m_fields.find(field.get());
	Assert(it != m_fields.end());
	
	return it->second;
}

std::string SavableManager::getDiff() const
{
	std::vector<std::string> result;
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		// Check if the field has been changed
		FieldValueMap::const_iterator it = m_orig.find(field.get());
		if(it == m_orig.end())
			continue;
		
		// Retreive the original value.
		FieldValuePtr origValue = it->second;
		Assert(origValue);
		
		// Retreive the current value.
		FieldValuePtr currentValue = getValue(field);
		Assert(currentValue);
			
		std::string line = "Changed field '";
		if(field->isKey() && !field->isPrimaryKey())
			line.append(field->getName().substr(2));
		else
			line.append(field->getName());
		line.append("' from '");
		
		if(field->isText())
			line.append(origValue->getStringValue());
		else
			line.append(String::Get()->fromInt(origValue->getIntegerValue()));
			
		line.append("' to '");
		
		if(field->isText())
			line.append(currentValue->getStringValue());
		else
			line.append(String::Get()->fromInt(currentValue->getIntegerValue()));
			
		line.append("'.");
			
		result.push_back(line);
	}	
	
	return String::Get()->unlines(result, "\n", 0);
}

bool SavableManager::isDirty() const
{
	return m_orig.size() > 0;
}

void SavableManager::setKeys(KeysPtr keys)
{
	Assert(m_locked);
	Assert(keys);
	Assert(keys->size() == m_table->primarykeysize());
		
	for(KeyImplMap::const_iterator it = keys->begin(); it != keys->end(); it++) {
		Assert(it->first->getTable() == m_table);
		setValue(it->second);
	}
}

void SavableManager::setValue(FieldValuePtr value)
{
	Assert(m_locked);
	Assert(value);
	Assert(value->getTable() == m_table);
	
	FieldImpl* field = value->getField().get();
	Assert(field);
	
	Assert(m_fields.find(field) != m_fields.end());
	
	// Store the original value in m_orig
	if(m_orig.find(field) == m_orig.end())
		m_orig[field] = m_fields[field];
		
	m_fields[field] = value;
}

void SavableManager::setValue(FieldImplPtr field, cstring value) // delegator
{	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	setValue(fieldvalue);
}

void SavableManager::setValue(FieldImplPtr field, value_type value) // delegator
{	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	setValue(fieldvalue);
}

void SavableManager::setValue(KeyImplPtr field, value_type value) // delegator
{
	KeyValuePtr fieldvalue(new KeyValue(field, value));
	setValue(fieldvalue);
}

void SavableManager::cleanup()
{
	Assert(m_locked);
	
	m_orig.clear();
}

std::string SavableManager::toString()
{
	std::string name = getTable()->getName();
	name.append(" <");
	name.append(getKeys()->toString());
	name.append(">");
	
	Strings fieldlist;
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		ValuePtr value = getValue(field);
		Assert(value);
		
		std::string line = field->getName();
		line.append(": ");
				
		if(field->isText())		
			line.append(value->getStringValue());
		else	
			line.append(String::Get()->fromInt(value->getIntegerValue()));
			
		fieldlist.push_back(line);
	}
		
	return String::Get()->box(fieldlist, name);
}

Strings SavableManager::fieldList() const
{
	Strings result;
	
	for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		ValuePtr value = getValue(field);
		
		if(field->isText()) {
			result.push_back(value->getStringValue());			
		}
		else
		{
			// TODO lookup foreign keys
			/*
			if(field->isKey()) 
			{			
				int keyvalue = value->getIntegerValue();
				
				KeyImplPtr keyimpl = boost::static_pointer_cast<KeyImpl>(field);
				KeyValuePtr key(new KeyValue(keyimpl, keyvalue));
				
				try {
					SavableManagerPtr manager = SavableManager::bykey(key);
				} catch(RowNotFoundException& e) {
					// TODO handle
				}
				
				// result.push_back(manager->getfield(manager->getIdentifyingField())->getStringValue());
				// TODO
				// continue;
			}
			*/
		
			result.push_back(String::Get()->fromInt(value->getIntegerValue()));
		}
	}
	
	return result;
}
