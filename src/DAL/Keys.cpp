/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Keys.h"
#include "Parse.h"
#include "StringUtilities.h"
#include "TableImpl.h"

Keys::Keys(TableImplPtr table) : 
m_table(table) 
{ 
	Assert(table); 
}

Keys::Keys(TableImplPtr table, cstring initstring) :
m_table(table)
{
	Assert(table);
	Parse p(initstring);
		
	try
	{
		for(FieldImplVector::const_iterator it = m_table->begin(); it != m_table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
				
			KeyImplPtr keyimpl = boost::static_pointer_cast<KeyImpl>(field);
			Assert(keyimpl);
			
			std::string keystring = p.getword();
			int value = atoi(keystring.c_str());
			KeyValuePtr key(new KeyValue(keyimpl, value));
			
			addKey(key);
		}
	}
	catch(std::exception& e) 
	{
		throw std::invalid_argument("Keys::Keys(), could not parse initstring.");
	}
}

Keys::~Keys() 
{ 
	
}

KeyValuePtr Keys::getKey(KeyImplPtr key) const
{
	Assert(key);
	KeyImplMap::const_iterator it = m_keys.find(key.get());
	Assert(it != m_keys.end());
		
	return it->second;
}

TableImplPtr Keys::getTable() const 
{ 
	return m_table; 
}

void Keys::addKey(KeyValuePtr key)
{
	Assert(key);
	Assert(key->getTable() == m_table);
	Assert(key->isPrimaryKey());
	
	KeyImpl* keyimpl = key->getKeyImpl().get();		
	m_keys[keyimpl] = key;
}

void Keys::addKey(KeyImplPtr keyimpl, value_type id)
{
	Assert(keyimpl);
	Assert(id);
	Assert(keyimpl->getTable() == m_table);
	
	KeyValuePtr key(new KeyValue(keyimpl, id));
		
	m_keys[key->getKeyImpl().get()] = key;
}
	
std::string Keys::toString() const
{
	std::string result;
	
	for(KeyImplMap::const_iterator it = m_keys.begin(); it != m_keys.end(); it++)
	{
		if(it != m_keys.begin())
			result.append(", ");
		
		KeyValuePtr key = it->second;		
		result.append(String::Get()->fromInt(key->getIntegerValue()));
	}
	
	return result;
}

size_t Keys::size() const 
{ 
	return m_keys.size(); 
}

KeyValuePtr Keys::first() const 
{ 
	Assert(m_keys.size() == 1);
	KeyValuePtr key = m_keys.begin()->second;		
	return key;
}

KeyImplMap::const_iterator Keys::begin() const 
{ 
	return m_keys.begin(); 
}

KeyImplMap::const_iterator Keys::end() const 
{ 
	return m_keys.end(); 
}

KeyImplMap::const_iterator Keys::find(KeyImplPtr key) const 
{ 
	Assert(key); 
	
	return m_keys.find(key.get()); 
}

void Keys::setDirty(KeysPtr oldKeys)
{
	for(KeyImplMap::iterator it = m_keys.begin(); it != m_keys.end(); it++)
	{
		KeyValuePtr key = it->second;
		KeyImplMap::const_iterator other = oldKeys->find(key->getKeyImpl());
		
		// new key (previously empty)
		if(other == oldKeys->end()) 
		{
			key->setDirty(true);
			continue;
		}
							
		KeyValuePtr otherKey = other->second;
		if(key->getIntegerValue() != otherKey->getIntegerValue())
			key->setDirty(true);
	}
}

Strings Keys::getDiff(KeysPtr orig) const
{
	Strings result;
	
	for(KeyImplMap::const_iterator it = m_keys.begin(); it != m_keys.end(); it++)
	{
		KeyValuePtr key = it->second;
		Assert(key);
		if(!key->isDirty())
			continue;
			
		KeyImplPtr keyimpl = key->getKeyImpl();
		Assert(keyimpl);
		
		KeyValuePtr origKey = orig->getKey(keyimpl);
		Assert(origKey);
			
		std::string line = "Changed key '";
		line.append(keyimpl->getName());
		line.append("' from '");
		line.append(String::Get()->fromInt(origKey->getIntegerValue()));
		line.append("' to '");
		line.append(String::Get()->fromInt(key->getIntegerValue()));
		line.append("'.");
		
		result.push_back(line);
	}
	
	return result;
}
