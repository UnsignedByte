/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Table.h
 * This file contains the Table class.
 *
 * @see Table
 */ 

#include "Types.h"

/**
 * This class represents a table in the database.
 *
 * @deprecated This super class might be removed since it is not used much.
 */ 
class Table
{
public:
	/** 
	 * Returns the name of this table.
	 *
	 * @deprecated use getName() instead. 
	 * @see getName 
	 */
	const std::string& tableName() const;
	
	/** Returns the name of this table. */
	cstring getName() const;
	
	
	/** Returns an iterator to the first element of the FieldDef's of this table. */
	virtual FieldDefVector::const_iterator defbegin() const = 0;
	
	/** Return an iterator to the 'end' iterator of the FieldDef's of this table. */
	virtual FieldDefVector::const_iterator defend() const = 0;
	
	/** Returns the size of the FieldDef's of this table. */
	virtual size_t defsize() const = 0;
	
	
	/** Returns the first key (it's name as an std::string). */
	virtual std::string firstKey() const = 0;
	
	/** Returns an iterator to the first Key definition pair. */
	virtual TableMap::const_iterator keybegin() const = 0;
	
	/** Returns the 'end' iterator of the Key definitions. */
	virtual TableMap::const_iterator keyend() const = 0;
	
	/** Returns the size of the Key definitions. */
	virtual size_t keysize() const = 0;
	
	/** Returns wether this table has a singular primary key. */
	virtual bool hasSingularPrimaryKey() const = 0;
	
protected:
	/** Creates a new table with the specified name. */
	Table(std::string name);
	
	/** Destructor, a noop. */
	virtual ~Table();
		
	friend SmartPtrDelete(Table);

	std::string m_name; /**< The name of this table. */
};
