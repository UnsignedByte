/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file TableDef.h
 * This file contains the TableDef class.
 *
 * @see TableDef
 */ 

#include "Table.h"
#include "Types.h"

/**
 * This class represents the definition of a table.
 * 
 * Based on this class TableImpl's will be generated.
 */ 
class TableDef : public boost::enable_shared_from_this<TableDef>
{
public:
	/** Constructs a new TableDef with the specified name. */
	TableDef(std::string name);
	
	/** Destructor, a noop. */
	~TableDef();
	
	
	/** Returns the name of this table. */	
	cstring getName() const;
	
	
	/** Adds a primary key with the specified name. */
	void addPK(const std::string& name);
	
	/** Adds a foreign key to the specified table. */
	void addFPK(TableDefPtr table);
	
	/** Adds a foreign key to the specified table with the specified suffix. */
	void addFPK(TableDefPtr table, const std::string& suffix);

	
	/** Adds an integer value with the specified name. */
	void addValue(const std::string& name);
	
	/** Adds an integer valueu with the specified name and default value. */
	void addValue(const std::string& name, value_type defaultvalue);
	
	/** Adds a text value with the specified name. */
	void addTextField(const std::string& name);
	
	/** 
	 * Adds a text value with the specified name and provide lookup.
	 *
	 * This field should at all times be unique.
	 * However, this is not enforced and as such lookups based on this fields are not guaranteed to always return the same value. 
	 */ 
	void addLookupTextField(const std::string& name);
	
	/** Adds a text valueu with the specified name and default value. */
	void addTextField(const std::string& name, const std::string& defaulttext);
	
	/** Adds a foreign key field. */
	void addFK(TableDefPtr table);
	
	/** Adds a foreign key field with the specified suffix. */
	void addFK(TableDefPtr table, const std::string& suffix);

	/** Returns the foreign name of keys to this table. */
	const std::string& tableForeignName() const;
	
	
	/** Returns an iterator to the first element of the FieldDef's of this table. */
	FieldDefVector::const_iterator begin() const { return m_deffields.begin(); }
	
	/** Return an iterator to the 'end' iterator of the FieldDef's of this table. */
	FieldDefVector::const_iterator end() const { return m_deffields.end(); }
	
	/** Returns the size of the FieldDef's of this table. */
	size_t size() const { return m_deffields.size(); }
	
	
	/** Retursn the amount of primary keys this table has. */
	size_t primarykeysize() const { return m_primarykeysize; }
	
private:	
	std::string m_name; /**< The name of this table. */
	std::string m_foreignname; /**< The foreign name of this table. */
	FieldDefVector m_deffields; /**< The defined fields. */
	size_t m_primarykeysize; /**< The amount of primary keys for this table. */
};
