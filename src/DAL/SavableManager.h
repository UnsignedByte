/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file SavableManager.h
 * This file container the SavableManager class.
 *
 * @see SavableManager.
 */ 

#include "Types.h"

/** The type used to cache integer type field values. */
typedef std::map<KeyImpl*, value_type> KeyFieldMap;

/** The type used to cache text type field values. */
typedef std::pair<FieldImpl*, std::string> TextFieldPair;

/** The type used to cache SavableManagers by their keys in. */ 
typedef std::map<KeyFieldMap, SavableManagerPtr> ByKeyCache;

/** The type used to cache SavableManagers by their value in. */
typedef std::map<TextFieldPair, SavableManagerPtr> ByValueCache;

/**
 * This class represents one row in the database.
 *
 * It may be used to perform CRUD operations on the database.
 * (Create, Retreive, Update, Delete) 
 * 
 * Trivial locking is implemented through <code>lock</code> and <code>unlock</code>.
 * That is, it is asserted that a lock was aquired when performing a write operation.
 * However, it is not checked that it was the called that aquired the lock.
 * It is therefore the responsibility of the caller not to use write operation unless it previously aquired a lock.
 *
 * @see lock
 * @see unlock 
 */ 
class SavableManager : public boost::enable_shared_from_this<SavableManager>
{
	public:		
		/** 
		 * Select multiple entries from the database restricting on the specified values.
		 * 
		 * This method wraps the creation of the SelectionMask and the call to the database.
		 * This is the preferred method of selection multiple rows from the database.
		 * 
		 * @param values The values to filter on.
		 * @return A SavableManagers bucket containing the retreived data.
		 * @see SelectionMask 
		 */
		static SavableManagersPtr getmulti(FieldValuesPtr values);
		
		/** 
		 * Select multiple entries from the database restricting on the specified values.
		 * 
		 * This method wraps the creation of the SelectionMask and the call to the database.
		 * This is the preferred method of selection multiple rows from the database.
		 * 
		 * @param values The values to filter on.
		 * @param joins The joins to extend the selection with. 
		 * @return A SavableManagers bucket containing the retreived data.
		 * @see SelectionMask 
		 */
		static SavableManagersPtr getmulti(FieldValuesPtr values, const Joins& joins);
		
		/**
		 * Select multiple entries from the database restricting with the specified mask.
		 *
		 * This method uses the specified mask to make the call to the database. 
		 * This is the preferred method of selection multiple rows from the database. 
		 *
		 * @param mask The mask to use.
		 * @return A SavableManagers bucket containing the retreived data.
		 * @see SelectionMask
		 */ 
		static SavableManagersPtr getmulti(SelectionMaskPtr mask);
		
		/** Returns a new manager for the specified table, it is not yet stored in the database. */
		static SavableManagerPtr getnew(TableImplPtr table);
		
		/** Returns a manager for the specified table using the specified key. */
		static SavableManagerPtr bykey(KeyValuePtr key);
		
		/** Returns a manager using the specified keys, these are asserted to be all the keys for this table.*/
		static SavableManagerPtr bykeys(KeysPtr keys);
		
		/** Returns a manager using the specified value, this value should uniquely identify an entry. */
		static SavableManagerPtr byvalue(ValuePtr value);
		
		/** Wrapper that looks up a manager by the specified value and returns the keys. */
		static KeysPtr lookupvalue(ValuePtr value);	
		
		/** Count how many entries there are that satisfy this mask. */
		static size_t count(SelectionMaskPtr mask);
		
		/** Count how many entries there are for the given keys set. */
		static size_t count(KeysPtr keys);

		
		/** Remove the row this manager represents from the databse. */
		void erase();
		
		/** Save the row this manager represents to the database. */
		void save();
		
		/** Discards any changes made to this manager. */
		void discard();
		
		/** Wether the manager has been saved to the database or not. */
		bool exists() const;
		
		/** Whether a write lock could be aquired for this manager. */
		bool lock();
		
		/** Releases the lock, it is asserted at run-time that a lock was previously aquired. */
		void unlock();

		
		/** Binds the keys of this manager to the specified statement. */
		void bindKeys(sqlite3* db, sqlite3_stmt* stmt, const int startpos = 1) const;
		
		/** Binds the fields of this manager to the specified statement. */
		void bindFields(sqlite3* db, sqlite3_stmt* stmt, const int startpos = 1) const;
		
		/** First binds the keys and then the fields of this manager to the specified statement. */
		void bindUpdate(sqlite3* db, sqlite3_stmt* stmt) const;
		
		/** Binds the lookup field of this manager to the specified statement. */
		void bindLookup(sqlite3* db, sqlite3_stmt* stmt) const;
		
		
		/** 
		 * Parse an insertion from the database.
		 *
		 * Only valid on a table with a singular primary key. 
		 * That is, sqlite3_last_insert_rowid must return the id of the inserted row.
		 */
		void parseInsert(sqlite3* db);
		
		/** Read in this managers fields from a selection from the database. */
		void parseSelect(sqlite3_stmt* stmt, const int startpos = 0);
		
		/** Read in the keys and the fields for this manager from a lookup from the database. */
		void parseLookup(sqlite3_stmt* stmt);


		/** Returns the table this manager belongs to. */
		TableImplPtr getTable() const;
						
		/** Returns the value of the specified field of the row this manager represents . */
		ValuePtr getValue(FieldImplPtr field) const;
		
		/** Returns the keys in this manager. */
		KeysPtr getKeys() const;
		
		/** Returns the size of the primary keys for this manager. */
		size_t primaryKeySize() const;
		
		
		/** Returns a string representation of this manager. */
		std::string toString();
		
		/** Returns an array of strings, one for each field in this row. */
		Strings fieldList() const;
		
		/** Returns a string representation of the valueus of this manager compared to the database. */
		std::string getDiff() const;
		
		/** Wether any changes been made to this manager that have not been saved to the databse yet. */
		bool isDirty() const;
		
		
		/** Sets this managers keys to the specified values, it is asserted at runtime that a lock has been aquired. */
		void setKeys(KeysPtr keys);
		
		/** Sets the specified field of this manager to the specified value, it is asserted at runtime that a lock has been aquired. */
		void setValue(ValuePtr value);
		
		/** Sets the specified field of this manager to the specified value, it is asserted at runtime that a lock has been aquired. */
		void setValue(FieldImplPtr field, cstring value);
		
		/** Sets the specified field of this manager to the specified value, it is asserted at runtime that a lock has been aquired. */
		void setValue(FieldImplPtr field, value_type value);
		
		/** Sets the specified field of this manager to the specified value, it is asserted at runtime that a lock has been aquired. */
		void setValue(KeyImplPtr field, value_type value);

	private:
		/** This is a managed class, use the factory methods insteaed of this constructor to create a new instance. */
		SavableManager(TableImplPtr table);
		
		/** Destructor, a noop. */
		~SavableManager();
		
		
		/** Unsets the dirty field and all the dirty fields of all keys and fields. */
		void cleanup();		
				
		
		/** Cache the specified manager. */
		static void doFullCache(SavableManagerPtr manager);
		
		/** Uncache the specified manager. */
		static void doUncache(SavableManagerPtr manager);
		
		/** Recache the specified manager. */
		static void doRecache(SavableManagerPtr manager);
		
		/** Construct a map from the specified manager. */
		static KeyFieldMap constructMap(SavableManagerPtr keys);
		
		/** Construct a map from the specified keys bucket. */
		static KeyFieldMap constructMap(KeysPtr keys);
		
		/** Construct a pair from a value object. */
		static TextFieldPair constructPair(FieldValuePtr value);
		
		/** Returns a string presentation of a KeyField map. */
		static std::string fromMap(const KeyFieldMap& map);
		
				
		static ByKeyCache ms_byKeyCache; /**< Lookup to retreive managers by the keys. */
		static ByValueCache ms_byValueCache; /**< Lookup to retreive managers by value. */		
		
		
		friend SmartPtrDelete(SavableManager);
		friend class SelectionMask;
		
		TableImplPtr m_table; /**< The table this manager belongs to. */
		ValuePtr m_lookupvalue; /**< The value the lookup will use. */
				
 		FieldValueMap m_fields; /**< The value of this manager. */
		FieldValueMap m_orig; /**< The values of this manager as they currently are in the database. */
				
		bool m_newentry; /**< Whether this manager is saved to the database yet. */
		bool m_locked; /**< Whether this manager is locked for writing. */
};
