/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "KeyImpl.h"
#include "KeyValue.h"

KeyImpl::KeyImpl(TableImplPtr nativeTable, TableImplPtr foreignTable, cstring name, bool primary, bool lookup) :
FieldImpl(nativeTable, name, false, lookup), 
m_foreignTable(foreignTable), 
m_primary(primary)
{ 
	Assert(nativeTable); 
	Assert(foreignTable);
}

FieldValuePtr KeyImpl::newValue()
{	
	FieldImplPtr field = shared_from_this();
	KeyImplPtr thisptr = boost::static_pointer_cast<KeyImpl>(field);
	Assert(thisptr.get() == this);
	
	KeyValuePtr result(new KeyValue(thisptr, 0));
	return result;
}

TableImplPtr KeyImpl::getForeignTable() const
{ 
	return m_foreignTable; 
}

bool KeyImpl::isForeignKey() const
{ 
	return getForeignTable() != getTable(); 
}
