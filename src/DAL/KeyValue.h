/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file KeyValue.h
 * This file contains the KeyValue class.
 *
 * @see KeyValue 
 */ 

#include "FieldValue.h"
#include "Types.h"

/**
 * This class stores the value of the key of a row.
 *
 * It may be used as if it were a regular Value object.
 * Calls to getStringValue are illegal, but this is already asserted due to the field not being a text field. 
 * 
 * @see Value
 */ 
class KeyValue : public FieldValue
{
public:
	/** Constructs a new KeyValue for to the specified field with the specified value. */
	KeyValue(KeyImplPtr key, value_type value);
	
	/** Destructor, a noop. */
	~KeyValue();
	
	
	/** Returns what field this key belongs to. */
	KeyImplPtr getKeyImpl() const;
	
	
	/** Returns that this is a key. */
	bool isKey() const { return true; }
	
	/** Whether this is a primary key. */
	bool isPrimaryKey() const;
	
	
	/** Wether there are any unsaved changes to this key. */
	bool isDirty() const;
	
	/** Sets wether there are any unsaved changes to this key. */
	void setDirty(bool dirty);

private:
	KeyImplPtr m_key; /**< The field this KeyValue belongs to. */
	value_type m_value; /**< The value of this key. */
	bool m_dirty; /**< Wether there are unsaved changes to this key. */
};
