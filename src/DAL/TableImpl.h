/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file TableImpl.h
 * This file contains the TableImpl class.
 *
 * @see TableImpl
 */ 

#include "Types.h"

/**
 * This class represents a generated (concrete version of a) TableDef.
 *
 * This class should not be instantiated manually.
 * Instead, the generated ones from TableImpls should be used.
 *
 * @see TableImpls 
 * @see TableDef 
 * @see Table 
 */ 
class TableImpl
{
public:	
	/** Constructs a new TableImpl based on this specific table. */
	TableImpl(cstring name);
	
	/** Destructor, a noop. */
	virtual ~TableImpl() { }

	
	/** Returns the name of this table. */
	cstring getName() const;
	
	
	/** 
	 * Returns a list of all rows in this table after applying mask.
	 * 
	 * @param mask The mask to apply, an 'empty' (not NULL!) mask is allowed. 
	 * @return The first entry is a header, the ones after that are the rows. 
	 */
	Strings tableList(SelectionMaskPtr mask);	
	
	
	/** Returns an iterator to the first element of the FieldImpl's of this table. */
	FieldImplVector::const_iterator begin() const { return m_fields.begin(); }
	
	/** Return the 'end' iterator of the FieldImpl's of this table. */
	FieldImplVector::const_iterator end() const { return m_fields.end(); }
	
	/** Returns the size of the FieldImpl's of this table. */
	size_t size() const { return m_fields.size(); }
	
	/** Retursn the amount of primary keys this table has. */
	size_t primarykeysize() const { return m_primarykeysize; }
	
	/** Returns the first key of this table, keysize is asserted to be 1. */
	KeyImplPtr firstkey() const;
	
	
	/** Initializes the table's fields. */
	virtual void Initialize() { updateFieldCount(); }
	
protected:
	/** Updates the fieldcount. */
	void updateFieldCount();

	std::string m_name; /**< The name of this table. */
	FieldImplVector m_fields; /**< The fields generated for this table. */
	size_t m_primarykeysize; /**< The amount of primary keys for this table. */
};
