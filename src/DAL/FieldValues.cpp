/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "FieldValue.h"
#include "FieldValues.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Parse.h"
#include "StringUtilities.h"
#include "TableImpl.h"

FieldValues::FieldValues(TableImplPtr table) : 
m_table(table) 
{ 
	Assert(table); 
}

FieldValues::~FieldValues() 
{ 
	
}

ValuePtr FieldValues::getField(FieldImplPtr field) const
{
	Assert(field);
	ValueMap::const_iterator it = m_fields.find(field.get());
	Assert(it != m_fields.end());
		
	return it->second;
}

TableImplPtr FieldValues::getTable() const 
{ 
	return m_table; 
}

void FieldValues::addValue(ValuePtr value)
{
	Assert(value);
	
	TableImplPtr table = value->getTable();
	bool isReachableTable = false;
	
	if(table == m_table)
		isReachableTable = true;
	
	for(TableImplVector::const_iterator it = m_joinedTables.begin(); it != m_joinedTables.end(); it++)
	{
		TableImplPtr joinedTable = *it;
		if(table == joinedTable)
			isReachableTable = true;
	}
	
	Assert(isReachableTable);
		
	m_fields[value->getField().get()] = value;
}

void FieldValues::addValue(FieldImplPtr field, value_type value)
{
	Assert(field);
	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	addValue(fieldvalue);
}

void FieldValues::addValue(FieldImplPtr field, cstring value)
{
	Assert(field);
	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	addValue(fieldvalue);
}

void FieldValues::addJoinedTable(TableImplPtr joinedTable)
{
	Assert(joinedTable);
	
	m_joinedTables.push_back(joinedTable);
}
	
size_t FieldValues::size() const 
{ 
	return m_fields.size(); 
}

ValuePtr FieldValues::first() const 
{
	Assert(m_fields.size() > 0);
	
	return m_fields.begin()->second; 
}

ValueMap::const_iterator FieldValues::begin() const 
{ 
	return m_fields.begin(); 
}

ValueMap::const_iterator FieldValues::end() const 
{ 
	return m_fields.end(); 
}

ValueMap::const_iterator FieldValues::find(FieldImplPtr field) const
{
	Assert(field); 
	
	return m_fields.find(field.get()); 
}

