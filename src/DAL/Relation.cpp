/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Keys.h"
#include "Relation.h"
#include "SavableManager.h"
#include "TableImpl.h"

Relation::Relation(TableImplPtr table) :
m_table(table),
m_keys(new Keys(table))
{
	Assert(table);
	
	m_manager = SavableManager::getnew(table);
	bool locked = m_manager->lock();
	Assert(locked); // new manager, should always be unlocked
}

Relation::~Relation()
{
	
}

void Relation::addKey(KeyImplPtr keyimpl, value_type value)
{
	Assert(keyimpl);
	Assert(value);
	
	KeyValuePtr key(new KeyValue(keyimpl, value));
	m_keys->addKey(key);
}

void Relation::addField(FieldImplPtr field, value_type intvalue)
{
	Assert(field);
	
	ValuePtr value(new FieldValue(field, intvalue));
	m_manager->setValue(value);
}

void Relation::addField(FieldImplPtr field, cstring stringvalue)
{
	Assert(field);
	
	ValuePtr value(new FieldValue(field, stringvalue));
	m_manager->setValue(value);
}

void Relation::save()
{
	m_manager->setKeys(m_keys);
	m_manager->save();
	m_manager->unlock();
}
