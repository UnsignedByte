/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file SavableManagers.h
 * This file contains the SavableManagers class. 
 *
 * @see SavableManagers
 */ 

#include "Types.h"

/**
 * This class is a bucket for SavableManager objects.
 *
 * It will only add SavableManagers that belong to the same table the bucket was constructed with.
 * This is asserted at run-time. 
 * 
 * @see SavableManager 
 */ 
class SavableManagers
{
public:
	/** Constructs a SavableManager bucket with the specified table. */
	SavableManagers(TableImplPtr table);
	
	/** Destructor, a noop. */
	~SavableManagers();
	
	/** Returns the table this SavableManager bucket is for. */
	TableImplPtr getTable() const;
	
	
	/** Add a manager to the bucket, it is asserted at run-time to belong to the same table this bucket was constructed with. */
	void addManager(SavableManagerPtr manager);
	
	
	/** Returns a header that details all the fields for the table this SavableManager bucket was constructed with. */
	Strings getHeader() const;
	
	
	/** Returns the size of the SavableManagers in this bucket. */
	size_t size() const { return m_managers.size(); }
	
	/** Returns the first manager from the result, the resultsize is asserted to be 1 at runtime. */
	SavableManagerPtr first() const { Assert(size() == 1); return *m_managers.begin(); }
	
	/** Returns an iterator to the first SavableManager in this bucket. */
	SavableManagerVector::const_iterator begin() const { return m_managers.begin(); }
	
	/** Returns the 'end' iterator of the SavableManagers in this bucket. */
	SavableManagerVector::const_iterator end() const { return m_managers.end(); }

private:
	TableImplPtr m_table; /**< The table this SavableManager bucket is for. */
	SavableManagerVector m_managers; /**< The SavableManagers contained in this bucket. */
};
