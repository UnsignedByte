/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file Statements.h
 * This file contains the Statements class.
 *
 * @see Statements 
 */ 

#include "Types.h"

/**
 * This is a bucket class for prepared sqlite3 statements.
 */ 
class Statements
{
public:
	/** Creates an empty statements bucket. */
	Statements();	
	
	/** Destructor, a noop. */
	~Statements();
	
	/** Returns the prepared statement to erase an entry from this table. */
	sqlite3_stmt* getErase() const;
	
	/** Returns the prepared statement to insert an entry into this table. */
	sqlite3_stmt* getInsert() const;
	
	/** Returns the prepared statement to update an entry from this table. */
	sqlite3_stmt* getUpdate() const;
	
	/** Returns the prepared statement to select an entry from this table. */
	sqlite3_stmt* getSelect() const;
	
	/** Returns the prepared statement to look up an entry from this table on the specified field. */
	sqlite3_stmt* getLookup(FieldPtr field);
	
	/** Returns the prepared statement to list all entries in a table. */
	sqlite3_stmt* getList() const;
	
	
	/** Sets the prepared statement to erase an entry from this table. */
	void setErase(sqlite3_stmt* erase);
	
	/** Sets the prepared statement to insert an entry into this table. */
	void setInsert(sqlite3_stmt* insert);
	
	/** Sets the prepared statement to update an entry in this table. */
	void setUpdate(sqlite3_stmt* update);
	
	/** Sets the prepared statement to select an entry from this table. */
	void setSelect(sqlite3_stmt* select);
	
	/** Sets the prepared statement to look up an entry from this table on the specified field. */
	void setLookup(FieldPtr field, sqlite3_stmt* lookup);
	
	/** Sets the prepared statement to list all entries in a table. */
	void setList(sqlite3_stmt* list);	
	
	/** Commit the changes to the database (finalize all prepared statements). */
	void commit();
	
private:
	typedef std::map<Field*, sqlite3_stmt*> fieldmap; /**< A type of a map containing the SQL for each field's lookup*/
	
	sqlite3_stmt* m_insert; /**< The prepared statement to insert an entry into this table. */
	sqlite3_stmt* m_erase; /**< The prepared statement to erase an entry from this table. */
	sqlite3_stmt* m_update; /**< The prepared statement to update an entry in this table. */
	sqlite3_stmt* m_select; /**< The prepared statement to select an entry from this table. */
	fieldmap m_lookup; /**< The map containing the prepared statement for each field's lookup. */
	sqlite3_stmt* m_list; /**< The prepared statement to list all entries in a table. */
};

typedef SmartPtr<Statements> StatementsPtr; /**< The type of a pointer to a statements bucket. */
