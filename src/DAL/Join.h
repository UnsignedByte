/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Join.h
 * This file contains the Join class. 
 *
 * @see Join 
 */ 

#include "Types.h"

/**
 * This class represents a checked join between two tables.
 *
 * With this class a join may be specified bewteen two tables.
 * The native key should belong to the native table and the foreign key to the join table.
 * Only one of the two keys may be a non-foreign key. 
 * Even so, either key should point to the other table.
 * This is asserted at run-time. 
 */ 
class Join
{
public:
	/** Construct a Join between the specified tables with the specified keys. */
	Join(TableImplPtr nativeTable, TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey);	
	
	/** Destructor, a noop. */
	~Join();
	
	
	/** Returns the native table. */
	TableImplPtr getNativeTable() const;
	
	/** Returns the join table. */
	TableImplPtr getJoinTable() const;
	
	/** Returns the native key. */
	KeyImplPtr getNativeKey() const;
	
	/** Returns the foreign key. */
	KeyImplPtr getForeignKey() const;	
	
private:
	TableImplPtr m_nativeTable; /**< The native table. */
	TableImplPtr m_joinTable; /**< The join table. */
	KeyImplPtr m_nativeKey; /**< The native key. */
	KeyImplPtr m_foreignKey; /**< The foreign key. */
};
