/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <sstream>

#include "FieldDef.h"
#include "Global.h"
#include "KeyDef.h"
#include "StringUtilities.h"
#include "TableDef.h"

TableDef::TableDef(std::string name) :
m_name(name),
m_primarykeysize(0)
{
	std::string foreignname;
	foreignname.append("fk");
	
	std::string tablename = name;
	char first = tablename[0];
	first = toupper(first);
	
	tablename[0] = first;
	foreignname.append(tablename);
	
	m_foreignname = foreignname;
}

TableDef::~TableDef()
{
	
}

cstring TableDef::getName() const
{
	return m_name;
}

void TableDef::addPK(const std::string& name)
{
	FieldDefPtr field(new KeyDef(shared_from_this(), name, true));
	m_deffields.push_back(field);
	m_primarykeysize++;
}

void TableDef::addFPK(TableDefPtr table)
{
	Assert(table);
	addFPK(table, Global::Get()->EmptyString);
}

void TableDef::addFPK(TableDefPtr table, const std::string& suffix)
{
	Assert(table);
	
	std::string name;
	name.append(table->tableForeignName());
	name.append(suffix);
	
	FieldDefPtr field(new KeyDef(table, name, true));
	m_deffields.push_back(field);
	m_primarykeysize++;
}

void TableDef::addValue(const std::string& name)
{
	FieldDefPtr field(new FieldDef(name, false));
	m_deffields.push_back(field);
}

void TableDef::addValue(const std::string& name, value_type defaultvalue)
{
	FieldDefPtr field(new FieldDef(name, false, String::Get()->fromInt(defaultvalue)));
	m_deffields.push_back(field);
}

void TableDef::addTextField(const std::string& name)
{
	FieldDefPtr field(new FieldDef(name, true));
	m_deffields.push_back(field);	
}

void TableDef::addLookupTextField(const std::string& name)
{
	FieldDefPtr field(new FieldDef(name, true, true));
	m_deffields.push_back(field);
}

void TableDef::addTextField(const std::string& name, const std::string& defaulttext)
{
	FieldDefPtr field(new FieldDef(name, true, defaulttext));
	m_deffields.push_back(field);
}

void TableDef::addFK(TableDefPtr table)
{
	Assert(table);
	
	addFK(table, Global::Get()->EmptyString);
}

void TableDef::addFK(TableDefPtr table, const std::string& suffix)
{	
	Assert(table);
	
	std::string name;
	name.append(table->tableForeignName());
	name.append(suffix);

	FieldDefPtr field(new KeyDef(table, name, false));
	m_deffields.push_back(field);
	// TODO, add SQLite triggers
}

const std::string& TableDef::tableForeignName() const
{	
	return m_foreignname;
}
