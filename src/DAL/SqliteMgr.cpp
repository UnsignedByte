/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "DatabaseMgr.h"
#include "FieldImpl.h"
#include "Global.h"
#include "Join.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "SavableManager.h"
#include "SavableManagers.h"
#include "SelectionMask.h"
#include "SqliteMgr.h"
#include "StatementStrings.h"
#include "Statements.h"
#include "TableImpl.h"

extern bool g_printsql;

SqliteMgr::SqliteMgr()
{	
	m_db = DatabaseMgr::Get()->DB();
	m_odb = m_db->grabdb();
	
	Assert(m_db);
	Assert(m_odb);
}

SqliteMgr::~SqliteMgr()
{
	m_db->freedb(m_odb);
}

void SqliteMgr::doInsert(SavableManager* bindable)
{
	Assert(bindable);
	
	TableImpl* table = bindable->getTable().get();
	Assert(table);
	
	sqlite3_stmt* insert = getInsertStmt(table);
	Assert(insert);
	
	sqlite3_reset(insert);
	
	if(table->primarykeysize() > 1)	
		bindable->bindKeys(m_odb->db, insert);
		
	doStatement(insert);
	
	if(table->primarykeysize() == 1)
		bindable->parseInsert(m_odb->db);
	
	commit(table);
}

void SqliteMgr::doErase(SavableManager* bindable)
{
	Assert(bindable);
	
	TableImpl* table = bindable->getTable().get();
	Assert(table);
	
	sqlite3_stmt* erase = getEraseStmt(table);
	Assert(erase);
	
	sqlite3_reset(erase);
	
	bindable->bindKeys(m_odb->db, erase);
	doStatement(erase);	
	
	commit(table);
}

void SqliteMgr::doUpdate(SavableManager* bindable)
{
	Assert(bindable);
	
	TableImpl* table = bindable->getTable().get();
	Assert(table);
	
	// This table doesn't have any properties, it need not be updated.
	if(table->size() <= table->primarykeysize())
		return;
	
	sqlite3_stmt* update = getUpdateStmt(table);
	Assert(update);
	
	sqlite3_reset(update);
	
	bindable->bindUpdate(m_odb->db, update);
	doStatement(update);
	
	commit(table);
}

void SqliteMgr::doSelect(SavableManager* bindable)
{
	Assert(bindable);
	
	TableImpl* table = bindable->getTable().get();
	Assert(table);
	
	sqlite3_stmt* select = getSelectStmt(table);
	Assert(select);
	
	sqlite3_reset(select);
	
	bindable->bindKeys(m_odb->db, select);
	bool row = doStatement(select);
	if(row)
		bindable->parseSelect(select);
	else
		throw RowNotFoundException("SqliteMgr::doSelect(), no row.");
}

void SqliteMgr::doLookup(SavableManager* bindable, FieldPtr field)
{
	Assert(bindable);
	Assert(field);
	
	TableImpl* table = bindable->getTable().get();
	Assert(table);
	
	sqlite3_stmt* lookup = getLookupStmt(table, field);
	Assert(lookup);
	
	sqlite3_reset(lookup);
	
	bindable->bindLookup(m_odb->db, lookup);
	bool row = doStatement(lookup);
	if(row)
	{
		bindable->parseLookup(lookup);
		doSelect(bindable);
	}
	else
		throw RowNotFoundException("SqliteMgr::doLookup(), no row.");
}

void SqliteMgr::doSelectMulti(SelectionMask* mask)
{
	Assert(mask);
	
	sqlite3_stmt* selectMulti = getSelectMultiStmt(mask, false);
	Assert(selectMulti);
	
	sqlite3_reset(selectMulti);
	
	mask->bindSelectMulti(m_odb->db, selectMulti);
	
	bool good = true;
	for(int i = 0; good; i++)
	{
		good = doStatement(selectMulti);
		if(good)
			mask->parseRow(selectMulti);
	}
	
	sqlite3_finalize(selectMulti);
}

void SqliteMgr::doCount(SelectionMask* mask)
{
	Assert(mask);
	
	sqlite3_stmt* count = getSelectMultiStmt(mask, true);
	Assert(count);
	
	sqlite3_reset(count);
	
	mask->bindSelectMulti(m_odb->db, count);
	
	bool good = doStatement(count);
	
	if(good)
		mask->parseCount(count);	
	
	sqlite3_finalize(count);
}

void SqliteMgr::commit(TableImpl* table)
{
	Assert(table);
	m_statements.clear();
}

bool SqliteMgr::doStatement(sqlite3_stmt* stmt)
{
	Assert(stmt);
	
	int rc = sqlite3_step(stmt);

	switch(rc) {
	case SQLITE_DONE:
		return false;
	case SQLITE_ROW:
		return true;
	}

	throw SqliteError(m_odb->db);
}

StatementsPtr SqliteMgr::getStatements(TableImpl* table)
{
	Assert(table);
	
	StatementsPtr statements = m_statements[table];
	if(statements)
		return statements;
	
	statements = StatementsPtr(new Statements());
	
	m_statements[table] = statements;
	return statements;
}

StatementStringsPtr SqliteMgr::getStatementStrings(TableImpl* table)
{
	Assert(table);
	
	StatementStringsPtr statements = m_statementstrings[table];
	if(statements)
		return statements;
	
	statements = StatementStringsPtr(new StatementStrings());
	
	m_statementstrings[table] = statements;
	return statements;
}

sqlite3_stmt* SqliteMgr::getInsertStmt(TableImpl* table)
{
	Assert(table);
	
	StatementsPtr statements = getStatements(table);
	sqlite3_stmt* statement = statements->getInsert();
	if(statement)
		return statement;
		
	std::string sql;
	
	StatementStringsPtr statementstrings = getStatementStrings(table);
	cstring statementstring = statementstrings->getInsert();
	
	if(statementstring.size() != 0)
	{
		sql = statementstring;
	}
	else
	{
		sql.append("INSERT INTO ");
		sql.append(table->getName());
		sql.append(" (");
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(it != table->begin())
				sql.append(", ");
			
			sql.append(field->getName());
		}
		sql.append(") VALUES(");
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(it != table->begin())
				sql.append(", ");

			if(table->primarykeysize() == 1)
				sql.append("NULL");
			else
				sql.append("?");
		}
		sql.append(");");
			
		statementstrings->setInsert(sql);
	}
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	Assert(m_leftover == NULL || strlen(m_leftover) == 0);

	statements->setInsert(statement);
	return statement;
}

sqlite3_stmt* SqliteMgr::getEraseStmt(TableImpl* table)
{
	StatementsPtr statements = getStatements(table);
	sqlite3_stmt* statement = statements->getErase();
	if(statement)
		return statement;
		
	std::string sql;
	
	StatementStringsPtr statementstrings = getStatementStrings(table);
	cstring statementstring = statementstrings->getErase();
	
	if(statementstring.size() != 0)
	{
		sql = statementstring;
	}
	else
	{
		sql.append("DELETE FROM ");
		sql.append(table->getName());
		sql.append(" WHERE ");
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(it != table->begin())
				sql.append(", ");

			sql.append(field->getName());
			sql.append("=?");
		}
		sql.append(";");
	
		statementstrings->setErase(sql);
	}
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);

	Assert(m_leftover == NULL || strlen(m_leftover) == 0);		
	
	statements->setErase(statement);
	return statement;
}

sqlite3_stmt* SqliteMgr::getUpdateStmt(TableImpl* table)
{
	StatementsPtr statements = getStatements(table);
	sqlite3_stmt* statement = statements->getUpdate();
	if(statement)
		return statement;

	std::string sql;
	
	StatementStringsPtr statementstrings = getStatementStrings(table);
	cstring statementstring = statementstrings->getUpdate();
	
	if(statementstring.size() != 0)
	{
		sql = statementstring;
	}
	else
	{
		sql.append("UPDATE ");
		sql.append(table->getName());
		sql.append(" SET ");
		
		bool comspace = false;
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(field->isPrimaryKey())
				continue;
			
			if(comspace)
				sql.append(", ");

			sql.append(field->getName());
			sql.append("=?");
			
			comspace = true;
		}
		sql.append(" WHERE ");
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(it != table->begin())
				sql.append(" AND ");

			sql.append(field->getName());
			sql.append("=?");
		}	
		sql.append(";");
		
		statementstrings->setUpdate(sql);
	}

	if(g_printsql) {
		Global::Get()->printsql(sql);
	}

	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);

	Assert(m_leftover == NULL || strlen(m_leftover) == 0);
		
	statements->setUpdate(statement);
	return statement;
}

sqlite3_stmt* SqliteMgr::getSelectStmt(TableImpl* table)
{
	StatementsPtr statements = getStatements(table);
	sqlite3_stmt* statement = statements->getSelect();
	if(statement)
		return statement;
		
	std::string sql;
	
	StatementStringsPtr statementstrings = getStatementStrings(table);
	cstring statementstring = statementstrings->getSelect();
	
	if(statementstring.size() != 0)
	{
		sql = statementstring;
	}
	else
	{
		sql.append("SELECT ");
		
		bool comspace = false;
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(field->isPrimaryKey())
				continue;
			
			if(comspace)
				sql.append(", ");

			sql.append(field->getName());
			comspace = true;
		}
		
		/**
		 * Prevent queries in the form "SELECT  FROM ....", this is for tables that consist of only primary keys.
		 */ 
		if(table->primarykeysize() == table->size())
			sql.append("*");
		 
		sql.append(" FROM ");
		sql.append(table->getName());
		sql.append(" WHERE ");
		
		bool andspace = false;
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(andspace)
				sql.append(" AND ");

			sql.append(field->getName());
			sql.append("=?");
			andspace = true;
		}
		sql.append(";");
	
		statementstrings->setSelect(sql);
	}
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
		
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	Assert(m_leftover == NULL || strlen(m_leftover) == 0);
		
	statements->setSelect(statement);
	return statement;
}

sqlite3_stmt* SqliteMgr::getLookupStmt(TableImpl* table, FieldPtr lookupfield)
{
	StatementsPtr statements = getStatements(table);
	sqlite3_stmt* statement = statements->getLookup(lookupfield);
	if(statement)
		return statement;
		
	std::string sql;
	
	StatementStringsPtr statementstrings = getStatementStrings(table);
	cstring statementstring = statementstrings->getLookup(lookupfield);
	
	if(statementstring.size() != 0)
	{
		sql = statementstring;
	}
	else
	{
		sql.append("SELECT ");
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(it != table->begin())
				sql.append(", ");

			sql.append(field->getName());
		}
		sql.append(" FROM ");
		sql.append(table->getName());
		sql.append(" WHERE ");
		sql.append(lookupfield->getName());
		sql.append("=?");
		sql.append(";");
			
		statementstrings->setLookup(lookupfield, sql);
	}
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);

	Assert(m_leftover == NULL || strlen(m_leftover) == 0);

	statements->setLookup(lookupfield, statement);
	return statement;
}

sqlite3_stmt* SqliteMgr::getSelectMultiStmt(SelectionMask* mask, bool count)
{
	Assert(mask);
	
	std::string sql;
	sqlite3_stmt* statement;
	
	TableImplPtr table = mask->getTable();
	Assert(table);

	sql.append("SELECT ");
	
	if(count) {
		sql.append("COUNT(*)");
	}
	else
	{
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(it != table->begin())
				sql.append(", ");

			sql.append(field->getTable()->getName());
			sql.append(".");
			sql.append(field->getName());
		}
	}
	
	sql.append(" FROM ");
	sql.append(table->getName());
	
	/**
	 * Allow for queries that retreive data from foreigh tables "SELECT a, b, c, FROM Table INNER JOIN Other ON tableid = fkTable WHERE d = 5;"
	 */ 
	for(Joins::const_iterator it = mask->joinsbegin(); it != mask->joinsend(); it++)
	{
		JoinPtr join = *it;
		sql.append(" INNER JOIN ");
		sql.append(join->getJoinTable()->getName());
		sql.append(" ON ");
		sql.append(join->getNativeKey()->getName());
		sql.append(" = ");
		sql.append(join->getForeignKey()->getName());
	}	
	
	/**
	 * Prevent queries in the form "SELECT a, b, c FROM table WHERE ;", this is for the unmasked selection.
	 */ 
	if(mask->size())
		sql.append(" WHERE ");
	
	for(Strings::const_iterator it = mask->begin(); it != mask->end(); it++)
	{
		if(it != mask->begin())
			sql.append(" AND ");

		sql.append(*it);
		sql.append("=?");
	}
	sql.append(";");
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
		
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	Assert(m_leftover == NULL || strlen(m_leftover) == 0);
		
	return statement;
}

bool SqliteMgr::databasePopulated()
{
	std::string sql("SELECT * FROM SQLITE_MASTER;");
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	sqlite3_stmt* statement;
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	Assert(m_leftover == NULL || strlen(m_leftover) == 0);
	
	bool populated = doStatement(statement);
	sqlite3_finalize(statement);
	
	return populated;
}

bool SqliteMgr::tableValid(TableImplPtr table)
{
	Assert(table);
	
	std::string sql = tableQuery(table);
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	sqlite3_stmt* statement;
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	bool success = doStatement(statement);
	sqlite3_finalize(statement);
	
	return success;
}

void SqliteMgr::initTable(TableImplPtr table)
{
	Assert(table);
		
	std::string sql = SqliteMgr::Get()->creationQuery(table);
	
	if(g_printsql) {
		Global::Get()->printsql(sql);
	}
	
	sqlite3_stmt* statement;
	int errorcode = sqlite3_prepare_v2(m_odb->db, sql.c_str(), (int)sql.size(), &statement, &m_leftover);

	if(errorcode != SQLITE_OK)
		throw SqliteError(m_odb->db, sql);
		
	Assert(m_leftover == NULL || strlen(m_leftover) == 0);	
	doStatement(statement);
	sqlite3_finalize(statement);
	
	return;	
}

std::string SqliteMgr::tableQuery(TableImplPtr table) const
{
	Assert(table);
	
	std::string result;
	result.append("SELECT type FROM sqlite_master WHERE tbl_name='");
	result.append(table->getName());
	result.append("' and sql='");
	
	result.append(creationQuery(table, true));
	
	result.append("';");
	
	return result;
}

/*
	"CREATE TABLE IF NOT EXISTS %s("
	"%s INTEGER PRIMARY KEY AUTOINCREMENT"
	",versiontext TEXT"
	",grantgroup INTEGER RESTRAINT grantgroup DEFAULT 1"
	");",
*/
std::string SqliteMgr::creationQuery(TableImplPtr table, bool verify) const
{
	Assert(table);
	
	std::string result;
	
	if(verify)
		result.append("CREATE TABLE ");
	else
		result.append("CREATE TABLE IF NOT EXISTS ");
		
	result.append(table->getName());
	result.append("(");
	
	for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
	{
		FieldImplPtr field = *it;
		Assert(field);
		
		bool psk = (field->isPrimaryKey() && table->primarykeysize() == 1);
		
		if(it != table->begin())
			result.append(", ");
			
		result.append(field->getName());
		
		if(field->isText()) {
			result.append(" TEXT");
		}
		else
		{
			result.append(" INTEGER");
			
			if(psk)
				result.append(" PRIMARY KEY AUTOINCREMENT");
		}
				
		if(field->isLookup() && !psk)
		{
			result.append(" RESTRAINT ");
			result.append(field->getName());
			result.append(" UNIQUE");
		}
		
		std::string defaultvalue = field->getDefaultValue();		
		
		if(defaultvalue.size() != 0)
		{
			Assert(!field->isLookup());
			Assert(!psk);
			
			result.append(" RESTRAINT ");
			result.append(field->getName());
			result.append(" DEFAULT ");
			
			if(field->isText())
				result.append("'");
				
			result.append(defaultvalue);
			
			if(field->isText())
				result.append("'");		
		}
	}
	
	if(table->primarykeysize() > 1)
	{
		result.append(", PRIMARY KEY(");
		
		bool comspace = false;
		for(FieldImplVector::const_iterator it = table->begin(); it != table->end(); it++)
		{
			FieldImplPtr field = *it;
			Assert(field);
			
			if(!field->isPrimaryKey())
				continue;
			
			if(comspace)
				result.append(", ");
				
			result.append(field->getName());
			comspace = true;
		}
		result.append(")");
	}
		
	result.append(")");
	
	if(!verify)
		result.append(";");
	
	return result;
}

std::string SqliteMgr::creationString(FieldImplPtr field) const
{
	Assert(field);
	
	std::string result = field->getName();
	
	if(field->isText())
		result.append(" TEXT");
	else
		result.append(" INTEGER");
	
	if(field->getDefaultValue().size() != 0)
	{
		result.append(" RESTRAINT ");
		result.append(field->getName());
		result.append(" DEFAULT ");
		
		if(field->isText())
			result.append("'");
		result.append(field->getDefaultValue());
		if(field->isText())
			result.append("'");		
	}
	
	return result;
}
