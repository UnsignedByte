/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Database.h"
#include "DatabaseMgr.h"
#include "FieldImpl.h"
#include "Global.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Keys.h"
#include "SqliteMgr.h"
#include "TableImpl.h"

std::string DatabaseMgr::m_staticpath = Global::Get()->EmptyString;

void DatabaseMgr::Initialize(const std::string& path)
{
	Free();
	m_staticpath = path;
	Get();
	m_staticpath = Global::Get()->EmptyString;
}

DatabaseMgr::DatabaseMgr() :
m_path(m_staticpath),
m_db(new Database(m_path))
{
	Assert(m_staticpath != Global::Get()->EmptyString);
}

DatabaseMgr::~DatabaseMgr()
{	
	delete m_db;
}

Database* DatabaseMgr::DB()
{
	Assert(m_db);
	return m_db;
}

Database& DatabaseMgr::DBref()
{
	Assert(m_db);
	return *m_db;
}
