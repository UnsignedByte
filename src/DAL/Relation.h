/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Relation.h
 * This file contains the Relation class.
 *
 * @see Relation 
 */ 

#include "Types.h"

/**
 * This class may be used to easily create a new relation.
 *
 * Using this class a new entry in a relational table is made.
 * This entry connects two rows in the different tables the relational table refers to.
 * Keys may be added throug the <code>addKey</code> function.
 * Fields within the relational table may be set with the <code>addField</code> method.
 * When the relation is fully specified the entry may be added with the <code>save</code> method. 
 * Note: <code>save</code> may only be called once! 
 *
 * @see addKey
 * @see addField 
 * @see save 
 */ 
class Relation
{
public:
	/** Construct a new Relation object for the specified table. */
	Relation(TableImplPtr table);
	
	/** Destructor, a noop. */
	~Relation();
	
	
	/** Returns the relational table this Relation belongs to. */
	TableImplPtr getTable() { return m_table; }
	
	
	/** Add a key from the specified field with the specified value. */
	void addKey(KeyImplPtr key, value_type value);
	
	/** Add a value from the specified field with the specified value. */
	void addField(FieldImplPtr field, value_type value);
	
	/** Add a value from the specified field with the specified value. */
	void addField(FieldImplPtr field, cstring value);
	
	
	/** Save the Relation to the database. */
	void save();
	
private:
	TableImplPtr m_table; /**< The table this relation belongs to. */
	KeysPtr m_keys; /**< The keys that were added to this relation. */
	SavableManagerPtr m_manager; /**< The savable manager that will be used to save the relation. */
};
