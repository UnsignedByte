/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "Join.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "TableImpl.h"

Join::Join(TableImplPtr nativeTable, TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey) :
m_nativeTable(nativeTable),
m_joinTable(joinTable), 
m_nativeKey(nativeKey), 
m_foreignKey(foreignKey)
{
	Assert(nativeTable);
	Assert(joinTable);
	Assert(nativeKey);
	Assert(foreignKey);	
	
	Assert(nativeKey->getTable() == m_nativeTable);
	Assert(foreignKey->getTable() == joinTable);		
	
	TableImplPtr nnt = nativeKey->getTable();			// native	native 	table
	TableImplPtr nft = nativeKey->getForeignTable();	// native	foreign	table
	TableImplPtr fnt = foreignKey->getTable();			// foreign	native 	table
	TableImplPtr fft = foreignKey->getForeignTable();	// foreign	foreign	table
	
	bool nfk = nativeKey->isForeignKey();				// native	foreign	key
	bool ffk = foreignKey->isForeignKey();				// foreign	foreign	key
	
	/**
		A -> B
		A <- AB
	 */
	
	bool AtoB 		= (nft == fnt && nfk && !ffk); // A -> B	
	bool AfromAB 	= (fft == nnt && nnt == nft); // A <- AB	
	
	Assert(AtoB || AfromAB);
}

Join::~Join()
{
	
}

TableImplPtr Join::getNativeTable() const
{
	return m_nativeTable;
}

TableImplPtr Join::getJoinTable() const
{ 
	return m_joinTable; 
}

KeyImplPtr Join::getNativeKey() const
{ 
	return m_nativeKey; 
}

KeyImplPtr Join::getForeignKey() const
{ 
	return m_foreignKey; 
}
