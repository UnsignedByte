/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Keys.h
 * This file contains the Keys class.
 *
 * @see Keys 
 */ 

#include "Types.h"

/**
 * This is a bucket class for KeyValues.
 *
 * @see KeyValue 
 */ 
class Keys
{
public:
	/** Constructs a KeyValue bucket for the specified table. */
	Keys(TableImplPtr table);
	
	/** Constructs a KeyValue bucket for the specified table, tries to initialize with values from the specified string. */
	Keys(TableImplPtr table, cstring initstring);
	
	/** Destructor, a noop. */
	~Keys();
	
	
	/** Returns the table this KeyValue bucket belongs to. */
	TableImplPtr getTable() const;
	
	
	/** Add a key to the bucket. */
	void addKey(KeyValuePtr key);
	
	/** Add a key to the bucket from the specified field with the specified value. */
	void addKey(KeyImplPtr key, value_type value);
	
	
	/** Returns a string representation of the keys in the bucket. */
	std::string toString() const;
	
	/** Returns a vector of strings representing the difference between the other Keys bucket with one entry for each different field. */
	Strings getDiff(KeysPtr orig) const;
	
	
	/** Returns the size of the keys in the bucket. */
	size_t size() const;
	
	/** Returns the first KeyValue in the bucket. */
	KeyValuePtr first() const;
	
	/** Returns an iterator to the first key in the bucket. */
	KeyImplMap::const_iterator begin() const;
	
	/** Returns the 'end' iterator of the keys in the bucket. */
	KeyImplMap::const_iterator end() const;
	
	/** Returns an iterator to the result of 'find' with this specific key as argument. */
	KeyImplMap::const_iterator find(KeyImplPtr key) const;
	
	/** Returns a KeyValue for this field, asserted not to be the 'end' iterator. */
	KeyValuePtr getKey(KeyImplPtr key) const;
	
	
	/** Determines which keys have changed compared to the other set and marks them as dirty. */
	void setDirty(KeysPtr oldKeys);

private:
	TableImplPtr m_table; /**< The table this KeyValue bucket belongs to. */
	KeyImplMap m_keys; /**< The keys contained in this bucket. */
};
