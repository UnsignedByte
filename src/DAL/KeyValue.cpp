/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "TableImpl.h"

KeyValue::KeyValue(KeyImplPtr key, value_type value) : 
FieldValue(key, value),
m_key(key), 
m_dirty(false)
{ 
	Assert(key); 
}

KeyValue::~KeyValue() 
{ 
	
}
	
KeyImplPtr KeyValue::getKeyImpl() const 
{ 
	return m_key; 
}

bool KeyValue::isDirty() const
{
	return m_dirty;
}

void KeyValue::setDirty(bool dirty)
{
	m_dirty = dirty;
}

bool KeyValue::isPrimaryKey() const
{ 
	return m_key->isPrimaryKey(); 
}
