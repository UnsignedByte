/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file SqliteMgr.h
 * This file contains the SqliteMgr class.
 *
 * @see SqliteMgr 
 */ 

#include <Database.h>

#include "Types.h"

class Statements;
typedef SmartPtr<Statements> StatementsPtr; /**< The type of a pointer to a prepared statement bucket. */

class StatementStrings;
typedef SmartPtr<StatementStrings> StatementStringsPtr; /**< The type of a pointer to a sql statement bucket. */

typedef std::map<TableImpl*, StatementsPtr>  TableStatements; /**< The type of a TableImpl to statements pointer map. */
typedef std::map<TableImpl*, StatementStringsPtr>  TableStatementStrings; /**< The type of a TableImpl to a sql statment pointer map. */

/**
 * This class interfaces between savable managers and an Sqlite database.
 */ 
class SqliteMgr : public Singleton<SqliteMgr>
{
	public:
		/** Execute an insertion with the specified savable manager. */
		void doInsert(SavableManager* bindable);
		
		/** Execute an erase with the specified savable manager. */
		void doErase(SavableManager* bindable);
		
		/** Execute an update with the specified savable manager. */
		void doUpdate(SavableManager* bindable);
		
		/** Execute a selection with the specified savable manager. */
		void doSelect(SavableManager* bindable);
		
		/** Execute a lookup on the specified field with the specified savable manager. */
		void doLookup(SavableManager* bindable, FieldPtr field);
		
		/** Execute a selection on multiple entries from the databse with the specified mask. */
		void doSelectMulti(SelectionMask* mask);
		
		/** Counts all rows that match this mask. */
		void doCount(SelectionMask* mask);
		
		
		/** Returns the query to create the specified table. */
		std::string tableQuery(TableImplPtr table) const;
		
		/** Whether the database is populated. */
		bool databasePopulated();
		
		/** Initialize the specified table. */
		void initTable(TableImplPtr table);
		
		/** Whether the table matches out definitions. */
		bool tableValid(TableImplPtr table);
	
	private:
		/** 
		 * This is a singleton class, use the Get method to get the instance.
		 *
		 * @see Get
		 */ 
		SqliteMgr();
		
		/**
		 * This is a singleton class, use the Free method to free the instance.
		 *
		 * @see Free 
		 */ 
		~SqliteMgr();		
		friend class Singleton<SqliteMgr>;
		
		/** Execute the specified statement once, returns true if there are more rows available. */
		bool doStatement(sqlite3_stmt* stmt);
		
		/** Commits changes to table, currently clears all prepared statements. */
		void commit(TableImpl* table);
		
		
		/** Returns the prepared statement to insert an entry into the specified table. */
		sqlite3_stmt* getInsertStmt(TableImpl* table);
		
		/** Returns the prepared statement to erase an entry from the specified table. */
		sqlite3_stmt* getEraseStmt(TableImpl* table);
		
		/** Returns the perparedstatement to update an entry in the specified table. */
		sqlite3_stmt* getUpdateStmt(TableImpl* table);
		
		/** Returns the perparedstatement to select an entry from the specified table. */
		sqlite3_stmt* getSelectStmt(TableImpl* table);
		
		/** Returns the perparedstatement to lookup an entry for the specified table on the specified field. */
		sqlite3_stmt* getLookupStmt(TableImpl* table, FieldPtr field);
		
		/** 
		 * Returns the perparedstatement to select multiple entries from the specified table. 
		 *
		 * When count is true instead of selecting fields it will perform <code>select count(*)</code>.
		 */
		sqlite3_stmt* getSelectMultiStmt(SelectionMask* table, bool count);
		
		
		/** Returns the prepared statements bucket for the specified table. */
		StatementsPtr getStatements(TableImpl* table);
		
		/** Returns the SQL statements buckeet for the specified table. */
		StatementStringsPtr getStatementStrings(TableImpl* table);
		
		
		/** 
		 * Returns the query to create the specified table.
		 * 
		 * @param table The table to retreive the creation query for. 
		 * @param verify Whether the query is to verify the entry in the database or to insert it. 
		 */ 
		std::string creationQuery(TableImplPtr table, bool verify = false) const;
		
		/** Returns the query to create a specific field in the table. */
		std::string creationString(FieldImplPtr  table) const;
		
		
		Database* m_db; /**< The database used. */
		Database::OPENDB* m_odb; /**< The database connection used. */
		const char* m_leftover; /**< The leftover from preparing a statement. */
				
		TableStatements m_statements; /**< A map containing the prepared statements per table. */
		TableStatementStrings m_statementstrings; /**< A map containing the SQL statements per table. */
};
