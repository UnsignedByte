/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file FieldDef.h
 * This file contains the FieldDef class. 
 *
 * @see FieldDef 
 */ 

#include "Global.h"
#include "Types.h"

/**
 * This class represents the definition of a field in a table.
 * 
 * Based on this class FieldImpl's will be generated.
*/ 
class FieldDef
{
public:
	/** Construct a FieldDef with the specified name, defaultvalue and 'textness'. */
	FieldDef(const std::string& name, bool text, const std::string& defaultvalue = Global::Get()->EmptyString);
	
	/** Construct a FieldDef with the specified name, lookup and 'textness'. */
	FieldDef(const std::string& name, bool text, bool lookup);
	
	/** Destructor, a noop. */
	virtual ~FieldDef();
	
	
	/** Returns the SQL string to generate this field. */
	std::string creationString() const;	
	
	/** Returns the name of the field. */
	const std::string& getName() const { return m_name; }
	
	/** Returns whether this field is a text field. */
	bool isText() const { return m_text; }
	
	/** Returns the default value for this field. */
	const std::string& getDefaultValue() const { return m_defaultvalue; };
	
	/** Whether this is a lookup field. */
	bool isLookup() const { return m_lookup; }
	
	/** Whether this field is a key. */
	virtual bool isKey() const { return false; }
	
	/** Whether this field is a primary key. */
	virtual bool isPrimaryKey() const { return false; }
	
private:
	std::string m_name; /**< The name of this field. */
	bool m_text; /**< Whether this field is a text field. */
	std::string m_defaultvalue; /**< The default value for this field. */
	bool m_lookup; /**< Should this field be unique. */
};
