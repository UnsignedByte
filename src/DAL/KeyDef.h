/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file KeyDef.h
 * This file contains the KeyDef class. 
 *
 * @see KeyDef 
 */ 

#include "FieldDef.h"
#include "Types.h"

/**
 * This class represents the definition of a key field in a table.
 * 
 * Based on this class KeyImpl's will be generated.
*/ 
class KeyDef : public FieldDef
{
public:
	/** Constructs a key for the specified foreign table with the specified name. */
	KeyDef(TableDefPtr foreignTable, cstring name, bool primary) : FieldDef(name, false, false), m_foreignTable(foreignTable), m_primary(primary) { Assert(foreignTable); }
		
	/** Destructor, a noop. */
	~KeyDef() { }
	
	
	/** Returns that this field is a key field. */
	bool isKey() const { return true; }
		
	/** Whether this field is a primary key field. */
	bool isPrimaryKey() const { return m_primary; }
	
	/** Returns the foreign table for this key, is NULL for native keys. */
	TableDefPtr getForeignTable() { return m_foreignTable; }

private:	
	TableDefPtr m_foreignTable; /**< The foreign table for this key. */
	bool m_primary; /**< Whether this key is a primary key. */
};
