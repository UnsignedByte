/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FieldImpl.h"
#include "FieldValue.h"
#include "KeyImpl.h"
#include "TableImpl.h"

FieldImpl::FieldImpl(TableImplPtr table, cstring name, bool text, cstring defaultvalue) :
Field(name, text),
m_table(table),
m_defaultvalue(defaultvalue),
m_lookup(false)
{
	Assert(table);
}

FieldImpl::FieldImpl(TableImplPtr table, cstring name, bool text, bool lookup) :
Field(name, text),
m_table(table),
m_lookup(lookup)
{
	Assert(table);
}

FieldImpl::~FieldImpl()
{
	
}

FieldValuePtr FieldImpl::newValue()
{
	SmartPtr<FieldImpl> thisptr = shared_from_this();
	Assert(thisptr);
	
	FieldValuePtr result(new FieldValue(thisptr));
	return result;
}
