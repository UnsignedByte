/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file DatabaseMgr.h
 * This file contains the DatabaseMgr class.
 * 
 * @see DatabaseMgr 
 */ 

#include "Database.h"
#include "Types.h"

/**
 * This class manages the database connection.
 *
 * Before calling the <code>Get</code> method <code>Initialize</code> should have been called at least once.
 * <code>Initialize</code> will set the path to the database to use when the instance is created.
 * Since the path is specific to each instance each call to <code>Free</code> should be followed by <code>Initialize</code>.
 * Ofcourse, the last call to <code>Free</code> (when shutting down) should not be followed by <code>Initialize</code>.
 *
 * @see Get
 * @see Initialize 
 * @see Free 
 */ 
class DatabaseMgr : public Singleton<DatabaseMgr>
{
public:
	/** Initialize the DatabaseMgr with a specific path. */
	static void Initialize(const std::string& path);	
	
	/** Returns a pointer to the Database. */
	Database* DB();
	
	/** Returns a reference to the Database. */
	Database& DBref();	

private:
	/** This is a singleton class, use <code>Get</code> to get the instance. */
	DatabaseMgr();
	
	/** Hide the copy constructor. */
	DatabaseMgr(const DatabaseMgr& rhs);
	
	/** Hide the assignment constructor. */
	DatabaseMgr operator=(const DatabaseMgr& rhs);
	
	/** This is a singleton class, use <code>Free</code> to free the instance. */
	~DatabaseMgr();
	
	friend class Singleton<DatabaseMgr>;

	std::string m_path; /**< The path to the database to use. */
	Database* m_db; /**< A pointer to the database. */

	static std::string m_staticpath; /**< The path to the database to use for the next instance. */
};
