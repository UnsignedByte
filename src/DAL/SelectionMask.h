/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file SelectionMask.h
 * This file contains the SelectionMask class.
 *
 * @see SelectionMask
 */ 

#include "Types.h"

typedef std::vector<KeysPtr> KeysVector; /**< The type of multiple key bucket pointers. */

/**
 * This class is meant to be used to filter queries to the database.
 *
 * When used in conjunction with <code>selectMulti</code> it allows selecting specific row's from the database from a native table.
 * Through <code>addJoin</code> additional foreign tables may be joined into the query.
 * With <code>addField</code> a restriction for that specific field may be applied.
 * Finally the result of the query may be retreived with <code>getResult</code>.
 *
 * @see SqliteMgr::selectMulti 
 * @see addJoin 
 * @see addField 
 * @see getResult 
 */ 
class SelectionMask
{
	public:
		/** Constructs a SelectionMask for this specific table. */
		SelectionMask(TableImplPtr table);
		
		/** Destructor, a noop. */
		~SelectionMask();
		
		/** Returns the native table this SelectionMask is for. */
		TableImplPtr getTable() const;
		
		/** Adds a field restriction, the row's returned will all have the specified value. */
		void addField(ValuePtr value);
		
		/** Adds a field restriction, the row's returned will all have the specified value. */
		void addField(FieldImplPtr field, cstring value);
		
		/** Adds a field restriction, the row's returned will all have the specified value. */
		void addField(FieldImplPtr field, value_type value);
		
		/** 
		 * Adds a join to the specified table from the native table using the specified native and foreign keys. 
		 * When creating a join from an already joined table, specify that table. 
		 * 
		 * @param joinTable The table to join on.
		 * @param nativeKey The native key to use in the join, this key should belong to the native table.
		 * @param foreignKey The foreign key to use in the join, this key should belong to the specified foreign table. 
		 *
		 * @see addJoin 
		 */
		void addJoin(TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey);
		
		/**
		 * Adds a join to the specified table from the specified table using the specified native and foreign keys.
		 * 
		 * @param nativeTable The table to join from, this table should have been added with addJoin before.
		 * @param joinTable The table to join on.
		 * @param nativeKey The native key to use in the join, this key should belong to the native table.
		 * @param foreignKey The foreign key to use in the join, this key should belong to the specified foreign table.
		 *
		 * @see addJoin 
		 */ 
		void addJoin(TableImplPtr nativeTable, TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey);
		
		/** Adds the specified join, this join should be from a table that is either the native table or joined previously. */ 
		void addJoin(JoinPtr join);
		
		
		/** Bind the specified statement's values. */
		void bindSelectMulti(sqlite3* db, sqlite3_stmt* statement) const;
		
		/** Parse a row from the statement and store it. */
		void parseRow(sqlite3_stmt* statement);
		
		/** Parse a count from the statement and store it. */
		void parseCount(sqlite3_stmt* statement);
		
		
		/** Returns the size of the fields restricted on. */
		size_t size() { return m_restrictions.size(); }
		
		/** Returns an iterator to the first restriction fields. */
		Strings::const_iterator begin() { return m_restrictions.begin(); }
		
		/** Returns the 'end' iterator of the restriction fields. */
		Strings::const_iterator end() { return m_restrictions.end(); }			
		
		
		/** Returns the size of the joins. */
		size_t joinssize() { return m_joins.size(); }
		
		/** Returns an iterator to the first join. */
		Joins::const_iterator joinsbegin() { return m_joins.begin(); }
		
		/** Returns the 'end' iterator of the joins. */
		Joins::const_iterator joinsend() { return m_joins.end(); }
		
		
		/** Returns the result of the lookup. */
		SavableManagersPtr getResult() { return m_result; }
		
		/** Returns the row count. */
		size_t getCount() { return m_count; }
		
	private:
		/** Returns wether the specified table has been joined (or is equal to the native table) or not. */
		bool isReachableTable(TableImplPtr table);
	
		TableImplPtr m_nativeTable; /**< The native table for this selection. */
		ValuesPtr m_values; /**< The value restrictions for this selection. */
		TableImplVector m_foreignTables; /**< The foreign tables for this selection. */
		Joins m_joins; /**< The joins for this selection. */
		Strings m_restrictions; /**< The fields this selection restricts. */
		
		SavableManagersPtr m_result; /**< The result of the selection. */
		size_t m_count; /**< The amount of entries that matches. */
};
