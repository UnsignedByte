/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Field.h
 * This file contains the Field class.
 *
 * @see Field 
 */ 

#include "Types.h"

/**
 * This class represents a field in a database table.
 *
 * @deprecated This super class might be removed since it is not used. 
 */ 
class Field
{
public:
	/** Creates a new field with the specified name and 'textness'. */
	Field(cstring name, bool text = false);
	
	/** Destructor, a noop. */
	virtual ~Field();

	/** Returns the name of the field. */
	const std::string& getName() const { return m_name; }
	
	/** Returns whether this field is a text field. */
	bool isText() const { return m_text; }
	
	/** Returns wether this field is a key. */
	virtual bool isKey() const { return false; }
	
protected:
	std::string m_name; /**< The name of this field. */
	bool m_text; /**< Whether this field is a text field. */
};
