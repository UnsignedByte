/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "FieldImpl.h"
#include "FieldValue.h"
#include "FieldValues.h"
#include "Join.h"
#include "KeyDef.h"
#include "KeyImpl.h"
#include "KeyValue.h"
#include "Keys.h"
#include "Relation.h"
#include "SavableManager.h"
#include "SavableManagers.h"
#include "SelectionMask.h"
#include "TableImpl.h"

SelectionMask::SelectionMask(TableImplPtr table) :
m_nativeTable(table),
m_values(ValuesPtr(new FieldValues(table))),
m_result(SavableManagersPtr(new SavableManagers(table))),
m_count(0)
{
	Assert(table);
}

SelectionMask::~SelectionMask()
{
	
}

TableImplPtr SelectionMask::getTable() const
{
	return m_nativeTable;
}

void SelectionMask::addField(FieldValuePtr value)
{
	Assert(value);
	Assert(isReachableTable(value->getTable()));
	
	std::string fieldname = value->getTable()->getName();
	fieldname.append(".");
	fieldname.append(value->getField()->getName());
	m_restrictions.push_back(fieldname);
	m_values->addValue(value);
}

void SelectionMask::addField(FieldImplPtr field, cstring value)
{
	Assert(field);
	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	addField(fieldvalue);
}

void SelectionMask::addField(FieldImplPtr field, value_type value)
{
	Assert(field);
	
	FieldValuePtr fieldvalue(new FieldValue(field, value));
	addField(fieldvalue);
}

void SelectionMask::addJoin(TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey)
{
	Assert(joinTable);
	Assert(nativeKey);
	Assert(foreignKey);
	
	addJoin(m_nativeTable, joinTable, nativeKey, foreignKey);
}

void SelectionMask::addJoin(TableImplPtr nativeTable, TableImplPtr joinTable, KeyImplPtr nativeKey, KeyImplPtr foreignKey)
{
	Assert(joinTable);
	Assert(nativeKey);
	Assert(foreignKey);		
	Assert(foreignKey->getTable() == joinTable);
	Assert(nativeKey->getTable() == nativeTable);
	
	Assert(isReachableTable(nativeTable));
		
	JoinPtr join(new Join(nativeTable, joinTable, nativeKey, foreignKey));
	addJoin(join);
}

void SelectionMask::addJoin(JoinPtr join)
{
	Assert(join);
	Assert(isReachableTable(join->getNativeKey()->getTable()));
	
	m_joins.push_back(join);
	m_foreignTables.push_back(join->getJoinTable());
	m_values->addJoinedTable(join->getJoinTable());
}

void SelectionMask::bindSelectMulti(sqlite3* db, sqlite3_stmt* statement) const
{
	Assert(db);
	Assert(statement);
	
	int n = 1;
	for(ValueMap::const_iterator it = m_values->begin(); it != m_values->end(); it++)
	{
		ValuePtr value = it->second;
		int rc = 0;
		
		if(value->getField()->isText())
		{
			std::string stringvalue = value->getStringValue();
			rc = sqlite3_bind_text(statement, n, stringvalue.c_str(), stringvalue.size(), SQLITE_TRANSIENT);
		}
		else
		{
			value_type intvalue = value->getIntegerValue();
			rc = sqlite3_bind_int64(statement, n, intvalue);
		}
		
		if(rc != SQLITE_OK)
			throw SqliteError(db);
			
		n++;
	}
}

void SelectionMask::parseRow(sqlite3_stmt* statement)
{
	Assert(statement);
		
	SavableManagerPtr manager = SavableManager::getnew(m_nativeTable);	
	
	bool locked = manager->lock();
	Assert(locked); // new, so should always be unlocked
	
	/// Begin locked section
	manager->parseLookup(statement);
	manager->parseSelect(statement, manager->primaryKeySize());
	manager->cleanup();
	/// End locked section
	
	manager->unlock();
	
	m_result->addManager(manager);
	m_count = m_result->size();
}

void SelectionMask::parseCount(sqlite3_stmt* statement)
{
	Assert(statement);
	
	size_t count = sqlite3_column_int64(statement, 0);
	m_count = count;
}

bool SelectionMask::isReachableTable(TableImplPtr table)
{
	bool isReachableTable = false;
	
	if(table == m_nativeTable)
		isReachableTable = true;
	
	for(TableImplVector::const_iterator it = m_foreignTables.begin(); it != m_foreignTables.end(); it++)
	{
		TableImplPtr foreignTable = *it;
		if(table == foreignTable)
			isReachableTable = true;
	}
	
	return isReachableTable;
}
