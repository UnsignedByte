##
## Auto Generated makefile, please do not edit
##
ProjectName:=Sockets

## Debug
ifeq ($(type),Debug)
ConfigurationName :=Debug
IntermediateDirectory :=./Debug
OutDir := $(IntermediateDirectory)
LinkerName:=g++
ArchiveTool :=ar rcu
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix :=.o
DebugSwitch :=-gstab
IncludeSwitch :=-I
LibrarySwitch :=-l
OutputSwitch :=-o 
LibraryPathSwitch :=-L
PreprocessorSwitch :=-D
SourceSwitch :=-c 
CompilerName :=g++
OutputFile :=../../lib/libubsockets.a
Preprocessors :=
ObjectSwitch :=-o 
ArchiveOutputSwitch := 
CmpOptions :=-g -Wall $(Preprocessors)
LinkOptions := 
IncludePath :=  $(IncludeSwitch). 
RcIncludePath :=
Libs :=
LibPath := 
endif

Objects=$(IntermediateDirectory)/Base64$(ObjectSuffix) $(IntermediateDirectory)/Debug$(ObjectSuffix) $(IntermediateDirectory)/Event$(ObjectSuffix) $(IntermediateDirectory)/EventHandler$(ObjectSuffix) $(IntermediateDirectory)/EventTime$(ObjectSuffix) $(IntermediateDirectory)/File$(ObjectSuffix) $(IntermediateDirectory)/IEventOwner$(ObjectSuffix) $(IntermediateDirectory)/Ipv4Address$(ObjectSuffix) $(IntermediateDirectory)/Ipv6Address$(ObjectSuffix) $(IntermediateDirectory)/Lock$(ObjectSuffix) \
	$(IntermediateDirectory)/MemFile$(ObjectSuffix) $(IntermediateDirectory)/Mutex$(ObjectSuffix) $(IntermediateDirectory)/Parse$(ObjectSuffix) $(IntermediateDirectory)/RandomNumber$(ObjectSuffix) $(IntermediateDirectory)/ResolvServer$(ObjectSuffix) $(IntermediateDirectory)/ResolvSocket$(ObjectSuffix) $(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix) $(IntermediateDirectory)/Socket$(ObjectSuffix) $(IntermediateDirectory)/SocketHandler$(ObjectSuffix) $(IntermediateDirectory)/socket_include$(ObjectSuffix) \
	$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix) $(IntermediateDirectory)/StdoutLog$(ObjectSuffix) $(IntermediateDirectory)/StreamSocket$(ObjectSuffix) $(IntermediateDirectory)/TcpSocket$(ObjectSuffix) $(IntermediateDirectory)/Thread$(ObjectSuffix) $(IntermediateDirectory)/UdpSocket$(ObjectSuffix) $(IntermediateDirectory)/Utility$(ObjectSuffix) 

##
## Main Build Tragets 
##
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile):  $(Objects)
	$(ArchiveTool) $(ArchiveOutputSwitch)$(OutputFile) $(Objects)

./Debug:
	@test -d ./Debug || mkdir ./Debug


PreBuild:


##
## Objects
##
$(IntermediateDirectory)/Base64$(ObjectSuffix): Base64.cpp $(IntermediateDirectory)/Base64$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Base64.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Base64$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Base64$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Base64$(ObjectSuffix) -MF$(IntermediateDirectory)/Base64$(ObjectSuffix).d -MM Base64.cpp

$(IntermediateDirectory)/Debug$(ObjectSuffix): Debug.cpp $(IntermediateDirectory)/Debug$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Debug.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Debug$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Debug$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Debug$(ObjectSuffix) -MF$(IntermediateDirectory)/Debug$(ObjectSuffix).d -MM Debug.cpp

$(IntermediateDirectory)/Event$(ObjectSuffix): Event.cpp $(IntermediateDirectory)/Event$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Event.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Event$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Event$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Event$(ObjectSuffix) -MF$(IntermediateDirectory)/Event$(ObjectSuffix).d -MM Event.cpp

$(IntermediateDirectory)/EventHandler$(ObjectSuffix): EventHandler.cpp $(IntermediateDirectory)/EventHandler$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)EventHandler.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EventHandler$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EventHandler$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EventHandler$(ObjectSuffix) -MF$(IntermediateDirectory)/EventHandler$(ObjectSuffix).d -MM EventHandler.cpp

$(IntermediateDirectory)/EventTime$(ObjectSuffix): EventTime.cpp $(IntermediateDirectory)/EventTime$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)EventTime.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EventTime$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EventTime$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EventTime$(ObjectSuffix) -MF$(IntermediateDirectory)/EventTime$(ObjectSuffix).d -MM EventTime.cpp

$(IntermediateDirectory)/File$(ObjectSuffix): File.cpp $(IntermediateDirectory)/File$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)File.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/File$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/File$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/File$(ObjectSuffix) -MF$(IntermediateDirectory)/File$(ObjectSuffix).d -MM File.cpp

$(IntermediateDirectory)/IEventOwner$(ObjectSuffix): IEventOwner.cpp $(IntermediateDirectory)/IEventOwner$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)IEventOwner.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/IEventOwner$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/IEventOwner$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/IEventOwner$(ObjectSuffix) -MF$(IntermediateDirectory)/IEventOwner$(ObjectSuffix).d -MM IEventOwner.cpp

$(IntermediateDirectory)/Ipv4Address$(ObjectSuffix): Ipv4Address.cpp $(IntermediateDirectory)/Ipv4Address$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Ipv4Address.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Ipv4Address$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Ipv4Address$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Ipv4Address$(ObjectSuffix) -MF$(IntermediateDirectory)/Ipv4Address$(ObjectSuffix).d -MM Ipv4Address.cpp

$(IntermediateDirectory)/Ipv6Address$(ObjectSuffix): Ipv6Address.cpp $(IntermediateDirectory)/Ipv6Address$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Ipv6Address.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Ipv6Address$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Ipv6Address$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Ipv6Address$(ObjectSuffix) -MF$(IntermediateDirectory)/Ipv6Address$(ObjectSuffix).d -MM Ipv6Address.cpp

$(IntermediateDirectory)/Lock$(ObjectSuffix): Lock.cpp $(IntermediateDirectory)/Lock$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Lock.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Lock$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Lock$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Lock$(ObjectSuffix) -MF$(IntermediateDirectory)/Lock$(ObjectSuffix).d -MM Lock.cpp

$(IntermediateDirectory)/MemFile$(ObjectSuffix): MemFile.cpp $(IntermediateDirectory)/MemFile$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)MemFile.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/MemFile$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/MemFile$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/MemFile$(ObjectSuffix) -MF$(IntermediateDirectory)/MemFile$(ObjectSuffix).d -MM MemFile.cpp

$(IntermediateDirectory)/Mutex$(ObjectSuffix): Mutex.cpp $(IntermediateDirectory)/Mutex$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Mutex.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Mutex$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Mutex$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Mutex$(ObjectSuffix) -MF$(IntermediateDirectory)/Mutex$(ObjectSuffix).d -MM Mutex.cpp

$(IntermediateDirectory)/Parse$(ObjectSuffix): Parse.cpp $(IntermediateDirectory)/Parse$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Parse.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Parse$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Parse$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Parse$(ObjectSuffix) -MF$(IntermediateDirectory)/Parse$(ObjectSuffix).d -MM Parse.cpp

$(IntermediateDirectory)/RandomNumber$(ObjectSuffix): RandomNumber.cpp $(IntermediateDirectory)/RandomNumber$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)RandomNumber.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/RandomNumber$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/RandomNumber$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/RandomNumber$(ObjectSuffix) -MF$(IntermediateDirectory)/RandomNumber$(ObjectSuffix).d -MM RandomNumber.cpp

$(IntermediateDirectory)/ResolvServer$(ObjectSuffix): ResolvServer.cpp $(IntermediateDirectory)/ResolvServer$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)ResolvServer.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ResolvServer$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ResolvServer$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ResolvServer$(ObjectSuffix) -MF$(IntermediateDirectory)/ResolvServer$(ObjectSuffix).d -MM ResolvServer.cpp

$(IntermediateDirectory)/ResolvSocket$(ObjectSuffix): ResolvSocket.cpp $(IntermediateDirectory)/ResolvSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)ResolvSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ResolvSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ResolvSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ResolvSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/ResolvSocket$(ObjectSuffix).d -MM ResolvSocket.cpp

$(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix): SmtpdSocket.cpp $(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SmtpdSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix).d -MM SmtpdSocket.cpp

$(IntermediateDirectory)/Socket$(ObjectSuffix): Socket.cpp $(IntermediateDirectory)/Socket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Socket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Socket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Socket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Socket$(ObjectSuffix) -MF$(IntermediateDirectory)/Socket$(ObjectSuffix).d -MM Socket.cpp

$(IntermediateDirectory)/SocketHandler$(ObjectSuffix): SocketHandler.cpp $(IntermediateDirectory)/SocketHandler$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SocketHandler.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SocketHandler$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SocketHandler$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SocketHandler$(ObjectSuffix) -MF$(IntermediateDirectory)/SocketHandler$(ObjectSuffix).d -MM SocketHandler.cpp

$(IntermediateDirectory)/socket_include$(ObjectSuffix): socket_include.cpp $(IntermediateDirectory)/socket_include$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)socket_include.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/socket_include$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/socket_include$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/socket_include$(ObjectSuffix) -MF$(IntermediateDirectory)/socket_include$(ObjectSuffix).d -MM socket_include.cpp

$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix): SSLInitializer.cpp $(IntermediateDirectory)/SSLInitializer$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)SSLInitializer.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix) -MF$(IntermediateDirectory)/SSLInitializer$(ObjectSuffix).d -MM SSLInitializer.cpp

$(IntermediateDirectory)/StdoutLog$(ObjectSuffix): StdoutLog.cpp $(IntermediateDirectory)/StdoutLog$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)StdoutLog.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/StdoutLog$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/StdoutLog$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/StdoutLog$(ObjectSuffix) -MF$(IntermediateDirectory)/StdoutLog$(ObjectSuffix).d -MM StdoutLog.cpp

$(IntermediateDirectory)/StreamSocket$(ObjectSuffix): StreamSocket.cpp $(IntermediateDirectory)/StreamSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)StreamSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/StreamSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/StreamSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/StreamSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/StreamSocket$(ObjectSuffix).d -MM StreamSocket.cpp

$(IntermediateDirectory)/TcpSocket$(ObjectSuffix): TcpSocket.cpp $(IntermediateDirectory)/TcpSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)TcpSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/TcpSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TcpSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/TcpSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/TcpSocket$(ObjectSuffix).d -MM TcpSocket.cpp

$(IntermediateDirectory)/Thread$(ObjectSuffix): Thread.cpp $(IntermediateDirectory)/Thread$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Thread.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Thread$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Thread$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Thread$(ObjectSuffix) -MF$(IntermediateDirectory)/Thread$(ObjectSuffix).d -MM Thread.cpp

$(IntermediateDirectory)/UdpSocket$(ObjectSuffix): UdpSocket.cpp $(IntermediateDirectory)/UdpSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)UdpSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/UdpSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/UdpSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/UdpSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/UdpSocket$(ObjectSuffix).d -MM UdpSocket.cpp

$(IntermediateDirectory)/Utility$(ObjectSuffix): Utility.cpp $(IntermediateDirectory)/Utility$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Utility.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Utility$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Utility$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Utility$(ObjectSuffix) -MF$(IntermediateDirectory)/Utility$(ObjectSuffix).d -MM Utility.cpp

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/Base64$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Base64$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Debug$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Debug$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Event$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Event$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EventHandler$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EventHandler$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EventTime$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EventTime$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/File$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/File$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/IEventOwner$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/IEventOwner$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Ipv4Address$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Ipv4Address$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Ipv6Address$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Ipv6Address$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Lock$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Lock$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/MemFile$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/MemFile$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Mutex$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Mutex$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Parse$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Parse$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/RandomNumber$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/RandomNumber$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ResolvServer$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ResolvServer$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ResolvSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ResolvSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SmtpdSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Socket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Socket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SocketHandler$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SocketHandler$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/socket_include$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/socket_include$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SSLInitializer$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SSLInitializer$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/StdoutLog$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/StdoutLog$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/StreamSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/StreamSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/TcpSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/TcpSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Thread$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Thread$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/UdpSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/UdpSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Utility$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Utility$(ObjectSuffix).d
	$(RM) $(OutputFile)

-include $(IntermediateDirectory)/*.d

