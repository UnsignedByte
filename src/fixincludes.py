#!/usr/bin/env python

def doSortIncludes(name):
	print('Processing ' + name)

	try:
		includesfound = False

		precontent = []
		gincludes = []
		includes = []
		postcontent = []

		# Open the file and read it
		file = open(name)
		lines = file.readlines()

		# Parse the content, seperating between includes and 'the rest'
		for line in lines:
			if line.startswith('//NOSORT'):
				return

			if line.startswith('#include <'):
				gincludes.append(line)
				includesfound = True
			elif line.startswith('#include'):
				includes.append(line)
				includesfound = True
			else:
				if not includesfound:
					precontent.append(line)
				else:
					postcontent.append(line)
	finally:
		file.close()

	# If there are no includes, don't touch the file
	if len(includes) == 0 and len(gincludes) == 0:
		return

	if len(gincludes) > 0:
		gincludes.sort()
		gincludes.append("\n")

	if len(includes) > 0:
		includes.sort()
		includes.append("\n")

	precontentclean = []
	cleaning = True

	precontent.reverse()

	# Clean up any trailing spaces before the includes
	for line in precontent:
		if cleaning and (len(line) == 0 or line.expandtabs().isspace() ):
			continue

		precontentclean.append(line)
		cleaning = False
		
	# Reverse the content again
	precontentclean.reverse()
	precontentclean.append("\n")

	postcontentclean = []
	cleaning = True

	# Clean up any leading spaces after the includes
	for line in postcontent:
		if cleaning and (len(line) == 0 or line.expandtabs().isspace() ):
			continue

		postcontentclean.append(line)
		cleaning = False

	# Assemble the resulting file
	result = precontentclean + gincludes + includes + postcontentclean

	# Write the new and improved file
	try:
		fixfile = open(name, 'w')
		fixfile.writelines(result)
	finally:
		fixfile.close()



if __name__ == "__main__":
	import sys
	
	filenames = sys.argv

	if len(filenames) == 1:
		import glob
		print("no args, using all *.cpp and *.h files")
		
		filenames = filenames[0:1] + glob.glob('*.cpp') + glob.glob('*.h')
		

	for filename in filenames[1:]:
	#	doRemoveWhitespace(filename)
		doSortIncludes(filename)
