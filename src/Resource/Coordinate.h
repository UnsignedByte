/***************************************************************************
 *   Copyright (C) 2007 by Daniel Brody                                    *
 *   erasnode@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file Coordinate.h
 * This file contains the Coordinate class. 
 *
 * @see Coordinate 
 */ 

#include "Types.h"

/**
 * This file is an 'enumeration' class with all the directions.
 */ 
class Coordinate
{
public:
	static Coordinate NORTH; /**< The 'north' coordinate. */
	static Coordinate NUP; /**< The 'north up' coordinate. */
	static Coordinate NDOWN; /**< The 'north down' coordinate. */

	static Coordinate SOUTH; /**< The 'south' coordinate. */
	static Coordinate SUP; /**< The 'south up' coordinate. */
	static Coordinate SDOWN; /**< The 'south down' coordinate. */

	static Coordinate EAST; /**< The 'east' coordinate. */
	static Coordinate EUP; /**< The 'east up' coordinate. */
	static Coordinate EDOWN; /**< The 'east down' coordinate. */

	static Coordinate WEST; /**< The 'west' coordinate. */
	static Coordinate WUP; /**< The 'west up' coordinate. */
	static Coordinate WDOWN; /**< The 'west down' coordinate. */

	static Coordinate NW; /**< The 'northwest' coordinate. */
	static Coordinate NWUP; /**< The 'northwest up' coordinate. */
	static Coordinate NWDOWN; /**< The 'northwest down' coordinate. */

	static Coordinate NE; /**< The 'northeast' coordinate. */
	static Coordinate NEUP; /**< The 'northeast up' coordinate. */
	static Coordinate NEDOWN; /**< The 'northeast down' coordinate. */

	static Coordinate SW; /**< The 'southwest' coordinate. */
	static Coordinate SWUP; /**< The 'southwest up' coordinate. */
	static Coordinate SWDOWN; /**< The 'southwest down' coordinate. */

	static Coordinate SE; /**< The 'southeast' coordinate. */
	static Coordinate SEUP; /**< The 'southeast up' coordinate. */
	static Coordinate SEDOWN; /**< The 'southeast down' coordinate. */

	static Coordinate UP; /**< The 'up' coordinate. */
	static Coordinate DOWN; /**< The 'down' coordinate. */

	/**
	 * Specialized constructor
	 * 
	 * @param X initial X coordinate
	 * @param Y initial Y coordinate
	 * @param Z initial Z coordinate
	 */
	Coordinate(signed long X, signed long Y, signed long Z);
	
	/**
	 * Copy Constructor
	 * 
	 * @param rhs Copy from
	 */
	Coordinate(const Coordinate& rhs);
	
	/**
	 * Default destructor
	 */
	~Coordinate();
	
	/**
	 * Overloaded addition operator
	 * 
	 * @param rhs Coordinate to be added
	 * @return this+rhs
	 */
	Coordinate operator+= (const Coordinate& rhs);
	
	/**
	 * Overloaded addition operator
	 * 
	 * @param rhs Coordinate to be added
	 * @return this+rhs
	 */
	Coordinate operator+ (const Coordinate & rhs) const;

	/**
	 * Overloaded substraction operator
	 * 
	 * @param rhs Coordinate to be substracted
	 * @return this-rhs
	 */
	Coordinate operator-= (const Coordinate & rhs);

	/**
	 * Overloaded substraction operator
	 * 
	 * @param rhs Coordinate to be substracted
	 * @return this-rhs
	 */
	Coordinate operator- (const Coordinate & rhs) const;
	
	/**
	 * Returns the complement of this direction.
	 */ 
	Coordinate getComplement() const;

	/**
	 * Getter
	 * 
	 * @return X coordinate
	 */
	signed long getXCoordinate() const;
	
	/**
	 * Getter
	 * 
	 * @return Y Coordinate
	 */
	signed long getYCoordinate() const;
	
	/**
	 * Getter
	 * 
	 * @return Z coordinate
	 */
	signed long getZCoordinate() const;
	
	
	/**
	 * Returns whether this coordinate represents a direction (abs(x,y,z) <= 1).
	 * 
	 * @return Whether this coordinate satisfies "-1 <= i <= 1" for i={x,y,z}.
	 */
	bool isDirection() const;
	
	/** Whether all components are 0. */
	bool isNullVector() const;
	
	/** Returns a string representation of this coordinate, */
	std::string toString() const;
	
	/** Returns the direction name of this coordinate if it is one. */
	std::string toDirectionString() const;
	   
private:
	/**
	 * Hide default constructor
	 */
	Coordinate() {};

	signed long m_xcoordinate;
	signed long m_ycoordinate;
	signed long m_zcoordinate;
};

typedef std::vector<Coordinate> Coordinates; /**< The type of multiple Coordinates. */
