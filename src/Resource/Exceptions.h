/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file Exceptions.h 
 * This file contains the custom exceptions used.
 *
 * @see ExcecutionException 
 * @see SavableLocked 
 * @see RowNotFoundException 
 * @see SqliteError 
 */ 

#include "Types.h"

/**
 * This exception is thrown when an exception occured as the result of a user command.
 */ 
class ExcecutionException : public std::runtime_error
{
	public:
		/** Constructs a new ExcecutionException. */
		ExcecutionException(std::exception& e, value_type accountid, cstring line);
		
		~ExcecutionException() throw () { }
		
		const char* what() const throw() { return m_msg.c_str(); }
		
	private:
		std::exception& m_exception;
		std::string m_msg;
		value_type m_accountid;
		std::string m_line;
};

/**
 * This exception is thrown when a savable was already locked while trying to perform an operation that needs the ock.
 */ 
class SavableLocked : public std::runtime_error
{
	public:
		/** Construct a new SavableLocked exception. */
		SavableLocked() : std::runtime_error("Savable was already locked.") { }
};

/**
 * This exception is thrown when no row was found as the result of an SQL query.
 */ 
class RowNotFoundException : public std::runtime_error
{
	public:
		/** Construct a new RowNotFoundException with the specified message. */
		RowNotFoundException(const std::string& arg) : std::runtime_error(arg) { }
};

/**
 * This exception is thrown when an error occured as the result of an SQL query.
 */ 
class SqliteError : public std::runtime_error
{
	public:
		/** Construct a new SqliteError from the specified database. */
		SqliteError(sqlite3*);
		
		/** Construct a new SqliteError from the specified database. */
		SqliteError(sqlite3*, const std::string& sql);
		
		/** Destructor, a noop. */
		~SqliteError() throw() { }
		
		const char* what() const throw();

	private:
		int m_sqliteErrno; /**< The number of the error. */
		const char* m_sqliteError; /**< The message of the error. */
		std::string m_msg; /**< The compiled message. */
};
