/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <algorithm>
#include <cctype>
#include <math.h>
#include <sstream>

#include "Parse.h"
#include "StringUtilities.h"

Strings String::lines(const std::string& input, const char* separator)
{
	Assert(separator);
	
	Parse p(input, separator, 1);
	Strings result;

	while(true)
	{
		std::string line = p.getword();
		if(line.size())
			result.push_back(line);
		else
			break;
	}

	return result;
}

std::string String::unlines(const Strings& input, const char* filler, int extra)
{
	Assert(filler);
	
	std::string result;
	int i = 0;
	
	for(Strings::const_iterator it = input.begin(); it != input.end(); it++)
	{
		if(i != 0)
		{
			if(extra && i%extra == 0)
				result += "\n";
			else
				result += filler;
		}
		

		result += (*it);
		i++;
	}

	return result;
}

Strings String::unlines(const Strings& input)
{
	Strings result;
	
	for(Strings::const_iterator it = input.begin(); it != input.end(); it++)
	{
		Parse p(*it, "\n", 1);
		while(true)
		{
			std::string line = p.getword();
			if(line.size())
				result.push_back(line);
			else
				break;
		}
	}
	
	return result;
}

size_t String::maxlength(const Strings& input)
{
	size_t champ = 0;
	for(Strings::const_iterator it = input.begin(); it != input.end(); it++)
	{
		size_t current = it->size();
		if(current > champ)
			champ = current;
	}
	return champ;
}

std::string String::box(const Strings& unparsedcontent, const std::string& header)
{
	bool useheader = (header != Global::Get()->EmptyString);
	Strings content = unlines(unparsedcontent);

	size_t length = maxlength(content);
	if(useheader && header.size() > length)
		length = header.size();
	
	length += 4;

	std::string result;	
	std::string linefill;
	std::string spacefill;
	std::string fill;

	linefill.assign(length, '_');
	spacefill.assign(length, ' ');
	fill.assign( (length-header.size())/2 + 2,' ');

	std::string title;
	if(useheader)
		title = (Global::Get()->sprintf("%s%s%s\n", fill.c_str(), header.c_str(), fill.c_str()));

	std::string toplet(Global::Get()->sprintf("__%s__\n", linefill.c_str()));
	std::string top(Global::Get()->sprintf("|\\%s/|\n", linefill.c_str()));
	std::string body(Global::Get()->sprintf("||%s||\n", spacefill.c_str()));
	std::string footlet(Global::Get()->sprintf("||%s||\n", linefill.c_str()));
	std::string footer(Global::Get()->sprintf("|/%s\\|\n", linefill.c_str()));

	if(useheader)
		result.append(title);
	result.append(toplet);
	result.append(top);
	result.append(body);
	for(Strings::const_iterator it = content.begin(); it != content.end(); it++)
	{		
		if(!it->size())
			continue;
			
		result.append(Global::Get()->sprintf("||  %s%.*s||\n",it->c_str(), length - it->size() - 2, spacefill.c_str()));
	}
	result.append(footlet);
	result.append(footer);

	return result;
}

std::string String::toupper(const std::string& input)
{
	std::string convertToUpper = input;
	std::transform(convertToUpper.begin(), convertToUpper.end(), convertToUpper.begin(), (int(*)(int)) std::toupper);
	return convertToUpper ;
}

std::string String::tolower(const std::string& input)
{
	std::string convertToUpper = input;
	std::transform(convertToUpper.begin(), convertToUpper.end(), convertToUpper.begin(), (int(*)(int)) std::tolower);
	return convertToUpper ;
}

std::string String::countWord(value_type value)
{
	std::string result = fromInt(value);
	
	switch(value)
	{
		case 1:
			result.append("st");
			break;
		
		case 2:
			result.append("nd");
			break;
		
		case 3:
			result.append("rd");
			break;
			
		default:
			result.append("th");
			break;
	}
	
	return result;
}

std::string String::fromInt(value_type value)
{
	 std::ostringstream o;
     if(!(o << value)) 
		return "String::fromInt(), ERROR!";
        
	return o.str();
}

std::string String::fromLong(signed long value)
{
	 std::ostringstream o;
     if(!(o << value)) 
		return "String::fromLong(), ERROR!";
        
	return o.str();
}

Strings String::createTable(const std::vector<Strings>& content, const Strings& header)
{
	Strings result;
	
	const size_t maxlinesize = 80;
	const size_t fieldcount = header.size();	
	
	std::vector<int> fieldsizes = calculateMinimumFieldSizes(content, fieldcount);
	rebaseMinimumFieldSizes(fieldsizes, header);
	capFieldSize(fieldsizes, maxlinesize);
	
	std::string theheader = packFields(header, fieldsizes);
	
	result.push_back(theheader);
	
	for(std::vector<Strings>::const_iterator it = content.begin(); it != content.end(); it++)
	{
		Strings fields = *it;
		Assert(fields.size() == fieldcount);
		
		std::string line = packFields(fields, fieldsizes);
		result.push_back(line);
	}
	
	return result;
}

std::vector<int> String::calculateMinimumFieldSizes(const std::vector<Strings>& content, int fieldcount)
{
	std::vector<int> result(fieldcount);
		
	for(std::vector<Strings>::const_iterator it = content.begin(); it != content.end(); it++)
	{
		Strings line = *it;
		int i = 0;
		for(Strings::const_iterator itl = line.begin(); itl != line.end(); itl++)
		{
			std::string field = *itl;
			int fieldsize = field.size();
			fieldsize++; // reserve space for column separator
			
			if(fieldsize > result[i])
				result[i] = fieldsize;
				
			i++;
		}		
	}
	
	return result;
}

void String::rebaseMinimumFieldSizes(std::vector<int>& sizes, const Strings& header)
{
	int i = 0;
	for(Strings::const_iterator it = header.begin(); it != header.end(); it++)
	{
		std::string field = *it;
		int fieldsize = field.size();
		
		if(fieldsize > sizes[i])
			sizes[i] = fieldsize;		
		
		i++;
	}
}

void String::capFieldSize(std::vector<int>& sizes, const int maxlinesize)
{
	int totalsize = 0;
	int maxfairsize = maxlinesize / sizes.size();
	int unfairfields = 0;
	int slack = 0;
	
	for(size_t i = 0; i < sizes.size(); i++)
	{
		int size = sizes[i];
		totalsize += size;	
		if(size > maxfairsize)
			unfairfields++;
			
		if(size < maxfairsize)
			slack += maxfairsize - size;
	}
	
	if(totalsize > maxlinesize)
		capUnfairFields(sizes, unfairfields, maxfairsize, slack);
}

void String::capUnfairFields(std::vector<int>& sizes, int unfairFields, int maxFairSize, int slack)
{
	Assert(unfairFields > 0);	
		
	for(size_t i = 0; i < sizes.size(); i++)
	{		
		if(sizes[i] > maxFairSize)
		{	
			Assert(unfairFields > 0);
			Assert(slack >= 0);
			
			int size = sizes[i];
			int toomuch = size - maxFairSize;
			int myslack = slack / unfairFields;
			if(myslack > toomuch)
				myslack = toomuch;
				
			sizes[i] = maxFairSize + myslack;
			slack -= myslack;
			unfairFields--;								
		}
	}
}

std::string String::packFields(const Strings& fields, const std::vector<int>& fieldsizes)
{
	std::string result;
	
	int i = 0;
	for(Strings::const_iterator it = fields.begin(); it != fields.end(); it++)
	{
		result.append("|");			
		
		std::string field = *it;
		int fieldsize = field.size();
		if(fieldsize < fieldsizes[i])
		{
			size_t padding = fieldsizes[i] - fieldsize;
			std::string suffix(padding, ' ');
			result.append(field);
			result.append(suffix);
		}
		else if(fieldsize == fieldsizes[i])
			result.append(field);		
		else
			result.append(field.substr(0, fieldsizes[i]));		
		
		i++;
	}
	
	result.append("|");
	
	return result;
}

/**
	7 | 1  | 4  | 6  | 7 |  9  | 12 | 24 = 70
	0 | -5 | -3 | -1 | 0 |  3  |  5 | 17 = -14
	7 | 1  | 4  | 6  | 7 |  7  |  7 |  7 = +10
 */	

/**
	9  | 12 | 24 = 70
	3  |  5 | 17 = -14
	9  | 11 | 11 = +0/0
 */
