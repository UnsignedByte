/***************************************************************************
 *   Copyright (C) 2007 by Daniel Brody                                    *
 *   erasnode@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Coordinate.h"
#include "StringUtilities.h"

Coordinate Coordinate::NORTH	(0, 1, 0);
Coordinate Coordinate::NUP		(0, 1, 1);
Coordinate Coordinate::NDOWN	(0, 1, -1);

Coordinate Coordinate::SOUTH	(0, -1, 0);
Coordinate Coordinate::SUP		(0, -1, 1);
Coordinate Coordinate::SDOWN	(0, -1, -1);

Coordinate Coordinate::EAST		(1, 0, 0);
Coordinate Coordinate::EUP		(1, 0, 1);
Coordinate Coordinate::EDOWN	(1, 0, -1);

Coordinate Coordinate::WEST		(-1, 0, 0);
Coordinate Coordinate::WUP		(-1, 0, 1);
Coordinate Coordinate::WDOWN	(-1, 0, -1);

Coordinate Coordinate::NW		(-1, 1, 0);
Coordinate Coordinate::NWUP		(-1, 1, 1);
Coordinate Coordinate::NWDOWN	(-1, 1, -1);

Coordinate Coordinate::NE		(1, 1, 0);
Coordinate Coordinate::NEUP		(1, 1, 1);
Coordinate Coordinate::NEDOWN	(1, 1, -1);

Coordinate Coordinate::SW		(-1, -1, 0);
Coordinate Coordinate::SWUP		(-1, -1, 1);
Coordinate Coordinate::SWDOWN	(-1, -1, -1);

Coordinate Coordinate::SE		(1, -1, 0);
Coordinate Coordinate::SEUP		(1, -1, 1);
Coordinate Coordinate::SEDOWN	(1, -1, -1);

Coordinate Coordinate::UP		(0, 0, 1);
Coordinate Coordinate::DOWN		(0, 0, -1);

Coordinate::Coordinate(signed long X, signed long Y, signed long Z) : 
m_xcoordinate(X), 
m_ycoordinate(Y), 
m_zcoordinate(Z)
{
	  
}

Coordinate::~Coordinate()
{
	
}

Coordinate::Coordinate(const Coordinate& rhs) : 
m_xcoordinate(rhs.getXCoordinate()), 
m_ycoordinate(rhs.getYCoordinate()), 
m_zcoordinate(rhs.getZCoordinate())
{
	  
}

Coordinate Coordinate::operator+= (const Coordinate & rhs)
{
	m_xcoordinate = getXCoordinate()+rhs.getXCoordinate();
	m_ycoordinate = getYCoordinate()+rhs.getYCoordinate();
	m_zcoordinate = getZCoordinate()+rhs.getZCoordinate();
						
	return *this;
}

Coordinate Coordinate::operator+ (const Coordinate & rhs) const
{
	return Coordinate(	getXCoordinate()+rhs.getXCoordinate(), 
						getYCoordinate()+rhs.getYCoordinate(), 
						getZCoordinate()+rhs.getZCoordinate()	);
}

Coordinate Coordinate::operator-= (const Coordinate & rhs)
{
	m_xcoordinate = getXCoordinate()-rhs.getXCoordinate();
	m_ycoordinate = getYCoordinate()-rhs.getYCoordinate();
	m_zcoordinate = getZCoordinate()-rhs.getZCoordinate();
						
	return *this;
}

Coordinate Coordinate::operator- (const Coordinate & rhs) const
{
	return Coordinate(	getXCoordinate()-rhs.getXCoordinate(), 
						getYCoordinate()-rhs.getYCoordinate(), 
						getZCoordinate()-rhs.getZCoordinate()	);
}

signed long Coordinate::getXCoordinate() const
{
	return m_xcoordinate;
}
	
signed long Coordinate::getYCoordinate() const
{
	return m_ycoordinate;
}
	
signed long Coordinate::getZCoordinate() const
{
	return m_zcoordinate;
}

bool Coordinate::isDirection() const
{
	if(abs(m_xcoordinate) > 1) return false;
	if(abs(m_ycoordinate) > 1) return false;
	if(abs(m_zcoordinate) > 1) return false;
	
	if(isNullVector()) return false;
	
	return true;
}
std::string Coordinate::toString() const
{
	std::string result;
	
	result.append("x: ");
	result.append(String::Get()->fromLong(getXCoordinate()));
	
	result.append(", y: ");
	result.append(String::Get()->fromLong(getYCoordinate()));
	
	
	result.append(", z: ");
	result.append(String::Get()->fromLong(getZCoordinate()));
	
	return result;
}

std::string Coordinate::toDirectionString() const
{
	Assert(isDirection());
	
	std::string result;
	
	int x = getXCoordinate();
	int y = getYCoordinate();
	int z = getZCoordinate();
		
	if(x > 0)
		result.append("north");
	if(x < 0)
		result.append("south");
	
	if(y > 0)		
		result.append("east");
	if(y < 0)
		result.append("west");	
	
	if(z > 0)
		result.append("up");
	if(z < 0)
		result.append("down");
		
	return result;
}

bool Coordinate::isNullVector() const
{
	return (!m_xcoordinate && !m_ycoordinate && !m_zcoordinate);
}

Coordinate Coordinate::getComplement() const
{
	Assert(isDirection());
	
	int x = 0 - getXCoordinate();
	int y = 0 - getYCoordinate();
	int z = 0 - getZCoordinate();

	Coordinate complement(x, y, z);
	Assert(complement.isDirection());
	
	return complement;
}
