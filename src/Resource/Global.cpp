/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdarg.h>

#include "Global.h"

bool g_shutdown = false;
bool g_nocatch = false;
bool g_printsql = false;
bool g_printbugs = true;
bool g_printstatus = true;
bool g_printlogs = false;

Global::Global() :
EmptyString(),
OOCIdentifier('@'),
SocialIdentifier('*')
{
	
}

Global::~Global()
{

}

std::string Global::sprintf(const char* format, ...)
{
	Assert(format);
	
	va_list args;
	va_start(args, format);
	vsnprintf(m_workspace, MAXSIZE, format, args);
	va_end(args);

	return m_workspace;
}

std::string Global::sprint(va_list& args, const char* format)
{
	Assert(format);
	
	vsnprintf(m_workspace, MAXSIZE, format, args);
	return m_workspace;
}

void Global::printraw(const std::string& msg)
{
	printf(msg.c_str());
}

void Global::printbug(const std::string& msg)
{
	if(!g_printbugs)
		return;
	
	printraw("[BUG   ] ");
	printraw(msg);
}

void Global::printlog(const std::string& msg)
{	
	if(!g_printlogs)
		return;
	
	printraw("[LOG   ] ");
	printraw(msg);
}

void Global::printsql(const std::string& msg)
{
	if(!g_printsql)
		return;
	
	printraw("[SQL   ] ");
	printraw(msg);
	printraw("\n");
}

void Global::printexception(const std::string& msg)
{
	printraw("[ERROR ] ");
	printraw(msg);
}

void Global::printexception(const std::string& msg, const std::string& what)
{
	printexception(msg);
	
	printraw("[   MSG] ");
	printraw(what);
}

void Global::printstatus(const std::string& msg)
{
	if(!g_printstatus)
		return;
	
	printraw("[STATUS] ");
	printraw(msg);
}

void Global::printmode(const std::string& msg)
{
	printraw("[MODE  ] ");
	printraw(msg);
}
