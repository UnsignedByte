/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/** 
 * @file GameVersion.h
 * This file contains the game's name and version info. 
 *
 * @see game 
 */

namespace game
{
	const char vname[] = "hp"; /**< The game's name. */
	const char vstring[] = "1.0.0"; /**<  A string representation of the game's version. */
	const value_type major = 1; /**< The major part of the game version. */
	const value_type minor = 0; /**< The minor part of the game version. */
	const value_type micro = 0; /**< The micro part of the game version. */
	
	const char ustring[] = "8.3"; /**< The ubuntu style string representation of the game's version. */
	const value_type year = 8; /**< The year this build was made. */
	const value_type month = 3; /**< The month this build was made. */
};
