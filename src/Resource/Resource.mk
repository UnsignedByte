##
## Auto Generated makefile, please do not edit
##
ProjectName:=Resource

## Debug
ifeq ($(type),Debug)
ConfigurationName :=Debug
IntermediateDirectory :=./Debug
OutDir := $(IntermediateDirectory)
LinkerName:=g++
ArchiveTool :=ar rcu
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix :=.o
DebugSwitch :=-gstab
IncludeSwitch :=-I
LibrarySwitch :=-l
OutputSwitch :=-o 
LibraryPathSwitch :=-L
PreprocessorSwitch :=-D
SourceSwitch :=-c 
CompilerName :=g++
OutputFile :=../../lib/libubresource.a
Preprocessors :=
ObjectSwitch :=-o 
ArchiveOutputSwitch := 
CmpOptions :=-g -Wall $(Preprocessors)
LinkOptions := -O0
IncludePath :=  $(IncludeSwitch). $(IncludeSwitch)../Sockets 
RcIncludePath :=
Libs :=
LibPath := $(LibraryPathSwitch). 
endif

Objects=$(IntermediateDirectory)/Assert$(ObjectSuffix) $(IntermediateDirectory)/Coordinate$(ObjectSuffix) $(IntermediateDirectory)/Global$(ObjectSuffix) $(IntermediateDirectory)/Path$(ObjectSuffix) $(IntermediateDirectory)/sha2$(ObjectSuffix) $(IntermediateDirectory)/StringUtilities$(ObjectSuffix) $(IntermediateDirectory)/Exceptions$(ObjectSuffix) 

##
## Main Build Tragets 
##
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile):  $(Objects)
	$(ArchiveTool) $(ArchiveOutputSwitch)$(OutputFile) $(Objects)

./Debug:
	@test -d ./Debug || mkdir ./Debug


PreBuild:


##
## Objects
##
$(IntermediateDirectory)/Assert$(ObjectSuffix): Assert.cpp $(IntermediateDirectory)/Assert$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Assert.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Assert$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Assert$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Assert$(ObjectSuffix) -MF$(IntermediateDirectory)/Assert$(ObjectSuffix).d -MM Assert.cpp

$(IntermediateDirectory)/Coordinate$(ObjectSuffix): Coordinate.cpp $(IntermediateDirectory)/Coordinate$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Coordinate.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Coordinate$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Coordinate$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Coordinate$(ObjectSuffix) -MF$(IntermediateDirectory)/Coordinate$(ObjectSuffix).d -MM Coordinate.cpp

$(IntermediateDirectory)/Global$(ObjectSuffix): Global.cpp $(IntermediateDirectory)/Global$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Global.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Global$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Global$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Global$(ObjectSuffix) -MF$(IntermediateDirectory)/Global$(ObjectSuffix).d -MM Global.cpp

$(IntermediateDirectory)/Path$(ObjectSuffix): Path.cpp $(IntermediateDirectory)/Path$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Path.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Path$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Path$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Path$(ObjectSuffix) -MF$(IntermediateDirectory)/Path$(ObjectSuffix).d -MM Path.cpp

$(IntermediateDirectory)/sha2$(ObjectSuffix): sha2.c $(IntermediateDirectory)/sha2$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)sha2.c $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/sha2$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/sha2$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/sha2$(ObjectSuffix) -MF$(IntermediateDirectory)/sha2$(ObjectSuffix).d -MM sha2.c

$(IntermediateDirectory)/StringUtilities$(ObjectSuffix): StringUtilities.cpp $(IntermediateDirectory)/StringUtilities$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)StringUtilities.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/StringUtilities$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/StringUtilities$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/StringUtilities$(ObjectSuffix) -MF$(IntermediateDirectory)/StringUtilities$(ObjectSuffix).d -MM StringUtilities.cpp

$(IntermediateDirectory)/Exceptions$(ObjectSuffix): Exceptions.cpp $(IntermediateDirectory)/Exceptions$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Exceptions.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Exceptions$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Exceptions$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Exceptions$(ObjectSuffix) -MF$(IntermediateDirectory)/Exceptions$(ObjectSuffix).d -MM Exceptions.cpp

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/Assert$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Assert$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Coordinate$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Coordinate$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Global$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Global$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Path$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Path$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/sha2$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/sha2$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/StringUtilities$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/StringUtilities$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Exceptions$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Exceptions$(ObjectSuffix).d
	$(RM) $(OutputFile)

-include $(IntermediateDirectory)/*.d

