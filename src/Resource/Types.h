/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file Types.h
 * This file contains all type definitions. 
 */ 

//NOSORT

#include <string>
#include <vector>
#include <map>
#include <stdexcept>
#include <set>

#include <sqlite3.h>

/** The type used to store database keys. */
typedef unsigned long value_type;

/** The type to store multiple database keys with. */
typedef std::set<value_type> value_types;

/** The type of a constant string reference. */
typedef const std::string& cstring;

/** The type of multiple non-const strings. */
typedef std::vector<std::string> Strings;

#include "smart_ptr.h"
#include "singleton.h"
#include "Exceptions.h"
#include "Assert.h"

class Table;
typedef SmartPtr<Table> TablePtr; /**< The type of a pointer to a table. */
typedef std::vector<TablePtr> TableVector; /**< The type of multiple table pointers. */

class TableDef;
typedef SmartPtr<TableDef> TableDefPtr; /**< The type of a pointer to a table def. */
typedef std::vector<TableDefPtr> TableDefVector; /**< The type of multiple table def pointers. */

class TableImpl;
typedef SmartPtr<TableImpl> TableImplPtr; /**< The type of a pointer to a table impl. */
typedef std::vector<TableImplPtr> TableImplVector; /**< The type of multiple table impl pointers. */

class KeyDef;
typedef SmartPtr<KeyDef> KeyDefPtr; /**< The type of a pointer to a key def. */
typedef std::vector<KeyDefPtr> KeyDefs; /**< The type of multiple key def pointers. */

class KeyImpl;
typedef SmartPtr<KeyImpl> KeyImplPtr; /**< The type of a pointer to a key impl. */
typedef std::vector<KeyImplPtr> KeyImpls; /**< The type of multiple key impl pointers. */

class KeyValue;
typedef SmartPtr<KeyValue> KeyValuePtr; /**< The type of a pointer to a key value. */

class Field;
typedef SmartPtr<Field> FieldPtr; /**< The type of a pointer to a field. */
typedef std::vector<FieldPtr> FieldVector; /**< The type multiple field pointers. */

class FieldDef;
typedef SmartPtr<FieldDef> FieldDefPtr; /**< The type of a pointer to a field def. */
typedef std::vector<FieldDefPtr> FieldDefVector; /**< The type of multiple field def pointers. */

class FieldImpl;
typedef SmartPtr<FieldImpl> FieldImplPtr; /**< The type of a pointer to a field impl. */
typedef std::vector<FieldImplPtr> FieldImplVector; /**< The type of multiple field impl pointers. */

class FieldValue;
typedef SmartPtr<FieldValue> ValuePtr; /**< The type of a pointer to a value. */
typedef SmartPtr<FieldValue> FieldValuePtr; /**< A more descriptive name of of the type of a pointer to a value. */


/**
 * The type of a map from foreign key name to Table.
 * 
 * @deprecated Use KeyDefs instead. 
 * @see KeyDefs
 */ 
typedef std::map<std::string, TablePtr> TableMap;

/** The type of a map from KeyImpl pointers to KeyValue pointers. */ 
typedef std::map<KeyImpl*, KeyValuePtr> KeyImplMap;

/** The type of a map from FieldImpl pointers to Value pointers. */ 
typedef std::map<FieldImpl*, ValuePtr> FieldValueMap;

/** The type of a map from FieldImpl poinntiers to Value pointers. */
typedef std::map<FieldImpl*, ValuePtr> ValueMap;


class Keys;
typedef SmartPtr<Keys> KeysPtr; /**< The type of a pointer to a keys bucket. */

class FieldValues;
typedef SmartPtr<FieldValues> ValuesPtr; /**< The type of a pointer to a values bucket. */
typedef SmartPtr<FieldValues> FieldValuesPtr; /**< A more precise name for the type of a pointer to a values bucket. */

class SavableManagers;
typedef SmartPtr<SavableManagers> SavableManagersPtr; /**< The type of a pointer to a savable managers bucket. */


class SavableManager;
typedef SmartPtr<SavableManager> SavableManagerPtr; /**< The type of a pointer to a savable manager. */
typedef std::vector<SavableManagerPtr> SavableManagerVector; /**< The type of multiple savable manager pointers. */

class SelectionMask;
typedef SmartPtr<SelectionMask> SelectionMaskPtr; /**< The type of a pointer to a selection mask. */

class Relation;
typedef SmartPtr<Relation> RelationPtr; /**< The type of a pointer to a relation. */
typedef std::vector<RelationPtr> Relations; /**< The type of multiple relation pointers. */

class Join;
typedef SmartPtr<Join> JoinPtr; /**< The type of a pointer to a join. */
typedef std::vector<JoinPtr> Joins; /**< The type of multiple join pointers. */

class Actor;
typedef SmartPtr<Actor> ActorPtr; /**< The type of a pointer to an actor. */

class UBSocket;

class Savable;
typedef SmartPtr<Savable> SavablePtr; /**< The type of a pointer to a Savable. */
