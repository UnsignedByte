/***************************************************************************
 *   Copyright (C) 2008 by Vegard Nossum                                   *
 *   vegard.nossum@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file Assert.h
 * This file contains the Assertion exception and macro. 
 *
 * @see Assertion
 * @see Assert 
 */ 

#include <exception>
#include <string>

/**
 * This exception is thrown when an assertion fails.
 */ 
class Assertion : public std::exception
{
	public:
		/**
		 * Constructs an assertion with the specified attributes.
		 *
		 * @param expr The expression of the assertion that failed.
		 * @param file The file in which the assertion failed.
		 * @param line The line in which the assertion failed.
		 * @param func The function in which the assertion failed.
		 * @param pretty_func A 'pretty' name of the function in which the assertion failed. 
		 */ 
		Assertion(const char* expr,
			const char* file, unsigned int line,
			const char* func, const char* pretty_func);
			
		/** Destructor, a noop. */
		~Assertion() throw();

	public:
		/** Override the msg of this exception to describe the assertion failure.*/
		const char *what() const throw();

	private:
		std::string m_msg; /**< The msg that describes the failed assertion. */
};

/** Assert that x is true, if it is not an Assertion exception is thrown. */
#define Assert(x)					\
	if(!(x)) {						\
		throw Assertion("" # x,				\
			__FILE__, __LINE__,			\
			__FUNCTION__, __PRETTY_FUNCTION__);	\
	}
