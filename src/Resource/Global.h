/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Global.h
 * This file contains the Global class. 
 *
 * @see Global 
 */ 

#include <stdarg.h>

#include "Types.h"

/**
 * This class is used to contain otherwise global variables and methods.
 */ 
class Global : public Singleton<Global>
{
public:
	/** The maximum character buffer size. */
	static const int MAXSIZE = (1<<16);
	
	/** The 'empty' string. */
	std::string EmptyString;
	
	/** The character that identifies an OOC command. */
	char OOCIdentifier;
	
	/** The character that identifies emotes. */
	char SocialIdentifier;

	/** Returns the formatted data. */
	std::string sprintf(const char* format, ...);
	
	/** Returns the formatted data. */
	std::string sprint(va_list& args, const char* format);
	
	
	/*
	 * Logging
	 */ 
	
	
	/** Prints the specified raw msg. */
	void printraw(const std::string& msg);
	
	/** Prints the specified mode msg. */
	void printmode(const std::string& msg);

	/** Prints the specified bug msg. */
	void printbug(const std::string& msg);
	
	/** Prints the specified exception msg. */
	void printexception(const std::string& msg);
	
	/** Prints the specified exception msg. */
	void printexception(const std::string& msg, const std::string& what);
	
	/** Prints the specified sql msg. */
	void printsql(const std::string& msg);
	
	/** Prints the specified status msg. */
	void printstatus(const std::string& msg);

	/** Prints the specified log msg. */
	void printlog(const std::string& msg);

private:
	/** This is a singleton class, use <code>Get</code> to retreive the instance. */
	Global(void);
	
	/** This is a singleton class, use <code>Free</code> to free the instance. */
	~Global(void);
	
	friend class Singleton<Global>;

	char m_workspace[MAXSIZE]; /**< The workspace to use when using sprintf/bugf/logf. */
};

