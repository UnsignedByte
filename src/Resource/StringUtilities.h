/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file StringUtilities.h
 * This file contains the String class. 
 * 
 * @see String
 */ 

#include "Global.h"
#include "Types.h"

/**
 * This class provides a set of utility functions that operate on strings.
 */ 
class String : public Singleton<String>
{
public:
	/**
	 * Returns a vector of strings splitting the input at the specified separator.
	 *
	 * @param input The input to search for the separator in.
	 * @param separator The separator to serach for.
	 * @return The input, split into lines at each separator. 
	 */ 
	Strings lines(const std::string& input, const char* separator = "\n");
	
	/**
	 * Returns a string with the input strings concatenated.
	 *
	 * @param input The input to put together. 
	 * @param filler The character to put between characters parts of the input.
	 * @param newlineat After how many lines of the input a newline will be added, when 0 no extra newlines will be added. 
	 */ 
	std::string unlines(const Strings& input, const char* filler, int newlineat = 1);
	
	/**
	 * Convert all '\n' into in the input to new lines.
	 *
	 * @param input The input to search for lines containing '\n'.
	 * @return The output in which lines containing '\n' are split. 
	 */ 
	Strings unlines(const Strings& input);
	
	/** Find the longest line in the input and return it's lenght. */
	size_t maxlength(const Strings& content);
	
	/**
	 * Draw an ASCII art box around the content using the specified header.
	 *
	 * @param content The lines that should be contained inside the box.
	 * @param header The header that will be put above the result (centered), will be left out if none is specified.
	 * @return One string that contains the 'boxed' content. 
	 */ 
	std::string box(const Strings& content, const std::string& header = Global::Get()->EmptyString);
	
	/**
	 * Creates a ASCII table out of the content using the specified header.
	 *
	 * @param content The 'body' of the table, is asserted to be of same 'length' as the header.
	 * @param header The header to use for the table.
	 * @return A set of strings that contain the table, each one row of the table. 
	 */ 
	Strings createTable(const std::vector<Strings>& content, const Strings& header);
	
	/** Returns the 'count word' representation of an integer ('1st', '2nd', '3rd', etc). */
	std::string countWord(value_type value);
	
	/** Returns a string representation of an integer. */
	std::string fromInt(value_type value);
	
	/** Returns a string representation of an integer. */
	std::string fromLong(signed long value);
		
	/** Converts a string to upper case. */
	std::string toupper(const std::string& convertToUpper);
	
	/** Converts a string to lower case. */
	std::string tolower(const std::string& convertToLower);

	/** Calculates the minimum size of each field in the content using the specified fieldcount. */
	std::vector<int> calculateMinimumFieldSizes(const std::vector<Strings>& content, int fieldcount);
	
	/** Adjusts the minimum field sizes to take in account the size of the header. */
	void rebaseMinimumFieldSizes(std::vector<int>& sizes, const Strings& header);
	
	/** Cap the field sizes so that their sum is equal to or less than the specified maximum. */
	void capFieldSize(std::vector<int>& sizes, const int maxlinesize);
	
	/** Cap the fields that are unproporitonally large using the specified attributes. */
	void capUnfairFields(std::vector<int>& sizes, int unfairFields, int maxFairSize, int slack);
	
	/** Pack the specified fields into one string using the specified fieldsizes. */
	std::string packFields(const Strings& fields, const std::vector<int>& fieldsizes);

private:
	/** This is a singleton class, use <code>Get</code> to retreive the instance. */
	String(void) {};
	
	/** This is a singleton class, use <code>Free</code> to free the instance. */
	~String(void) {};

	friend class Singleton<String>;	
};
