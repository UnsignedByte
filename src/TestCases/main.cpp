/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string>
#include <vector>

#include "Account.h"
#include "Array.h"
#include "Chunk.h"
#include "ChunkImporter.h"
#include "ChunkManager.h"
#include "DatabaseMgr.h"
#include "DirectionParser.h"
#include "FieldImpls.h"
#include "FieldValues.h"
#include "Join.h"
#include "Managers.h"
#include "RoomManager.h"
#include "SavableHeaders.h"
#include "SelectionMask.h"
#include "SqliteMgr.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "smart_ptr.h"

extern bool g_shutdown;

void TestChunkImporter()
{
	value_type key = mud::Managers::Get()->Chunk->Add()->first()->getIntegerValue();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(key);
	
	chunk->setDescription("Some chunk thingy.");
	chunk->setTags("Chunk Thingy");
	chunk->Save();
	
	printf("Creating importer...\n");
	std::string input;
	input.append("Algemene beschrijving.\n");
	input.append("*Eerste detail.\n");
	input.append("\tMet wat extra beschrijving.\n");
	input.append("\tDrie regels in totaal.\n");
	input.append("*Tweede detail.\n");
	input.append("*Derde detail.\n");
	input.append("**Met een subdetail\n");
	input.append("\tMet wat extra text in subdetail\n");
	input.append("**Nog een subdetail\n");
	input.append("*Vierde detail\n");
	input.append("**Subdetail\n");
	input.append("***Subsub detail\n");
	input.append("***Subsub detail twee\n");
	input.append("****Subsubsub detail\n");
	input.append("**Subdetail\n");
	ChunkImporterPtr importer(new ChunkImporter(input));
	printf(importer->getResult().c_str());
	printf("\n");
	
	printf("Applying chunk now...\n");
	importer->Apply(chunk);
	printf("\n");
}

void TestStringUtilities()
{
	std::string input;
	input.append("Dit is de eerste regel.\n");
	input.append("Dit is de tweede regel.\n");
	input.append("Derde regel.\n");
	input.append("Regel vier.\n");
	input.append("Laatste regel.\n");
	
	Strings results = String::Get()->lines(input,"\n");
	for(Strings::iterator it = results.begin(); it != results.end(); it++)
	{
		printf(it->c_str());
		printf("|\n");
	}
	printf("\n\n");
	
	Strings inputs;
	inputs.push_back("Dit is de eerste regel.");
	inputs.push_back("Dit is de tweede regel.");
	inputs.push_back("Derde regel.");
	inputs.push_back("Regel vier.");
	inputs.push_back("Laatste regel.");
	
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "+", 0).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "~", 1).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "#", 2).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "@", 3).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "=", 4).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "&", 5).c_str());
	printf("\n=========\n");
	printf(String::Get()->unlines(inputs, "&", 6).c_str());
	printf("\n=========\n");
	printf("\n\n");
	
	Strings inputs_two;
	inputs_two.push_back("Dit is de eerste regel.\nMet wat extra's.");
	inputs_two.push_back("Dit is de tweede regel.");
	inputs_two.push_back("Derde regel.\nMet iets erachter.\nTwee ietsen zelfs");
	inputs_two.push_back("Regel vier.\nZit.\nVol.\nMet.\nExtra.\nRegels.\nZes in totaal.");
	inputs_two.push_back("Lege regels!\n\n\n\n\n\n\n\n");
	inputs_two.push_back("Laatste regel.");
	
	Strings results_two = String::Get()->unlines(inputs_two);
	
	printf("\n=========\n");
	for(Strings::iterator it = results_two.begin(); it != results_two.end(); it++)
	{
		printf(it->c_str());
		printf("|\n");
	}
	printf("\n=========\n");
	printf("\n\n");
	
	printf("\n=========\n");
	printf(String::Get()->box(inputs_two).c_str());
	printf("\n=========\n");
	
	printf("\n");
}

void printSavables(SavableManagersPtr result)
{
	for(SavableManagerVector::const_iterator it = result->begin(); it != result->end(); it++)
	{
		SavableManagerPtr manager = *it;
		FieldValuePtr detailid = manager->getValue(db::TableImpls::Get()->DETAILS->DETAILID);
		ValuePtr key = manager->getValue(db::TableImpls::Get()->DETAILS->KEY);
		ValuePtr description = manager->getValue(db::TableImpls::Get()->DETAILS->DESCRIPTION);
		
		printf("%s (%d): %s.\n", key->getStringValue().c_str(), detailid->getIntegerValue(), description->getStringValue().c_str());
	}
}

void printSavableRoom(SavableManagersPtr result)
{
	for(SavableManagerVector::const_iterator it = result->begin(); it != result->end(); it++)
	{
		SavableManagerPtr manager = *it;
		FieldValuePtr roomid = manager->getValue(db::TableImpls::Get()->ROOMS->ROOMID);
		ValuePtr name = manager->getValue(db::TableImpls::Get()->ROOMS->NAME);
		ValuePtr description = manager->getValue(db::TableImpls::Get()->ROOMS->DESCRIPTION);
		
		printf("%s (%d): %s.\n", name->getStringValue().c_str(), roomid->getIntegerValue(), description->getStringValue().c_str());
	}
}

void TestSelectionMask()
{
	TableImplPtr table = db::TableImpls::Get()->DETAILS;
	
	SelectionMaskPtr mask(new SelectionMask(table));
	SqliteMgr::Get()->doSelectMulti(mask.get());
	
	printf("Full table:\n");
	SavableManagersPtr result = mask->getResult();
	printSavables(result);
	printf("\n");
	
	mask.reset(new SelectionMask(table));
	FieldValuePtr key(new FieldValue(db::TableImpls::Get()->DETAILS->KEY, "keythree"));
	mask->addField(key);
	SqliteMgr::Get()->doSelectMulti(mask.get());
	
	printf("Just three\n");
	result = mask->getResult();
	printSavables(result);
	
	mask.reset(new SelectionMask(table));
	
	TableImplPtr joinTable = db::TableImpls::Get()->DETAILROOM;
	KeyImplPtr nativeKey = db::TableImpls::Get()->DETAILS->DETAILID;
	KeyImplPtr foreignKey = db::TableImpls::Get()->DETAILROOM->FKDETAILS;	
	mask->addJoin(joinTable, nativeKey, foreignKey);
	
	TableImplPtr roomTable = db::TableImpls::Get()->ROOMS;
	KeyImplPtr reverseNativeKey = db::TableImpls::Get()->DETAILROOM->FKROOMS;
	KeyImplPtr reverseForeignKey = db::TableImpls::Get()->ROOMS->ROOMID;
	mask->addJoin(joinTable, roomTable, reverseNativeKey, reverseForeignKey);
	
	FieldValuePtr foreignField(new FieldValue(db::TableImpls::Get()->ROOMS->LENGTH, 1));
	mask->addField(foreignField);	
	
	SqliteMgr::Get()->doSelectMulti(mask.get());
	
	printf("Join restriction\n");
	result = mask->getResult();
	printSavables(result);	
}

void TestSelectionMaskJoin()
{
	TableImplPtr table = db::TableImpls::Get()->ROOMS;
	
	SelectionMaskPtr mask(new SelectionMask(table));
	
	TableImplPtr joinTable = db::TableImpls::Get()->CLUSTERS;
	KeyImplPtr nativeKey = db::TableImpls::Get()->ROOMS->FKCLUSTERS;
	KeyImplPtr foreignKey = db::TableImpls::Get()->CLUSTERS->CLUSTERID;
	mask->addJoin(joinTable, nativeKey, foreignKey);
		
	FieldValuePtr foreignField(new FieldValue(db::TableImpls::Get()->CLUSTERS->NAME, "Space"));
	mask->addField(foreignField);
	
	SqliteMgr::Get()->doSelectMulti(mask.get());
	
	printf("Join direct\n");
	SavableManagersPtr  result = mask->getResult();
	printSavableRoom(result);
}

void tableFormattingHelper(int* sizesarray, int arraysize)
{
	std::vector<int> sizes;	
	for(int i = 0; i < arraysize; i++) {
		sizes.push_back(sizesarray[i]);
	}
	
	String::Get()->capFieldSize(sizes, sizes.size()*7);
	int totalsize = 0;
	
	printf("[");
	for(std::vector<int>::const_iterator it = sizes.begin(); it != sizes.end(); it++)
	{		
		if(it != sizes.begin())
			printf(" , ");
			
		int size = *it;
		printf("%d", size);
		totalsize += size;
	}
	printf("] = %d (%d).\n", totalsize, sizes.size()*7);
}

void TestTableFormatting()
{
	int sizes1 [] = { 7, 1, 4, 6, 7, 9, 12, 24 };	
	tableFormattingHelper(sizes1, 8);
	
	int sizes2 [] = { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 };	
	tableFormattingHelper(sizes2, 10);	
	
	int sizes3 [] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 100 };	
	tableFormattingHelper(sizes3, 10);	
	
	int sizes4 [] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 9, 15 };	
	tableFormattingHelper(sizes4, 15);	
}

void TestRoomList()
{
	Strings lines = mud::Managers::Get()->Room->List();
	std::string result = String::Get()->box(lines, "Rooms");
	printf("%s\n", result.c_str());
}

void TestInserting()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->VERSION);
	bool locked = manager->lock();
	Assert(locked); // we're the only one active, should always be lockable	
	manager->save();
	manager->unlock();
}

void printCoordinates(Coordinates coordinates)
{
	if(!coordinates.size())
	{
		printf("-\n");
		return;
	}
	
	printf("[\n");
	
	for(Coordinates::const_iterator it = coordinates.begin(); it != coordinates.end(); it++)
	{
		Coordinate coordinate = *it;
		
		if(coordinate.isDirection())
			printf("%s\n", coordinate.toDirectionString().c_str());
		else
			printf("%s\n", coordinate.toString().c_str());
	}
	
	printf("]\n");
}

void TestDirections()
{		
	DirectionParser n("n");
	printCoordinates(n.getResult());
	
	DirectionParser e("e");
	printCoordinates(e.getResult());
	
	DirectionParser s("s");
	printCoordinates(s.getResult());
	
	DirectionParser w("w");
	printCoordinates(w.getResult());
	
	DirectionParser ne("ne");
	printCoordinates(ne.getResult());
	
	// Should be rejected
	DirectionParser ns("ns");
	printCoordinates(ns.getResult());
	
	DirectionParser nw("nw");
	printCoordinates(nw.getResult());
	
	// Should be rejected
	DirectionParser nn("nn");
	printCoordinates(nn.getResult());
	
	DirectionParser nwu("nwu");
	printCoordinates(nwu.getResult());
	
	DirectionParser nwu_swd("nwu;swd");
	printCoordinates(nwu_swd.getResult());
	
	DirectionParser nsu_nuswd("nsu;nuswd");	
	printCoordinates(nsu_nuswd.getResult());
	
	DirectionParser n_n_("n;n;");
	printCoordinates(n_n_.getResult());
	
	DirectionParser n_n__n("n;n;;n");
	printCoordinates(n_n__n.getResult());
}

int main()
{ 
	printf("Initializing tables...\n");
	db::TableImpls::Get()->Initialize();
	
	printf("Opening database...\n");
	std::string dbname = "TestCase";
	dbname.append(".db");
	DatabaseMgr::Initialize(dbname);
	printf("Done.\n");
	
	/*
	printf("[starttest:Simple]\n");
	TestSimple();
	printf("[endtest:Simple]\n");
	printf("\n");
	
	printf("[starttest:Base]\n");
	TestBase();
	printf("[endtest:Simple]\n");
	printf("\n");
	
	printf("[starttest:Derived]\n");
	TestDerived();
	printf("[endtest:Simple]\n");
	printf("\n");
	
	printf("[starttest:BaseDerived]\n");
	TestBaseDerived();
	printf("[endtest:BaseDerived]\n");
	printf("\n");
	
	printf("[starttest:PointerPass]\n");
	TestPointerPass();
	printf("[endtest:PointerPass]\n");
	printf("\n");	
	
	printf("[starttest:NullPointer]\n");
	TestNullPtr();
	printf("[endtest:NullPointer]\n");
	printf("\n");
	*/

	/*
	printf("[starttest:ChunkImporter]\n");
	TestChunkImporter();
	printf("[endtest:ChunkImporter]\n");
	printf("\n");

	printf("[starttest:StringUtilities]\n");
	TestStringUtilities();
	printf("[endtest:StringUtilities]\n");
	printf("\n");	
	
	printf("[starttest:SelectionMask]\n");
	TestSelectionMask();
	printf("[endtest:SelectionMask]\n");
	printf("\n");	
	
	printf("[starttest:SelectionMaskJoin]\n");
	TestSelectionMaskJoin();
	printf("[endtest:SelectionMaskJoin]\n");
	printf("\n");	
	
	printf("[starttest:TableFormatting]\n");
	TestTableFormatting();
	printf("[endtest:TableFormatting]\n");
	printf("\n");	
	
	printf("[starttest:RoomList]\n");
	TestRoomList();
	printf("[endtest:RoomList]\n");
	printf("\n");	 
	
	printf("[starttest:Inserting]\n");
	TestInserting();
	printf("[endtest:Inserting]\n");
	printf("\n");
	 */
	
	printf("[starttest:Directions]\n");
	TestDirections();
	printf("[endtest:Directions]\n");
	printf("\n");
	
	g_shutdown = true;
	
	printf("Freeing global...\n");
	Global::Free();
	printf("Freeing tables...\n");
	
	Tables::Free();
	printf("Exiting...\n");
	
	return 0;
}
