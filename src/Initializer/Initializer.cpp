/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "AccountManager.h"
#include "Area.h"
#include "AreaManager.h"
#include "Channel.h"
#include "ChannelManager.h"
#include "Character.h"
#include "CharacterManager.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "Cluster.h"
#include "ClusterManager.h"
#include "Colour.h"
#include "ColourManager.h"
#include "Command.h"
#include "CommandManager.h"
#include "Exit.h"
#include "ExitManager.h"
#include "FieldImpls.h"
#include "FieldValues.h"
#include "GameVersion.h"
#include "Global.h"
#include "GrantGroup.h"
#include "GrantGroupManager.h"
#include "Initializer.h"
#include "Managers.h"
#include "Race.h"
#include "RaceManager.h"
#include "Room.h"
#include "RoomManager.h"
#include "SavableHeaders.h"
#include "SavableTypes.h"
#include "Sector.h"
#include "SectorManager.h"
#include "SqliteMgr.h"
#include "StringUtilities.h"
#include "TableImpls.h"

bool Initializer::VerifyDatabaseVersion()
{
	bool equal = true;

	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->VERSION->VERSIONID, 1));
	SavableManagerPtr manager;
	
	try {
		manager = SavableManager::bykey(key);
	} catch(RowNotFoundException& e) {
		Global::Get()->printlog("Could not retreive version info.");
		return false;
	}

	if(manager->getValue(db::TableImpls::Get()->VERSION->MAJOR)->getIntegerValue() != game::major)
	{
		Global::Get()->printlog("Major / Major mismatch.\n");
		equal = false;
	}
	
	if(manager->getValue(db::TableImpls::Get()->VERSION->MINOR)->getIntegerValue() != game::minor)
	{
		Global::Get()->printlog("Minor / Minor mismatch.\n");
		equal = false;
	}
	
	if(manager->getValue(db::TableImpls::Get()->VERSION->MICRO)->getIntegerValue() != game::micro)
	{
		Global::Get()->printlog("Micro / Micro mismatch.\n");
		equal = false;
	}
	
	if(manager->getValue(db::TableImpls::Get()->VERSION->VERSIONTEXT)->getStringValue().compare(game::vstring))
	{
		Global::Get()->printlog("Versiontext / Vstring mismatch.\n");
		
		std::string msg = "dbversion is: '";
		msg.append(manager->getValue(db::TableImpls::Get()->VERSION->VERSIONTEXT)->getStringValue());		
		msg.append("', ours is '");
		msg.append(game::vstring);		
		msg.append("'\n");
		
		Global::Get()->printlog(msg);
		
		equal = false;
	}

	return equal;
}

void Initializer::InitTables(TableImplVector::const_iterator begin, TableImplVector::const_iterator end)
{	
	for(TableImplVector::const_iterator it = begin; it != end; it++)
	{
		TableImplPtr table = *it;
		Assert(table);
		
		SqliteMgr::Get()->initTable(table);
	}
	
	return;
}

bool Initializer::VerifyTables(TableImplVector::const_iterator begin, TableImplVector::const_iterator end)
{
	bool good = true;
	
	for(TableImplVector::const_iterator it = begin; it != end; it++)
	{
		TableImplPtr table = *it;
		Assert(table);
		
		bool success = SqliteMgr::Get()->tableValid(table);
		if(!success)
		{
			std::string msg = "Table ";
			msg.append((*it)->getName());
			msg.append("'s creation query does not match the one from the input database.\n");
			
			Global::Get()->printlog(msg);
			
			good = false;
		}
	}
	
	return good;
}

void Initializer::InitDatabase()
{	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->VERSION->VERSIONID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->VERSION);
		bool locked = manager->lock();
		Assert(locked); // we're the only one active, should always be lockable

		ValuePtr value;
		
		KeysPtr keys(new Keys(db::TableImpls::Get()->VERSION));
		keys->addKey(db::TableImpls::Get()->VERSION->VERSIONID, 1);
		manager->setKeys(keys);
		
		value = ValuePtr(new FieldValue(db::TableImpls::Get()->VERSION->MAJOR, game::major));
		manager->setValue(value);

		value = ValuePtr(new FieldValue(db::TableImpls::Get()->VERSION->MINOR, game::minor));
		manager->setValue(value);

		value = ValuePtr(new FieldValue(db::TableImpls::Get()->VERSION->MICRO, game::micro));
		manager->setValue(value);
		
		value = ValuePtr(new FieldValue(db::TableImpls::Get()->VERSION->VERSIONTEXT, std::string(game::vstring)));
		manager->setValue(value);
		
		manager->save();
		manager->unlock();
	}
		
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ACCOUNTS->ACCOUNTID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Account->Add();
		mud::AccountPtr account = mud::Managers::Get()->Account->GetByKey(keys->first()->getIntegerValue());
		
		account->setName(game::vname);
		account->setPassword("qq");
		account->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->RACES->RACEID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
		
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Race->Add();
		mud::RacePtr race = mud::Managers::Get()->Race->GetByKey(keys->first()->getIntegerValue());
		
		race->setName("human");
		race->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ENTITIES->ENTITYID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Character->Add();
		mud::CharacterPtr character = mud::Managers::Get()->Character->GetByKey(keys->first()->getIntegerValue());
		
		character->setName(game::vname);
		character->setDescription("This is the default character.");
		character->setRace(1);
		character->setChunk(1);
		character->Save();
	}
	
	try
	{
		KeysPtr keys(new Keys(db::TableImpls::Get()->CHARACTERACCOUNT));
		keys->addKey(db::TableImpls::Get()->CHARACTERACCOUNT->FKACCOUNTS, 1);
		keys->addKey(db::TableImpls::Get()->CHARACTERACCOUNT->FKENTITIES, 1);
		SavableManagerPtr manager = SavableManager::bykeys(keys);
	}
	catch(RowNotFoundException& e)
	{
		RelationPtr relation(new Relation(db::TableImpls::Get()->CHARACTERACCOUNT));
		relation->addKey(db::TableImpls::Get()->CHARACTERACCOUNT->FKACCOUNTS, 1);
		relation->addKey(db::TableImpls::Get()->CHARACTERACCOUNT->FKENTITIES, 1);
		relation->save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->SECTORS->SECTORID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Sector->Add();
		mud::SectorPtr sector = mud::Managers::Get()->Sector->GetByKey(keys->first()->getIntegerValue());
		
		sector->setName("grass");
		sector->setSymbol(".");
		sector->setMoveCost(1);
		sector->setWater(0);
		sector->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->CHANNELS->CHANNELID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Channel->Add();
		mud::ChannelPtr channel = mud::Managers::Get()->Channel->GetByKey(keys->first()->getIntegerValue());
		
		channel->setName("ooc");
		channel->setDescription("The Out of Character channel.");
		channel->setNeedLogin(1);
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->GRANTGROUPS->GRANTGROUPID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->GrantGroup->Add();
		mud::GrantGroupPtr grantgroup = mud::Managers::Get()->GrantGroup->GetByKey(keys->first()->getIntegerValue());
		
		grantgroup->setName("All");
		grantgroup->setDefaultGrant(1);
		grantgroup->setDefaultLog(0);
		grantgroup->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->GRANTGROUPS->GRANTGROUPID, 2));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->GrantGroup->Add();
		mud::GrantGroupPtr grantgroup = mud::Managers::Get()->GrantGroup->GetByKey(keys->first()->getIntegerValue());		
		
		grantgroup->setName("Admin");
		grantgroup->setDefaultGrant(0);
		grantgroup->setDefaultLog(1);
		grantgroup->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->GRANTGROUPS->GRANTGROUPID, 3));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->GrantGroup->Add();
		mud::GrantGroupPtr grantgroup = mud::Managers::Get()->GrantGroup->GetByKey(keys->first()->getIntegerValue());
		
		grantgroup->setName("Builder");
		grantgroup->setDefaultGrant(0);
		grantgroup->setDefaultLog(0);
		grantgroup->Save();
	}
	
	try
	{
		KeysPtr keys(new Keys(db::TableImpls::Get()->PERMISSIONS));
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, 1);
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, 2);		
		SavableManagerPtr manager = SavableManager::bykeys(keys);
	}
	catch(RowNotFoundException& e)
	{
		RelationPtr relation(new Relation(db::TableImpls::Get()->PERMISSIONS));
		relation->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, 1);
		relation->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, 2);
		relation->addField(db::TableImpls::Get()->PERMISSIONS->GRANT, 1);
		relation->addField(db::TableImpls::Get()->PERMISSIONS->LOG, 1);
		relation->save();
	}
	
	try
	{
		KeysPtr keys(new Keys(db::TableImpls::Get()->PERMISSIONS));
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, 1);
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, 3);		
		SavableManagerPtr manager = SavableManager::bykeys(keys);
	}
	catch(RowNotFoundException& e)
	{
		RelationPtr relation(new Relation(db::TableImpls::Get()->PERMISSIONS));
		relation->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, 1);
		relation->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, 3);
		relation->addField(db::TableImpls::Get()->PERMISSIONS->GRANT, 1);
		relation->addField(db::TableImpls::Get()->PERMISSIONS->LOG, 0);
		relation->save();
	}
}

void Initializer::InitColours()
{
	const struct colour colours[] = 
	{
		{ "Restore", "^", "0;0m" },
		{ "Dark Red", "r", "31m" },
		{ "Dark Green", "g", "32m" },
		{ "Dark Yellow", "y", "33m"},
		{ "Dark Blue", "b", "34m" },
		{ "Dark Purple", "m", "35m" },
		{ "Dark Cyan", "c", "36m" },
		{ "Gray", "w", "37m" },
		{ "Black", "l", "30m" },
		// Light colours
		{ "Light Red", "R", "1;31m" },
		{ "Light blue", "G", "1;32m" },
		{ "Light Yellow", "Y", "1;33m" },
		{ "Light Blue", "B", "1;34m" },
		{ "Light Magenta", "M", "1;35m" },
		{ "Light Cyan", "C", "1;36m" },
		{ "White", "W", "1;37m" },
		{ "Light Gray", "L", "1;30m" },
	};

	int size = sizeof(colours) / sizeof(colours[0]);
	printf("Colours: %d.\n", size);

	for(int i = 0; i < size; i++)
	{
		try
		{
			ValuePtr value(new FieldValue(db::TableImpls::Get()->COLOURS->CODE, colours[i].code));
			SavableManagerPtr manager = SavableManager::byvalue(value);
		}
		catch(RowNotFoundException& e)
		{
			KeysPtr keys = mud::Managers::Get()->Colour->Add();
			mud::ColourPtr thecolour = mud::Managers::Get()->Colour->GetByKey(keys->first()->getIntegerValue());
		
			thecolour->setName(colours[i].name);
			thecolour->setCode(colours[i].code);
			thecolour->setColourString(colours[i].cstr);
			thecolour->setAnsi(1);
			thecolour->Save();
		}
	}
}

void Initializer::InitCommands()
{
	const struct command commands[] = 
	{
		{ "Account::Commands",1,1,1,0,"This command will list all available commands."},
		{ "Account::Login",1,1,1,0,"This command allows you to log in with one of your IC characters."},
		{ "Account::OLC",3,1,1,0,"This command will drop you into the OLC hub from which you can select any of the OLC editors."},
		{ "Account::OOC",1,1,1,0,"This command will drop you into OOC mode so that you can do ooc commands without the OOC prefix."},
		{ "Account::New",1,1,1,0,"This command will start the creation of a new IC character."},
		{ "Account::Quit",1,1,1,0,"This command will log you out and disconnect you, note that in other editors it will bring you back to the previous editor."},
		{ "Account::Shutdown",2,1,1,0,"This command will shut down the server."},
		{ "Account::List",1,1,1,0,"This command will provide you with a list of your characters."},
	};

	int size = sizeof(commands) / sizeof(commands[0]);
	printf("Commands: %d.\n", size);

	for(int i = 0; i < size; i++)
	{
		try
		{
			ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->NAME, commands[i].name));
			SavableManagerPtr manager = SavableManager::byvalue(value);
		}
		catch(RowNotFoundException& e)
		{
			KeysPtr keys = mud::Managers::Get()->Command->Add();
			mud::CommandPtr thecommand = mud::Managers::Get()->Command->GetByKey(keys->first()->getIntegerValue());
		
			thecommand->setName(commands[i].name);
			thecommand->setHelp(commands[i].help);
			
			if(commands[i].grantgroup != 0)			
				thecommand->setGrantGroup(commands[i].grantgroup);
			
			if(commands[i].highforce == 0 || commands[i].highforce == 1)
				thecommand->setHighForce(commands[i].highforce);								
		
			if(commands[i].force == 0 || commands[i].force == 1)
				thecommand->setForce(commands[i].force);
			
			if(commands[i].lowforce == 0 || commands[i].lowforce == 1)
				thecommand->setLowForce(commands[i].lowforce);
			
			thecommand->Save();
		}
	}
}

void Initializer::InitSpace()
{
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->AREAS->AREAID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Area->Add();
		mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(keys->first()->getIntegerValue());
		
		area->setName("Space");
		area->setDescription("This is space, the final frontier.");
		area->setHeight(1);
		area->setWidth(1);
		area->setLength(1);
		area->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->CLUSTERS->CLUSTERID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Cluster->Add();
		mud::ClusterPtr cluster = mud::Managers::Get()->Cluster->GetByKey(keys->first()->getIntegerValue());
		
		cluster->setName("Alpha quadrant");
		cluster->setDescription("This is the alpha quadrant, where Earth lies.");
		cluster->setArea(1);
		cluster->Save();
	}
	
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ROOMS->ROOMID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Room->Add();
		mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(keys->first()->getIntegerValue());
		
		room->setName("The Void");
		room->setDescription("You are in The Void.");
		room->setSector(1);
		room->setCluster(1);
		room->Save();
	}
		
	try
	{
		KeyValuePtr key(new KeyValue(db::TableImpls::Get()->CHUNKS->CHUNKID, 1));
		SavableManagerPtr manager = SavableManager::bykey(key);
		
	}
	catch(RowNotFoundException& e)
	{
		KeysPtr keys = mud::Managers::Get()->Chunk->Add();
		mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(keys->first()->getIntegerValue());
		
		chunk->setRoom(1);
		chunk->setName("The void");
		chunk->Save();
	}
}

void Initializer::InitSampleChunks(int size)
{
	int xdim = size;
	int ydim = size;
	int zdim = size;
	
	KeysPtr keys;
		
	// Sample sector
	keys = mud::Managers::Get()->Sector->Add();
	mud::SectorPtr sector = mud::Managers::Get()->Sector->GetByKey(keys->first()->getIntegerValue());	
	
	sector->setName("Stone");
	sector->setMoveCost(1);
	sector->setSymbol("_");
	sector->setWater(0);
	sector->Save();
	
	// Sample area
	keys = mud::Managers::Get()->Area->Add();
	mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(keys->first()->getIntegerValue());
	
	area->setName("The cube");
	area->setDescription("This is an example area, it contains a cube to try out nagivation.");
	area->Save();
	
	
	
	// Centre
	mud::ChunkPtr centre;
	
	keys = mud::Managers::Get()->Chunk->Add();
	centre = mud::Managers::Get()->Chunk->GetByKey(keys->first()->getIntegerValue());
	
	centre->setName("Core of the cube ");
	centre->setDescription("You are in the centre part of the Cube.");
	centre->setTags("Core Cube");
	centre->Save();
	
	
	
	// Defs
	mud::ClusterPtr cluster;	
	mud::RoomPtr room;
	mud::ChunkPtr chunk;
	
	for(int z = 0; z < zdim; z++)
	{
		std::string zpart;
				
		if(z < zdim/3)
			zpart = "top";
		else if(z < zdim*2/3)
			zpart = "middle";
		else
			zpart = "bottom";
		
		keys = mud::Managers::Get()->Cluster->Add();
		cluster = mud::Managers::Get()->Cluster->GetByKey(keys->first()->getIntegerValue());
		
		cluster->setName(zpart + std::string(" of the cube ")  + String::Get()->fromInt(z));
		cluster->setDescription(std::string("You are in the ") + zpart + std::string(" part of the Cube."));
		cluster->setArea(area->getID());
		cluster->Save();
	
	
		for(int y = 0; y < ydim; y++)
		{
			std::string ypart;
			
			if(y < ydim/3)
				ypart = "left";
			else if(y < ydim*2/3)
				ypart = "centre";
			else
				ypart = "right";
			
			keys = mud::Managers::Get()->Room->Add();
			room = mud::Managers::Get()->Room->GetByKey(keys->first()->getIntegerValue());
			
			room->setName(ypart + std::string(" of the cube ") + String::Get()->fromInt(y));
			room->setDescription(std::string("You are in the ") + ypart + std::string(" part of the Cube."));
			room->setSector(sector->getID());
			room->setCluster(cluster->getID());
			room->Save();
	
			for(int x = 0; x < xdim; x++)
			{
				std::string xpart;
			
				if(x < xdim/3)
					xpart = "front";
				else if(x < xdim*2/3)
					xpart = "middle";
				else
					xpart = "back";
				
				keys = mud::Managers::Get()->Chunk->Add();
				chunk = mud::Managers::Get()->Chunk->GetByKey(keys->first()->getIntegerValue());
				
				chunk->setName(xpart + std::string(" of the cube ")  + String::Get()->fromInt(x));
				chunk->setDescription(std::string("You are in the ") + xpart + std::string(" part of the Cube."));
				chunk->setRoom(room->getID());
				chunk->setTags(zpart + " " + ypart + " " + xpart + std::string(" Cube"));
				chunk->Save();
				
				int centrepos = (size+1)/2-1;
				
				if(z==centrepos && y==centrepos && x==centrepos)
				{
					centre->setRoom(room->getID());
					centre->Save();
				}
				else
				{
					mud::ExitPtr exit;
					
					keys = mud::Managers::Get()->Exit->Add();
					exit = mud::Managers::Get()->Exit->GetByKey(keys->first()->getIntegerValue());
					
					Coordinate c(x-centrepos,y-centrepos,z-centrepos);
					exit->setDirection(c);
					exit->setFromChunk(centre->getID());
					exit->setToChunk(chunk->getID());
					exit->Save();
					
					
					keys = mud::Managers::Get()->Exit->Add();
					exit = mud::Managers::Get()->Exit->GetByKey(keys->first()->getIntegerValue());
					
					exit->setDirection(c.getComplement());
					exit->setFromChunk(chunk->getID());
					exit->setToChunk(centre->getID());
					exit->Save();
				}
			}
		}
	}
}
