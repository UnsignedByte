/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>

#include "DatabaseMgr.h"
#include "FieldImpls.h"
#include "GameVersion.h"
#include "Global.h"
#include "Initializer.h"
#include "SavableHeaders.h"
#include "SqliteMgr.h"
#include "TableImpls.h"

const int MAXSIZE = (1<<16);
char m_workspace[MAXSIZE];

extern bool g_shutdown;

void exitfunc()
{
	g_shutdown = true;
	
	printf("Freeing global...\n");
	Global::Free();
	printf("Freeing tables...\n");
	Tables::Free();
	printf("Freeing sqlitemgr...\n");
	SqliteMgr::Free();
	printf("Exiting...\n");
	// std::cin.get();
	return;
}

int main(int argc, char** argv)
{	
	atexit(exitfunc);
	
	int exampleSize = 0;
	
	if(argc > 1)
	{
		std::string param(argv[1]);
		if(!param.substr(0, 11).compare("--examples="))
		{
			std::string rest = param.substr(11);
			exampleSize = atoi(rest.c_str());
			
			if(exampleSize <= 0)
			{
				printf("Please specify an example size, '%s' is not a valid size.\n", rest.c_str());
				return -1;
			}			
		}
	}
	
	db::TableImpls::Get()->Initialize();
	
	printf("%s database initializer for db v%s.\n", game::vname, game::vstring);
	std::string dbname = game::vname;
	dbname.append(".db");

	printf("Opening or creating '%s'...\n", dbname.c_str());
	DatabaseMgr::Initialize(dbname);
	
	Initializer init(DatabaseMgr::Get()->DB());
	
	printf("Checking if database exists...\n");
	bool initialized = false;
	bool succes;
	succes = SqliteMgr::Get()->databasePopulated();
	if(!succes)
	{
		printf("Database does not exist...\n");
		printf("Creating tables...\n");
		init.InitTables(db::TableImpls::Get()->begin(), db::TableImpls::Get()->end());
		initialized = true;
	}
	
	printf("checking if database version matches...\n");
	succes = initialized || init.VerifyDatabaseVersion();
	if(!succes)
	{
		printf("Database version does not match!\n");
		printf("(Move the existing database if you wish to create a fresh copy)\n");
		return 1;
	}

	printf("Database is of most recent version!\n");	
	
	printf("Checking if tables match...\n");
	succes = init.VerifyTables(db::TableImpls::Get()->begin(), db::TableImpls::Get()->end());
	if(!succes)
	{
		printf("Database tables are not up to date!\n");
		printf("(Move the existing database if you wish to create a fresh copy)\n");
		return 1;
	}

	printf("Initializing database...\n");
	init.InitDatabase();

	printf("Initializing colours...\n");
	init.InitColours();
	
	printf("Initializing commands...\n");
	init.InitCommands();

	if(exampleSize > 0)
	{
		printf("Initializing example...\n");
		init.InitSampleChunks(exampleSize);
	}
	else
	{
		printf("Initializing space...\n");
		init.InitSpace();
	}

	return 0;
}
