/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Initializer.h
 * This file contains the Initializer class. 
 *
 * @see Initializer 
 */ 

#include "Types.h"

class Database;

/**
 * This class will check and initialize the database.
 *
 * It will first check whether the database is already initialized.
 * If it is not yet initialized it will create the appropriate tables.
 * Therafter it will verify that the schema is good (regardless of earlier actions).
 * When all is well it will initialize the Database with some default values.
 * Also the Colours and Commands table will be filled.
 */ 
class Initializer
{
public:
	/** Constructs an Initializer with the specified database connection. */
	explicit Initializer(Database* db) : m_db(db) {};
	
	/** Destructor, a noop. */
	~Initializer() {};
	
	
	/** Whether the database version is the same as the version that was used to build this Initializer. */
	bool VerifyDatabaseVersion();
	
	/** Whether the tables match. */
	bool VerifyTables(TableImplVector::const_iterator begin, TableImplVector::const_iterator end);
	
	
	/** Create the tables in the database. */
	void InitTables(TableImplVector::const_iterator begin, TableImplVector::const_iterator end);
	
	/** Fill the tables with some default values. */
	void InitDatabase();
	
	/** Fill the colour table. */
	void InitColours();
	
	/** Fill the command table. */
	void InitCommands();
	
	/** Creates 'the void'. */
	void InitSpace();
	
	/** Creates a set of sample chunks. */
	void InitSampleChunks(int size);

private:
	/** Hide the copy constructor. */
	Initializer(const Initializer& rhs);
	
	/** Hide the assignment operator. */
	Initializer operator=(const Initializer& rhs);

	Database* m_db; /**< The database connection to use. */
	
	/** A struct to store colour definitions in. */
	struct colour
	{
		std::string name; /**< The name of the colour. */
		std::string code; /**< The colourcode of the colour. */
		std::string cstr; /**< The string value of the colour. */
	};
	
	/** A struct to store command definitions in. */
	struct command
	{
		std::string name; /**< The fully qualified ('Editor::Name' format) name of the command. */
		value_type grantgroup; /**< The grantgroup this command belongs to. */
		int highforce; /**< Whether it is possible to highforce someone to execute this command. */
		int force; /**< Whether it is possible to force someone to execute this command. */
		int lowforce; /**< Whether it is possible to lowforce someone to execute this command. */
		std::string help; /**< A help string describing what the command does. */
	};
};
