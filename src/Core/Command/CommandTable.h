/***************************************************************************
 *   Copyright (C) 2008 by Vegard Nossum                                   *
 *   vegard.nossum@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file CommandTable.h
 * This file contains the CommandTable template. 
 *
 * @see CommandTable 
 */ 

#include <cstring>
#include <string>
#include <vector>

#include "Assert.h"
#include "CommandBinding.h"

/**
 * This class performs lookup of input to commands.
 *
 * The lookup performs a binary search on the bindings table. 
 * It also checks for partial matches. 
 */ 
template<typename T>
class CommandTable
{
	typedef CommandBinding<T> Binding; /**< The type of a binding to a specific clsas. */
	typedef CommandObject<T> Object; /**< The type of a CommandObject of a specific class. */

public:
	/** Constructs a CommandTable using the specified bindings table that is of the specified size.*/
	CommandTable(const Binding* bindings, const unsigned int n);
	
	/** Destructor, a noop. */
	virtual ~CommandTable();

	
	/** Performs a lookup on the specified word and returns the associated binding. */
	const Binding* getBinding(const std::string& action);
	
	/** Performs a lookup on the specified word and returns the associated object. */
	const Object* getObject(const std::string& action);


	/** Returns a list of known commands. */
	const std::vector<std::string>& getCommandsVector() const;

private:
	const Binding* m_bindings; /**< The bindings we search on. */
	const unsigned int m_n; /**< The size of the bindings. */
	std::vector<std::string> m_words; /**< The list of known commands. */
};

template<typename T>
CommandTable<T>::CommandTable(const Binding* bindings, const unsigned int n):
	m_bindings(bindings),
	m_n(n),
	m_words(n)
{
	/* Make sure the table is sorted. If it isn't, binary search won't work. */
	for(unsigned int i = 1; i < m_n; ++i)
		Assert(std::strcmp(m_bindings[i - 1].m_alias, m_bindings[i].m_alias) < 0);
	
	for(unsigned int i = 0; i < m_n; ++i)
		m_words[i] = m_bindings[i].m_alias;
}

template<typename T>
CommandTable<T>::~CommandTable()
{
}

static bool
string_lt_partial(const char* a, const char* b)
{
	while(*a && *b && *a == *b) {
		++a;
		++b;
	}

	if(*a < *b && *b)
		return true;
		
	return false;
}

static bool
string_eq_partial(const char* a, const char* b)
{
	while(*a && *b && *a == *b) {
		++a;
		++b;
	}

	if(*a == *b || !*b)
		return true;
		
	return false;
}

template<typename T>
const CommandBinding<T>*
CommandTable<T>::getBinding(const std::string& action)
{
	const char* word = action.c_str();
	unsigned int left = 0;
	unsigned int right = m_n;

	while(left < right) {
		unsigned int modus = left + (right - left) / 2;
		if(string_lt_partial(m_bindings[modus].m_alias, word))
			left = modus + 1;
		else
			right = modus;
	}

	if(left < m_n)
	{
		CommandBinding<T> command = m_bindings[left];
		if(command.fullName())
		{
			if(!strncmp(command.m_alias, word, action.size()))
				return &m_bindings[left];
		}
		else
		{
			if(string_eq_partial(command.m_alias, word))
				return &m_bindings[left];
		}
	}

	return NULL;
}

template<typename T>
const CommandObject<T>*
CommandTable<T>::getObject(const std::string& action)
{
	const CommandBinding<T>* b = getBinding(action);
	if(!b)
		return NULL;

	return &b->getCommand();
}

template<typename T>
const std::vector<std::string>&
CommandTable<T>::getCommandsVector() const
{
	return m_words;
}
