/***************************************************************************
 *   Copyright (C) 2008 by Vegard Nossum                                   *
 *   vegard.nossum@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file CommandBinding.h
 * This file contains the CommandBinding template. 
 *
 * @see CommandBinding 
 */ 

#include "CommandObject.h"

/**
 * This template defines a binding from the specified alias to the specified CommandObject.
 */ 
template<typename T>
class CommandBinding {
	public:
		/** Constructs a new CommandBinding with the specified alias to the specified CommandObject. */
		CommandBinding(const char* alias, const CommandObject<T>& command);
		
		/** Destructor, a noop. */
		~CommandBinding();
		
		/** Returns the command associated with this binding. */
		const CommandObject<T>& getCommand() const;
		
		/** Whether the binding should only allow full matches. */
		bool fullName() const;
		
		
	public:
		const char* m_alias; /**< The alias associated with this binding. Public to optimize the binary search for speed. */
	
	private:
		const CommandObject<T>& m_command; /**< The CommandObject associated with this binding. */
};

template<typename T>
CommandBinding<T>::CommandBinding(const char* alias,
	const CommandObject<T>& command):
	m_alias(alias),
	m_command(command)
{
}

template<typename T>
CommandBinding<T>::~CommandBinding()
{
}

template<typename T>
const CommandObject<T>&
CommandBinding<T>::getCommand() const
{
	return m_command;
}

template<typename T>
bool
CommandBinding<T>::fullName() const
{
	return m_command.fullName();
}
