/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file CommandInfoObject.h
 * This file contains the CommandInfoObject class.
 *
 * @see CommandInfoObject 
 */ 

#include "CommandObject.h"

/**
 * This template may be used in a similar way to the CommandObject template.
 *
 * The difference is that this template allows specifying additional information.
 * As such, this template provides only functionality to retreive that information.
 *
 * @see CommandObject 
 */ 
template<class T>
class CommandInfoObject : public CommandObject<T>
{
	public:
		typedef void (T::*CommandFunction)(const std::string& argument); /**< The type of the function to bind on. */
	
		/** Construct a CommandInfoObject, using CommandObject to bind the specified function to the specified name. */
		CommandInfoObject(const char* name, CommandFunction command, bool needobject = false, bool needlock = false, bool fullname = false);
		
		/** Whether this command requires an object. */
		bool needObject() const;
		
		/** Whether this command requires a lock. */
		bool needLock() const;
		
	private:
		bool m_needobject;
		bool m_needlock;		
};

template <class T> 
CommandInfoObject<T>::CommandInfoObject(const char* name, CommandFunction command, bool needobject, bool needlock, bool fullname) :
CommandObject<T>(name, command, fullname),
m_needobject(needobject),
m_needlock(needlock)
{

}

template <class T> 
bool CommandInfoObject<T>::needObject() const
{ 
	return m_needobject; 
}

template <class T> 
bool CommandInfoObject<T>::needLock() const
{ 
	return m_needlock; 
}
