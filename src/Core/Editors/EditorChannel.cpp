/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Channel.h"
#include "ChannelManager.h"
#include "EditorBool.h"
#include "EditorChannel.h"
#include "EditorString.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorChannel E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName(	"Name", 	&E::editName, 	true, true);
static O editDescription("Description",&E::editDescription,true, true);
static O editNeedLogin("NeedLogin", &E::editNeedLogin,	true, true);
static O showChannel(	"Show", 	&E::showChannel,	true, false);
static O saveChannel(	"Save", 	&E::saveChannel,	true, true);

static B commands[] = {
	B("description", editDescription),
	B("name", editName),
	B("needlogin", editNeedLogin),
	B("save", saveChannel),
	B("show", showChannel),	
};

EditorChannel::EditorChannel(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_channel()
{
	listCommands(Global::Get()->EmptyString);
}

EditorChannel::~EditorChannel(void)
{

}

void EditorChannel::OnFocus()
{		
	switch(m_target)
	{
		case M_NONE:
			return;
			
		case M_DESCRIPTION:
			m_channel->setDescription(m_value);
			break;
		
		case M_NEEDLOGIN:
			m_channel->setNeedLogin(m_yesno);
			break;
	}
	
	m_target = M_NONE;
}

std::string EditorChannel::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const ChannelCommand* act = (ChannelCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorChannel::dispatch(const std::string& action, const std::string& argument)
{
	const ChannelCommand* act = (ChannelCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_channel)
	{
		m_sock->Send("You need to be editing an channel first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_channel->Lock();			
		} catch(SavableLocked& e) {
			m_sock->Send("The channel you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}
	
	act->Run(this, argument);		
	return;
}

SavablePtr EditorChannel::getEditing()
{
	return m_channel;
}

TableImplPtr EditorChannel::getTable()
{
	return db::TableImpls::Get()->AREAS;
}

KeysPtr EditorChannel::addNew()
{
	return mud::Managers::Get()->Channel->Add();
}

std::vector<std::string> EditorChannel::getList()
{
	return mud::Managers::Get()->Channel->List();
}

void EditorChannel::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_channel.reset();
		return;
	}
	
	m_channel = mud::Managers::Get()->Channel->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorChannel::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorChannel::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Channel name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Channel name changed from '%s' to '%s'.\n", m_channel->getName().c_str(), argument.c_str());
	m_channel->setName(argument);
	return;
}

void EditorChannel::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		m_sock->SetEditor(new EditorString(m_sock, m_value));
		m_target = M_DESCRIPTION;
		return;

		return;
	}

	m_sock->Sendf("Channel description changed from '%s' to '%s'.\n", m_channel->getDescription().c_str(), argument.c_str());
	m_channel->setDescription(argument);
	return;
}

void EditorChannel::editNeedLogin(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Do players need to be logged in to use hear the channel?\n");		
		m_sock->SetEditor(new EditorBool(m_sock, m_yesno));
		m_target = M_NEEDLOGIN;
		return;
	}

	if(!argument.compare("yes"))
	{
		m_channel->setNeedLogin(true);
		return;
	}
	
	if(!argument.compare("no"))
	{
		m_channel->setNeedLogin(false);
		return;
	}

	editNeedLogin(Global::Get()->EmptyString);
	return;
}

void EditorChannel::showChannel(const std::string& argument)
{
	m_sock->Send(m_channel->toString());
}

void EditorChannel::saveChannel(const std::string& argument)
{
	m_sock->Sendf("Saving channel '%s'.\n", m_channel->getName().c_str());
	m_channel->Save();
	m_sock->Send("Saved.\n");
	return;
}
