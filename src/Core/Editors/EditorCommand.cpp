/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Command.h"
#include "CommandManager.h"
#include "EditorCommand.h"
#include "EditorOLC.h"
#include "GrantGroup.h"
#include "GrantGroupManager.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

using mud::Command;

typedef EditorCommand E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 	name		function	  need: object lock
static O editName(		"Name", 	&E::editName,		true, true);
static O editGrantGroups("GrantGroup",&E::editGrantGroups,true, true);
static O editHighForce(	"HighForce", &E::editHighForce,	true, true);
static O editForce(		"Force", 	&E::editForce,		true, true);
static O editLowForce(	"LowForce", &E::editLowForce,	true, true);
static O showCommand(	"Show", 	&E::showCommand,	true, false);
static O saveCommand(	"Save", 	&E::saveCommand,	true, true);

static const B commands[] = {
	B("force", editForce),
	B("grantgroup", editGrantGroups),
	B("highforce", editHighForce),
	B("lowforce", editLowForce),
	B("name", editName),
	B("save", saveCommand),
	B("show", showCommand),
};

EditorCommand::EditorCommand(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_command()
{
	listCommands(Global::Get()->EmptyString);
}

EditorCommand::~EditorCommand(void)
{

}

std::string EditorCommand::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const CommandCommand* act = (CommandCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorCommand::dispatch(const std::string& action, const std::string& argument)
{
	const CommandCommand* act = (CommandCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_command)
	{
		m_sock->Send("You need to be editing a command first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_command->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The command you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorCommand::getEditing()
{
	return m_command;
}

TableImplPtr EditorCommand::getTable()
{
	return db::TableImpls::Get()->COMMANDS;
}

KeysPtr EditorCommand::addNew()
{
	return mud::Managers::Get()->Command->Add();
}

std::vector<std::string> EditorCommand::getList()
{
	return mud::Managers::Get()->Command->List();
}

void EditorCommand::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_command.reset();
		return;
	}
	
	m_command = mud::Managers::Get()->Command->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorCommand::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorCommand::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Command name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Command name changed from '%s' to '%s'.\n", m_command->getName().c_str(), argument.c_str());
	m_command->setName(argument);
	return;
}

void EditorCommand::editGrantGroups(const std::string& argument)
{
	if(!m_command->Exists())
	{
		m_sock->Send("For some reason the command you are editing does not exist.\n");
		return;
	}
	
	try
	{
		long id = mud::Managers::Get()->GrantGroup->lookupByName(argument);
		m_sock->Sendf("Grantgroup changed from '%d' to '%d'.\n", m_command->getGrantGroup(), id);
		m_command->setGrantGroup(id);
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Sendf("'%s' is not a valid grantgroup!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->GrantGroup->List(), "GrantGroups"));
	}
}

void EditorCommand::editHighForce(const std::string& argument)
{
	if(!argument.compare("Enable"))
	{
		m_command->setHighForce(true);
		return;
	}
	
	if(!argument.compare("Disable"))
	{
		m_command->setHighForce(false);
		return;
	}

	m_sock->Send("Please specify a force level!\n");
	m_sock->Send("Choose between 'Enable' and 'Disable'.\n");
	return;
}

void EditorCommand::editForce(const std::string& argument)
{
	if(!argument.compare("Enable"))
	{
		m_command->setForce(true);
		return;
	}
	
	if(!argument.compare("Disable"))
	{
		m_command->setForce(false);
		return;
	}

	m_sock->Send("Please specify a force level!\n");
	m_sock->Send("Choose between 'Enable' and 'Disable'.\n");
	return;
}

void EditorCommand::editLowForce(const std::string& argument)
{
	if(!argument.compare("Enable"))
	{
		m_command->setLowForce(true);
		return;
	}
	
	if(!argument.compare("Disable"))
	{
		m_command->setLowForce(false);
		return;
	}

	m_sock->Send("Please specify a force level!\n");
	m_sock->Send("Choose between 'Enable' and 'Disable'.\n");
	return;
}

void EditorCommand::showCommand(const std::string& argument)
{
	m_sock->Send(m_command->toString());
	return;
}

void EditorCommand::saveCommand(const std::string& argument)
{
	m_sock->Sendf("Saving command '%s'.\n", m_command->getName().c_str());
	m_command->Save();
	m_sock->Send("Saved.\n");
	return;
}

