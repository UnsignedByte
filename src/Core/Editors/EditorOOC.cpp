/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Channel.h"
#include "ChannelManager.h"
#include "Character.h"
#include "CharacterManager.h"
#include "EditorOOC.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "UBHandler.h"
#include "UBSocket.h"

typedef EditorOOC E;
typedef CommandObject<E> O;
typedef CommandBinding<E> B;

static O listCommands("Commands", &E::listCommands);
static O sendOOCMessage("OOC", &E::sendOOCMessage);
static O listOnlineCharacters("Who", &E::listOnlineCharacters);
static O listCharacters("Laston", &E::listCharacters);
static O quitEditor("Quit", &E::quitEditor);

static const B commands[] = {
	B("?", listCommands),
	B("laston", listCharacters),	
	B("ooc", sendOOCMessage),
	B("quit", quitEditor),
	B("who", listOnlineCharacters),
};

EditorOOC::EditorOOC(UBSocket* sock) :
Editor(sock),
m_commands(commands, array_size(commands))
{

}

EditorOOC::~EditorOOC()
{
	
}

std::string EditorOOC::lookup(const std::string& action)
{	
	const OOCCommand* act = m_commands.getObject(action);
	if(act)
		return act->getName();

	std::string actionstripped = action;
	actionstripped.erase(0, 1);
	act = m_commands.getObject(actionstripped);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorOOC::dispatch(const std::string& action, const std::string& argument)
{
	const OOCCommand* act = m_commands.getObject(action);
	
	if(act)
	{
		act->Run(this, argument);
		return;
	}
	
	std::string actionstripped = action;
	actionstripped.erase(0, 1);
	act = m_commands.getObject(actionstripped);

	Assert(act);
	act->Run(this, argument);
}

void EditorOOC::listCommands(const std::string& argument)
{
	m_sock->Send(String::Get()->box(m_commands.getCommandsVector(), "OOC"));
	m_sock->Send("\n");
	return;
}

void EditorOOC::sendOOCMessage(const std::string& argument)
{
	Assert(m_sock->hasAccount());
	
	try {
		mud::ChannelPtr channel = mud::Managers::Get()->Channel->GetByName("ooc");
		std::map<SOCKET,Socket *> ref = UBHandler::Get()->Sockets();
		for (std::map<SOCKET,Socket *>::iterator it = ref.begin(); it != ref.end(); it++)
		{
			UBSocket* sock = UBSocket::Cast(it->second, false);
			if(sock && sock->canReceiveChannel(channel)) {
					sock->Sendf("%s ooc> '%s'.\n", m_sock->GetAccount()->getName().c_str(), argument.c_str());			
			}
		}
	} catch(RowNotFoundException& e) {
		Global::Get()->printbug("Could not retreive the OOC chan!\n");
	}
	
	return;	
}

void EditorOOC::listOnlineCharacters(const std::string& argument)
{
	std::map<SOCKET,Socket *> ref = UBHandler::Get()->Sockets();
	for (std::map<SOCKET,Socket *>::iterator it = ref.begin(); it != ref.end(); it++)
	{
		UBSocket* sock = UBSocket::Cast(it->second, false);
		if(sock)
		{
			if(!sock->hasAccount())
				continue;
				
			m_sock->Sendf("- %s -\n", sock->GetAccount()->getName().c_str());
		}
	}
	return;
}

void EditorOOC::listCharacters(const std::string& argument)
{
	m_sock->Send(String::Get()->box(mud::Managers::Get()->Character->List(), "Characters"));
}

void EditorOOC::quitEditor(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	m_sock->PopEditor();
	return;
}

bool EditorOOC::supportPrefixes() const
{
	return false;
}
