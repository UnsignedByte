/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "EditorOLC.h"
#include "EditorSector.h"
#include "Managers.h"
#include "Sector.h"
#include "SectorManager.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorSector E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName(	"Name", 	&E::editName,	true, true);
static O editSymbol("Description",&E::editSymbol,true,true);
static O editMoveCost("Height", &E::editMoveCost,true, true);
static O editWater(	"Water", 	&E::editWater,	true, true);
static O showSector("Show", 	&E::showSector,	true, false);
static O saveSector("Save", 	&E::saveSector,	true, true);

static const B commands[] = {
	B("movecost", editMoveCost),
	B("name", editName),
	B("save", saveSector),
	B("show", showSector),
	B("symbol", editSymbol),
	B("water", editWater),
};

EditorSector::EditorSector(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_sector()
{
	listCommands(Global::Get()->EmptyString);
}

EditorSector::~EditorSector(void)
{

}

std::string EditorSector::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const SectorCommand* act = (SectorCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorSector::dispatch(const std::string& action, const std::string& argument)
{
	const SectorCommand* act = (SectorCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
		
	if(!m_sector)
	{
		m_sock->Send("You need to be editing a sector first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}

	if(act->needLock()) 
	{
		try {
			m_sector->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The sector you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorSector::getEditing()
{
	return m_sector;
}

TableImplPtr EditorSector::getTable()
{
	return db::TableImpls::Get()->SECTORS;
}

KeysPtr EditorSector::addNew()
{
	return mud::Managers::Get()->Sector->Add();
}

std::vector<std::string> EditorSector::getList()
{
	return mud::Managers::Get()->Sector->List();
}

void EditorSector::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_sector.reset();
		return;
	}
	
	m_sector = mud::Managers::Get()->Sector->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorSector::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorSector::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Sector name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Sector name changed from '%s' to '%s'.\n", m_sector->getName().c_str(), argument.c_str());
	m_sector->setName(argument);
	return;
}

void EditorSector::editMoveCost(const std::string& argument)
{
	long movecost = atoi(argument.c_str());

	if(movecost < 0)
	{
		m_sock->Sendf("%s is not a valid movecost!", argument.c_str());
		return;
	}

	m_sock->Sendf("Sector movecost changed from '%d' to '%d'.\n", m_sector->getMoveCost(), movecost);
	m_sector->setMoveCost(movecost);
	return;
}

void EditorSector::editWater(const std::string& argument)
{
	m_sock->Sendf("Sector water flag toggled.\n");
	if(m_sector->isWater())
		m_sector->setWater(false);
	else
		m_sector->setWater(true);
	return;
}


void EditorSector::editSymbol(const std::string& argument)
{
	if(!m_sector->Exists())
	{
		m_sock->Send("For some reason the sector you are editing does not exist.\n");
		return;
	}

	if(argument.size() != 1)
	{
		m_sock->Send("Please provide one character to set as symbol!");
		return;
	}

	m_sock->Sendf("Sector symbol changed from '%s' to '%s'.\n", m_sector->getSymbol().c_str(), argument.c_str());
	m_sector->setSymbol(argument);
	return;
}

void EditorSector::showSector(const std::string& argument)
{
	if(!argument.compare("compact"))
	{
		m_sock->Sendf("%s(%s): %d%s", 
			m_sector->getName().c_str(),
			m_sector->getSymbol().c_str(),
			m_sector->getMoveCost(),
			(m_sector->isWater() ? " (water)\n" : "\n"));
	}
	else
	{
		m_sock->Sendf("Name: '%s'.\n", m_sector->getName().c_str());
		m_sock->Sendf("Symbol: '%s'.\n", m_sector->getSymbol().c_str());
		m_sock->Sendf("Movecost: '%d'.\n", m_sector->getMoveCost());
		if(m_sector->isWater())
			m_sock->Send("Sector is water type.\n");
	}
	return;
}

void EditorSector::saveSector(const std::string& argument)
{
	m_sock->Sendf("Saving sector '%s'.\n", m_sector->getName().c_str());
	m_sector->Save();
	m_sock->Send("Saved.\n");
	return;
}
