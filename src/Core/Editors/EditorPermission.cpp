/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "AccountManager.h"
#include "Array.h"
#include "EditorOLC.h"
#include "EditorPermission.h"
#include "GrantGroup.h"
#include "GrantGroupManager.h"
#include "Managers.h"
#include "Permission.h"
#include "PermissionManager.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

using mud::Permission;

typedef EditorPermission E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//						name		function	  need: object lock
static O editAccount(	"Account",	&E::editAccount,	true, true);
static O editGrantGroup("GrantGroup",&E::editGrantGroup,true, true);
static O editGrant(		"Grant", 	&E::editGrant,		true, true);
static O editLogging(	"Logging",	&E::editLogging,	true, true);
static O showPermission("Show", 	&E::showPermission,	true, false);
static O savePermission("Save", 	&E::savePermission, true, true);

static const B commands[] = {
	B("account", editAccount),
	B("grant", editGrant),
	B("grantgroup", editGrantGroup),
	B("logging", editLogging),
	B("save", savePermission),
	B("show", showPermission),
};

EditorPermission::EditorPermission(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_permission()
{
	listCommands(Global::Get()->EmptyString);
}

EditorPermission::~EditorPermission(void)
{

}

std::string EditorPermission::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const PermissionCommand* act = (PermissionCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorPermission::dispatch(const std::string& action, const std::string& argument)
{
	const PermissionCommand* act = (PermissionCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_permission)
	{
		m_sock->Send("You need to be editing a permission first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_permission->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The permission you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorPermission::getEditing()
{
	return m_permission;
}

TableImplPtr EditorPermission::getTable()
{
	return db::TableImpls::Get()->PERMISSIONS;
}

KeysPtr EditorPermission::addNew()
{
	throw std::runtime_error("EditorPermission::addNew(), Not yet implemented.");
}

std::vector<std::string> EditorPermission::getList()
{
	return mud::Managers::Get()->Permission->List();
}

void EditorPermission::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_permission.reset();
		return;
	}
	
	mud::PermissionPtr permission = mud::Managers::Get()->Permission->GetByKeys(keys);
	m_permission = permission;
	return;
}

std::vector<std::string> EditorPermission::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorPermission::editAccount(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify an account!\n");
		return;
	}

	try
	{
		long id = mud::Managers::Get()->Account->lookupByName(argument);
		m_sock->Sendf("Preparing to edit permission with account '%d'.\n", id);
		m_account = id;
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Sendf("'%s' is not a valid account!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->Account->List(), "Accounts"));
	}
}

void EditorPermission::editGrantGroup(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify a grantgroup!\n");
		return;
	}

	try
	{
		long id = mud::Managers::Get()->GrantGroup->lookupByName(argument);
		m_sock->Sendf("Preparing to edit permission with grangroup '%d'.\n", id);
		m_grantgroup = id;
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Sendf("'%s' is not a valid grantgroup!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->GrantGroup->List(), "GrantGroups"));
	}
}

void EditorPermission::editGrant(const std::string& argument)
{
	if(!argument.compare("Enable"))
	{
		m_permission->setGrant(true);
		return;
	}
	
	if(!argument.compare("Disable"))
	{
		m_permission->setGrant(false);
		return;
	}

	m_sock->Send("Please specify a grant level!\n");
	m_sock->Send("Choose between 'Enable' and 'Disable'.\n");
	return;
}

void EditorPermission::editLogging(const std::string& argument)
{
	if(!argument.compare("Enable"))
	{
		m_permission->setLog(true);
		return;
	}
	
	if(!argument.compare("Disable"))
	{
		m_permission->setLog(false);
		return;
	}
	
	m_sock->Send("Please specify a log level!\n");
	m_sock->Send("Choose bewteen 'Enable' and 'Disable'.\n");
	return;
}

void EditorPermission::savePermission(const std::string& argument)
{
	m_sock->Send("Saving...\n");
	m_permission->Save();
	m_sock->Send("Saved.\n");
	return;
}

void EditorPermission::showPermission(const std::string& argument)
{
	m_sock->Send(m_permission->toString());
}
