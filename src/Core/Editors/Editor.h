/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Editor.h
 * This file contains the Editor class. 
 *
 * @see Editor 
 */ 

#include "SavableTypes.h"
#include "Types.h"

/**
 * This class defines an interface for UBSocket to use in handling lines from the user.
 *
 * @see UBSocket 
 */ 
class Editor
{
public:
	/** Constructs an Editor with the specified socket. */
	Editor(UBSocket* sock);
	
	/** Destructor, a noop. */
	virtual ~Editor(void);


	/** This function is called whenever the user sends an empty line. */
	virtual void OnEmptyLine() {}
	
	/** 
	 * This function is called whenever a user sends a line.
	 *
	 * The default implementation will handle the input and use <code>lookup</code> to find the matching command.
	 * If matched it will dispatch it with the <code>dispatch</code> functions.
	 *
	 * @see lookup
	 * @see dispatch 
	 */ 
	virtual void OnLine(const std::string& line);
	
	/** This function is called whenever this editor becomes the editor that has the focus. */
	virtual void OnFocus() { }
	
	
	/** Returns the name of this editor. */
	virtual std::string name() = 0;
	
	/** Returns the prompt for this editor. */
	virtual std::string prompt(); // { return Global::Get()->EmptyString; };
		
	/** Returns the full name of a command of the specified action. */
	virtual std::string lookup(const std::string& action); // { return Global::Get()->EmptyString; };
	
	/** Dispatches the specified action with the specified argument. */
	virtual void dispatch(const std::string& action, const std::string& argument) { return; };
	
	/** Whether the current editor is capable of receiving messages from the specified channel. */
	virtual bool canReceiveChannel(mud::ChannelPtr channel) const;
	
	/** Whether prefixes should be handled in the current editor. */
	virtual bool supportPrefixes() const;
	
	
	/** Send a message to the user. */
	void Send(const std::string& msg);
	
	/** Send a formatted message to the user. */
	void Sendf(const char* format, ...);
	
	/** Disconnect the user. */
	void Disconnect();

protected:
	UBSocket* m_sock; /**< The socket this Editor is associated with. */
	
private:
	/** Hide the copy constructor. */
	Editor(const Editor& rhs);
};
