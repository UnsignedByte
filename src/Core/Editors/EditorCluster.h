/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "CommandInfoObject.h"
#include "CommandTable.h"
#include "OLCEditor.h"
#include "Types.h"

class EditorCluster : public OLCEditor
{
public:
	typedef CommandInfoObject<EditorCluster> ClusterCommand; /**< The type of a CommandInfoObject for this Editor. */

	EditorCluster(UBSocket* sock);
	EditorCluster(UBSocket* sock, mud::AreaPtr parentArea);
	~EditorCluster(void);
	
	void OnFocus();

	std::string name() { return "Cluster"; };
	std::string prompt() { return "Cluster> "; };
	
	std::string lookup(const std::string& action);
	void dispatch(const std::string& action, const std::string& argument);
	
	SavablePtr getEditing();
	TableImplPtr getTable();
	KeysPtr addNew();
	std::vector<std::string> getList();
	std::vector<std::string> getCommands();
	void setEditing(KeysPtr keys);
	
	void editName(const std::string& argument);	
	void editDescription(const std::string& argument);
	void editArea(const std::string& argument);
	void startRooms(const std::string& argument);
	void showCluster(const std::string& argument);
	void saveCluster(const std::string& argument);
	void clearParentArea(const std::string& argument);
	void setParentArea(const std::string& argument);	

private:
	enum E_TARGET
	{
		M_NONE,
		M_DESCRIPTION,
	};

	CommandTable<EditorCluster> m_commands;
	mud::ClusterPtr m_cluster;
	mud::AreaPtr m_parentArea;
	EditorCluster::E_TARGET m_target;
	std::string m_value;

	EditorCluster(const EditorCluster& rhs);
	EditorCluster operator=(const EditorCluster& rhs);
};
