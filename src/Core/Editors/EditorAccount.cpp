/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "CharacterManager.h"
#include "Command.h"
#include "EditorAccount.h"
#include "EditorNewCharacter.h"
#include "EditorOLC.h"
#include "EditorOOC.h"
#include "EditorPlaying.h"
#include "FieldImpls.h"
#include "Managers.h"
#include "PCharacter.h"
#include "PCharacterManager.h"
#include "Permission.h"
#include "Room.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

extern bool g_quit;

typedef EditorAccount E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//						name		function			fullname
static O listCommands(	"Commands", &E::listCommands);
static O beginLogin(	"Login",	&E::beginLogin);
static O beginOLC(		"OLC",		&E::beginOLC);
static O beginOOC(		"OOC",		&E::beginOOC);
static O beginCreation(	"New",		&E::beginCreation,	true);
static O quitEditor(	"Quit",		&E::quitEditor, 	true);
static O shutdownGame(	"Shutdown",	&E::shutdownGame,	true);
static O listCharacters("List",		&E::listCharacters);

static B commands[] = {
	B("?", listCommands),
	B("list", listCharacters),
	B("login", beginLogin),
	B("new", beginCreation),
	B("olc", beginOLC),
	B("ooc", beginOOC),
	B("quit", quitEditor),
	B("shutdown", shutdownGame),
};

EditorAccount::EditorAccount(UBSocket* sock) :
Editor(sock),
m_commands(commands, array_size(commands))
{
	listCommands(Global::Get()->EmptyString);
}

EditorAccount::~EditorAccount(void)
{

}

std::string EditorAccount::lookup(const std::string& action)
{
	const AccountCommand* act = (AccountCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorAccount::dispatch(const std::string& action, const std::string& argument)
{
	const AccountCommand* act = (AccountCommand*)m_commands.getObject(action);

	Assert(act);
	act->Run(this, argument);
}

void EditorAccount::beginLogin(const std::string &argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please choose a character:\n");
		listCharacters(Global::Get()->EmptyString);
		return;
	}

	if(mud::PCharacterManager::Get()->isActive(argument))
	{
		m_sock->Send("You are already playing that character!\n");
		return;
	}

	int id = 0;

	try
	{
		id = mud::Managers::Get()->Character->lookupByName(argument);
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Send("No such character.\n");
		m_sock->Send("\n");
		beginLogin(Global::Get()->EmptyString);
		return;
	}
	
	KeysPtr keys(new Keys(db::TableImpls::Get()->CHARACTERACCOUNT));
	KeyValuePtr key;
	
	key = KeyValuePtr(new KeyValue(db::TableImpls::Get()->CHARACTERACCOUNT->FKACCOUNTS, id));
	keys->addKey(key);
	key = KeyValuePtr(new KeyValue(db::TableImpls::Get()->CHARACTERACCOUNT->FKENTITIES, m_sock->GetAccount()->getID()));
	keys->addKey(key);
	
	bool hasAccount = false;
	try
	{
		SavableManagerPtr manager = SavableManager::bykeys(keys);
		hasAccount = true;
	} catch(RowNotFoundException& e) { }
	
	if(!hasAccount)
	{
		m_sock->Sendf("You don't have a character named '%s'!\n", argument.c_str());
		beginLogin(Global::Get()->EmptyString);
		return;
	}

	mud::PCharacterPtr ch = mud::PCharacterManager::Get()->LoadByKey(m_sock, id);	
	m_sock->Sendf("Welcome back, %s\n", argument.c_str());
	m_sock->SetEditor(new EditorPlaying(m_sock, ch));
	return;
}

void EditorAccount::listCharacters(const std::string &argument)
{
	Assert(m_sock->hasAccount());
	mud::AccountPtr account = m_sock->GetAccount();
	
	Strings characters = mud::Managers::Get()->Character->List(account);
	
	if(characters.size() == 0)
	{
		m_sock->Send("You have no characters yet!\n");
		return;
	}
	
	m_sock->Send(String::Get()->box(characters, "Characters"));
	m_sock->Send("\n");
	return;
}

void EditorAccount::listCommands(const std::string &argument)
{
	m_sock->Send(String::Get()->box(m_commands.getCommandsVector(), "Account"));
	m_sock->Send("\n");
	return;
}

void EditorAccount::beginOLC(const std::string &argument)
{
	m_sock->Send("Dropping you into OLC mode!\n");
	m_sock->SetEditor(new EditorOLC(m_sock));
	return;
}

void EditorAccount::beginOOC(const std::string& argument)
{
	m_sock->Send("Dropping you into OOC mode!\n");
	m_sock->SetEditor(new EditorOOC(m_sock));
}

void EditorAccount::beginCreation(const std::string &argument)
{
	m_sock->Send("Dropping you into Character Creation mode!\n");
	m_sock->SetEditor(new EditorNewCharacter(m_sock));
	return;
}

void EditorAccount::quitEditor(const std::string &argument)
{
	m_sock->Send("Thank you for visiting.\n");
	m_sock->SetCloseAndDelete();
	return;
}

void EditorAccount::shutdownGame(const std::string &argument)
{
	m_sock->Send("Shutting down...\n");
	g_quit = true;
	m_sock->Send("Game will shut down after this loop!\n");
	return;
}
