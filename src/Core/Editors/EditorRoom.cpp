/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Cluster.h"
#include "ClusterManager.h"
#include "EditorChunk.h"
#include "EditorCluster.h"
#include "EditorDetail.h"
#include "EditorOLC.h"
#include "EditorRoom.h"
#include "EditorString.h"
#include "Managers.h"
#include "Room.h"
#include "RoomManager.h"
#include "Sector.h"
#include "SectorManager.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

using mud::Room;

typedef EditorRoom E;
typedef CommandBinding<E> B;
typedef CommandInfoObject<E> O;

//					 	name		function  need: object lock
static O listClusters	("Clusters",&E::listClusters,false, false, true); // prevent accidental ambiguity with Cluster
static O startDetails	("Details", &E::startDetails);
static O startChunks	("Chunks", &E::startChunks);
static O clearParentCluster("ClearParentCluster", &E::clearParentCluster);
static O setParentCluster("SetParentCluster", &E::setParentCluster);
static O showRoom		("Show",	&E::showRoom,	true, true);
static O saveRoom		("Save",	&E::saveRoom,	true, true);
static O editName		("Name",	&E::editName,	true, true);
static O editDescription("Description",	&E::editDescription, true, true);
static O editSector		("Sector",	&E::editSector,	true, true);
static O editCluster	("Cluster",	&E::editCluster,true, true, true);

static const B commands[] = {
	B("all",		clearParentCluster),
	B("chunks",		startChunks),
	B("cluster",	editCluster),
	B("clusters",	listClusters),
	B("description",editDescription),
	B("details",	startDetails),
	B("filter",		setParentCluster),
	B("name",		editName),
	B("save",		saveRoom),
	B("sector",		editSector),
	B("show",		showRoom),
};

EditorRoom::EditorRoom(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_room(),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
}

EditorRoom::EditorRoom(UBSocket* sock, mud::ClusterPtr parentCluster) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_room(),
m_target(M_NONE),
m_parentCluster(parentCluster)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Rooms that belong to cluster '%d' are shown, to clear this restriction type 'all'.\n", parentCluster->getID());
}

EditorRoom::~EditorRoom(void)
{

}

void EditorRoom::OnFocus()
{		
	switch(m_target)
	{
		case M_NONE:
			return;
			
		case M_DESCRIPTION:
			m_room->setDescription(m_value);
			break;
	}
	
	m_target = M_NONE;
}

std::string EditorRoom::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const RoomCommand* act = (RoomCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorRoom::dispatch(const std::string& action, const std::string& argument)
{
	const RoomCommand* act = (RoomCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
		
	if(!m_room && act->needObject())
	{
		m_sock->Send("You need to be editing a room first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}

	if(act->needLock())
	{
		try {
			m_room->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The room you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorRoom::getEditing()
{
	return m_room;
}

TableImplPtr EditorRoom::getTable()
{
	return db::TableImpls::Get()->ROOMS;
}

KeysPtr EditorRoom::addNew()
{
	return mud::Managers::Get()->Room->Add();
}

std::vector<std::string> EditorRoom::getList()
{
	return mud::Managers::Get()->Room->List(m_parentCluster);
}

void EditorRoom::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_room.reset();
		return;
	}
	
	m_room = mud::Managers::Get()->Room->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorRoom::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorRoom::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Room name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Room name changed from '%s' to '%s'.\n", m_room->getName().c_str(), argument.c_str());
	m_room->setName(argument);
	return;
}

void EditorRoom::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		m_sock->SetEditor(new EditorString(m_sock, m_value));
		m_target = M_DESCRIPTION;
		return;
	}

	m_sock->Sendf("Room description changed from '%s' to '%s'.\n", m_room->getDescription().c_str(), argument.c_str());
	m_room->setDescription(argument);
	return;
}

void EditorRoom::editSector(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify a sector type!\n");
		return;
	}

	try
	{
		long id = mud::Managers::Get()->Sector->lookupByName(argument);
		mud::SectorPtr sector = mud::Managers::Get()->Sector->GetByKey(id);
		m_sock->Sendf("Sector type changed to '%s'.\n", argument.c_str());
		m_room->setSector(id);
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Sendf("'%s' is not a valid sector type!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->Sector->List(), "Sectors"));
	}
}

void EditorRoom::editCluster(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify an cluster!\n");
		return;
	}

	try
	{
		value_type id = atoi(argument.c_str());
		mud::ClusterPtr sector = mud::Managers::Get()->Cluster->GetByKey(id);
		m_sock->Sendf("Cluster changed to '%s'.\n", argument.c_str());
		m_room->setCluster(id);
	}
	catch(RowNotFoundException& e)
	{
		m_sock->Sendf("'%s' is not a valid cluster!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->Cluster->List(), "Clusters"));
	}
}

void EditorRoom::saveRoom(const std::string& argument)
{
	m_sock->Sendf("Saving room '%s'.\n", m_room->getName().c_str());
	if(argument.size())
		m_room->Save(m_sock->GetAccount()->getID(), argument);
	else
		m_room->Save(m_sock->GetAccount()->getID(), Global::Get()->EmptyString);
	m_sock->Send("Saved.\n");
	return;
}

void EditorRoom::showRoom(const std::string& argument)
{
	m_sock->Send(m_room->toFullString());
}

void EditorRoom::listClusters(const std::string& argument)
{
	m_sock->Send(String::Get()->box(mud::Managers::Get()->Cluster->List(), "Clusters"));
}

void EditorRoom::startDetails(const std::string& argument)
{
	m_sock->Send("Dropping you into Detail Edit mode!\n");
	m_sock->SetEditor(new EditorDetail(m_sock));
	return;
}

void EditorRoom::startChunks(const std::string& argument)
{
	if(!argument.compare("."))
	{
		if(!m_room)
		{
			m_sock->Send("You can only use '.' when you are already editing a room.\n");
			m_sock->Send("You may specifiy a room to filter the Chunk Editor on by it's id.\n");
			return;
		}
		
		m_sock->Send("Dropping you into filtered Chunk Edit mode!\n");
		m_sock->SetEditor(new EditorChunk(m_sock, m_room));
		return;
	}
				
	if(argument.size())
	{
		value_type id = atoi(argument.c_str());
		
		try {
			mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(id);
			m_sock->Send("dropping you into filtered Chunk Edit mode!\n");
			m_sock->SetEditor(new EditorChunk(m_sock, room));
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("Unknown room '%s'.\n", argument.c_str());
		}
		
		return;
	}
	
	m_sock->Send("Dropping you into Chunk Edit mode!\n");
	m_sock->SetEditor(new EditorChunk(m_sock));
	return;
}

void EditorRoom::clearParentCluster(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	m_parentCluster.reset();
	return;
}

void EditorRoom::setParentCluster(const std::string& argument)
{
	if(argument.size())
	{
		m_sock->Send("Please provide an cluster to filter on.\n");
		return;
	}
	
	try {
		int id = atoi(argument.c_str());
		mud::ClusterPtr cluster = mud::Managers::Get()->Cluster->GetByKey(id);
		m_parentCluster = cluster;
		m_sock->Send("Ok.\n");
		m_sock->Sendf("Only Rooms that belong to cluster '%d' are shown, to clear this restriction type 'all'.\n", m_parentCluster->getID());
	} catch(RowNotFoundException& e) {
		m_sock->Sendf("'%s' is not an cluster.\n", argument.c_str());
	}	
}
