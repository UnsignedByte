/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "CommandObject.h"
#include "CommandTable.h"
#include "Editor.h"
#include "SavableTypes.h"
#include "Types.h"

class EditorPlaying : public Editor
{
public:
	typedef CommandObject<EditorPlaying> PlayingCommand; /**< The type of a CommandObject for this Editor. */

	EditorPlaying(UBSocket* sock, mud::PCharacterPtr character);
	~EditorPlaying(void);

	std::string name() { return "Playing"; };
	std::string prompt() { return "> "; };
	
	std::string lookup(const std::string& action);
	void dispatch(const std::string& action, const std::string& argument);

	void listCharacters(const std::string& argument);	
	void listCommands(const std::string& argument);
	void listExits(const std::string& argument);
	void showScore(const std::string& argument);
	void look(const std::string& argument);
	void say(const std::string& argument);
	void startMovement(const std::string& argument);
	void deleteCharacter(const std::string& argument);
	void quitEditor(const std::string& argument);

private:
	CommandTable<EditorPlaying> m_commands;
	mud::PCharacterPtr m_char; // current active PCharacter
	
	EditorPlaying(const EditorPlaying& rhs);
	EditorPlaying operator=(const EditorPlaying& rhs);
};
