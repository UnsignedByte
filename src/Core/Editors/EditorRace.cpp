/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "CommandBinding.h"
#include "CommandTable.h"
#include "EditorOLC.h"
#include "EditorRace.h"
#include "Managers.h"
#include "Race.h"
#include "RaceManager.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorRace E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName	("Name",	&E::editName,	true, true);
static O saveRace	("Save",	&E::saveRace,	true, true);
static O showRace	("Show",	&E::showRace,	true, false);

static const B commands[] = {
	B("name",		editName),
	B("save",		saveRace),
	B("show",		showRace),
};

EditorRace::EditorRace(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_race()
{
	listCommands(Global::Get()->EmptyString);
}

EditorRace::~EditorRace(void)
{
}

std::string EditorRace::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const RaceCommand* act = (RaceCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorRace::dispatch(const std::string& action, const std::string& argument)
{
	const RaceCommand* act = (RaceCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_race)
	{
		m_sock->Send("You need to be editing an race first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}

	if(act->needLock()) 
	{
		try {
			m_race->Lock();			
		} catch(SavableLocked& e) {
			m_sock->Send("The race you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}
	
	act->Run(this, argument);		
	return;
}

SavablePtr EditorRace::getEditing()
{
	return m_race;
}

TableImplPtr EditorRace::getTable()
{
	return db::TableImpls::Get()->RACES;
}

KeysPtr EditorRace::addNew()
{
	return mud::Managers::Get()->Race->Add();
}

std::vector<std::string> EditorRace::getList()
{
	return mud::Managers::Get()->Race->List();
}

std::vector<std::string> EditorRace::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorRace::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_race.reset();
		return;
	}
	
	m_race = mud::Managers::Get()->Race->GetByKey(keys->first()->getIntegerValue());
	return;
}

void EditorRace::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Race name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Race name changed from '%s' to '%s'.\n", m_race->getName().c_str(), argument.c_str());
	m_race->setName(argument);
	return;
}

void EditorRace::showRace(const std::string& argument)
{
	m_sock->Send(m_race->toString());
}

void EditorRace::saveRace(const std::string& argument)
{
	m_sock->Sendf("Saving race '%s'.\n", m_race->getName().c_str());
	m_race->Save();
	m_sock->Send("Saved.\n");
	return;
}
