/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "EditorGrantGroup.h"
#include "EditorOLC.h"
#include "GrantGroup.h"
#include "GrantGroupManager.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

using mud::GrantGroup;

typedef EditorGrantGroup E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 	name		function  need: object lock
static O editName(		"Name", 	&E::editName,	true, true);
static O editImplication("Description", &E::editImplication, true, true);
static O showGrantGroup("Show", 	&E::showGrantGroup, true, false);
static O saveGrantGroup("Save", 	&E::saveGrantGroup, true, false);

static const B commands[] = {
	B("implication", editImplication),
	B("name", editName),
	B("save", saveGrantGroup),
	B("show", showGrantGroup),
};

EditorGrantGroup::EditorGrantGroup(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_grantgroup()
{
	listCommands(Global::Get()->EmptyString);
}

EditorGrantGroup::~EditorGrantGroup(void)
{

}

std::string EditorGrantGroup::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const GrantGroupCommand* act = (GrantGroupCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorGrantGroup::dispatch(const std::string& action, const std::string& argument)
{
	const GrantGroupCommand* act = (GrantGroupCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_grantgroup)
	{
		m_sock->Send("You need to be editing a grantgroup first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_grantgroup->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The grantgroup you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorGrantGroup::getEditing()
{
	return m_grantgroup;
}

TableImplPtr EditorGrantGroup::getTable()
{
	return db::TableImpls::Get()->GRANTGROUPS;
}

KeysPtr EditorGrantGroup::addNew()
{
	return mud::Managers::Get()->GrantGroup->Add();
}

std::vector<std::string> EditorGrantGroup::getList()
{
	return mud::Managers::Get()->GrantGroup->List();
}

void EditorGrantGroup::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_grantgroup.reset();
		return;
	}
	
	m_grantgroup = mud::Managers::Get()->GrantGroup->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorGrantGroup::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorGrantGroup::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("GrantGroup name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("GrantGroup name changed from '%s' to '%s'.\n", m_grantgroup->getName().c_str(), argument.c_str());
	m_grantgroup->setName(argument);
	return;
}

void EditorGrantGroup::editImplication(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify the implicated grantgroup!\n");
		return;
	}

	int implication = atoi(argument.c_str());
	if(implication <= 0)
	{
		m_sock->Sendf("Please specify an implication > 0!\n");
		return;
	}

	m_sock->Sendf("GrantGroup implication changed from '%d' to '%d'.\n", m_grantgroup->getImplication(), implication);
	m_grantgroup->setImplication(implication);
}

void EditorGrantGroup::showGrantGroup(const std::string& argument)
{
	m_sock->Send(m_grantgroup->toString());
	return;
}

void EditorGrantGroup::saveGrantGroup(const std::string& argument)
{
	m_sock->Sendf("Saving grantgroup '%s'.\n", m_grantgroup->getName().c_str());
	m_grantgroup->Save();
	m_sock->Send("Saved.\n");
	return;
}
