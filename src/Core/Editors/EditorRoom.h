/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "CommandInfoObject.h"
#include "CommandTable.h"
#include "OLCEditor.h"
#include "SavableTypes.h"
#include "Types.h"

class EditorRoom : public OLCEditor
{
public:
	typedef CommandInfoObject<EditorRoom> RoomCommand; /**< The type of a CommandInfoObject for this Editor. */
	
	EditorRoom(UBSocket* sock);
	EditorRoom(UBSocket* sock, mud::ClusterPtr parentRoom);
	~EditorRoom(void);
	
	void OnFocus();

	std::string name() { return "Room"; };
	std::string prompt() { return "Room> "; };
	
	std::string lookup(const std::string& action);
	void dispatch(const std::string& action, const std::string& argument);
	
	SavablePtr getEditing();
	TableImplPtr getTable();
	KeysPtr addNew();
	std::vector<std::string> getList();
	std::vector<std::string> getCommands();
	void setEditing(KeysPtr keys);
	
	void editName(const std::string& argument);
	void editDescription(const std::string& argument);
	void editSector(const std::string& argument);
	void editCluster(const std::string& argument);
	void clearParentCluster(const std::string& argument);
	void setParentCluster(const std::string& argument);
	
	void showRoom(const std::string& argument);
	void saveRoom(const std::string& argument);
	void startDetails(const std::string& argument);
	void startChunks(const std::string& argument);
	void listClusters(const std::string& argument);

private:
	EditorRoom(const EditorRoom& rhs);
	EditorRoom operator=(const EditorRoom& rhs);

private:
	CommandTable<EditorRoom> m_commands;	

	enum E_TARGET
	{
		M_NONE,
		M_DESCRIPTION,
	};

	mud::RoomPtr m_room;
	std::string m_value;
	EditorRoom::E_TARGET m_target;
	mud::ClusterPtr m_parentCluster;
};
