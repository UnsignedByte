/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "CommandInfoObject.h"
#include "CommandTable.h"
#include "OLCEditor.h"
#include "SavableTypes.h"
#include "Types.h"

class EditorDetail : public OLCEditor
{
public:
	typedef CommandInfoObject<EditorDetail> DetailCommand; /**< The type of a CommandInfoObject for this Editor. */

	EditorDetail(UBSocket* sock);
	EditorDetail(UBSocket* sock, mud::DetailPtr detail);
	EditorDetail(UBSocket* sock, mud::AreaPtr parentArea);
	EditorDetail(UBSocket* sock, mud::RoomPtr parentRoom);
	EditorDetail(UBSocket* sock, mud::ChunkPtr parentChunk);
	~EditorDetail(void);
	
	void OnFocus();

	std::string name() { return "Detail"; };
	std::string prompt() { return "Detail> "; };
	
	std::string lookup(const std::string& action);
	void dispatch(const std::string& action, const std::string& argument);
	
	SavablePtr getEditing();
	TableImplPtr getTable();
	KeysPtr addNew();
	std::vector<std::string> getList();
	std::vector<std::string> getCommands();
	void setEditing(KeysPtr keys);
	
	void editKey(const std::string& argument);
	void editDescription(const std::string& argument);
	void showDetail(const std::string& argument);
	void saveDetail(const std::string& argument);
	void clearParents(const std::string& argument);
	void setParent(const std::string& argument);

private:
	enum E_TARGET
	{
		M_NONE,
		M_DESCRIPTION,
	};
	
	CommandTable<EditorDetail> m_commands;
	mud::DetailPtr m_detail;
	mud::AreaPtr m_parentArea;
	mud::RoomPtr m_parentRoom;
	mud::ChunkPtr m_parentChunk;
	
	std::string m_value;
	EditorDetail::E_TARGET m_target;
	
	EditorDetail(const EditorDetail& rhs);
	EditorDetail operator=(const EditorDetail& rhs);
};
