/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Assert.h"
#include "Command.h"
#include "DatabaseMgr.h"
#include "EditorOLC.h"
#include "OLCEditor.h"
#include "Permission.h"
#include "Savable.h"
#include "StringUtilities.h"
#include "UBSocket.h"

typedef OLCEditor E;
typedef CommandObject<E> O;
typedef CommandBinding<E> B;

static O listCommands("Commands", &E::listCommands);
static O newSavable("New", &E::newSavable);
static O deleteSavable("Delete", &E::deleteSavable);
static O editSavable("Edit", &E::editSavable);
static O quitEditor("Quit", &E::quitEditor);
static O doneEditing("Done", &E::doneEditing);
static O discardChanges("Discard", &E::discardChanges);
static O listSavable("List", &E::listSavable);	

static const B commands[] = {
	B("?", listCommands),
	B("discard", discardChanges),
	B("done", doneEditing),
	B("edit", editSavable),
	B("erase", deleteSavable),
	B("list", listSavable),
	B("new", newSavable),
	B("quit", quitEditor),
};

OLCEditor::OLCEditor(UBSocket* sock) : 
Editor(sock),
m_olccommands(commands, array_size(commands))
{
	
}

OLCEditor::~OLCEditor()
{
	
}

std::string OLCEditor::lookup(const std::string& action)
{
	const SavableCommand* act = m_olccommands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void OLCEditor::dispatch(const std::string& action, const std::string& argument)
{
	const SavableCommand* act = m_olccommands.getObject(action);
	
	Assert(act);
	act->Run(this, argument);
}

void OLCEditor::newSavable(const std::string& argument)
{
	KeysPtr keys = addNew();
	if(!keys->size())
	{
		Sendf("Could not create a new %s!\n", name().c_str());
		return;
	}

	Sendf("%s created!\n", name().c_str());
	setEditing(keys);
	return;
}

void OLCEditor::editSavable(const std::string& argument)
{
	TableImplPtr table = getTable();
	
	if(argument.size() == 0)
	{
		Send("Please specify an id to edit.\n");
		Send("Type 'list' to see a list of available id's.\n");
		Sendf("Type 'new' to create a new %s!\n", name().c_str());
		return;
	}
	
	KeysPtr keys;
	
	try {
		keys = KeysPtr(new Keys(table, argument));
	} catch(std::invalid_argument& e)
	{
		Send("Please specify an id to edit.\n");
		Sendf("'%s' is not a valid id.\n", argument.c_str());
		return;
	}
	
	int count = SavableManager::count(keys);

	if(count < 1)
	{
		Send("Please specify an id to edit.\n");
		Sendf("'%s' does not exist!\n", argument.c_str());
		return;
	}

	Assert(count == 1);	

	setEditing(keys);
	m_sock->Sendf("You are now editing id %s> ", argument.c_str());
	m_sock->Send(getEditing()->toString());
	m_sock->Send("\n");
	return;
}

void OLCEditor::deleteSavable(const std::string& argument)
{
	if(argument.size() == 0)
	{
		Send("Please specify an id to delete.\n");
		Send("Type 'list' to see a list of available id's.\n");
		return;
	}

	KeysPtr keys;

	try
	{
		keys = KeysPtr(new Keys(getTable(), argument));
	}
	catch(std::invalid_argument& e)
	{
		Send("Please specify an id to edit.\n");
		Sendf("'%s' is not a valid id.\n", argument.c_str());
	}

	int count = SavableManager::count(keys);

	if(count < 1)
	{
		Send("Please specify an id to delete.\n");
		Sendf("'%s' does not exist!\n", argument.c_str());
		return;
	}

	Assert(count == 1);	

	SavableManagerPtr manager = SavableManager::bykeys(keys);
	bool locked = manager->lock();
	
	if(!locked)
	{
		m_sock->Sendf("Could not lock '%s', it is being edited by someone else!\n", argument.c_str());
		return;
	}
	
	manager->erase();
	manager->unlock();

	m_sock->Sendf("Deleted id %s.\n", keys->toString().c_str());
	m_sock->Send("\n");
	return;
}

void OLCEditor::listSavable(const std::string& argument)
{
	std::string name = this->name();
	name.append("s");
	Strings list = getList();
	if(list.size())
		m_sock->Send(String::Get()->box(list,name));
	else
		m_sock->Sendf("No %ss.\n", this->name().c_str());
	return;
}

void OLCEditor::listCommands(const std::string& argument)
{
	m_sock->Send(String::Get()->box(m_olccommands.getCommandsVector(), name()));
	m_sock->Send("\n");
	std::string name = this->name();
	name.append("Editing");
	m_sock->Send(String::Get()->box(getCommands(), name));
	m_sock->Send("\n");
	return;
}

void OLCEditor::quitEditor(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	SavablePtr editing = getEditing();
	
	if(editing)
	{
		try {
			editing->Save();
		} catch(SavableLocked& e) {
			// if they don't have a lock, they didn't edit anything anyway
			// so no need to save or notify them
		}
	}
	
	m_sock->PopEditor();
	return;
}

void OLCEditor::doneEditing(const std::string& argument)
{		
	SavablePtr editing = getEditing();
	
	if(editing)
	{
		m_sock->Send("Ok.\n");
		
		try {
			editing->Save();			
		} catch(SavableLocked& e) {
			// if they don't have a lock, they didn't edit anything anyway
			// so no need to save or notify them
		}
	} else {
		m_sock->Send("You are not editing anything, so no need to type 'done'.\n");
	}

	TableImplPtr table = getTable();
	Assert(table);

	KeysPtr keys(new Keys(table));
	setEditing(keys);
	return;
}

void OLCEditor::discardChanges(const std::string& argument)
{
	if(argument.compare("confirm"))
	{
		m_sock->Send("Please type 'discard confirm' to discard your changes.\n");
		return;
	}
	
	m_sock->Send("Ok.\n");
	SavablePtr editing = getEditing();
	
	if(editing)
	{
		try {
			editing->Lock();
			editing->Discard();
		} catch(SavableLocked& e) {
			m_sock->Send("You cannot discard all changes as you are not the one editing!\n");
		}
	} else {
		m_sock->Send("You're not editing anything!\n");
	}
		
	return;
}
