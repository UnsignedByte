/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Area.h"
#include "AreaManager.h"
#include "Array.h"
#include "Cluster.h"
#include "ClusterManager.h"
#include "EditorCluster.h"
#include "EditorRoom.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorCluster E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName(	"Name",		&E::editName,	true, true);
static O editDescription("Description", &E::editDescription, true, true);
static O editArea(	"Area",		&E::editArea,	true, true);
static O showCluster(	"Show",		&E::showCluster,	true, false);
static O saveCluster(	"Save",		&E::saveCluster,	true, true);
static O startRooms("Rooms",&E::startRooms);
static O clearParentArea("ClearParentArea", &E::clearParentArea);
static O setParentArea("SetParentArea", &E::setParentArea);

static const B commands[] = {
	B("all", clearParentArea),
	B("area", editArea),
	B("description", editDescription),	
	B("filter", setParentArea),
	B("name", editName),	
	B("rooms", startRooms),
	B("save", saveCluster),
	B("show", showCluster),
};

EditorCluster::EditorCluster(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_cluster(),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
}

EditorCluster::EditorCluster(UBSocket* sock, mud::AreaPtr parentArea) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_cluster(),
m_parentArea(parentArea),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Clusters that belong to area '%d' are shown, to clear this restriction type 'all'.\n", parentArea->getID());
}

EditorCluster::~EditorCluster(void)
{

}

void EditorCluster::OnFocus()
{		
	switch(m_target)
	{
		case M_NONE:
			return;
			
		case M_DESCRIPTION:			
			editDescription(m_value);
			break;
	}
	
	m_target = M_NONE;
}

std::string EditorCluster::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const ClusterCommand* act = (ClusterCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorCluster::dispatch(const std::string& action, const std::string& argument)
{
	const ClusterCommand* act = (ClusterCommand*)m_commands.getObject(action);
	
	if(!act) 
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_cluster)
	{
		m_sock->Send("You need to be editing a cluster first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_cluster->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The cluster you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorCluster::getEditing()
{
	return m_cluster;
}

TableImplPtr EditorCluster::getTable()
{
	return db::TableImpls::Get()->CHUNKS;
}

KeysPtr EditorCluster::addNew()
{
	return mud::Managers::Get()->Cluster->Add();
}

std::vector<std::string> EditorCluster::getList()
{
	return mud::Managers::Get()->Cluster->List(m_parentArea);
}

void EditorCluster::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_cluster.reset();
		return;
	}
	
	m_cluster = mud::Managers::Get()->Cluster->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorCluster::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorCluster::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Cluster name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Cluster name changed from '%s' to '%s'.\n", m_cluster->getName().c_str(), argument.c_str());
	m_cluster->setName(argument);
	return;
}

void EditorCluster::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		return;
	}

	m_sock->Sendf("Cluster description changed from '%s' to '%s'.\n", m_cluster->getDescription().c_str(), argument.c_str());
	m_cluster->setDescription(argument);
	return;
}

void EditorCluster::editArea(const std::string& argument)
{	
	int id = atoi(argument.c_str());
	if(id <= 0)
	{
		m_sock->Send("Please specify a area this Cluster belongs to.\n");
	}
	
	std::string oldname;
	try {
		mud::AreaPtr oldarea = mud::Managers::Get()->Area->GetByKey(m_cluster->getArea());
		oldname = oldarea->getName();
	} catch(RowNotFoundException& e) {
		oldname = String::Get()->fromInt(m_cluster->getArea());
	}
	
	try
	{
		mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(id);
		m_sock->Sendf("Area changed from '%s' to '%s'.\n", oldname.c_str(), area->getName().c_str());
		m_cluster->setArea(id);
	}
	catch(RowNotFoundException& e) 
	{
		m_sock->Sendf("'%s' is not a valid area!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->Area->List(), "Areas"));
		return;
	}
}

void EditorCluster::startRooms(const std::string& argument)
{
	if(!argument.compare("."))
	{
		if(!m_cluster)
		{
			m_sock->Send("You can only use '.' when you are already editing a cluster.\n");
			m_sock->Send("You may specifiy a cluster to filter the Room Editor on by it's id.\n");
			return;
		}
		
		m_sock->Send("Dropping you into filtered Room Edit mode!\n");
		m_sock->SetEditor(new EditorRoom(m_sock, m_cluster));
		return;
	}
				
	if(argument.size())
	{
		value_type id = atoi(argument.c_str());
		
		try {
			mud::ClusterPtr cluster = mud::Managers::Get()->Cluster->GetByKey(id);
			m_sock->Send("Dropping you into filtered Room Edit mode!\n");
			m_sock->SetEditor(new EditorRoom(m_sock, cluster));
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("Unknown cluster '%s'.\n", argument.c_str());
		}
		
		return;
	}
	
	m_sock->Send("Dropping you into Room Edit mode!\n");
	m_sock->SetEditor(new EditorRoom(m_sock));
	return;
}

void EditorCluster::showCluster(const std::string& argument)
{
	m_sock->Send(m_cluster->toString());
}

void EditorCluster::saveCluster(const std::string& argument)
{
	m_sock->Sendf("Saving cluster '%s'.\n", m_cluster->getName().c_str());
	m_cluster->Save();
	m_sock->Send("Saved.\n");
	return;
}

void EditorCluster::clearParentArea(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	m_parentArea.reset();
	return;
}

void EditorCluster::setParentArea(const std::string& argument)
{
	if(argument.size())
	{
		m_sock->Send("Please provide a area to filter on.\n");
		return;
	}
	
	try {
		int id = atoi(argument.c_str());
		mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(id);
		m_parentArea = area;
		m_sock->Send("Ok.\n");
		m_sock->Sendf("Only Clusters that belong to area '%d' are shown, to clear this restriction type 'all'.\n", m_parentArea->getID());
	} catch(RowNotFoundException& e) {
		m_sock->Sendf("'%s' is not a area.\n", argument.c_str());
	}	
}
