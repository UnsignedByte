/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Area.h"
#include "AreaManager.h"
#include "Array.h"
#include "EditorArea.h"
#include "EditorCluster.h"
#include "EditorDetail.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorArea E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName(	"Name", 	&E::editName, 	true, true);
static O editDescription("Description",&E::editDescription,true, true);
static O editHeight("Height", 	&E::editHeight,	true, true);
static O editWidth(	"Width", 	&E::editWidth,	true, true);
static O showArea(	"Show", 	&E::showArea,	true, false);
static O saveArea(	"Save", 	&E::saveArea,	true, true);
static O startClusters("Clusters", &E::startClusters);
static O startDetails("Details",&E::startDetails);

static B commands[] = {
	B("clusters", startClusters),
	B("description", editDescription),
	B("details", startDetails),
	B("height", editHeight),
	B("name", editName),	
	B("save", saveArea),
	B("show", showArea),
	B("width", editWidth),
};


EditorArea::EditorArea(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_area()
{
	listCommands(Global::Get()->EmptyString);
}

EditorArea::~EditorArea(void)
{

}

std::string EditorArea::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const AreaCommand* act = (AreaCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorArea::dispatch(const std::string& action, const std::string& argument)
{
	const AreaCommand* act = (AreaCommand*)m_commands.getObject(action);
	
	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_area)
	{
		m_sock->Send("You need to be editing an area first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_area->Lock();			
		} catch(SavableLocked& e) {
			m_sock->Send("The area you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}
	
	act->Run(this, argument);		
	return;
}

SavablePtr EditorArea::getEditing()
{
	return m_area;
}

TableImplPtr EditorArea::getTable()
{
	return db::TableImpls::Get()->AREAS;
}

KeysPtr EditorArea::addNew()
{
	return mud::Managers::Get()->Area->Add();
}

std::vector<std::string> EditorArea::getList()
{
	return mud::Managers::Get()->Area->List();
}

void EditorArea::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_area.reset();
		return;
	}
	
	m_area = mud::Managers::Get()->Area->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorArea::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorArea::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Area name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Area name changed from '%s' to '%s'.\n", m_area->getName().c_str(), argument.c_str());
	m_area->setName(argument);
	return;
}

void EditorArea::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		return;
	}

	m_sock->Sendf("Area description changed from '%s' to '%s'.\n", m_area->getDescription().c_str(), argument.c_str());
	m_area->setDescription(argument);
	return;
}

void EditorArea::editHeight(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify the area's height!\n");
		return;
	}

	int height = atoi(argument.c_str());
	if(height <= 0)
	{
		m_sock->Sendf("Please specify a height > 0!\n");
		return;
	}

	m_sock->Sendf("Area height changed from '%d' to '%d'.\n", m_area->getHeight(), height);
	m_area->setHeight(height);
	return;
}

void EditorArea::editWidth(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Please specify the area's width!\n");
		return;
	}

	int width = atoi(argument.c_str());
	if(width <= 0)
	{
		m_sock->Sendf("Please specify a width > 0!\n");
		return;
	}

	m_sock->Sendf("Area width changed from '%d' to '%d'.\n", m_area->getWidth(), width);
	m_area->setWidth(width);
	return;
}

void EditorArea::showArea(const std::string& argument)
{
	m_sock->Send(m_area->toString());
}

void EditorArea::saveArea(const std::string& argument)
{
	m_sock->Sendf("Saving area '%s'.\n", m_area->getName().c_str());
	m_area->Save();
	m_sock->Send("Saved.\n");
	return;
}

void EditorArea::startClusters(const std::string& argument)
{
	if(!argument.compare("."))
	{
		if(!m_area)
		{
			m_sock->Send("You can only use '.' when you are already editing an area.\n");
			m_sock->Send("You may specifiy an area to filter the Cluster Editor on by it's id.\n");
			return;
		}
		
		m_sock->Send("Dropping you into filtered Cluster Edit mode!\n");
		m_sock->SetEditor(new EditorCluster(m_sock, m_area));
		return;
	}
				
	if(argument.size())
	{
		value_type id = atoi(argument.c_str());
		
		try {
			mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(id);
			m_sock->Send("dropping you into filtered Cluster Edit mode!\n");
			m_sock->SetEditor(new EditorCluster(m_sock, area));
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("Unknown cluster '%s'.\n", argument.c_str());
		}
		
		return;
	}
	
	m_sock->Send("Dropping you into Cluster Edit mode!\n");
	m_sock->SetEditor(new EditorCluster(m_sock));
	return;
}

void EditorArea::startDetails(const std::string& argument)
{
	m_sock->Send("Dropping you into Detail Edit mode!\n");
	m_sock->SetEditor(new EditorDetail(m_sock));
	return;
}
