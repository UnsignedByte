/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "EditorSocial.h"
#include "Managers.h"
#include "Parse.h"
#include "PCharacter.h"
#include "Social.h"
#include "SocialManager.h"
#include "StringUtilities.h"
#include "UBSocket.h"

using mud::PCharacter;

EditorSocial::EditorSocial(UBSocket* sock, mud::PCharacterPtr character) :
Editor(sock),
m_char(character)
{
	Assert(character);
}

EditorSocial::EditorSocial(UBSocket* sock, mud::PCharacterPtr character, cstring line) :
Editor(sock),
m_char(character),
m_line(line)
{
	Assert(character);
}

EditorSocial::~EditorSocial(void)
{
	
}

void EditorSocial::OnLine(const std::string& str)
{	
	if(!str.compare("quit"))
	{
		m_sock->Send("Ok.\n");
		m_sock->PopEditor();
		return;
	}
	
	std::string emote;
	
	if(str[0] != Global::Get()->SocialIdentifier)
	{	
		Parse p(str);
		std::string rawaction = p.getword();
		std::string action = String::Get()->tolower(rawaction);	
		
		mud::SocialPtr social;
		
		try {
			social = mud::Managers::Get()->Social->GetByName(action);
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("'%s' is not a valid social, if you want to emote start out with a '%c'.\n", action.c_str(), Global::Get()->SocialIdentifier);
			return;
		}
		
		emote = social->getMessage();
	}
		
	value_type chunkid = m_char->getChunk();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	
	std::string message = m_char->getName();
	
	message.append(" ");
	message.append(emote);
	
	chunk->Send(message);
}

void EditorSocial::OnFocus()
{		
	if(m_line.size())
	{	
		if(m_line.compare("quit"))
			OnLine(m_line);
			
		m_sock->PopEditor();
	}
}

void EditorSocial::OnEmptyLine()
{
	m_sock->Send("Ok.\n");
	m_sock->PopEditor();
	return;
}
