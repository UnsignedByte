/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Assert.h"
#include "Character.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "DirectionParser.h"
#include "EditorAccount.h"
#include "EditorMovement.h"
#include "Exit.h"
#include "ExitManager.h"
#include "Managers.h"
#include "PCharacter.h"
#include "PCharacterManager.h"
#include "StringUtilities.h"
#include "UBSocket.h"

using mud::PCharacter;

EditorMovement::EditorMovement(UBSocket* sock, mud::PCharacterPtr character) :
Editor(sock),
m_char(character)
{
	Assert(character);
}

EditorMovement::EditorMovement(UBSocket* sock, mud::PCharacterPtr character, cstring line) :
Editor(sock),
m_char(character),
m_line(line)
{
	Assert(character);
}

EditorMovement::~EditorMovement(void)
{
	
}

void EditorMovement::OnLine(const std::string& str)
{	
	if(!str.compare("quit"))
	{
		m_sock->Send("Ok.\n");
		m_sock->PopEditor();
		return;
	}
	
	DirectionParser p(str);
	Coordinates result = p.getResult();
	
	if(!result.size())
	{		
		m_sock->Sendf("'%s' is not a valid direction.\n", str.c_str());
		return;
	}
	
	for(Coordinates::const_iterator it = result.begin(); it != result.end(); it++)
	{
		Coordinate coordinate = *it;	
		Assert(coordinate.isDirection());
		
		value_type chunkid = m_char->getChunk();
		mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
		Assert(chunk);
		
		value_type exitid = chunk->getExitAt(coordinate);
		
		if(!exitid)
		{
			m_sock->Sendf("There is no exit to the %s.\n", coordinate.toDirectionString().c_str());
			return;
		} else {
			m_sock->Sendf("Going %s.\n", coordinate.toDirectionString().c_str());
		}
		
		mud::ExitPtr exit = mud::Managers::Get()->Exit->GetByKey(exitid);
		Assert(exit->getFromChunk() == chunkid);
		
		value_type tochunkid = exit->getToChunk();
		Assert(tochunkid);
		
		m_char->MoveToChunk(tochunkid);
	}
}

void EditorMovement::OnFocus()
{		
	if(m_line.size())
	{	
		if(m_line.compare("quit"))
			OnLine(m_line);
			
		m_sock->PopEditor();
	}
}

void EditorMovement::OnEmptyLine()
{
	m_sock->Send("Ok.\n");
	m_sock->PopEditor();
	return;
}
