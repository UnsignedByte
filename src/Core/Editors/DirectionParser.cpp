/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <boost/spirit/core.hpp>
#include <boost/spirit/symbols/symbols.hpp>
#include <boost/spirit/utility/lists.hpp>

#include "DirectionParser.h"

using namespace boost::spirit;

struct add_dir
{
    add_dir(Coordinate& coordinate, Coordinate rhs) : 
	m_coordinate(coordinate), 
	m_rhs(rhs) 
	{

	}
	
    void operator()(const char&) const 
	{ 
		m_coordinate += m_rhs; 
	}
    Coordinate& m_coordinate;
	Coordinate m_rhs;
};

struct add_and_reset
{
    add_and_reset(std::vector<Coordinate>& coordinates, Coordinate& coordinate) : 
	m_coordinates(coordinates), 
	m_coordinate(coordinate) 
	{ 

	}
	
    void operator()(char const*, char const*) const {
		Coordinate copy = m_coordinate;
		m_coordinates.push_back(copy);
		m_coordinate = m_coordinate - copy;
	}
	
	std::vector<Coordinate>& m_coordinates;
	Coordinate& m_coordinate;
};


struct direction : public grammar<direction>
{
    template <typename ScannerT>
    struct definition
    {
        definition(direction const& self)
        {
			// Create the rule that will match a directional component
			first = 
				list_p(
							!(
								ch_p('n')[add_dir(self.m_coordinate, Coordinate(1,	0, 0))] |
								ch_p('s')[add_dir(self.m_coordinate, Coordinate(-1,	0, 0))]
							) >>
							
							!(
								ch_p('w')[add_dir(self.m_coordinate, Coordinate(0, -1,	0))] |
								ch_p('e')[add_dir(self.m_coordinate, Coordinate(0, 1,	0))]
							) >>
							
							!(
								ch_p('u')[add_dir(self.m_coordinate, Coordinate(0, 0, 1	))] |
								ch_p('d')[add_dir(self.m_coordinate, Coordinate(0, 0, -1))]
							) >>
									
							eps_p[add_and_reset(self.m_coordinates, self.m_coordinate)],
							
						ch_p(';')
					);
        }

        rule<ScannerT> first;
        rule<ScannerT> const& start() const { return first; }
    };

    direction(std::vector<Coordinate>& coordinates, Coordinate& coordinate) : 
	m_coordinate(coordinate),
	m_coordinates(coordinates)
	{
		
	}
    Coordinate& m_coordinate;
	std::vector<Coordinate>& m_coordinates;
};

void DirectionParser::parseDirections()
{	
	Coordinate coordinate(0,0,0);
	direction direction_p(m_result, coordinate);
	
	bool match = parse(m_directions.c_str(), direction_p).full;
	if(!match)
		m_result.clear();
		
	size_t i = 0;
	bool good = true;
	while(good)
	{
		if(i >= m_result.size())
			break;
		
		if(m_result[i].isNullVector())
			m_result.erase(m_result.begin()+i);
		else
			i++;
	}
}

DirectionParser::DirectionParser(cstring directions) :
m_directions(directions)
{
	parseDirections();
}

DirectionParser::~DirectionParser()
{
	
}
