/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Assert.h"
#include "EditorAccount.h"
#include "EditorArea.h"
#include "EditorChannel.h"
#include "EditorChunk.h"
#include "EditorColour.h"
#include "EditorCommand.h"
#include "EditorDetail.h"
#include "EditorMobile.h"
#include "EditorOLC.h"
#include "EditorOLC.h"
#include "EditorRace.h"
#include "EditorRoom.h"
#include "EditorSector.h"
#include "StringUtilities.h"
#include "UBSocket.h"

typedef EditorOLC E;
typedef CommandObject<E> O;
typedef CommandBinding<E> B;

static O startAreas("Areas", &E::startAreas);
static O startChannels("Channels", &E::startChannels);
static O startChunks("Chunks", &E::startChunks);
static O startColours("Colours", &E::startColours);
static O startCommands("Commands", &E::startCommands);
static O startDetails("Details", &E::startDetails);
static O startMobiles("Mobiles", &E::startMobiles);
static O startRaces("Races", &E::startRaces);
static O startRooms("Rooms", &E::startRooms);
static O startScripts("Scripts", &E::startScripts);
static O startSectors("Sectors", &E::startSectors);
static O listCommands("?", &E::listCommands);
static O quitEditor("Quit", &E::quitEditor);

static B commands[] = {
	B("?", listCommands),
	B("areas", startAreas),
	B("channels", startChannels),
	B("chunks", startChunks),
	B("colour", startColours),
	B("commands", startCommands),
	B("details", startDetails),
	B("mobiles", startMobiles),
	B("quit", quitEditor),
	B("races", startRaces),
	B("rooms", startRooms),
	B("scripts", startScripts),
	B("sectors", startSectors),
};

EditorOLC::EditorOLC(UBSocket* sock) :
Editor(sock),
m_commands(commands, array_size(commands))
{
	listCommands(Global::Get()->EmptyString);
}

EditorOLC::~EditorOLC(void)
{

}

std::string EditorOLC::lookup(const std::string& action)
{
	const OLCCommand* act = m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorOLC::dispatch(const std::string& action, const std::string& argument)
{
	const OLCCommand* act = m_commands.getObject(action);

	Assert(act);
	act->Run(this, argument);
}

void EditorOLC::startAreas(const std::string& argument)
{
	m_sock->Send("Dropping you into Area Edit mode!\n");
	m_sock->SetEditor(new EditorArea(m_sock));
	return;
}

void EditorOLC::startRooms(const std::string& argument)
{
	m_sock->Send("Dropping you into Room Edit mode!\n");
	m_sock->SetEditor(new EditorRoom(m_sock));
	return;
}

void EditorOLC::startMobiles(const std::string& argument)
{
	m_sock->Send("Dropping you into Mobile Edit mode!\n");
	m_sock->SetEditor(new EditorMobile(m_sock));
	return;
}

void EditorOLC::startSectors(const std::string& argument)
{
	m_sock->Send("Dropping you into Sector Edit mode!\n");
	m_sock->SetEditor(new EditorSector(m_sock));
	return;
}

void EditorOLC::startColours(const std::string& argument)
{
	m_sock->Send("Dropping you into Colours Edit mode!\n");
	m_sock->SetEditor(new EditorColour(m_sock));
	return;
}

void EditorOLC::startCommands(const std::string& argument)
{
	m_sock->Send("Dropping you into Command Edit mode!\n");
	m_sock->SetEditor(new EditorCommand(m_sock));
	return;
}

void EditorOLC::startChunks(const std::string& argument)
{
	m_sock->Send("Dropping you into Chunk Edit mode!\n");
	m_sock->SetEditor(new EditorChunk(m_sock));
	return;
}

void EditorOLC::startRaces(const std::string& argument)
{
	m_sock->Send("Dropping you into Race Edit mode!\n");
	m_sock->SetEditor(new EditorRace(m_sock));
	return;
}

void EditorOLC::startDetails(const std::string& argument)
{
	m_sock->Send("Dropping you into Detail Edit mode!\n");
	m_sock->SetEditor(new EditorDetail(m_sock));
	return;
}

void EditorOLC::startChannels(const std::string& argument)
{
	m_sock->Send("Dropping you into Channel Edit mode!\n");
	m_sock->SetEditor(new EditorChannel(m_sock));
	return;
}

void EditorOLC::startScripts(const std::string& argument)
{
	m_sock->Send("Dropping you into Script Edit mode!\n");
	m_sock->Send("Scripting is not yet implemented, sorry.\n");
	// m_sock->SetEditor(new EditorScript(m_sock));
	return;
}

void EditorOLC::listCommands(const std::string& argument)
{
	m_sock->Send(String::Get()->box(m_commands.getCommandsVector(), "OLC"));
	m_sock->Send("\n");
	return;
}

void EditorOLC::quitEditor(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	// m_sock->SetEditor(new EditorAccount(m_sock));
	m_sock->PopEditor();
	return;
}
