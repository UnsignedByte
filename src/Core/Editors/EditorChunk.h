/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "CommandInfoObject.h"
#include "CommandTable.h"
#include "OLCEditor.h"
#include "SavableTypes.h"
#include "Types.h"

class ChunkImporter;
typedef SmartPtr<ChunkImporter> ChunkImporterPtr;

class EditorChunk : public OLCEditor
{
public:
	typedef CommandInfoObject<EditorChunk> ChunkCommand; /**< The type of a CommandInfoObject for this Editor. */

	EditorChunk(UBSocket* sock);
	EditorChunk(UBSocket* sock, mud::RoomPtr parentRoom);
	~EditorChunk(void);
	
	void OnFocus();

	std::string name() { return "Chunk"; };
	std::string prompt() { return "Chunk> "; };
	
	std::string lookup(const std::string& action);
	void dispatch(const std::string& action, const std::string& argument);
	
	SavablePtr getEditing();
	TableImplPtr getTable();
	KeysPtr addNew();
	std::vector<std::string> getList();
	std::vector<std::string> getCommands();
	void setEditing(KeysPtr keys);
	
	void editName(const std::string& argument);
	void editDescription(const std::string& argument);
	void editTags(const std::string& argument);
	void editRoom(const std::string& argument);
	void startDetails(const std::string& argument);
	void importChunk(const std::string& argument);
	void showChunk(const std::string& argument);
	void saveChunk(const std::string& argument);
	void clearParentRoom(const std::string& argument);
	void setParentRoom(const std::string& argument);

private:
	enum E_TARGET
	{
		M_NONE,
		M_IMPORT,
		M_IMPORTACCEPT,
		M_IMPORTSAVECHUNK,
	};
	
	CommandTable<EditorChunk> m_commands;
	mud::ChunkPtr m_chunk;
	mud::RoomPtr m_parentRoom;
	
	std::string m_value;
	bool m_yesno;
	EditorChunk::E_TARGET m_target;
	ChunkImporterPtr m_importer;
	
	EditorChunk(const EditorChunk& rhs);
	EditorChunk operator=(const EditorChunk& rhs);
};
