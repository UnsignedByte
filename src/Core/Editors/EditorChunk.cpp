/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Chunk.h"
#include "ChunkImporter.h"
#include "ChunkManager.h"
#include "EditorBool.h"
#include "EditorChunk.h"
#include "EditorDetail.h"
#include "EditorString.h"
#include "Managers.h"
#include "Room.h"
#include "RoomManager.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorChunk E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

//					 name		function  need: object lock
static O editName(	"Name",		&E::editName,	true, true);
static O editDescription("Description", &E::editDescription, true, true);
static O editRoom(	"Room",		&E::editRoom,	true, true);
static O startDetails("Details",&E::startDetails);
static O importChunk("Import",	&E::importChunk);
static O showChunk(	"Show",		&E::showChunk,	true, false);
static O saveChunk(	"Save",		&E::saveChunk,	true, true);
static O clearParentRoom("ClearParentRoom", &E::clearParentRoom);
static O setParentRoom("SetParentRoom", &E::setParentRoom);

static const B commands[] = {
	B("all", clearParentRoom),
	B("description", editDescription),
	B("details", startDetails),
	B("filter", setParentRoom),
	B("import", importChunk),
	B("name", editName),
	B("room", editRoom),
	B("save", saveChunk),
	B("show", showChunk),
};

EditorChunk::EditorChunk(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_chunk(),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
}

EditorChunk::EditorChunk(UBSocket* sock, mud::RoomPtr parentRoom) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_chunk(),
m_parentRoom(parentRoom),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Chunks that belong to room '%d' are shown, to clear this restriction type 'all'.\n", parentRoom->getID());
}

EditorChunk::~EditorChunk(void)
{

}

void EditorChunk::OnFocus()
{		
	switch(m_target)
	{
		case M_NONE:
			return;
			
		case M_IMPORT:
			m_target = M_NONE;
			importChunk(m_value);
			break;
			
		case M_IMPORTACCEPT:
			m_target = M_NONE;
			importChunk(m_yesno ? "accept" : "reject");
			break;
			
		case M_IMPORTSAVECHUNK:
			m_target = M_NONE;
			importChunk(m_yesno ? "save" : "discard");
			break;
	}
	
	// m_target = M_NONE; // has to happen -before- importChunk is called since importChunk might set it to something else
}

std::string EditorChunk::lookup(const std::string& action)
{
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
		
	const ChunkCommand* act = (ChunkCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorChunk::dispatch(const std::string& action, const std::string& argument)
{
	const ChunkCommand* act = (ChunkCommand*)m_commands.getObject(action);
	
	if(!act) 
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_chunk)
	{
		m_sock->Send("You need to be editing a chunk first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_chunk->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The chunk you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorChunk::getEditing()
{
	return m_chunk;
}

TableImplPtr EditorChunk::getTable()
{
	return db::TableImpls::Get()->CHUNKS;
}

KeysPtr EditorChunk::addNew()
{
	return mud::Managers::Get()->Chunk->Add();
}

std::vector<std::string> EditorChunk::getList()
{
	return mud::Managers::Get()->Chunk->List(m_parentRoom);
}

void EditorChunk::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_chunk.reset();
		return;
	}
	
	m_chunk = mud::Managers::Get()->Chunk->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorChunk::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorChunk::editName(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Chunk name can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Chunk name changed from '%s' to '%s'.\n", m_chunk->getName().c_str(), argument.c_str());
	m_chunk->setName(argument);
	return;
}

void EditorChunk::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		return;
	}

	m_sock->Sendf("Chunk description changed from '%s' to '%s'.\n", m_chunk->getDescription().c_str(), argument.c_str());
	m_chunk->setDescription(argument);
	return;
}

void EditorChunk::editTags(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Chunk tags can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Chunk tag changed from '%s' to '%s'.\n", m_chunk->getTags().c_str(), argument.c_str());
	m_chunk->setTags(argument);
	return;
}

void EditorChunk::editRoom(const std::string& argument)
{	
	int id = atoi(argument.c_str());
	if(id <= 0)
	{
		m_sock->Send("Please specify a room this Chunk belongs to.\n");
	}
	
	std::string oldname;
	try {
		mud::RoomPtr oldroom = mud::Managers::Get()->Room->GetByKey(m_chunk->getRoom());
		oldname = oldroom->getName();
	} catch(RowNotFoundException& e) {
		oldname = String::Get()->fromInt(m_chunk->getRoom());
	}
	
	try
	{
		mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(id);
		m_sock->Sendf("Room changed from '%s' to '%s'.\n", oldname.c_str(), room->getName().c_str());
		m_chunk->setRoom(id);
	}
	catch(RowNotFoundException& e) 
	{
		m_sock->Sendf("'%s' is not a valid room!\n", argument.c_str());
		m_sock->Send(String::Get()->box(mud::Managers::Get()->Room->List(), "Rooms"));
		return;
	}
}

void EditorChunk::importChunk(const std::string& argument)
{
	bool createdImporter = false;
	
	/** 
	 * There is no argument and no importer yes, create one
	 */ 
	if(argument.size() == 0 && !m_importer)
	{
		m_sock->Send("No argument, dropping you into the String Editor.\n");
		m_sock->Send("Paste your description there, when done the chunk will be imported.\n");
		m_sock->SetEditor(new EditorString(m_sock, m_value));
		m_target = M_IMPORT;
		return;
	}
	
	if(!m_importer)
	{
		ChunkImporterPtr importer(new ChunkImporter(argument));
		m_importer = importer;
		createdImporter = true;
		m_sock->Send("Import complete.\n");
	}
	
	if(argument.size() == 0 || createdImporter)
	{
		m_sock->Send("Importing would result in the following Chunk:\n");
		m_sock->Send(m_importer->getResult());
		m_sock->Send("Do you want to accept these changes?\n");
		m_sock->SetEditor(new EditorBool(m_sock, m_yesno));
		m_target = M_IMPORTACCEPT;
		return; 
	}
	
	if(!argument.compare("accept") || !argument.compare("reject"))
	{
		if(!argument.compare("accept"))
		{
			m_importer->Apply(m_chunk);
			m_sock->Send("Would you like to apply this import to another chunk as well?\n");
		}
		else		
		{
			m_sock->Send("Ok, canceled.\n");
			m_sock->Send("Would you like to apply this import to another chunk instead?\n");
		}
		
		m_sock->Send("(If so, this import will be saved while you remain in the chunk editor.\n");
		m_sock->Send("The next time you run import, even on another chunk, this import will be applied again.)\n");
		m_sock->SetEditor(new EditorBool(m_sock, m_yesno));
		m_target = M_IMPORTSAVECHUNK;
		return;
	}
	
	if(!argument.compare("save") || !argument.compare("discard"))
	{
		if(!argument.compare("save"))
		{
			m_sock->Send("Allright, this import will not be deleted till you change editors (e.g., type 'quit').\n");
			return;
		}
		else
		{
			m_sock->Send("Allright, import discarded.\n");
			m_importer.reset();
			return;
		}
	}
	
	m_sock->Send("Unknown action.\n");
}

void EditorChunk::startDetails(const std::string& argument)
{
	m_sock->Send("Dropping you into Detail Edit mode!\n");
	m_sock->SetEditor(new EditorDetail(m_sock));
	return;
}

void EditorChunk::showChunk(const std::string& argument)
{
	m_sock->Send(m_chunk->toFullString());
}

void EditorChunk::saveChunk(const std::string& argument)
{
	m_sock->Sendf("Saving chunk '%s'.\n", m_chunk->getName().c_str());
	m_chunk->Save();
	m_sock->Send("Saved.\n");
	return;
}

void EditorChunk::clearParentRoom(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	m_parentRoom.reset();
	return;
}

void EditorChunk::setParentRoom(const std::string& argument)
{
	if(argument.size())
	{
		m_sock->Send("Please provide a room to filter on.\n");
		return;
	}
	
	try {
		int id = atoi(argument.c_str());
		mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(id);
		m_parentRoom = room;
		m_sock->Send("Ok.\n");
		m_sock->Sendf("Only Chunks that belong to room '%d' are shown, to clear this restriction type 'all'.\n", m_parentRoom->getID());
	} catch(RowNotFoundException& e) {
		m_sock->Sendf("'%s' is not a room.\n", argument.c_str());
	}	
}
