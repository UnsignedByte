/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <boost/spirit/core.hpp>

#include "Account.h"
#include "Area.h"
#include "AreaManager.h"
#include "Array.h"
#include "Assert.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "Detail.h"
#include "DetailManager.h"
#include "EditorBool.h"
#include "EditorDetail.h"
#include "EditorString.h"
#include "Managers.h"
#include "Room.h"
#include "RoomManager.h"
#include "SavableHeaders.h"
#include "StringUtilities.h"
#include "TableImpls.h"
#include "UBSocket.h"

typedef EditorDetail E;
typedef CommandInfoObject<E> O;
typedef CommandBinding<E> B;

using namespace boost::spirit;

//					 name		function  need: object lock
static O editKey(	"Key", 		&E::editKey, 	true, true);
static O editDescription("Description", &E::editDescription, true, true);
static O showDetail("Show", 	&E::showDetail, true, false);
static O saveDetail("Save", 	&E::saveDetail, true, true);
static O clearParents("ClearParents", &E::clearParents);
static O setParent(	"SetParent",&E::setParent);

static const B commands[] = {
	B("all", clearParents),
	B("description", editDescription),
	B("filter", setParent),
	B("name", editKey),
	B("save", saveDetail),
	B("show", showDetail),
};

EditorDetail::EditorDetail(UBSocket* sock) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_detail(),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
}

EditorDetail::EditorDetail(UBSocket* sock, mud::DetailPtr detail) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_detail(detail),
m_target(M_NONE)
{
	
}

EditorDetail::EditorDetail(UBSocket* sock, mud::AreaPtr parentArea) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_detail(),
m_parentArea(parentArea),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Details that belong to area '%d' are shown, to clear this restriction type 'all'.\n", parentArea->getID());
}

EditorDetail::EditorDetail(UBSocket* sock, mud::RoomPtr parentRoom) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_detail(),
m_parentRoom(parentRoom),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Details that belong to room '%d' are shown, to clear this restriction type 'all'.\n", parentRoom->getID());
}

EditorDetail::EditorDetail(UBSocket* sock, mud::ChunkPtr parentChunk) :
OLCEditor(sock),
m_commands(commands, array_size(commands)),
m_detail(),
m_parentChunk(parentChunk),
m_target(M_NONE)
{
	listCommands(Global::Get()->EmptyString);
	m_sock->Sendf("Only Details that belong to chunk '%d' are shown, to clear this restriction type 'all'.\n", parentChunk->getID());
}

EditorDetail::~EditorDetail(void)
{

}

void EditorDetail::OnFocus()
{		
	switch(m_target)
	{
		case M_NONE:
			return;
			
		case M_DESCRIPTION:
			m_detail->setDescription(m_value);
			break;
	}
	
	m_target = M_NONE;
}

std::string EditorDetail::lookup(const std::string& action)
{		
	std::string name = OLCEditor::lookup(action);
	if(name.size() != 0)
		return name;
	
	const DetailCommand* act = (DetailCommand*)m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorDetail::dispatch(const std::string& action, const std::string& argument)
{
	const DetailCommand* act = (DetailCommand*)m_commands.getObject(action);

	if(!act)
	{
		OLCEditor::dispatch(action, argument);
		return;
	}
	
	if(!m_detail && act->needObject())
	{
		m_sock->Send("You need to be editing a detail first.\n");
		m_sock->Send("(Use the 'edit' command.)\n");
		return;
	}
	
	if(act->needLock()) 
	{
		try {
			m_detail->Lock();
		} catch(SavableLocked& e) {
			m_sock->Send("The detail you are currently editing is locked (being edited by someone else), so you cannot edit it right now.\n");
			m_sock->Send("Please try again later.\n");
			return;
		}
	}

	act->Run(this, argument);		
	return;
}

SavablePtr EditorDetail::getEditing()
{
	return m_detail;
}

TableImplPtr EditorDetail::getTable()
{
	return db::TableImpls::Get()->DETAILS;
}

KeysPtr EditorDetail::addNew()
{
	return mud::Managers::Get()->Detail->Add();
}

std::vector<std::string> EditorDetail::getList()
{
	if(m_parentArea)
		return mud::Managers::Get()->Detail->List(m_parentArea);
	
	if(m_parentRoom)
		return mud::Managers::Get()->Detail->List(m_parentRoom);
	
	if(m_parentChunk)
		return mud::Managers::Get()->Detail->List(m_parentChunk);
		
	return mud::Managers::Get()->Detail->List();
}

void EditorDetail::setEditing(KeysPtr keys)
{
	if(!keys->size())
	{
		m_detail.reset();
		return;
	}
	
	m_detail = mud::Managers::Get()->Detail->GetByKey(keys->first()->getIntegerValue());
	return;
}

std::vector<std::string> EditorDetail::getCommands()
{
	return m_commands.getCommandsVector();
}

void EditorDetail::editKey(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("Detail key can't be zero length!\n");
		return;
	}

	m_sock->Sendf("Detail key changed from '%s' to '%s'.\n", m_detail->getKey().c_str(), argument.c_str());
	m_detail->setKey(argument);
	return;
}

void EditorDetail::editDescription(const std::string& argument)
{
	if(argument.size() == 0)
	{
		m_sock->Send("No argument, dropping you into the string editor!\n");
		m_sock->SetEditor(new EditorString(m_sock, m_value));
		m_target = M_DESCRIPTION;
		return;
	}

	m_sock->Sendf("Detail description changed from '%s' to '%s'.\n", m_detail->getDescription().c_str(), argument.c_str());
	m_detail->setDescription(argument);
	return;
}

void EditorDetail::showDetail(const std::string& argument)
{
	std::vector<std::string> result;
	std::string key = "Key: '";
	key.append(m_detail->getKey());
	key.append("'.");
	result.push_back(key);
	
	std::string description = "Description: '";
	description.append(m_detail->getDescription());
	description.append("'.");
	result.push_back(description);
	
	m_sock->Send(String::Get()->box(result, "Detail"));
}

void EditorDetail::saveDetail(const std::string& argument)
{
	m_sock->Sendf("Saving detail '%s'.\n", m_detail->getKey().c_str());
	if(argument.size())
		m_detail->Save(m_sock->GetAccount()->getID(), argument);
	else
		m_detail->Save(m_sock->GetAccount()->getID(), Global::Get()->EmptyString);	
	m_sock->Send("Saved.\n");
	return;
}

void EditorDetail::clearParents(const std::string& argument)
{
	m_sock->Send("Ok.\n");
	m_parentArea.reset();
	m_parentRoom.reset();
	m_parentChunk.reset();
	return;
}

void EditorDetail::setParent(const std::string& argument)
{
	if(!argument.size())
	{
		m_sock->Send("Please provide an area, room, or chunk to filter on.\n");
		return;
	}
	
	value_type id = 0;
	bool area = false;
	bool chunk = false;
	bool room = false;
	
	bool good = parse(argument.c_str(),
        //  Begin grammar
        (
			(str_p("area")[assign_a(area, true)] | 
			str_p("chunk")[assign_a(chunk, true)] | 
			str_p("room")[assign_a(room, true)])
			
			>> int_p[assign_a(id)]
        )
        ,
        //  End grammar
        space_p).full;
	
	if(!good)
	{
		m_sock->Send("Please specify what area, room, or chunk to filter on in the following format:\n");
		m_sock->Send("area/room/chunk id\n");
		return;
	}
	
	Assert(area || chunk || room);
	
	m_parentArea.reset();
	m_parentRoom.reset();
	m_parentChunk.reset();
	
		
	if(area)
	{
		try {		
			mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(id);
			m_parentArea = area;
			m_sock->Send("Ok.\n");
			m_sock->Sendf("Only Details that belong to area '%d' are shown, to clear this restriction type 'all'.\n", m_parentArea->getID());
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("'%s' is not an area.\n", argument.c_str());
		}
	}
	
	if(room)
	{
		try {		
			mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(id);
			m_parentRoom = room;
			m_sock->Send("Ok.\n");
			m_sock->Sendf("Only Details that belong to room '%d' are shown, to clear this restriction type 'all'.\n", m_parentRoom->getID());
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("'%s' is not a room.\n", argument.c_str());
		}
	}
	
	if(chunk)
	{
		try {		
			mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(id);
			m_parentChunk = chunk;
			m_sock->Send("Ok.\n");
			m_sock->Sendf("Only Details that belong to chunk '%d' are shown, to clear this restriction type 'all'.\n", m_parentChunk->getID());
		} catch(RowNotFoundException& e) {
			m_sock->Sendf("'%d' is not a chunk.\n", id);
		}
	}
}
