/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Array.h"
#include "Assert.h"
#include "Character.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "EditorMovement.h"
#include "EditorPlaying.h"
#include "Managers.h"
#include "PCharacter.h"
#include "PCharacterManager.h"
#include "StringUtilities.h"
#include "UBSocket.h"

using mud::PCharacter;

typedef EditorPlaying E;
typedef CommandObject<E> O;
typedef CommandBinding<E> B;

static O listCharacters("Characters",&E::listCharacters);
static O listCommands(	"Commands",	&E::listCommands);
static O listExits(		"Exits",	&E::listExits);
static O showScore(		"Score",	&E::showScore);
static O look(			"Look",		&E::look);
static O say(			"Say",		&E::say);
static O startMovement(	"Movement",	&E::startMovement);
static O deleteCharacter("Delete",	&E::deleteCharacter);
static O quitEditor(	"Quit",		&E::quitEditor);

static const B commands[] = {
	B("?", listCommands),
	B("delete", deleteCharacter),
	B("exits", listExits),	
	B("glance", listCharacters),
	B("go", startMovement),
	B("look", look),
	B("quit", quitEditor),
	B("say", say),
	B("score", showScore),
};

EditorPlaying::EditorPlaying(UBSocket* sock, mud::PCharacterPtr character) :
Editor(sock),
m_commands(commands, array_size(commands)),
m_char(character)
{	
	long chunkid = m_char->getChunk();
	mud::ChunkPtr inchunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	inchunk->addCharacter(m_char->getID());
	
	std::string msg = m_char->getName();
	msg.append(" enters the realm.\n");

	inchunk->Send(msg);
	
	m_char->OnSend(String::Get()->box(m_commands.getCommandsVector(), "Playing"));
}

EditorPlaying::~EditorPlaying(void)
{
	m_char->Save();
	mud::PCharacterManager::Get()->UnloadByKey(m_char->getID());
}

std::string EditorPlaying::lookup(const std::string& action)
{
	const PlayingCommand* act = m_commands.getObject(action);
	if(act)
		return act->getName();
		
	return Global::Get()->EmptyString;
}

void EditorPlaying::dispatch(const std::string& action, const std::string& argument)
{
	const PlayingCommand* act = m_commands.getObject(action);
	
	Assert(act);
	act->Run(this, argument);
}

void EditorPlaying::listCommands(const std::string& argument)
{
	m_char->OnSend(String::Get()->box(m_commands.getCommandsVector(), "Playing"));
	return;
}

void EditorPlaying::deleteCharacter(const std::string& argument)
{
	if(argument.compare("confirm"))
	{
		m_char->OnSend("If you really want to delete, please type 'delete confirm'.\n");
		return;
	}

	printf("Deleting PCharacter %s.\n", m_char->getName().c_str());

	m_char->OnSend("Goodbye.\n");
	m_sock->PopEditor();
	m_char->Delete();
	return;
}

void EditorPlaying::look(const std::string& argument)
{
	m_char->OnLook(argument);
}

void EditorPlaying::listExits(const std::string& argument)
{
	m_char->OnExits(argument);
}

void EditorPlaying::listCharacters(const std::string& argument)
{
	m_char->OnCharactersInRoom(argument);
}

void EditorPlaying::quitEditor(const std::string& argument)
{
	m_char->OnSend("Thank you for visiting.\n");
		
	value_type chunkid = m_char->getChunk();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	
	std::string msg = m_char->getName();
	msg.append(" fades from the realm.\n");
	
	chunk->Send(msg);	


	m_sock->PopEditor();	
}

void EditorPlaying::showScore(const std::string& argument)
{
	m_char->OnScore(argument);
}

void EditorPlaying::say(const std::string& argument)
{
	m_char->OnSay(argument);
}

void EditorPlaying::startMovement(const std::string& argument)
{	
	if(!argument.size())
	{
		m_sock->SetEditor(new EditorMovement(m_sock, m_char));
		return;
	}
		
	m_sock->SetEditor(new EditorMovement(m_sock, m_char, argument));
	return;
}
