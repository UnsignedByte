##
## Auto Generated makefile, please do not edit
##
ProjectName:=Core

## Debug
ifeq ($(type),Debug)
ConfigurationName :=Debug
IntermediateDirectory :=./Debug
OutDir := $(IntermediateDirectory)
LinkerName:=g++
ArchiveTool :=ar rcu
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix :=.o
DebugSwitch :=-gstab
IncludeSwitch :=-I
LibrarySwitch :=-l
OutputSwitch :=-o 
LibraryPathSwitch :=-L
PreprocessorSwitch :=-D
SourceSwitch :=-c 
CompilerName :=g++
OutputFile :=../../lib/libubcore.a
Preprocessors :=
ObjectSwitch :=-o 
ArchiveOutputSwitch := 
CmpOptions :=-g -Wall $(Preprocessors)
LinkOptions := -O0
IncludePath :=  $(IncludeSwitch). $(IncludeSwitch)../Sockets $(IncludeSwitch)../Resource $(IncludeSwitch)../DAL $(IncludeSwitch)../DB $(IncludeSwitch)../DB/Generated $(IncludeSwitch)../DB/Savables $(IncludeSwitch)../DB/Managers $(IncludeSwitch)./Editors $(IncludeSwitch)./Command $(IncludeSwitch)./Socket $(IncludeSwitch)./PCharacter 
RcIncludePath :=
Libs :=
LibPath := 
endif

Objects=$(IntermediateDirectory)/SQLSocket$(ObjectSuffix) $(IntermediateDirectory)/UBHandler$(ObjectSuffix) $(IntermediateDirectory)/UBSocket$(ObjectSuffix) $(IntermediateDirectory)/EditorMovement$(ObjectSuffix) $(IntermediateDirectory)/ChunkImporter$(ObjectSuffix) $(IntermediateDirectory)/Editor$(ObjectSuffix) $(IntermediateDirectory)/EditorAccount$(ObjectSuffix) $(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix) $(IntermediateDirectory)/EditorArea$(ObjectSuffix) $(IntermediateDirectory)/EditorBool$(ObjectSuffix) \
	$(IntermediateDirectory)/EditorChannel$(ObjectSuffix) $(IntermediateDirectory)/EditorChunk$(ObjectSuffix) $(IntermediateDirectory)/EditorCluster$(ObjectSuffix) $(IntermediateDirectory)/EditorColour$(ObjectSuffix) $(IntermediateDirectory)/EditorCommand$(ObjectSuffix) $(IntermediateDirectory)/EditorDetail$(ObjectSuffix) $(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix) $(IntermediateDirectory)/EditorMobile$(ObjectSuffix) $(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix) $(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix) \
	$(IntermediateDirectory)/EditorOLC$(ObjectSuffix) $(IntermediateDirectory)/EditorOOC$(ObjectSuffix) $(IntermediateDirectory)/EditorPermission$(ObjectSuffix) $(IntermediateDirectory)/EditorPlaying$(ObjectSuffix) $(IntermediateDirectory)/EditorRace$(ObjectSuffix) $(IntermediateDirectory)/EditorRoom$(ObjectSuffix) $(IntermediateDirectory)/EditorScript$(ObjectSuffix) $(IntermediateDirectory)/EditorSector$(ObjectSuffix) $(IntermediateDirectory)/EditorString$(ObjectSuffix) $(IntermediateDirectory)/OLCEditor$(ObjectSuffix) \
	$(IntermediateDirectory)/DirectionParser$(ObjectSuffix) $(IntermediateDirectory)/EditorSocial$(ObjectSuffix) $(IntermediateDirectory)/PCharacter$(ObjectSuffix) $(IntermediateDirectory)/PCharacterManager$(ObjectSuffix) 

##
## Main Build Tragets 
##
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile):  $(Objects)
	$(ArchiveTool) $(ArchiveOutputSwitch)$(OutputFile) $(Objects)

./Debug:
	@test -d ./Debug || mkdir ./Debug


PreBuild:


##
## Objects
##
$(IntermediateDirectory)/SQLSocket$(ObjectSuffix): Socket/SQLSocket.cpp $(IntermediateDirectory)/SQLSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Socket/SQLSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SQLSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SQLSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SQLSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/SQLSocket$(ObjectSuffix).d -MM Socket/SQLSocket.cpp

$(IntermediateDirectory)/UBHandler$(ObjectSuffix): Socket/UBHandler.cpp $(IntermediateDirectory)/UBHandler$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Socket/UBHandler.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/UBHandler$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/UBHandler$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/UBHandler$(ObjectSuffix) -MF$(IntermediateDirectory)/UBHandler$(ObjectSuffix).d -MM Socket/UBHandler.cpp

$(IntermediateDirectory)/UBSocket$(ObjectSuffix): Socket/UBSocket.cpp $(IntermediateDirectory)/UBSocket$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Socket/UBSocket.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/UBSocket$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/UBSocket$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/UBSocket$(ObjectSuffix) -MF$(IntermediateDirectory)/UBSocket$(ObjectSuffix).d -MM Socket/UBSocket.cpp

$(IntermediateDirectory)/EditorMovement$(ObjectSuffix): Editors/EditorMovement.cpp $(IntermediateDirectory)/EditorMovement$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorMovement.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorMovement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorMovement$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorMovement$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorMovement$(ObjectSuffix).d -MM Editors/EditorMovement.cpp

$(IntermediateDirectory)/ChunkImporter$(ObjectSuffix): Editors/ChunkImporter.cpp $(IntermediateDirectory)/ChunkImporter$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/ChunkImporter.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ChunkImporter$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ChunkImporter$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ChunkImporter$(ObjectSuffix) -MF$(IntermediateDirectory)/ChunkImporter$(ObjectSuffix).d -MM Editors/ChunkImporter.cpp

$(IntermediateDirectory)/Editor$(ObjectSuffix): Editors/Editor.cpp $(IntermediateDirectory)/Editor$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/Editor.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Editor$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Editor$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Editor$(ObjectSuffix) -MF$(IntermediateDirectory)/Editor$(ObjectSuffix).d -MM Editors/Editor.cpp

$(IntermediateDirectory)/EditorAccount$(ObjectSuffix): Editors/EditorAccount.cpp $(IntermediateDirectory)/EditorAccount$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorAccount.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorAccount$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorAccount$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorAccount$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorAccount$(ObjectSuffix).d -MM Editors/EditorAccount.cpp

$(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix): Editors/EditorAccountLogin.cpp $(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorAccountLogin.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix).d -MM Editors/EditorAccountLogin.cpp

$(IntermediateDirectory)/EditorArea$(ObjectSuffix): Editors/EditorArea.cpp $(IntermediateDirectory)/EditorArea$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorArea.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorArea$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorArea$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorArea$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorArea$(ObjectSuffix).d -MM Editors/EditorArea.cpp

$(IntermediateDirectory)/EditorBool$(ObjectSuffix): Editors/EditorBool.cpp $(IntermediateDirectory)/EditorBool$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorBool.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorBool$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorBool$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorBool$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorBool$(ObjectSuffix).d -MM Editors/EditorBool.cpp

$(IntermediateDirectory)/EditorChannel$(ObjectSuffix): Editors/EditorChannel.cpp $(IntermediateDirectory)/EditorChannel$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorChannel.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorChannel$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorChannel$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorChannel$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorChannel$(ObjectSuffix).d -MM Editors/EditorChannel.cpp

$(IntermediateDirectory)/EditorChunk$(ObjectSuffix): Editors/EditorChunk.cpp $(IntermediateDirectory)/EditorChunk$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorChunk.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorChunk$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorChunk$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorChunk$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorChunk$(ObjectSuffix).d -MM Editors/EditorChunk.cpp

$(IntermediateDirectory)/EditorCluster$(ObjectSuffix): Editors/EditorCluster.cpp $(IntermediateDirectory)/EditorCluster$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorCluster.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorCluster$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorCluster$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorCluster$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorCluster$(ObjectSuffix).d -MM Editors/EditorCluster.cpp

$(IntermediateDirectory)/EditorColour$(ObjectSuffix): Editors/EditorColour.cpp $(IntermediateDirectory)/EditorColour$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorColour.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorColour$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorColour$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorColour$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorColour$(ObjectSuffix).d -MM Editors/EditorColour.cpp

$(IntermediateDirectory)/EditorCommand$(ObjectSuffix): Editors/EditorCommand.cpp $(IntermediateDirectory)/EditorCommand$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorCommand.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorCommand$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorCommand$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorCommand$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorCommand$(ObjectSuffix).d -MM Editors/EditorCommand.cpp

$(IntermediateDirectory)/EditorDetail$(ObjectSuffix): Editors/EditorDetail.cpp $(IntermediateDirectory)/EditorDetail$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorDetail.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorDetail$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorDetail$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorDetail$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorDetail$(ObjectSuffix).d -MM Editors/EditorDetail.cpp

$(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix): Editors/EditorGrantGroup.cpp $(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorGrantGroup.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix).d -MM Editors/EditorGrantGroup.cpp

$(IntermediateDirectory)/EditorMobile$(ObjectSuffix): Editors/EditorMobile.cpp $(IntermediateDirectory)/EditorMobile$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorMobile.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorMobile$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorMobile$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorMobile$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorMobile$(ObjectSuffix).d -MM Editors/EditorMobile.cpp

$(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix): Editors/EditorNewAccount.cpp $(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorNewAccount.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix).d -MM Editors/EditorNewAccount.cpp

$(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix): Editors/EditorNewCharacter.cpp $(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorNewCharacter.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix).d -MM Editors/EditorNewCharacter.cpp

$(IntermediateDirectory)/EditorOLC$(ObjectSuffix): Editors/EditorOLC.cpp $(IntermediateDirectory)/EditorOLC$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorOLC.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorOLC$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorOLC$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorOLC$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorOLC$(ObjectSuffix).d -MM Editors/EditorOLC.cpp

$(IntermediateDirectory)/EditorOOC$(ObjectSuffix): Editors/EditorOOC.cpp $(IntermediateDirectory)/EditorOOC$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorOOC.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorOOC$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorOOC$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorOOC$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorOOC$(ObjectSuffix).d -MM Editors/EditorOOC.cpp

$(IntermediateDirectory)/EditorPermission$(ObjectSuffix): Editors/EditorPermission.cpp $(IntermediateDirectory)/EditorPermission$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorPermission.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorPermission$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorPermission$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorPermission$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorPermission$(ObjectSuffix).d -MM Editors/EditorPermission.cpp

$(IntermediateDirectory)/EditorPlaying$(ObjectSuffix): Editors/EditorPlaying.cpp $(IntermediateDirectory)/EditorPlaying$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorPlaying.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorPlaying$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorPlaying$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorPlaying$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorPlaying$(ObjectSuffix).d -MM Editors/EditorPlaying.cpp

$(IntermediateDirectory)/EditorRace$(ObjectSuffix): Editors/EditorRace.cpp $(IntermediateDirectory)/EditorRace$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorRace.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorRace$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorRace$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorRace$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorRace$(ObjectSuffix).d -MM Editors/EditorRace.cpp

$(IntermediateDirectory)/EditorRoom$(ObjectSuffix): Editors/EditorRoom.cpp $(IntermediateDirectory)/EditorRoom$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorRoom.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorRoom$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorRoom$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorRoom$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorRoom$(ObjectSuffix).d -MM Editors/EditorRoom.cpp

$(IntermediateDirectory)/EditorScript$(ObjectSuffix): Editors/EditorScript.cpp $(IntermediateDirectory)/EditorScript$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorScript.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorScript$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorScript$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorScript$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorScript$(ObjectSuffix).d -MM Editors/EditorScript.cpp

$(IntermediateDirectory)/EditorSector$(ObjectSuffix): Editors/EditorSector.cpp $(IntermediateDirectory)/EditorSector$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorSector.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorSector$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorSector$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorSector$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorSector$(ObjectSuffix).d -MM Editors/EditorSector.cpp

$(IntermediateDirectory)/EditorString$(ObjectSuffix): Editors/EditorString.cpp $(IntermediateDirectory)/EditorString$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorString.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorString$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorString$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorString$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorString$(ObjectSuffix).d -MM Editors/EditorString.cpp

$(IntermediateDirectory)/OLCEditor$(ObjectSuffix): Editors/OLCEditor.cpp $(IntermediateDirectory)/OLCEditor$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/OLCEditor.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/OLCEditor$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/OLCEditor$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/OLCEditor$(ObjectSuffix) -MF$(IntermediateDirectory)/OLCEditor$(ObjectSuffix).d -MM Editors/OLCEditor.cpp

$(IntermediateDirectory)/DirectionParser$(ObjectSuffix): Editors/DirectionParser.cpp $(IntermediateDirectory)/DirectionParser$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/DirectionParser.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/DirectionParser$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/DirectionParser$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/DirectionParser$(ObjectSuffix) -MF$(IntermediateDirectory)/DirectionParser$(ObjectSuffix).d -MM Editors/DirectionParser.cpp

$(IntermediateDirectory)/EditorSocial$(ObjectSuffix): Editors/EditorSocial.cpp $(IntermediateDirectory)/EditorSocial$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Editors/EditorSocial.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EditorSocial$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EditorSocial$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EditorSocial$(ObjectSuffix) -MF$(IntermediateDirectory)/EditorSocial$(ObjectSuffix).d -MM Editors/EditorSocial.cpp

$(IntermediateDirectory)/PCharacter$(ObjectSuffix): PCharacter/PCharacter.cpp $(IntermediateDirectory)/PCharacter$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)PCharacter/PCharacter.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/PCharacter$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/PCharacter$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/PCharacter$(ObjectSuffix) -MF$(IntermediateDirectory)/PCharacter$(ObjectSuffix).d -MM PCharacter/PCharacter.cpp

$(IntermediateDirectory)/PCharacterManager$(ObjectSuffix): PCharacter/PCharacterManager.cpp $(IntermediateDirectory)/PCharacterManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)PCharacter/PCharacterManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/PCharacterManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/PCharacterManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/PCharacterManager$(ObjectSuffix) -MF$(IntermediateDirectory)/PCharacterManager$(ObjectSuffix).d -MM PCharacter/PCharacterManager.cpp

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/SQLSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SQLSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/UBHandler$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/UBHandler$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/UBSocket$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/UBSocket$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorMovement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorMovement$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ChunkImporter$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ChunkImporter$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Editor$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Editor$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorAccount$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorAccount$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorAccountLogin$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorArea$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorArea$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorBool$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorBool$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorChannel$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorChannel$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorChunk$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorChunk$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorCluster$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorCluster$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorColour$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorColour$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorCommand$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorCommand$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorDetail$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorDetail$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorGrantGroup$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorMobile$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorMobile$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorNewAccount$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorNewCharacter$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorOLC$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorOLC$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorOOC$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorOOC$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorPermission$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorPermission$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorPlaying$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorPlaying$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorRace$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorRace$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorRoom$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorRoom$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorScript$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorScript$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorSector$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorSector$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorString$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorString$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/OLCEditor$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/OLCEditor$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/DirectionParser$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/DirectionParser$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EditorSocial$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EditorSocial$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/PCharacter$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/PCharacter$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/PCharacterManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/PCharacterManager$(ObjectSuffix).d
	$(RM) $(OutputFile)

-include $(IntermediateDirectory)/*.d

