/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file UBHandler.h
 * This file contains the UBHandler class.
 * 
 * @see UBHandler
 */ 

#include <SocketHandler.h>

class Socket;
typedef std::map<SOCKET,Socket *> socket_m; /**< The type of a socket number to Socket map. */

/**
 * This class will handle all Socket related events.
 *
 * This class does not use Singleton since SocketHandler already declares Get
 */
class UBHandler : public SocketHandler
{
	public:
		/** Iterate over all sockets and call SwitchEditor if it is an UBSocket. */
		void SwitchEditors();

		/** Iterate over all sockets and send a shutdown notice. */
		void Shutdown();
			
		/** Returns the map with sockets. */
		socket_m& Sockets();
		
		/**
		 * Static method to access the only pointer of this instance.
		 * @return a pointer to the only instance of this class
		 */
		static UBHandler* Get();
		
		/** Release resources. */
		static void Free();

	private:
		/** This is a singleton class, use <code>Get</code> to get an instance. */
		UBHandler(void) {};
		
		/** This is a singleton class, use <code>Free</code> to free the instance. */
		~UBHandler(void) {};
		
		/** Hide the copy constructor. */
		UBHandler(const UBHandler& rhs);
		
		/** Hide the assignment operator. */
		UBHandler operator=(const UBHandler& rhs);
		
		
		/** Send a shutdown notice to the specified socket. */
		void Shutdown(Socket* sock);
		
		
		static UBHandler* ms_instance; /**< The one instance of this class. */ 
};
