/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <Parse.h>
#include <assert.h>
#include <stdarg.h>

#include "Account.h"
#include "AccountManager.h"
#include "Channel.h"
#include "Colour.h"
#include "ColourManager.h"
#include "DatabaseMgr.h"
#include "EditorAccountLogin.h"
#include "EditorOOC.h"
#include "GameVersion.h"
#include "Global.h"
#include "Managers.h"
#include "UBSocket.h"

extern bool g_nocatch;

UBSocket::UBSocket(ISocketHandler& h) :
TcpSocket(h),
m_prompt(),
m_account(),
m_popeditor(false),
m_popLast(false),
m_editors(),
m_nexteditor(NULL),
m_forcer(NULL),
m_lowforced(false),
m_forced(false),
m_highforced(false),
m_hascolor(true),
m_colorcode('`')
{
	SetLineProtocol();
}

UBSocket::~UBSocket(void)
{
	while(!m_editors.empty())
	{
		delete m_editors.front();
		m_editors.pop_front();
	}

	if(m_nexteditor != NULL)
	{
		delete m_nexteditor;
		m_nexteditor = NULL;
	}
}

void UBSocket::OnAccept()
{
	std::string msg = "Login from: '";
	msg.append(GetRemoteAddress());
	msg.append("'\n");
	
	Global::Get()->printlog(msg);
	
	Sendf("Welcome to %s.\n", game::vname);
	Editor* p = new EditorAccountLogin(this);
	Assert(m_editors.empty());

	m_editors.push_front(p);
}

void UBSocket::OnLine(const std::string &line)
{			
	SwitchEditors();
	Assert(!m_editors.empty());
	
	if(g_nocatch)
	{
		handleLine(line);	
		return;
	}
	
	try 
	{	
		handleLine(line);
	}
	catch(std::exception& e) 
	{
		
		value_type accountid = 0;
		
		if(m_account) {
			accountid = m_account->getID();
		}
		
		throw ExcecutionException(e, accountid, line);
	}
}

void UBSocket::handleLine(const std::string& line)
{
	bool popLast = false;
		
	if(line.size() == 0)
	{
		m_editors.front()->OnEmptyLine();
		SendPrompt();
		return;
	}
	
	if(m_editors.front()->supportPrefixes() && line.size())
		popLast = handlePrefixes(line);	
	
	m_editors.front()->OnLine(line);
	
	if(popLast)
		PopEditor();
		
	SendPrompt();
}

mud::AccountPtr UBSocket::GetAccount() const
{
	Assert(m_account);
	return m_account;
}

bool UBSocket::hasForcer() const
{
	return m_forcer != NULL;
}

UBSocket* UBSocket::GetForcer() const
{
	Assert(m_forcer);
	return m_forcer;
}

bool UBSocket::hasAccount() const
{
	return m_account != NULL;
}

void UBSocket::SetPrompt(const std::string& prompt)
{
	m_prompt = prompt;
}

void UBSocket::SendPrompt()
{
	Send(m_prompt);
}

void UBSocket::SetEditor(Editor* edit, bool popLast)
{
	Assert(!m_nexteditor);

	SetPrompt();
	m_nexteditor = edit;
	m_popLast = popLast;
	return;
}

void UBSocket::SwitchEditors()
{		
	if(!m_popeditor && !m_nexteditor)
		return;
	
	Assert(!(m_popeditor && m_nexteditor));
	
	if(m_popeditor || m_popLast)
	{
		Assert(!(m_editors.empty()))
		
		delete m_editors.front();
		m_editors.pop_front();
		m_popeditor = false;
		m_popLast = false;
		SetPrompt(m_editors.front()->prompt());
	}

	if(m_nexteditor)
	{
		m_editors.push_front(m_nexteditor);
		m_nexteditor = NULL;
		SetPrompt(m_editors.front()->prompt());
	}
	
	m_editors.front()->OnFocus();
	
	if(m_nexteditor || m_popeditor)
		SwitchEditors();
	else
		SendPrompt();
}

void UBSocket::PopEditor()
{
	Assert(!m_popeditor);

	m_popeditor = true;
	SetPrompt();
}

void UBSocket::Send(const std::string& msg)
{
	if(!m_hascolor || msg.find(m_colorcode) == std::string::npos)
	{
		SendBuf(msg.c_str(), msg.size());
		return;
	}

	std::string buf = msg;
	buf.append("`^");

	for(size_t i = buf.find(m_colorcode); i != std::string::npos; i = buf.find(m_colorcode))
	{
		Assert(i < buf.size());

		std::string code = buf.substr(i+1, 1);

		buf.erase(i+1, 1);
		buf.erase(i, 1);

		try
		{
			mud::ColourPtr colour = mud::Managers::Get()->Colour->GetByCode(code);
			buf.insert(i, colour->getColourString());
		}
		catch(RowNotFoundException& e)
		{
			std::string msg = "Unknown colour code '";
			msg.append(code);
			msg.append("'!\n");
			
			Global::Get()->printbug(msg);
			continue;
		}
	}
	
	SendBuf(buf.c_str(), buf.size());

}

void UBSocket::Sendf(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	Send(Global::Get()->sprint(args, format));
	va_end(args);
}

UBSocket* UBSocket::Cast(Socket *sock, bool error)
{
	UBSocket* sock2 = dynamic_cast<UBSocket*>(sock);
	Assert(sock2 || !error);

	return sock2;
}

bool UBSocket::canReceiveChannel(mud::ChannelPtr channel)
{
	Editor* editor = m_editors.front();
	
	if(!editor->canReceiveChannel(channel))
		return false;
		
	if(m_account && !m_account->wantReceiveChannel(channel))
		return false;
	
	return true;
}

bool UBSocket::handlePrefixes(const std::string& line)
{
	bool popLast = false;
	
	if(line[0] == Global::Get()->OOCIdentifier)
	{		
		m_editors.push_front(new EditorOOC(this));
		popLast = true;
	}
	
	return popLast;
}
