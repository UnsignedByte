/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file UBSocket.h
 * This file contains the UBSocket class.
 * 
 * @see UBSocket 
 */ 

#include <ISocketHandler.h>
#include <TcpSocket.h>
#include <list>

#include "SavableHeaders.h"
#include "SavableTypes.h"

class Editor;
typedef std::list<Editor*> EditorList; /**< The type of multiple Editors. */

/**
 * This class handles all data from and to the user.
 *
 * When accepting an connection it will set the login editor.
 * All lines received are processed by calling the topmost editor's OnLine function. 
 */ 
class UBSocket : public TcpSocket
{
public:
	/** Constructs a UBSocket using the specified socket handler. */
	UBSocket(ISocketHandler& h);
	
	/** Destructor, deletes all editors.*/
	~UBSocket(void);


	/** Accept a new connection. */
	void OnAccept();

	/** Accept the next line of user input. */
	void OnLine(const std::string& line);
	
	
	/** Send text to the user. */
	void Send(const std::string& msg);
	
	/** Send formatted text to the user. */
	void Sendf(const char* format, ...);

	
	/** Whether this socket already has an account associated with it. */
	bool hasAccount() const;
	
	/** Returns the account associated with this socket, this account is asserted to have been assigned. */
	mud::AccountPtr GetAccount() const;
	
	/** Whether the user was forced to send their latest line. */
	bool hasForcer() const;
	
	/** Returns the socket that forced this user to send their latest line, it is asserted that there is a forcer. */
	UBSocket* GetForcer() const;
	
	/** Whether the current force is from an Admin. */
	bool isHighForced() const { return m_highforced; }
	
	/** Whether the current force is from a Forcer. */
	bool isForced() const { return m_forced; }
	
	/** Whether the current force is from another player. */
	bool isLowForced() const { return m_lowforced; }
		
	/** Is the user in a state capable of receiving the specified channel. */
	bool canReceiveChannel(mud::ChannelPtr channel);
	
	
	/** Sets the account associated with this user. */
	void SetAccount(mud::AccountPtr account) { m_account = account; }
	
	/** 
	 * Set the specified editor as the current editor
	 *
	 * The specified editor does not replace any previous editors.
	 * Instead it is prepended to a list of editors.
	 * This allows for 'stacked' editors.
	 *
	 * @param editor The editor that should be prepended to the editor list.
	 * @param popLast Remove the most recent editor before adding the current editor.
	 */
	void SetEditor(Editor* editor, bool popLast = false);
	
	/** Return to the previous editor, it is asserted that the user is never left with an empty editor list. */ 
	void PopEditor();
	
	/** 
	 * Sets the forces of the next commands. 
	 *
	 * The forcer remains in place untill <code>EndForce</code> is called.
	 * The optional parameters describe what authorization the forcer has.
	 *
	 * @param forcer The socket that will force the next commands. 
	 * @param weakForced Whether the force is done by another user.
	 * @param forced Whether the force is done by a Forcer.
	 * @param highForced Whether the force is done by an Admin.
	 *
	 * @see EndForce 
	 */ 
	void SetForcer(UBSocket* forcer, bool weakForced = true, bool forced = false, bool highForced = false);
	
	/** End the force. */
	void EndForce();

	/**
	 * This will switch editors.
	 *
	 * The <code>SetEditor</code> and <code>PopEditor</code> methods do not actually change editors.
	 * Instead they 'queue' editor changing actions till the end of the current 'loop', when this function is called. 
	 * Each editor will therefore return from the function that called <code>SetEditor</code> or <code>PopEditor</code>.
	 * As such, editors do not have to worry about being deleted after calling either of those functions. 
	 * 
	 * @see SetEditor
	 * @see PopEditor 
	 */ 
	void SwitchEditors();
	
	/** 
	 * This is an utility function that will cast the specified socket to an UBSocket.
	 *
	 * @param sock The socket to cast from.
	 * @param error Whether we should throw an exception when the socket could not be casted.
	 */ 
	static UBSocket* Cast(Socket* sock, bool error = true);

private:
	/** Hide the copy constructor. */
	UBSocket(const UBSocket& rhs);
	
	/** Hide the assignment operator. */
	UBSocket operator=(const UBSocket& rhs);
	
	void SetPrompt(const std::string& prompt = "");
	void SendPrompt();
	bool handlePrefixes(const std::string& line);
	void handleLine(const std::string& line);
	
	std::string m_prompt;		/**< Current prompt. */
	mud::AccountPtr m_account; 	/**< Current account. */
	bool m_popeditor;			/**< Whether we should delete the top editor next loop. */
	bool m_popLast;				/**< Whether we should delete the top editor when adding new one next loop. */
	EditorList m_editors; 		/**< The list of editors. */
	Editor* m_nexteditor; 		/**< The Editor to prepend to the list next loop. */
	UBSocket* m_forcer; 		/**< The socket that is current forcing the user. */
	bool m_lowforced; 			/**< Whether the user is forced by another user. */
	bool m_forced;				/**< Whether the user is forced by a Forcer. */
	bool m_highforced;			/**< Whether the user is forced by an Admin. */
	bool m_hascolor;			/**< Whether the user has colour enabled. */
	char m_colorcode;			/**< The character the user uses to indicate a colourcode. */
};
