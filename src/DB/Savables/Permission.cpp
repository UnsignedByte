/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Permission.h"
#include "PermissionManager.h"
#include "TableImpls.h"

using mud::Permission;

Permission::Permission(SavableManagerPtr area) :
m_permission(area)
{
	if(!m_permission)
		throw std::invalid_argument("Permission::Permission(), m_permission == NULL!");
}

Permission::~Permission(void)
{
	Unlock();
}

bool Permission::hasGrant() const
{
	return m_permission->getValue(db::TableImpls::Get()->PERMISSIONS->GRANT)->getBoolValue();
}

bool Permission::hasLog() const
{
	return m_permission->getValue(db::TableImpls::Get()->PERMISSIONS->LOG)->getBoolValue();
}

void Permission::setGrant(bool grant)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->PERMISSIONS->GRANT, grant ? 1 : 0));
	m_permission->setValue(value);
}

void Permission::setLog(bool log)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->PERMISSIONS->LOG, log ? 1 : 0));
	m_permission->setValue(value);
}

void Permission::Delete()
{
	Lock();
	m_permission->erase();
}

void Permission::Save()
{
	Lock();
	m_permission->save();
}

void Permission::Discard()
{
	Lock();
	m_permission->discard();
}

bool Permission::Exists()
{
	Lock();
	return m_permission->exists();
}

SavableManagerPtr Permission::getManager() const
{
	return m_permission;
}

TableImplPtr Permission::getTable() const
{
	return m_permission->getTable();
}
