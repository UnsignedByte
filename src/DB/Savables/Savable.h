/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * @file Savable.h
 * This file contains the Savable class. 
 *  
 * @see Savable 
 */ 

#include "Types.h"

/**
 * An interface to for SavableManager wrappers.
 * 
 * This interface is meant to wrap a SavableManager.
 * It provides basic functionality to allow general interaction with SavableManagers. 
 *  
 * @see SavableManager 
 */ 
class Savable
{
public:
	/** Construct a new Savable. */
	Savable(void);
	
	/** Destructor, a noop. */	
	virtual ~Savable(void);

	/** Delete this savable. */
	virtual void Delete() = 0;
	
	/** Save this savable. */
	virtual void Save() = 0;
	
	/** Delete this savable providing details as to who deleted it and why. */
	virtual void Delete(value_type accountid, const std::string& description) ;
	
	/** Save this savable providing details as to who deleted it and why. */
	virtual void Save(value_type accountid, const std::string& description) ;
	
	/** Discard changes made to this savable. */
	virtual void Discard() = 0;
	
	/** Wether this savable has been saved yet. */
	virtual bool Exists() = 0;
	
	/** Locks the manager if no lock was aquired yet.
	 *
	 * <code>getManager()</code> is asserted not to return NULL. 
	 * If no lock could be aquired throws SavableLocked.
	 * When called succesfully (that is, without throwing) a lock is aquired.
	 * If a lock is aquired, <code>Unlock</code> should be called before the savable is destroyed. 
	 *
	 * @see getManager
	 * @see SavableLocked
	 * @see Unlock 
	 */ 
	void Lock();
	
	/** Unlocks the savable if we previously aquired a lock. */
	void Unlock();
	
	/** Returns an elaborate string representation of the savable, getManager() is asserted not to return NULL. */
	virtual std::vector<std::string> toStrings() const;
	
	/** Returns a one-line string representation of the savable, getManager() is asserted not to return NULL. */
	virtual std::string toString() const;
	
	
	/** Returns the table this savable belongs to. */
	virtual TableImplPtr getTable() const = 0;
protected:	
	/** Returns the SavableManager this savable usesotherwise. */
	virtual SavableManagerPtr getManager() const = 0;
	
	bool m_haslock; /**< Whether we have a lock on our savable. */

private:	
	/** Hide the copy constructor. */
	Savable(const Savable& rhs);
};

typedef SmartPtr<Savable> SavablePtr; /**< The type of a pointer to a savable. */
