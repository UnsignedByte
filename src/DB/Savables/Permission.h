/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"

namespace mud
{
	class Permission : public Savable
	{
	public:
		/*
		 * Getters
		 */ 
		long getAccount() const;
		long getGrantGroup() const;
		
		bool hasGrant() const;
		bool hasLog() const;
		
		/*
		 * Setters
		 */ 
		void setGrant(bool grant);
		void setLog(bool log);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		
		/*
		 * Database utilities
		 */
		void Delete();
		void Save();
		void Discard();
		bool Exists();

	private:
		SavableManagerPtr m_permission;

		Permission(SavableManagerPtr Permission);
		Permission(const Permission& rhs);
		Permission operator=(const Permission& rhs);
		~Permission(void);
		
		friend class PermissionManager;
		friend SmartPtrDelete(mud::Permission);
	};
}
