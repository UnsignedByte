/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Account.h"
#include "Channel.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Account;

Account::Account(SavableManagerPtr dbaccount) :
m_account(dbaccount)
{
	Assert(dbaccount);
}

Account::~Account(void)
{

}

value_type Account::getID() const
{
	return m_account->getValue(db::TableImpls::Get()->ACCOUNTS->ACCOUNTID)->getIntegerValue();
}

const std::string& Account::getName() const
{
	return m_account->getValue(db::TableImpls::Get()->ACCOUNTS->NAME)->getStringValue();
}

const std::string& Account::getPassword() const
{
	return m_account->getValue(db::TableImpls::Get()->ACCOUNTS->PASSWORD)->getStringValue();
}

void Account::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ACCOUNTS->NAME, name));
	m_account->setValue(value);
	Unlock();
}

void Account::setPassword(const std::string& password)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ACCOUNTS->PASSWORD, password));
	m_account->setValue(value);
	Unlock();
}

void Account::Delete()
{
	Lock();
	m_account->erase();
	Unlock();
}

void Account::Save()
{
	Lock();
	m_account->save();
	Unlock();
}

void Account::Discard()
{
	Lock();
	m_account->discard();
	Unlock();
}

bool Account::Exists()
{
	return m_account->exists();
}

SavableManagerPtr Account::getManager() const
{
	return m_account;
}

TableImplPtr Account::getTable() const
{
	return m_account->getTable();
}

bool mud::Account::wantReceiveChannel(ChannelPtr channel) const
{
	// TODO check players preference
	return true;
}
