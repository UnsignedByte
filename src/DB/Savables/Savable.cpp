/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Savable.h"
#include "SavableManager.h"

Savable::Savable(void) :
m_haslock(false)
{ 
	
}

Savable::~Savable(void)
{	
	Assert(!m_haslock);
}

void Savable::Delete(value_type accountid, const std::string& description)
{ 
	this->Delete(); 
}

void Savable::Save(value_type accountid, const std::string& description)
{
	this->Save(); 
}

void Savable::Lock()
{
	if(m_haslock)
		return;
		
	SavableManagerPtr manager = getManager();
	Assert(manager);
		
	bool locked = manager->lock();
	if(locked)
	{
		m_haslock = true;
		return;
	}
	
	throw SavableLocked();
}

void Savable::Unlock()
{
	if(!m_haslock)
		return;
	
	SavableManagerPtr manager = getManager();
	Assert(manager);
	
	manager->unlock();
	m_haslock = false;
}

std::vector<std::string> Savable::toStrings() const
{
	SavableManagerPtr manager = getManager();
	Assert(manager);
	
	return manager->fieldList();
}

std::string Savable::toString() const
{
	SavableManagerPtr manager = getManager();
	Assert(manager);
	
	return manager->toString();
}

SavableManagerPtr Savable::getManager() const
{
	// Construct a NULL ptr.
	SavableManagerPtr result;
	return result;
}
