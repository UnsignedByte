/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Chunk.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"
#include "Managers.h"
#include "Character.h"
#include "CharacterManager.h"
#include "FieldValues.h"
#include "RoomManager.h"
#include "ClusterManager.h"
#include "DetailManager.h"
#include "AreaManager.h"
#include "Room.h"
#include "Cluster.h"
#include "Area.h"
#include "StringUtilities.h"
#include "ChunkManager.h"

using mud::Chunk;

std::map<value_type, value_types> Chunk::ms_charactersInRoom;

Chunk::Chunk(SavableManagerPtr chunk) :
m_chunk(chunk)
{
	Assert(chunk);
}

Chunk::~Chunk(void)
{
	Unlock();
}

value_type Chunk::getID() const
{
	return m_chunk->getValue(db::TableImpls::Get()->CHUNKS->CHUNKID)->getIntegerValue();
}

const std::string& Chunk::getName() const
{
	return m_chunk->getValue(db::TableImpls::Get()->CHUNKS->NAME)->getStringValue();	
}

const std::string& Chunk::getDescription() const
{
	return m_chunk->getValue(db::TableImpls::Get()->CHUNKS->DESCRIPTION)->getStringValue();	
}

const std::string& Chunk::getTags() const
{
	return m_chunk->getValue(db::TableImpls::Get()->CHUNKS->TAGS)->getStringValue();	
}

value_type Chunk::getRoom() const
{
	return m_chunk->getValue(db::TableImpls::Get()->CHUNKS->FKROOMS)->getIntegerValue();
}

void Chunk::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHUNKS->NAME, name));
	m_chunk->setValue(value);
}

void Chunk::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHUNKS->DESCRIPTION, description));
	m_chunk->setValue(value);
}

void Chunk::setTags(const std::string& tags)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHUNKS->TAGS, tags));
	m_chunk->setValue(value);
}

void Chunk::setRoom(value_type room)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHUNKS->FKROOMS, room));
	m_chunk->setValue(value);
}

void mud::Chunk::addCharacter(value_type characterid)
{
	SmartPtr<mud::Character> character = mud::Managers::Get()->Character->GetByKey(characterid);
	Assert(character->getChunk() == this->getID());
	
	ms_charactersInRoom[getID()].insert(characterid);
}

void mud::Chunk::removeCharacter(value_type characterid)
{
	SmartPtr<mud::Character> character = mud::Managers::Get()->Character->GetByKey(characterid);
	Assert(character->getChunk() == this->getID());
	
	value_types::const_iterator it = ms_charactersInRoom[getID()].find(characterid);
	Assert(it != end());
	ms_charactersInRoom[getID()].erase(it);
}

void mud::Chunk::Send(const std::string& msg)
{
	for(value_types::const_iterator it = begin(); it != end(); it++)
	{
		CharacterPtr character = mud::Managers::Get()->Character->GetByKey(*it);
		character->OnSend(msg);
	}
}

void Chunk::Delete()
{
	Lock();
	m_chunk->erase();
}

void Chunk::Save()
{
	Lock();
	m_chunk->save();
}

void Chunk::Discard()
{
	Lock();
	m_chunk->discard();
}

bool Chunk::Exists()
{
	return m_chunk->exists();
}

SavableManagerPtr Chunk::getManager() const
{
	return m_chunk;
}

TableImplPtr Chunk::getTable() const
{
	return m_chunk->getTable();
}

value_type mud::Chunk::getExitAt(Coordinate direction) const
{
	Assert(direction.isDirection());
	
	int x = direction.getXCoordinate();
	int y = direction.getYCoordinate();
	int z = direction.getZCoordinate();
	
	FieldValuesPtr values(new FieldValues(db::TableImpls::Get()->EXITS));	
	values->addValue(db::TableImpls::Get()->EXITS->FKCHUNKSFROM, getID());
	values->addValue(db::TableImpls::Get()->EXITS->X, x+1);
	values->addValue(db::TableImpls::Get()->EXITS->Y, y+1);
	values->addValue(db::TableImpls::Get()->EXITS->Z, z+1);
	
	SavableManagersPtr result = SavableManager::getmulti(values);
	if(result->size() > 0)
	{
		SavableManagerPtr manager = result->first();
		Assert(manager);
		
		FieldValuePtr value = manager->getValue(db::TableImpls::Get()->EXITS->EXITID);
		Assert(value);
		
		return value->getIntegerValue();
	}
	
	return 0;
}

value_types::const_iterator mud::Chunk::begin()
{ 
	return ms_charactersInRoom[getID()].begin(); 
}

value_types::const_iterator mud::Chunk::end()
{ 
	return ms_charactersInRoom[getID()].end(); 
}

value_type mud::Chunk::size()
{ 
	return ms_charactersInRoom[getID()].size(); 
}

std::string mud::Chunk::toString() const
{
	Strings result;
	std::string line;
	
	value_type chunkid = getID();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	Assert(chunk);
	
	value_type roomid = getRoom();
	mud::RoomPtr room = mud::Managers::Get()->Room->GetByKey(roomid);
	Assert(room);
	
	value_type clusterid = room->getCluster();
	mud::ClusterPtr cluster = mud::Managers::Get()->Cluster->GetByKey(clusterid);
	Assert(cluster);
	
	value_type areaid = cluster->getArea();
	mud::AreaPtr area = mud::Managers::Get()->Area->GetByKey(areaid);
	Assert(area);
	
	
	// Create the header
	line = std::string("[") + area->getName() + std::string(", ") + cluster->getName() + std::string("]");
	result.push_back(line);

	// Add the room description
	line = room->toString();
	result.push_back(line);
		
	// Add our own description
	line = getDescription();
	result.push_back(line);
	
	// Get our own details
	Strings details = mud::Managers::Get()->Detail->ReadableList(chunk);	
	
	// Add all details
	for(Strings::const_iterator it = details.begin(); it != details.end(); it++) {
		result.push_back(*it);
	}

	// Unline and add a trailing newline
	std::string unlined = String::Get()->unlines(result, "\n", 0);
	unlined.append("\n");
	
	return unlined;
}

std::string mud::Chunk::toFullString() const
{
	return Savable::toString();
}
