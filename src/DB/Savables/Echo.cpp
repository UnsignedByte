/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Echo.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Echo;
using namespace mud;

Echo::Echo(SavableManagerPtr echo) :
m_echo(echo)
{
	Assert(echo);
}

Echo::~Echo(void)
{
	Unlock();
}

const std::string& Echo::getMessage() const
{
	return m_echo->getValue(db::TableImpls::Get()->ECHOS->MESSAGE)->getStringValue();
}

value_type Echo::getAudibility() const
{
	return m_echo->getValue(db::TableImpls::Get()->ECHOS->AUDIBILITY)->getIntegerValue();
}

value_type Echo::getVisibility() const
{
	return m_echo->getValue(db::TableImpls::Get()->ECHOS->VISIBILITY)->getIntegerValue();
}

void Echo::setMessage(const std::string& message)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ECHOS->MESSAGE, message));
	m_echo->setValue(value);
}

void Echo::setAudibility(value_type audibility)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ECHOS->AUDIBILITY, audibility));
	m_echo->setValue(value);
}

void Echo::setVisibility(value_type visibility)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ECHOS->VISIBILITY, visibility));
	m_echo->setValue(value);
}

void Echo::Delete()
{
	Lock();
	m_echo->erase();
}

void Echo::Save()
{
	Lock();
	m_echo->save();
}

void Echo::Discard()
{
	Lock();
	m_echo->discard();
}

bool Echo::Exists()
{
	return m_echo->exists();
}

SavableManagerPtr Echo::getManager() const
{
	return m_echo;
}

TableImplPtr Echo::getTable() const
{
	return m_echo->getTable();
}
