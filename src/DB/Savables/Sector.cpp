/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Assert.h"
#include "Sector.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Sector;

Sector::Sector(SavableManagerPtr Sector) :
m_sector(Sector)
{
	Assert(Sector);
}

Sector::~Sector(void)
{
	Unlock();
}

value_type Sector::getID() const
{
	return m_sector->getValue(db::TableImpls::Get()->SECTORS->SECTORID)->getIntegerValue();
}

const std::string& Sector::getName() const
{
	return m_sector->getValue(db::TableImpls::Get()->SECTORS->NAME)->getStringValue();
}

const std::string& Sector::getSymbol() const
{
	return m_sector->getValue(db::TableImpls::Get()->SECTORS->SYMBOL)->getStringValue();
}

long Sector::getMoveCost() const
{
	return m_sector->getValue(db::TableImpls::Get()->SECTORS->MOVECOST)->getIntegerValue();
}

bool Sector::isWater() const
{
	return m_sector->getValue(db::TableImpls::Get()->SECTORS->WATER)->getBoolValue();
}


void Sector::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SECTORS->NAME, name));
	m_sector->setValue(value);
}

void Sector::setSymbol(const std::string& symbol)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SECTORS->SYMBOL, symbol));
	m_sector->setValue(value);
}

void Sector::setMoveCost(long movecost)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SECTORS->MOVECOST, movecost));
	m_sector->setValue(value);	
}

void Sector::setWater(bool water)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SECTORS->WATER, water ? 0 : 1));
	m_sector->setValue(value);	
}


void Sector::Delete()
{
	Lock();
	m_sector->erase();
}

void Sector::Save()
{
	Lock();
	m_sector->save();
}

void Sector::Discard()
{
	Lock();
	m_sector->discard();
}

bool Sector::Exists()
{
	Lock();
	return m_sector->exists();
}

SavableManagerPtr Sector::getManager() const
{
	return m_sector;
}

TableImplPtr Sector::getTable() const
{
	return m_sector->getTable();
}
