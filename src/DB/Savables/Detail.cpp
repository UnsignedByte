/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Detail.h"
#include "Global.h"
#include "FieldImpls.h"
#include "Trace.h"
#include "TraceManager.h"
#include "Managers.h"
#include "TableImpls.h"

using mud::Detail;

Detail::Detail(SavableManagerPtr detail) :
m_detail(detail)
{
	Assert(detail);
}

Detail::~Detail(void)
{
	Unlock();
}

value_type Detail::getID() const
{
	return m_detail->getValue(db::TableImpls::Get()->DETAILS->DETAILID)->getIntegerValue();
}

const std::string& Detail::getKey() const
{
	return m_detail->getValue(db::TableImpls::Get()->DETAILS->KEY)->getStringValue();	
}

const std::string& Detail::getDescription() const
{
	return m_detail->getValue(db::TableImpls::Get()->DETAILS->DESCRIPTION)->getStringValue();	
}

void Detail::setKey(const std::string& key)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->DETAILS->KEY, key));
	m_detail->setValue(value);
}

void Detail::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->DETAILS->DESCRIPTION, description));
	m_detail->setValue(value);
}

void Detail::Delete()
{
	Lock();
	m_detail->erase();
}

void Detail::Save()
{
	Lock();
	m_detail->save();
}

void mud::Detail::Delete(value_type accountid, const std::string& description)
{
	Assert(!"Not yet implemented.");
}

void mud::Detail::Save(value_type accountid, const std::string& description)
{		
	if(!m_detail->isDirty())
		return;
		
	Lock();
	
	KeysPtr keys = mud::Managers::Get()->Trace->Add();
	mud::TracePtr trace = mud::Managers::Get()->Trace->GetByKey(keys->first()->getIntegerValue());
	
	if(accountid)
	{
		trace->setAccount(accountid);
		
		if(description != Global::Get()->EmptyString)
			trace->setDescription(description);
	}
	
	trace->setDiff(m_detail->getDiff());
	trace->setTime(time(NULL));
	trace->Save(); // create the Trace
	
	RelationPtr relation(new Relation(db::TableImpls::Get()->TRACEDETAIL));
	relation->addKey(db::TableImpls::Get()->TRACEDETAIL->FKDETAILS, getID());
	relation->addKey(db::TableImpls::Get()->TRACEDETAIL->FKTRACES, keys->first()->getIntegerValue());
	relation->save(); // create the relation
	
	m_detail->save(); // save the room
}

void Detail::Discard()
{
	Lock();
	m_detail->discard();
}

bool Detail::Exists()
{
	return m_detail->exists();
}

SavableManagerPtr Detail::getManager() const
{
	return m_detail;
}

TableImplPtr Detail::getTable() const
{
	return m_detail->getTable();
}
