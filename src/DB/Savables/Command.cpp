/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Command.h"
#include "Global.h"
#include "GrantGroup.h"
#include "GrantGroupManager.h"
#include "PermissionManager.h"
#include "Managers.h"
#include "Account.h"
#include "Permission.h"
#include "TableImpls.h"

using mud::Command;

Command::Command(SavableManagerPtr Command) :
m_command(Command)
{
	Assert(Command);
}

Command::~Command(void)
{
	Unlock();
}

const std::string& Command::getName() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->NAME)->getStringValue();
}

const std::string& Command::getHelp() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->HELP)->getStringValue();
}

value_type Command::getGrantGroup() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->GRANTGROUP)->getIntegerValue();
}

bool Command::canHighForce() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->HIGHFORCE)->getBoolValue();
}

bool Command::canForce() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->FORCE)->getBoolValue();
}

bool Command::canLowForce() const
{
	return m_command->getValue(db::TableImpls::Get()->COMMANDS->LOWFORCE)->getBoolValue();
}


void Command::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->NAME, name));
	m_command->setValue(value);
}

void Command::setHelp(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->HELP, name));
	m_command->setValue(value);
}

void Command::setGrantGroup(value_type grantgroup)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->GRANTGROUP, grantgroup));
	m_command->setValue(value);
}

void Command::setHighForce(bool highforce)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->HIGHFORCE, highforce ? 0 : 1));
	m_command->setValue(value);
}

void Command::setForce(bool force)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->FORCE, force ? 0 : 1));
	m_command->setValue(value);
}

void Command::setLowForce(bool lowforce)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COMMANDS->LOWFORCE, lowforce ? 0 : 1));
	m_command->setValue(value);
}


bool Command::getGrant(value_type accountid)
{	
	try
	{
		KeysPtr keys(new Keys(db::TableImpls::Get()->PERMISSIONS));
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, getGrantGroup());
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, accountid);				
		
		PermissionPtr permission = mud::Managers::Get()->Permission->GetByKeys(keys);
		
		return permission->hasGrant();
	} catch(RowNotFoundException& e) {		
		return getDefaultGrant();
	}
}

bool Command::getDefaultGrant()
{
	try
	{
		long id = getGrantGroup();
		GrantGroupPtr grp = mud::Managers::Get()->GrantGroup->GetByKey(id);
		return grp->getDefaultGrant();
	}
	catch(RowNotFoundException& e) {
		return mud::Managers::Get()->Permission->defaultGrant;
	}
}

bool Command::getLog(value_type accountid)
{
	try
	{
		KeysPtr keys(new Keys(db::TableImpls::Get()->PERMISSIONS));
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKGRANTGROUPS, getGrantGroup());
		keys->addKey(db::TableImpls::Get()->PERMISSIONS->FKACCOUNTS, accountid);				
		
		PermissionPtr permission = mud::Managers::Get()->Permission->GetByKeys(keys);
		
		return permission->hasLog();
	} catch(RowNotFoundException& e) {		
		return getDefaultLog();
	}
}

bool Command::getDefaultLog()
{
	try
	{
		long id = getGrantGroup();
		GrantGroupPtr grp = mud::Managers::Get()->GrantGroup->GetByKey(id);
		return grp->getDefaultLog();
	}
	catch(RowNotFoundException& e) {		
		return mud::Managers::Get()->Permission->defaultLog;
	}
}

void Command::Delete()
{
	Lock();
	m_command->erase();
}

void Command::Save()
{
	Lock();
	m_command->save();
}

void Command::Discard()
{
	Lock();
	m_command->discard();
}

bool Command::Exists()
{
	return m_command->exists();
}

SavableManagerPtr Command::getManager() const
{
	return m_command;
}

TableImplPtr Command::getTable() const
{
	return m_command->getTable();
}
