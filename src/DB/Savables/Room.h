/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"

namespace mud
{	
	class Room : public Savable
	{
	public:
		/*
		 * Getters
		 */ 
		value_type getID() const;
		const std::string& getName() const;
		const std::string& getDescription() const;
		value_type getSector() const;
		value_type getCluster() const;
		value_type getHeight() const;
		value_type getWidth() const;
		value_type getLength() const;

		/*
		 * Setters
		 */ 
		void setName(const std::string name);
		void setDescription(const std::string description);
		void setSector(value_type sector);
		void setCluster(value_type cluster);
		void setHeight(value_type height);
		void setWidth(value_type width);
		void setLength(value_type length);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		std::string toString() const;
		std::string toFullString() const;
		
		/*
		 * Database utilities
		 */
		void Delete();
		void Save();
		void Delete(value_type accountid, const std::string& description);
		void Save(value_type accountid, const std::string& description);
		void Discard();
		bool Exists();

	private:
		SavableManagerPtr m_room;		

		Room(SavableManagerPtr room);
		Room(const Room& rhs);
		Room operator=(const Room& rhs);
		~Room(void);

		friend class RoomManager; // constructor
		friend SmartPtrDelete(mud::Room);
	};
}
