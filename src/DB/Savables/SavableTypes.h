/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#pragma once

/**
 * @file SavableTypes.h
 * This file contains Savable type definitions.
 */

#include "Savable.h"

namespace mud 
{ 
	class Managers;
	
	class Account;
	class Area;
	class Channel;
	class Character;
	class Chunk;
	class Cluster;
	class Colour;
	class Command;
	class Detail;
	class Echo;
	class Exit;
	class GrantGroup;
	class MCharacter;
	class PCharacter;
	class Permission;
	class Race;
	class Room;
	class Sector;
	class Social;
	class Trace;

	typedef SmartPtr<mud::Account> AccountPtr; /**< The type of a pointer to an Account. */
	typedef SmartPtr<mud::Area> AreaPtr; /**< The type of a pointer to an Area. */
	typedef SmartPtr<mud::Channel> ChannelPtr; /**< The type of a pointer to a Channel. */
	typedef SmartPtr<mud::Character> CharacterPtr; /**< The type of a pointer to a Character. */
	typedef SmartPtr<mud::Chunk> ChunkPtr; /**< The type of a pointer to a Chunk. */
	typedef SmartPtr<mud::Cluster> ClusterPtr; /**< The type of a pointer to a Cluster. */
	typedef SmartPtr<mud::Colour> ColourPtr; /**< The type of a pointer to a Colour. */
	typedef SmartPtr<mud::Command> CommandPtr; /**< The type of a pointer to a Command. */
	typedef SmartPtr<mud::Detail> DetailPtr; /**< The type of a pointer to a Detail. */
	typedef SmartPtr<mud::Echo> EchoPtr; /**< The type of a pointer to a Echo. */
	typedef SmartPtr<mud::Exit> ExitPtr; /**< The type of a pointer to a Exit. */
	typedef SmartPtr<mud::GrantGroup> GrantGroupPtr; /**< The type of a pointer to a GrantGroup. */
	typedef SmartPtr<mud::MCharacter> MCharacterPtr; /**< The type of a pointer to a MCharacter. */
	typedef SmartPtr<mud::PCharacter> PCharacterPtr; /**< The type of a pointer to a PCharacter. */
	typedef SmartPtr<mud::Permission> PermissionPtr; /**< The type of a pointer to a Permission. */
	typedef SmartPtr<mud::Race> RacePtr; /**< The type of a pointer to a Race. */
	typedef SmartPtr<mud::Room> RoomPtr; /**< The type of a pointer to a Room. */
	typedef SmartPtr<mud::Sector> SectorPtr; /**< The type of a pointer to a Sector. */
	typedef SmartPtr<mud::Social> SocialPtr; /**< The type of a pointer to a Social. */
	typedef SmartPtr<mud::Trace> TracePtr; /**< The type of a pointer to a Trace. */
};
