/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Cluster.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Cluster;

Cluster::Cluster(SavableManagerPtr cluster) :
m_cluster(cluster)
{
	Assert(cluster);
}

Cluster::~Cluster(void)
{
	Unlock();
}

value_type Cluster::getID() const
{
	return m_cluster->getValue(db::TableImpls::Get()->CLUSTERS->CLUSTERID)->getIntegerValue();
}

const std::string& Cluster::getName() const
{
	return m_cluster->getValue(db::TableImpls::Get()->CLUSTERS->NAME)->getStringValue();
}

const std::string& Cluster::getDescription() const
{
	return m_cluster->getValue(db::TableImpls::Get()->CLUSTERS->DESCRIPTION)->getStringValue();	
}

value_type Cluster::getArea() const
{
	return m_cluster->getValue(db::TableImpls::Get()->CLUSTERS->FKAREAS)->getIntegerValue();
}

void Cluster::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CLUSTERS->NAME, name));
	m_cluster->setValue(value);
}

void Cluster::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CLUSTERS->DESCRIPTION, description));
	m_cluster->setValue(value);
}

void Cluster::setArea(value_type area)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CLUSTERS->FKAREAS, area));
	m_cluster->setValue(value);
}

void Cluster::Delete()
{
	Lock();
	m_cluster->erase();
}

void Cluster::Save()
{
	Lock();
	m_cluster->save();
}

void Cluster::Discard()
{
	Lock();
	m_cluster->discard();
}

bool Cluster::Exists()
{
	return m_cluster->exists();
}

SavableManagerPtr Cluster::getManager() const
{
	return m_cluster;
}

TableImplPtr Cluster::getTable() const
{
	return m_cluster->getTable();
}
