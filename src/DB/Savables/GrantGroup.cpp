/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "GrantGroup.h"
#include "Global.h"
#include "Permission.h"
#include "PermissionManager.h"
#include "TableImpls.h"

using mud::GrantGroup;

GrantGroup::GrantGroup(SavableManagerPtr grantgroup) :
m_grantgroup(grantgroup)
{
	Assert(grantgroup);
}

GrantGroup::~GrantGroup(void)
{
	Unlock();
}

const std::string& GrantGroup::getName() const
{
	return m_grantgroup->getValue(db::TableImpls::Get()->GRANTGROUPS->NAME)->getStringValue();
}

value_type GrantGroup::getImplication() const
{
	return m_grantgroup->getValue(db::TableImpls::Get()->GRANTGROUPS->IMPLIES)->getIntegerValue();
}

bool GrantGroup::getDefaultGrant() const
{
	return m_grantgroup->getValue(db::TableImpls::Get()->GRANTGROUPS->DEFAULTGRANT)->getBoolValue();
}

bool GrantGroup::getDefaultLog() const
{
	return m_grantgroup->getValue(db::TableImpls::Get()->GRANTGROUPS->DEFAULTLOG)->getBoolValue();
}

void GrantGroup::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->GRANTGROUPS->NAME, name));
	m_grantgroup->setValue(value);
}

void GrantGroup::setImplication(value_type implication)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->GRANTGROUPS->IMPLIES, implication));
	m_grantgroup->setValue(value);
}

void GrantGroup::setDefaultGrant(bool defaultgrant)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->GRANTGROUPS->DEFAULTGRANT, defaultgrant));
	m_grantgroup->setValue(value);
}

void GrantGroup::setDefaultLog(bool defaultlog)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->GRANTGROUPS->DEFAULTLOG, defaultlog));
	m_grantgroup->setValue(value);
}

void GrantGroup::Delete()
{
	Lock();
	m_grantgroup->erase();
}

void GrantGroup::Save()
{
	Lock();
	m_grantgroup->save();
}

void GrantGroup::Discard()
{
	Lock();
	m_grantgroup->discard();
}

bool GrantGroup::Exists()
{
	Lock();
	return m_grantgroup->exists();
}

SavableManagerPtr GrantGroup::getManager() const
{
	return m_grantgroup;
}


TableImplPtr GrantGroup::getTable() const
{
	return m_grantgroup->getTable();
}
