/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Area.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Area;

Area::Area(SavableManagerPtr area) :
m_area(area)
{
	Assert(area);
}

Area::~Area(void)
{
	Unlock();
}

value_type Area::getID() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->AREAID)->getIntegerValue();
}

const std::string& Area::getName() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->NAME)->getStringValue();
}

const std::string& Area::getDescription() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->DESCRIPTION)->getStringValue();
}

value_type Area::getHeight() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->HEIGHT)->getIntegerValue();
}

value_type Area::getWidth() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->WIDTH)->getIntegerValue();
}

value_type Area::getLength() const
{
	return m_area->getValue(db::TableImpls::Get()->AREAS->LENGTH)->getIntegerValue();
}

void Area::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->AREAS->NAME, name));
	m_area->setValue(value);
}

void Area::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->AREAS->DESCRIPTION, description));
	m_area->setValue(value);
}

void Area::setHeight(value_type height)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->AREAS->HEIGHT, height));
	m_area->setValue(value);
}

void Area::setWidth(value_type width)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->AREAS->WIDTH, width));
	m_area->setValue(value);
}

void Area::setLength(value_type length)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->AREAS->LENGTH, length));
	m_area->setValue(value);
}

void Area::Delete()
{
	Lock();
	m_area->erase();
}

void Area::Save()
{
	Lock();
	m_area->save();
}

void Area::Discard()
{
	Lock();
	m_area->discard();
}

bool Area::Exists()
{
	return m_area->exists();
}

SavableManagerPtr Area::getManager() const
{
	return m_area;
}

TableImplPtr Area::getTable() const
{
	return m_area->getTable();
}
