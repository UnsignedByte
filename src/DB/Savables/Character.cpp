/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef _WIN32
#include <winsock2.h>
#endif

#include <stdarg.h>

#include "StringUtilities.h"
#include "FieldImpls.h"
#include "TableImpls.h"
#include "Character.h"
#include "Race.h"
#include "RaceManager.h"
#include "Area.h"
#include "Chunk.h"
#include "ChunkManager.h"
#include "AreaManager.h"
#include "Managers.h"
#include "CharacterManager.h"
#include "ExitManager.h"

using mud::Character;

Character::Character(SavableManagerPtr character) :
m_character(character)
{
	Assert(character);
}

Character::~Character(void)
{
	Unlock();
}

value_type Character::getID() const
{
	return m_character->getValue(db::TableImpls::Get()->ENTITIES->ENTITYID)->getIntegerValue();
}

const std::string& Character::getName() const
{
	return m_character->getValue(db::TableImpls::Get()->ENTITIES->NAME)->getStringValue();
}

const std::string& Character::getDescription() const
{
	return m_character->getValue(db::TableImpls::Get()->ENTITIES->DESCRIPTION)->getStringValue();
}

value_type Character::getRace() const
{
	return m_character->getValue(db::TableImpls::Get()->ENTITIES->FKRACES)->getIntegerValue();
}

value_type Character::getChunk() const
{
	return m_character->getValue(db::TableImpls::Get()->ENTITIES->FKCHUNKS)->getIntegerValue();
}


/**
 * \brief Setters
 */ 
void Character::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ENTITIES->NAME, name));
	m_character->setValue(value);
	Unlock();
}

void Character::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ENTITIES->DESCRIPTION, description));
	m_character->setValue(value);
	Unlock();
}

void Character::setRace(value_type race)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ENTITIES->FKRACES, race));
	m_character->setValue(value);
	Unlock();
}

void Character::setChunk(value_type chunk)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ENTITIES->FKCHUNKS, chunk));
	m_character->setValue(value);
	Unlock();
}


/**
 * Operations
 */ 

void mud::Character::MoveToChunk(value_type chunkid)
{
	mud::ChunkPtr inchunk = mud::Managers::Get()->Chunk->GetByKey(getChunk());
	mud::ChunkPtr tochunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	
	value_type id = getID();
		
	inchunk->removeCharacter(id);
	setChunk(chunkid);
	tochunk->addCharacter(id);
}

/**
 * Start of Output
 */ 
void Character::OnSend(const std::string& msg)
{
	// discard msg
	return;
}

void Character::OnSendf(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	OnSend(Global::Get()->sprint(args, format));
	va_end(args);
	return;
}
/**
 * End of Output
 */ 


/**
 * Start of Commands
 */ 	
 
void Character::OnLook(const std::string& msg)
{	
	long chunkid = getChunk();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	Assert(chunk);
	
	OnSend(chunk->toString());
	OnSend("\n");
	OnCharactersInRoom(Global::Get()->EmptyString);	
	OnSend("\n");	
	OnExits(Global::Get()->EmptyString);
	
	return;
}

void Character::OnCharactersInRoom(const std::string& msg)
{
	long chunkid = getChunk();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	Assert(chunk);
	
	for(value_types::const_iterator it = chunk->begin(); it != chunk->end(); it++)
	{
		value_type characterid = *it;
		
		CharacterPtr pch = mud::Managers::Get()->Character->GetByKey(characterid);
		Assert(pch);

		OnSendf("(%s) %s\n", pch->getName().c_str(), pch->getDescription().c_str());
	}
}

void Character::OnScore(const std::string& msg)
{
	OnSend(m_character->toString());
	return;
}

void Character::OnSay(const std::string& msg)
{
	if(msg.size() == 0)
	{
		OnSend("Say what?\n");
		return;
	}
	
	long chunkid = getChunk();
	ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	
	std::string line = getName();
	line.append(" says '");
	line.append(msg);
	line.append("'\n");

	chunk->Send(line);
	return;
}

void Character::OnExits(const std::string& msg)
{
	long chunkid = getChunk();
	mud::ChunkPtr chunk = mud::Managers::Get()->Chunk->GetByKey(chunkid);
	
	std::string result = "[";
	result.append(String::Get()->unlines(mud::Managers::Get()->Exit->ReadableList(chunk), " ", 0));
	result.append("]\n");
	
	OnSend(result);
}
	
/**
 * End of commands
 */ 

void Character::Delete() 
{ 
	Lock();
	m_character->erase();
	Unlock();
}

void Character::Save() 
{
	Lock();
	m_character->save();
	Unlock();
}

void Character::Discard()
{
	Lock();
	m_character->discard();
	Unlock();
}

bool Character::Exists() 
{ 
	return m_character->exists(); 
}

SavableManagerPtr Character::getManager() const
{
	return m_character;
}

TableImplPtr Character::getTable() const
{ 
	return m_character->getTable();
}
