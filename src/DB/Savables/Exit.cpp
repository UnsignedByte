/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Exit.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Exit;

Exit::Exit(SavableManagerPtr exit) :
m_exit(exit)
{
	Assert(m_exit);
}

Exit::~Exit(void)
{
	Unlock();
}

value_type Exit::getFromChunk() const
{
	return m_exit->getValue(db::TableImpls::Get()->EXITS->FKCHUNKSFROM)->getIntegerValue();
}

value_type Exit::getToChunk() const
{
	return m_exit->getValue(db::TableImpls::Get()->EXITS->FKCHUNKSTO)->getIntegerValue();
}

Coordinate Exit::getDirection() const
{
	/*
	 * The -1 is to correct from the +1 from below
	 */
	int x = m_exit->getValue(db::TableImpls::Get()->EXITS->X)->getIntegerValue()-1;
	int y = m_exit->getValue(db::TableImpls::Get()->EXITS->Y)->getIntegerValue()-1;
	int z = m_exit->getValue(db::TableImpls::Get()->EXITS->Z)->getIntegerValue()-1;
	
	Coordinate result(x, y, z);
	return result;
}

void Exit::setFromChunk(value_type room)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSFROM, room));
	m_exit->setValue(value);
}

void Exit::setToChunk(value_type room)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSTO, room));
	m_exit->setValue(value);
}

void Exit::setDirection(const Coordinate& direction)
{
	Assert(direction.isDirection());
	
	Lock();
	ValuePtr value;

	/*
	 * The +1 is to keep negative values from going into the database.	
	 */
	value = FieldValuePtr(new FieldValue(db::TableImpls::Get()->EXITS->X, direction.getXCoordinate()+1));
	m_exit->setValue(value);
	
	value = FieldValuePtr(new FieldValue(db::TableImpls::Get()->EXITS->Y, direction.getYCoordinate()+1));
	m_exit->setValue(value);
	
	value = FieldValuePtr(new FieldValue(db::TableImpls::Get()->EXITS->Z, direction.getZCoordinate()+1));
	m_exit->setValue(value);
}

void Exit::Delete()
{
	Lock();
	m_exit->erase();
}

void Exit::Save()
{
	Lock();
	m_exit->save();
}

void Exit::Discard()
{
	Lock();
	m_exit->discard();
}

bool Exit::Exists()
{
	return m_exit->exists();
}

SavableManagerPtr Exit::getManager() const
{
	return m_exit;
}

TableImplPtr Exit::getTable() const
{
	return m_exit->getTable();
}
