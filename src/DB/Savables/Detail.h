/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"

namespace mud
{
	class DetailManager;
	
	class Detail : public Savable
	{
	public:
		/*
		 * Getters
		 */
		value_type getID() const;
		const std::string& getKey() const;
		const std::string& getDescription() const;

		/*
		 * Setters
		 */
		void setKey(const std::string& name);
		void setDescription(const std::string& description);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		
		/*
		 * Database operations
		 */
		void Delete();
		void Save();
		void Delete(value_type accountid, const std::string& description);
		void Save(value_type accountid, const std::string& description);
		void Discard();
		bool Exists();

	private:
		friend class mud::DetailManager; // For constructor
		friend SmartPtrDelete(mud::Detail);
		
		SavableManagerPtr m_detail;

		/**
		 * Constructor
		 * @param detail The DB object
		 */
		Detail(SavableManagerPtr detail);
		
		Detail(const Detail& rhs);
		Detail operator=(const Detail& rhs);
			
		/** Destructor, unlocks the savable. */
		~Detail(void);
	};
}
