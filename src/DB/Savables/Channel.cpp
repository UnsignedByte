/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Channel.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Channel;

Channel::Channel(SavableManagerPtr channel) :
m_channel(channel)
{
	Assert(channel);
}

Channel::~Channel(void)
{
	Unlock();
}

const std::string& Channel::getName() const
{
	return m_channel->getValue(db::TableImpls::Get()->CHANNELS->NAME)->getStringValue();
}

const std::string& Channel::getDescription() const
{
	return m_channel->getValue(db::TableImpls::Get()->CHANNELS->DESCRIPTION)->getStringValue();
}

bool mud::Channel::needLogin() const
{
	return m_channel->getValue(db::TableImpls::Get()->CHANNELS->NEEDLOGIN)->getBoolValue();
}

void Channel::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHANNELS->NAME, name));
	m_channel->setValue(value);
}

void Channel::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHANNELS->DESCRIPTION, description));
	m_channel->setValue(value);
}

void mud::Channel::setNeedLogin(bool needLogin)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->CHANNELS->NEEDLOGIN, needLogin));
	m_channel->setValue(value);
}

void Channel::Delete()
{
	Lock();
	m_channel->erase();
}

void Channel::Save()
{
	Lock();
	m_channel->save();
}

void Channel::Discard()
{
	Lock();
	m_channel->discard();
}

bool Channel::Exists()
{
	return m_channel->exists();
}

SavableManagerPtr Channel::getManager() const
{
	return m_channel;
}

TableImplPtr Channel::getTable() const
{
	return m_channel->getTable();
}
