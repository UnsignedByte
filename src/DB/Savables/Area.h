/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"

namespace mud
{
	class AreaManager;
	
	class Area : public Savable
	{
	public:
		/*
		 * Getters
		 */
		value_type getID() const; 
		const std::string& getName() const;
		const std::string& getDescription() const;
		value_type getHeight() const;
		value_type getWidth() const;
		value_type getLength() const;

		/*
		 * Setters
		 */
		void setName(const std::string& name);
		void setDescription(const std::string& description);
		void setHeight(value_type height);
		void setWidth(value_type width);
		void setLength(value_type length);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		
		/*
		 * brief Database operations
		 */
		void Delete();
		void Save();
		void Discard();
		bool Exists();

	private:
		friend class mud::AreaManager; // For constructor
		friend SmartPtrDelete(mud::Area);
		
		SavableManagerPtr m_area;

		/**
		 * Constructor
		 * @param area The DB object
		 */
		Area(SavableManagerPtr area);
		
		Area(const Area& rhs);
		Area operator=(const Area& rhs);
			
		/** Destructor, unlocks the savable. */
		~Area(void);
	};
}
