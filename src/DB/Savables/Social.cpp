/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Social.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using mud::Social;

Social::Social(SavableManagerPtr social) :
m_social(social)
{
	Assert(social);
}

Social::~Social(void)
{
	Unlock();
}

value_type Social::getID() const
{
	return m_social->getValue(db::TableImpls::Get()->SOCIALS->SOCIALID)->getIntegerValue();
}

const std::string& Social::getName() const
{
	return m_social->getValue(db::TableImpls::Get()->SOCIALS->NAME)->getStringValue();
}

const std::string& Social::getMessage() const
{
	return m_social->getValue(db::TableImpls::Get()->SOCIALS->MESSAGE)->getStringValue();
}

void Social::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SOCIALS->NAME, name));
	m_social->setValue(value);
}

void Social::setMessage(const std::string& message)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->SOCIALS->MESSAGE, message));
	m_social->setValue(value);
}

void Social::Delete()
{
	Lock();
	m_social->erase();
}

void Social::Save()
{
	Lock();
	m_social->save();
}

void Social::Discard()
{
	Lock();
	m_social->discard();
}

bool Social::Exists()
{
	return m_social->exists();
}

SavableManagerPtr Social::getManager() const
{
	return m_social;
}

TableImplPtr Social::getTable() const
{
	return m_social->getTable();
}
