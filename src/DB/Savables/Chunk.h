/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"
#include "Coordinate.h"

namespace mud
{
	class ChunkManager;
	
	class Chunk : public Savable
	{
	public:
		/*
		 * Getters
		 */
		value_type getID() const;
		const std::string& getName() const;
		const std::string& getDescription() const;
		const std::string& getTags() const;
		value_type getRoom() const;
		
		/**
		 * Returns the Exit in the specified direction, if there is one.
		 *
		 * The specified coordinate should be a direction, this is asserted with isDirection at runtime.
		 * When no room could be found in the specified direction, 0 is returned. 
		 */ 
		value_type getExitAt(Coordinate direction) const;

		/*
		 * Setters
		 */
		void setName(const std::string& name);
		void setDescription(const std::string& description);
		void setTags(const std::string& tags);
		void setRoom(value_type room);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		std::string toString() const;
		std::string toFullString() const;
		
		/*
		 * Room operations
		 */ 
		void addCharacter(value_type characterid);
		void removeCharacter(value_type characterid);

		/** Returns an iterator to the first character in the room. */
		value_types::const_iterator begin() ;
		
		/** Returns the 'end' iterator of the characters in the room. */
		value_types::const_iterator end() ;
		
		/** Returns the size of the characters in the room. */
		value_type size() ;

		/** Sends a message to all characters in the room. */
		void Send(const std::string& msg);
		
		/*
		 * Database operations
		 */
		void Delete();
		void Save();
		void Discard();
		bool Exists();

	private:
		friend class mud::ChunkManager; // For constructor
		friend SmartPtrDelete(mud::Chunk);
		
		SavableManagerPtr m_chunk;
		static std::map<value_type,value_types> ms_charactersInRoom;

		/**
		 * \brief Constructor
		 * \param chunk The DB object
		 */
		Chunk(SavableManagerPtr chunk);
		
		Chunk(const Chunk& rhs);
		Chunk operator=(const Chunk& rhs);
			
		/** Destructor, unlocks the savable. */
		~Chunk(void);
	};
}
