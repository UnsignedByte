/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef _WIN32
#include <winsock2.h>
#endif

#include <stdarg.h>

#include "Room.h"
#include "Area.h"
#include "Sector.h"
#include "SectorManager.h"
#include "Character.h"
#include "CharacterManager.h"
#include "Trace.h"
#include "TraceManager.h"
#include "TableImpls.h"
#include "Managers.h"
#include "StringUtilities.h"
#include "Global.h"
#include "FieldImpls.h"
#include "Detail.h"
#include "DetailManager.h"
#include "RoomManager.h"

using mud::Room;

Room::Room(SavableManagerPtr room) :
m_room(room)
{
	Assert(room);
}

Room::~Room(void)
{	
	Unlock();
}

value_type Room::getID() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->ROOMID)->getIntegerValue();
}

const std::string& Room::getName() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->NAME)->getStringValue();
}

const std::string& Room::getDescription() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->DESCRIPTION)->getStringValue();
}

value_type Room::getSector() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->FKSECTORS)->getIntegerValue();
}

value_type Room::getCluster() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->FKCLUSTERS)->getIntegerValue();
}

value_type Room::getHeight() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->HEIGHT)->getIntegerValue();
}

value_type Room::getWidth() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->WIDTH)->getIntegerValue();
}

value_type Room::getLength() const
{
	return m_room->getValue(db::TableImpls::Get()->ROOMS->LENGTH)->getIntegerValue();
}


void Room::setName(const std::string name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->NAME, name));
	m_room->setValue(value);
}

void Room::setDescription(const std::string description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->DESCRIPTION, description));
	m_room->setValue(value);
}

void Room::setSector(value_type sector)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->FKSECTORS, sector));
	m_room->setValue(value);	
}

void Room::setCluster(value_type cluster)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->FKCLUSTERS, cluster));
	m_room->setValue(value);	
}

void Room::setHeight(value_type height)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->HEIGHT, height));
	m_room->setValue(value);
}

void Room::setWidth(value_type width)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->WIDTH, width));
	m_room->setValue(value);	
}

void Room::setLength(value_type length)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->LENGTH, length));
	m_room->setValue(value);	
}

void Room::Delete()
{
	Lock();
	m_room->erase();
}

void Room::Save()
{
	Lock();
	m_room->save();
}

void mud::Room::Delete(value_type accountid, const std::string& description)
{
	// TODO ?
	Assert(!"Not yet implemented");
}

void mud::Room::Save(value_type accountid, const std::string& description)
{	
	if(!m_room->isDirty())
		return;
	
	Lock();
	
	KeysPtr keys = mud::Managers::Get()->Trace->Add();
	mud::TracePtr trace = mud::Managers::Get()->Trace->GetByKey(keys->first()->getIntegerValue());
	
	if(accountid)
	{
		trace->setAccount(accountid);
		
		if(description != Global::Get()->EmptyString)
			trace->setDescription(description);
	}
	
	trace->setDiff(m_room->getDiff());
	trace->setTime(time(NULL));
	trace->Save(); // create the Trace
	
	RelationPtr relation(new Relation(db::TableImpls::Get()->TRACEROOM));
	relation->addKey(db::TableImpls::Get()->TRACEROOM->FKROOMS, getID());
	relation->addKey(db::TableImpls::Get()->TRACEROOM->FKTRACES, keys->first()->getIntegerValue());
	relation->save(); // create the relation
	
	m_room->save(); // save the room
}

void Room::Discard()
{
	Lock();
	m_room->discard();
}

bool Room::Exists()
{
	Lock();
	return m_room->exists();
}

#if 0
std::string Room::CreateMap(value_type id, long origx, long origy)
{
	std::string result;

	long count = DatabaseMgr::Get()->CountSavable(Tables::Get()->AREAS, id);

	if(count <= 0)
	{
		Global::Get()->bug("EditorMap::Map::Run() with areacount <= 0!\n");
		return std::string("For some reason the area the room you are in does not belong to an area?!\n");
	}

	Assert(count == 1);

	Area* p = Cache::Get()->GetArea(id);

	for(value_type y = 1; y <= p->getHeight(); y++)
	{
		std::string toprow;
		std::string thisrow;
		std::string bottomrow;

		for(value_type x = 1; x <= p->getWidth(); x++)
		{
			long rid = Cache::Get()->GetRoomID(id, x, y);
			long count = DatabaseMgr::Get()->CountSavable(Tables::Get()->ROOMS, rid);
			if(!count)
			{
				if(x == origx && y == origy)
				{
					toprow.append   ("   ");
					thisrow.append  (" o "); // Empty, current
					bottomrow.append("   ");
				}
				else
				{
					toprow.append   ("   ");
					thisrow.append  (" . "); // Empty, not current
					bottomrow.append("   ");
				}

			}
			else 
			{
				Room* room = Cache::Get()->GetRoom(rid);

				if(!room->isClosed(Exit::NORTHWEST))
					toprow.append("\\");
				else
					toprow.append(" ");

				if(!room->isClosed(Exit::NORTH))
					toprow.append("|");
				else
					toprow.append(" ");

				if(!room->isClosed(Exit::NORTHEAST))
					toprow.append("/");
				else
					toprow.append(" ");

				if(!room->isClosed(Exit::WEST))
					thisrow.append("-");
				else
					thisrow.append(" ");

				if(x == origx && y == origy)
				{
					thisrow.append("*");
				}
				else
				{					
					Sector* sector = Cache::Get()->GetSector(room->getSector());
					thisrow.append(sector->getSymbol());					
				}

				if(!room->isClosed(Exit::EAST))
					thisrow.append("-");
				else
					thisrow.append(" ");

				if(!room->isClosed(Exit::SOUTHWEST))
					bottomrow.append("/");
				else
					bottomrow.append(" ");

				if(!room->isClosed(Exit::SOUTH))
					bottomrow.append("|");
				else
					bottomrow.append(" ");

				if(!room->isClosed(Exit::SOUTHEAST))
					bottomrow.append("\\");
				else
					bottomrow.append(" ");
			}
		}

		result.append(toprow);
		result.append("\n");
		result.append(thisrow);
		result.append("\n");
		result.append(bottomrow);
		result.append("\n");
	}

	return result;
}
#endif

SavableManagerPtr Room::getManager() const
{
	return m_room;
}

TableImplPtr Room::getTable() const
{
	return m_room->getTable();
}

std::string Room::toString() const
{
	Strings result;
	std::string line;
	
	value_type roomid = getID();
	RoomPtr room = mud::Managers::Get()->Room->GetByKey(roomid);
	Assert(room);
	
	line = room->getDescription();
	result.push_back(line);
	
	Strings details = mud::Managers::Get()->Detail->ReadableList(room);	
	
	// Add all details
	for(Strings::const_iterator it = details.begin(); it != details.end(); it++) {
		result.push_back(*it);
	}
	
	return String::Get()->unlines(result, " ", 0);
}

std::string Room::toFullString() const
{
	return Savable::toString();
}
