/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "SavableHeaders.h"
#include "SavableTypes.h"
#include "Coordinate.h"

namespace mud
{
	class ExitManager;
	
	class Exit : public Savable
	{
	public:
		/*
		 * Getters
		 */
		value_type getFromChunk() const;
		value_type getToChunk() const;
		Coordinate getDirection() const;

		/*
		 * Setters
		 */
		void setFromChunk(value_type room);
		void setToChunk(value_type room);
		void setDirection(const Coordinate& direction);

		/*
		 * Utilities
		 */
		SavableManagerPtr getManager() const;
		TableImplPtr getTable() const;
		
		/*
		 * Database operations
		 */
		void Delete();
		void Save();
		void Discard();
		bool Exists();

	private:
		friend class mud::ExitManager; // For constructor
		friend SmartPtrDelete(mud::Exit);
		
		SavableManagerPtr m_exit;

		/**
		 * Constructor
		 * @param exit The DB object
		 */
		Exit(SavableManagerPtr exit);
		
		Exit(const Exit& rhs);
		Exit operator=(const Exit& rhs);
			
		/** Destructor, unlocks the savable. */
		~Exit(void);
	};
}
