/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Colour.h"
#include "Global.h"
#include "FieldImpls.h"
#include "TableImpls.h"

using namespace mud;

Colour::Colour(SavableManagerPtr object) :
m_colour(object)
{
	Assert(object);
}

Colour::~Colour(void)
{
	Unlock();
}

const std::string& Colour::getName() const
{
	return m_colour->getValue(db::TableImpls::Get()->COLOURS->NAME)->getStringValue();
}

const std::string& Colour::getCode() const
{
	return m_colour->getValue(db::TableImpls::Get()->COLOURS->CODE)->getStringValue();
}

bool Colour::isAnsi() const
{
	return m_colour->getValue(db::TableImpls::Get()->COLOURS->ANSI)->getBoolValue();
}

void Colour::setName(const std::string& name)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COLOURS->NAME, name));
	m_colour->setValue(value);	
}

void Colour::setColourString(const std::string& colourstring)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COLOURS->COLOURSTRING, colourstring));
	m_colour->setValue(value);
}

void Colour::setCode(const std::string& code)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COLOURS->CODE, code));
	m_colour->setValue(value);	
}

void Colour::setAnsi(bool ansi)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->COLOURS->ANSI, ansi ? 1 : 0));
	m_colour->setValue(value);
}

std::string Colour::getColourString()
{
	std::string restore = "\x1B[0;0m";
	std::string prefix = "\x1B[";
	std::string str = m_colour->getValue(db::TableImpls::Get()->COLOURS->COLOURSTRING)->getStringValue();
	
	std::string result = restore;
	result.append(prefix);
	result.append(str);

	return result;
}

void Colour::Delete()
{
	Lock();
	m_colour->erase();
}

void Colour::Save()
{
	Lock();
	m_colour->save();
}

void Colour::Discard()
{
	Lock();
	m_colour->discard();
}

bool Colour::Exists()
{
	return m_colour->exists();
}

SavableManagerPtr Colour::getManager() const
{
	return m_colour;
}

TableImplPtr Colour::getTable() const
{
	return m_colour->getTable();
}
