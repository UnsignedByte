/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Trace.h"
#include "Global.h"
#include "FieldImpls.h"
#include "Account.h"
#include "AccountManager.h"
#include "TableImpls.h"

using mud::Trace;
using namespace mud;

Trace::Trace(SavableManagerPtr trace) :
m_trace(trace)
{
	Assert(trace);
}

Trace::~Trace(void)
{
	Unlock();
}

const std::string& Trace::getDiff() const
{
	return m_trace->getValue(db::TableImpls::Get()->TRACES->DIFF)->getStringValue();
}

const std::string& Trace::getDescription() const
{
	return m_trace->getValue(db::TableImpls::Get()->TRACES->DESCRIPTION)->getStringValue();
}

value_type Trace::getAccount() const
{
	return m_trace->getValue(db::TableImpls::Get()->TRACES->FKACCOUNTS)->getIntegerValue();
}

time_t Trace::getTime() const
{
	return m_trace->getValue(db::TableImpls::Get()->TRACES->TIME)->getIntegerValue();
}

void Trace::setDiff(const std::string& diff)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->TRACES->DIFF, diff));
	m_trace->setValue(value);
}

void Trace::setDescription(const std::string& description)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->TRACES->DESCRIPTION, description));
	m_trace->setValue(value);
}

void Trace::setTime(time_t time)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->TRACES->TIME, time));
	m_trace->setValue(value);
}

void Trace::setAccount(value_type account)
{
	Lock();
	ValuePtr value(new FieldValue(db::TableImpls::Get()->TRACES->FKACCOUNTS, account));
	m_trace->setValue(value);
}


void Trace::Delete()
{
	Lock();
	m_trace->erase();
}

void Trace::Save()
{
	Lock();
	m_trace->save();
}

void Trace::Discard()
{
	Lock();
	m_trace->discard();
}

bool Trace::Exists()
{
	return m_trace->exists();
}

TableImplPtr Trace::getTable() const
{
	return m_trace->getTable();
}

SavableManagerPtr mud::Trace::getManager() const
{
	return m_trace;
}
