##
## Auto Generated makefile, please do not edit
##
ProjectName:=DB

## Debug
ifeq ($(type),Debug)
ConfigurationName :=Debug
IntermediateDirectory :=./Debug
OutDir := $(IntermediateDirectory)
LinkerName:=g++
ArchiveTool :=ar rcu
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix :=.o
DebugSwitch :=-gstab
IncludeSwitch :=-I
LibrarySwitch :=-l
OutputSwitch :=-o 
LibraryPathSwitch :=-L
PreprocessorSwitch :=-D
SourceSwitch :=-c 
CompilerName :=g++
OutputFile :=../../lib/libubdb.a
Preprocessors :=
ObjectSwitch :=-o 
ArchiveOutputSwitch := 
CmpOptions :=-g -Wall $(Preprocessors)
LinkOptions := 
IncludePath :=  $(IncludeSwitch). $(IncludeSwitch)../Sockets $(IncludeSwitch)../Resource $(IncludeSwitch)../DAL $(IncludeSwitch)./Generated $(IncludeSwitch)./Managers $(IncludeSwitch)./Savables 
RcIncludePath :=
Libs :=
LibPath := 
endif

Objects=$(IntermediateDirectory)/FieldImpls$(ObjectSuffix) $(IntermediateDirectory)/TableImpls$(ObjectSuffix) $(IntermediateDirectory)/Account$(ObjectSuffix) $(IntermediateDirectory)/Area$(ObjectSuffix) $(IntermediateDirectory)/Channel$(ObjectSuffix) $(IntermediateDirectory)/Character$(ObjectSuffix) $(IntermediateDirectory)/Chunk$(ObjectSuffix) $(IntermediateDirectory)/Cluster$(ObjectSuffix) $(IntermediateDirectory)/Colour$(ObjectSuffix) $(IntermediateDirectory)/Command$(ObjectSuffix) \
	$(IntermediateDirectory)/Detail$(ObjectSuffix) $(IntermediateDirectory)/Echo$(ObjectSuffix) $(IntermediateDirectory)/Exit$(ObjectSuffix) $(IntermediateDirectory)/GrantGroup$(ObjectSuffix) $(IntermediateDirectory)/MCharacter$(ObjectSuffix) $(IntermediateDirectory)/Permission$(ObjectSuffix) $(IntermediateDirectory)/Race$(ObjectSuffix) $(IntermediateDirectory)/Room$(ObjectSuffix) $(IntermediateDirectory)/Savable$(ObjectSuffix) $(IntermediateDirectory)/Sector$(ObjectSuffix) \
	$(IntermediateDirectory)/Trace$(ObjectSuffix) $(IntermediateDirectory)/AccountManager$(ObjectSuffix) $(IntermediateDirectory)/AreaManager$(ObjectSuffix) $(IntermediateDirectory)/ChannelManager$(ObjectSuffix) $(IntermediateDirectory)/CharacterManager$(ObjectSuffix) $(IntermediateDirectory)/ChunkManager$(ObjectSuffix) $(IntermediateDirectory)/ClusterManager$(ObjectSuffix) $(IntermediateDirectory)/ColourManager$(ObjectSuffix) $(IntermediateDirectory)/CommandManager$(ObjectSuffix) $(IntermediateDirectory)/DetailManager$(ObjectSuffix) \
	$(IntermediateDirectory)/EchoManager$(ObjectSuffix) $(IntermediateDirectory)/ExitManager$(ObjectSuffix) $(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix) $(IntermediateDirectory)/MCharacterManager$(ObjectSuffix) $(IntermediateDirectory)/PermissionManager$(ObjectSuffix) $(IntermediateDirectory)/RaceManager$(ObjectSuffix) $(IntermediateDirectory)/RoomManager$(ObjectSuffix) $(IntermediateDirectory)/SectorManager$(ObjectSuffix) $(IntermediateDirectory)/TraceManager$(ObjectSuffix) $(IntermediateDirectory)/Managers$(ObjectSuffix) \
	$(IntermediateDirectory)/SocialManager$(ObjectSuffix) 

##
## Main Build Tragets 
##
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile):  $(Objects)
	$(ArchiveTool) $(ArchiveOutputSwitch)$(OutputFile) $(Objects)

./Debug:
	@test -d ./Debug || mkdir ./Debug


PreBuild:


##
## Objects
##
$(IntermediateDirectory)/FieldImpls$(ObjectSuffix): Generated/FieldImpls.cpp $(IntermediateDirectory)/FieldImpls$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Generated/FieldImpls.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/FieldImpls$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/FieldImpls$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/FieldImpls$(ObjectSuffix) -MF$(IntermediateDirectory)/FieldImpls$(ObjectSuffix).d -MM Generated/FieldImpls.cpp

$(IntermediateDirectory)/TableImpls$(ObjectSuffix): Generated/TableImpls.cpp $(IntermediateDirectory)/TableImpls$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Generated/TableImpls.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/TableImpls$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TableImpls$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/TableImpls$(ObjectSuffix) -MF$(IntermediateDirectory)/TableImpls$(ObjectSuffix).d -MM Generated/TableImpls.cpp

$(IntermediateDirectory)/Account$(ObjectSuffix): Savables/Account.cpp $(IntermediateDirectory)/Account$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Account.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Account$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Account$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Account$(ObjectSuffix) -MF$(IntermediateDirectory)/Account$(ObjectSuffix).d -MM Savables/Account.cpp

$(IntermediateDirectory)/Area$(ObjectSuffix): Savables/Area.cpp $(IntermediateDirectory)/Area$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Area.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Area$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Area$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Area$(ObjectSuffix) -MF$(IntermediateDirectory)/Area$(ObjectSuffix).d -MM Savables/Area.cpp

$(IntermediateDirectory)/Channel$(ObjectSuffix): Savables/Channel.cpp $(IntermediateDirectory)/Channel$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Channel.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Channel$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Channel$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Channel$(ObjectSuffix) -MF$(IntermediateDirectory)/Channel$(ObjectSuffix).d -MM Savables/Channel.cpp

$(IntermediateDirectory)/Character$(ObjectSuffix): Savables/Character.cpp $(IntermediateDirectory)/Character$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Character.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Character$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Character$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Character$(ObjectSuffix) -MF$(IntermediateDirectory)/Character$(ObjectSuffix).d -MM Savables/Character.cpp

$(IntermediateDirectory)/Chunk$(ObjectSuffix): Savables/Chunk.cpp $(IntermediateDirectory)/Chunk$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Chunk.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Chunk$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Chunk$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Chunk$(ObjectSuffix) -MF$(IntermediateDirectory)/Chunk$(ObjectSuffix).d -MM Savables/Chunk.cpp

$(IntermediateDirectory)/Cluster$(ObjectSuffix): Savables/Cluster.cpp $(IntermediateDirectory)/Cluster$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Cluster.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Cluster$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Cluster$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Cluster$(ObjectSuffix) -MF$(IntermediateDirectory)/Cluster$(ObjectSuffix).d -MM Savables/Cluster.cpp

$(IntermediateDirectory)/Colour$(ObjectSuffix): Savables/Colour.cpp $(IntermediateDirectory)/Colour$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Colour.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Colour$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Colour$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Colour$(ObjectSuffix) -MF$(IntermediateDirectory)/Colour$(ObjectSuffix).d -MM Savables/Colour.cpp

$(IntermediateDirectory)/Command$(ObjectSuffix): Savables/Command.cpp $(IntermediateDirectory)/Command$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Command.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Command$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Command$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Command$(ObjectSuffix) -MF$(IntermediateDirectory)/Command$(ObjectSuffix).d -MM Savables/Command.cpp

$(IntermediateDirectory)/Detail$(ObjectSuffix): Savables/Detail.cpp $(IntermediateDirectory)/Detail$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Detail.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Detail$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Detail$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Detail$(ObjectSuffix) -MF$(IntermediateDirectory)/Detail$(ObjectSuffix).d -MM Savables/Detail.cpp

$(IntermediateDirectory)/Echo$(ObjectSuffix): Savables/Echo.cpp $(IntermediateDirectory)/Echo$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Echo.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Echo$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Echo$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Echo$(ObjectSuffix) -MF$(IntermediateDirectory)/Echo$(ObjectSuffix).d -MM Savables/Echo.cpp

$(IntermediateDirectory)/Exit$(ObjectSuffix): Savables/Exit.cpp $(IntermediateDirectory)/Exit$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Exit.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Exit$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Exit$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Exit$(ObjectSuffix) -MF$(IntermediateDirectory)/Exit$(ObjectSuffix).d -MM Savables/Exit.cpp

$(IntermediateDirectory)/GrantGroup$(ObjectSuffix): Savables/GrantGroup.cpp $(IntermediateDirectory)/GrantGroup$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/GrantGroup.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/GrantGroup$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/GrantGroup$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/GrantGroup$(ObjectSuffix) -MF$(IntermediateDirectory)/GrantGroup$(ObjectSuffix).d -MM Savables/GrantGroup.cpp

$(IntermediateDirectory)/MCharacter$(ObjectSuffix): Savables/MCharacter.cpp $(IntermediateDirectory)/MCharacter$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/MCharacter.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/MCharacter$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/MCharacter$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/MCharacter$(ObjectSuffix) -MF$(IntermediateDirectory)/MCharacter$(ObjectSuffix).d -MM Savables/MCharacter.cpp

$(IntermediateDirectory)/Permission$(ObjectSuffix): Savables/Permission.cpp $(IntermediateDirectory)/Permission$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Permission.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Permission$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Permission$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Permission$(ObjectSuffix) -MF$(IntermediateDirectory)/Permission$(ObjectSuffix).d -MM Savables/Permission.cpp

$(IntermediateDirectory)/Race$(ObjectSuffix): Savables/Race.cpp $(IntermediateDirectory)/Race$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Race.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Race$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Race$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Race$(ObjectSuffix) -MF$(IntermediateDirectory)/Race$(ObjectSuffix).d -MM Savables/Race.cpp

$(IntermediateDirectory)/Room$(ObjectSuffix): Savables/Room.cpp $(IntermediateDirectory)/Room$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Room.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Room$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Room$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Room$(ObjectSuffix) -MF$(IntermediateDirectory)/Room$(ObjectSuffix).d -MM Savables/Room.cpp

$(IntermediateDirectory)/Savable$(ObjectSuffix): Savables/Savable.cpp $(IntermediateDirectory)/Savable$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Savable.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Savable$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Savable$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Savable$(ObjectSuffix) -MF$(IntermediateDirectory)/Savable$(ObjectSuffix).d -MM Savables/Savable.cpp

$(IntermediateDirectory)/Sector$(ObjectSuffix): Savables/Sector.cpp $(IntermediateDirectory)/Sector$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Sector.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Sector$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Sector$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Sector$(ObjectSuffix) -MF$(IntermediateDirectory)/Sector$(ObjectSuffix).d -MM Savables/Sector.cpp

$(IntermediateDirectory)/Trace$(ObjectSuffix): Savables/Trace.cpp $(IntermediateDirectory)/Trace$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Savables/Trace.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Trace$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Trace$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Trace$(ObjectSuffix) -MF$(IntermediateDirectory)/Trace$(ObjectSuffix).d -MM Savables/Trace.cpp

$(IntermediateDirectory)/AccountManager$(ObjectSuffix): Managers/AccountManager.cpp $(IntermediateDirectory)/AccountManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/AccountManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/AccountManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/AccountManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/AccountManager$(ObjectSuffix) -MF$(IntermediateDirectory)/AccountManager$(ObjectSuffix).d -MM Managers/AccountManager.cpp

$(IntermediateDirectory)/AreaManager$(ObjectSuffix): Managers/AreaManager.cpp $(IntermediateDirectory)/AreaManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/AreaManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/AreaManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/AreaManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/AreaManager$(ObjectSuffix) -MF$(IntermediateDirectory)/AreaManager$(ObjectSuffix).d -MM Managers/AreaManager.cpp

$(IntermediateDirectory)/ChannelManager$(ObjectSuffix): Managers/ChannelManager.cpp $(IntermediateDirectory)/ChannelManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/ChannelManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ChannelManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ChannelManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ChannelManager$(ObjectSuffix) -MF$(IntermediateDirectory)/ChannelManager$(ObjectSuffix).d -MM Managers/ChannelManager.cpp

$(IntermediateDirectory)/CharacterManager$(ObjectSuffix): Managers/CharacterManager.cpp $(IntermediateDirectory)/CharacterManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/CharacterManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/CharacterManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/CharacterManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/CharacterManager$(ObjectSuffix) -MF$(IntermediateDirectory)/CharacterManager$(ObjectSuffix).d -MM Managers/CharacterManager.cpp

$(IntermediateDirectory)/ChunkManager$(ObjectSuffix): Managers/ChunkManager.cpp $(IntermediateDirectory)/ChunkManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/ChunkManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ChunkManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ChunkManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ChunkManager$(ObjectSuffix) -MF$(IntermediateDirectory)/ChunkManager$(ObjectSuffix).d -MM Managers/ChunkManager.cpp

$(IntermediateDirectory)/ClusterManager$(ObjectSuffix): Managers/ClusterManager.cpp $(IntermediateDirectory)/ClusterManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/ClusterManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ClusterManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ClusterManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ClusterManager$(ObjectSuffix) -MF$(IntermediateDirectory)/ClusterManager$(ObjectSuffix).d -MM Managers/ClusterManager.cpp

$(IntermediateDirectory)/ColourManager$(ObjectSuffix): Managers/ColourManager.cpp $(IntermediateDirectory)/ColourManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/ColourManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ColourManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ColourManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ColourManager$(ObjectSuffix) -MF$(IntermediateDirectory)/ColourManager$(ObjectSuffix).d -MM Managers/ColourManager.cpp

$(IntermediateDirectory)/CommandManager$(ObjectSuffix): Managers/CommandManager.cpp $(IntermediateDirectory)/CommandManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/CommandManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/CommandManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/CommandManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/CommandManager$(ObjectSuffix) -MF$(IntermediateDirectory)/CommandManager$(ObjectSuffix).d -MM Managers/CommandManager.cpp

$(IntermediateDirectory)/DetailManager$(ObjectSuffix): Managers/DetailManager.cpp $(IntermediateDirectory)/DetailManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/DetailManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/DetailManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/DetailManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/DetailManager$(ObjectSuffix) -MF$(IntermediateDirectory)/DetailManager$(ObjectSuffix).d -MM Managers/DetailManager.cpp

$(IntermediateDirectory)/EchoManager$(ObjectSuffix): Managers/EchoManager.cpp $(IntermediateDirectory)/EchoManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/EchoManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/EchoManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/EchoManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/EchoManager$(ObjectSuffix) -MF$(IntermediateDirectory)/EchoManager$(ObjectSuffix).d -MM Managers/EchoManager.cpp

$(IntermediateDirectory)/ExitManager$(ObjectSuffix): Managers/ExitManager.cpp $(IntermediateDirectory)/ExitManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/ExitManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/ExitManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ExitManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/ExitManager$(ObjectSuffix) -MF$(IntermediateDirectory)/ExitManager$(ObjectSuffix).d -MM Managers/ExitManager.cpp

$(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix): Managers/GrantGroupManager.cpp $(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/GrantGroupManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix) -MF$(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix).d -MM Managers/GrantGroupManager.cpp

$(IntermediateDirectory)/MCharacterManager$(ObjectSuffix): Managers/MCharacterManager.cpp $(IntermediateDirectory)/MCharacterManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/MCharacterManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/MCharacterManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/MCharacterManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/MCharacterManager$(ObjectSuffix) -MF$(IntermediateDirectory)/MCharacterManager$(ObjectSuffix).d -MM Managers/MCharacterManager.cpp

$(IntermediateDirectory)/PermissionManager$(ObjectSuffix): Managers/PermissionManager.cpp $(IntermediateDirectory)/PermissionManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/PermissionManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/PermissionManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/PermissionManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/PermissionManager$(ObjectSuffix) -MF$(IntermediateDirectory)/PermissionManager$(ObjectSuffix).d -MM Managers/PermissionManager.cpp

$(IntermediateDirectory)/RaceManager$(ObjectSuffix): Managers/RaceManager.cpp $(IntermediateDirectory)/RaceManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/RaceManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/RaceManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/RaceManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/RaceManager$(ObjectSuffix) -MF$(IntermediateDirectory)/RaceManager$(ObjectSuffix).d -MM Managers/RaceManager.cpp

$(IntermediateDirectory)/RoomManager$(ObjectSuffix): Managers/RoomManager.cpp $(IntermediateDirectory)/RoomManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/RoomManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/RoomManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/RoomManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/RoomManager$(ObjectSuffix) -MF$(IntermediateDirectory)/RoomManager$(ObjectSuffix).d -MM Managers/RoomManager.cpp

$(IntermediateDirectory)/SectorManager$(ObjectSuffix): Managers/SectorManager.cpp $(IntermediateDirectory)/SectorManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/SectorManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SectorManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SectorManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SectorManager$(ObjectSuffix) -MF$(IntermediateDirectory)/SectorManager$(ObjectSuffix).d -MM Managers/SectorManager.cpp

$(IntermediateDirectory)/TraceManager$(ObjectSuffix): Managers/TraceManager.cpp $(IntermediateDirectory)/TraceManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/TraceManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/TraceManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TraceManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/TraceManager$(ObjectSuffix) -MF$(IntermediateDirectory)/TraceManager$(ObjectSuffix).d -MM Managers/TraceManager.cpp

$(IntermediateDirectory)/Managers$(ObjectSuffix): Managers/Managers.cpp $(IntermediateDirectory)/Managers$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/Managers.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/Managers$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Managers$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/Managers$(ObjectSuffix) -MF$(IntermediateDirectory)/Managers$(ObjectSuffix).d -MM Managers/Managers.cpp

$(IntermediateDirectory)/SocialManager$(ObjectSuffix): Managers/SocialManager.cpp $(IntermediateDirectory)/SocialManager$(ObjectSuffix).d
	$(CompilerName) $(SourceSwitch)Managers/SocialManager.cpp $(CmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/SocialManager$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SocialManager$(ObjectSuffix).d:
	@$(CompilerName) $(CmpOptions) $(IncludePath) -MT$(IntermediateDirectory)/SocialManager$(ObjectSuffix) -MF$(IntermediateDirectory)/SocialManager$(ObjectSuffix).d -MM Managers/SocialManager.cpp

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/FieldImpls$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/FieldImpls$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/TableImpls$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/TableImpls$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Account$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Account$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Area$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Area$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Channel$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Channel$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Character$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Character$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Chunk$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Chunk$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Cluster$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Cluster$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Colour$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Colour$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Command$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Command$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Detail$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Detail$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Echo$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Echo$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Exit$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Exit$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/GrantGroup$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/GrantGroup$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/MCharacter$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/MCharacter$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Permission$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Permission$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Race$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Race$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Room$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Room$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Savable$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Savable$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Sector$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Sector$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Trace$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Trace$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/AccountManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/AccountManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/AreaManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/AreaManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ChannelManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ChannelManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/CharacterManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/CharacterManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ChunkManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ChunkManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ClusterManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ClusterManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ColourManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ColourManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/CommandManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/CommandManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/DetailManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/DetailManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/EchoManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/EchoManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/ExitManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ExitManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/GrantGroupManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/MCharacterManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/MCharacterManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/PermissionManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/PermissionManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/RaceManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/RaceManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/RoomManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/RoomManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SectorManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SectorManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/TraceManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/TraceManager$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/Managers$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Managers$(ObjectSuffix).d
	$(RM) $(IntermediateDirectory)/SocialManager$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/SocialManager$(ObjectSuffix).d
	$(RM) $(OutputFile)

-include $(IntermediateDirectory)/*.d

