/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Character.h"
#include "CharacterManager.h"
#include "TableImpls.h"
#include "FieldImpls.h"
#include "Account.h"

using mud::CharacterManager;
using mud::Character;
using mud::CharacterPtr;

std::vector<std::string> CharacterManager::List(mud::AccountPtr parentAccount)
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	if(parentAccount)
	{
		mask->addJoin(db::TableImpls::Get()->CHARACTERACCOUNT, db::TableImpls::Get()->ENTITIES->ENTITYID, db::TableImpls::Get()->CHARACTERACCOUNT->FKENTITIES);
		ValuePtr value(new KeyValue(db::TableImpls::Get()->CHARACTERACCOUNT->FKACCOUNTS, parentAccount->getID()));	
		mask->addField(value);
	}
	return GetTable()->tableList(mask);
}

TableImplPtr CharacterManager::GetTable()
{ 
	return db::TableImpls::Get()->ENTITIES; 
}

bool CharacterManager::IllegalName(const std::string& name)
{
	try
	{
		lookupByName(name);
		return true;
	}
	catch(RowNotFoundException& e) { }

	return false;
}

KeysPtr CharacterManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->ENTITIES);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::CharacterPtr CharacterManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ENTITIES->ENTITYID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	CharacterPtr p(new Character(manager));
	return p;
}

mud::CharacterPtr CharacterManager::GetByName(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->ENTITIES->NAME, value));
	SavableManagerPtr manager = SavableManager::byvalue(val);
	CharacterPtr p(new Character(manager));
	return p;
}

value_type CharacterManager::lookupByName(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->ENTITIES->NAME, value));
	KeysPtr keys = SavableManager::lookupvalue(val);
	value_type id = keys->getKey(db::TableImpls::Get()->ENTITIES->ENTITYID)->getIntegerValue();
	return id;
}
