/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "Types.h"

namespace mud
{	
	class AccountManager;
	class AreaManager;
	class ChannelManager;
	class CharacterManager;
	class ChunkManager;
	class ClusterManager;
	class ColourManager;
	class CommandManager;
	class DetailManager;
	class EchoManager;
	class ExitManager;
	class GrantGroupManager;
	class MCharacterManager;
	class PermissionManager;
	class RaceManager;
	class RoomManager;
	class SectorManager;
	class SocialManager;
	class TraceManager;
	
	/**
	 * This singleton the one instance of all other managers.
	 * 
	 * It is usefull to contain all Singletons in one place since now all singeltons may be freed with a single call.
	 * As such, this class is really a 'manager manager'.
	 */ 
	class Managers : public Singleton<mud::Managers>
	{
		public:
			AccountManager* Account; /**< The manager for Account objects. */
			AreaManager* Area; /**< The manager for Area objects. */
			ChannelManager* Channel; /**< The manager for Channel objects. */
			CharacterManager* Character; /**< The manager for Character objects. */
			ChunkManager* Chunk; /**< The manager for Chunk objects. */
			ClusterManager* Cluster; /**< The manager for Cluster objects. */
			ColourManager* Colour; /**< The manager for Colour objects. */
			CommandManager* Command; /**< The manager for Command objects. */
			DetailManager* Detail; /**< The manager for Detail objects. */
			EchoManager* Echo; /**< The manager for Echo objects. */
			ExitManager* Exit; /**< The manager for Exit objects. */
			GrantGroupManager* GrantGroup; /**< The manager for GrantGroup objects. */
			MCharacterManager* MCharacter; /**< The manager for MCharacter objects. */
			PermissionManager* Permission; /**< The manager for Permission objects. */
			RaceManager* Race; /**< The manager for Race objects. */
			RoomManager* Room; /**< The manager for Room objects. */
			SectorManager* Sector; /**< The manager for Sector objects. */
			SocialManager* Social; /**< The manager for Social objects. */
			TraceManager* Trace; /**< The manager for Trace objects. */
		
		private:
			/** This is a singleton class, use <code>Get</code> to get the instance. */
			Managers();
			
			/** Hide the copy constructor. */
			Managers(const Managers& rhs);
			
			/** Hide the assignment constructor. */
			Managers operator=(const Managers& rhs);
			
			/** This is a singleton class, use <code>Free</code> to free the instance. */
			~Managers();
			
			friend class Singleton<mud::Managers>;
	};
}
