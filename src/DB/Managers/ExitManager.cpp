/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ExitManager.h"
#include "Exit.h"
#include "TableImpls.h"
#include "FieldImpls.h"
#include "FieldValues.h"
#include "Chunk.h"

using mud::ExitManager;
using mud::Exit;
using mud::ExitPtr;

std::vector<std::string> ExitManager::List(mud::ChunkPtr from, mud::ChunkPtr to)
{	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	
	if(from)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSFROM, from->getID()));
		mask->addField(value);
	}
	
	if(to)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSTO, from->getID()));
		mask->addField(value);
	}
	
	return GetTable()->tableList(mask);
}

std::vector<std::string> ExitManager::ReadableList(mud::ChunkPtr from, mud::ChunkPtr to)
{	
	Strings result;
	
	FieldValuesPtr values(new FieldValues(GetTable()));
	
	if(from)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSFROM, from->getID()));
		values->addValue(value);
	}
	
	if(to)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->EXITS->FKCHUNKSTO, from->getID()));
		values->addValue(value);
	}
	
	SavableManagersPtr managers = SavableManager::getmulti(values);
	for(SavableManagerVector::const_iterator it = managers->begin(); it != managers->end(); it++)
	{
		SavableManagerPtr manager = *it;
		Assert(manager);
		
		mud::ExitPtr exit(new Exit(manager));
		
		Coordinate direction = exit->getDirection();		
		Assert(direction.isDirection());
		
		std::string line = direction.toDirectionString();		
		result.push_back(line);
	}
		
	return result;
}

TableImplPtr ExitManager::GetTable()
{
	return db::TableImpls::Get()->EXITS;
}

KeysPtr ExitManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->EXITS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::ExitPtr ExitManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->EXITS->EXITID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	ExitPtr p(new Exit(manager));
	return p;
}
