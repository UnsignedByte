/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "SectorManager.h"
#include "Sector.h"
#include "TableImpls.h"
#include "FieldImpls.h"

using mud::SectorManager;
using mud::Sector;
using mud::SectorPtr;

std::vector<std::string> SectorManager::List()
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	return GetTable()->tableList(mask);
}

TableImplPtr SectorManager::GetTable()
{
	return db::TableImpls::Get()->SECTORS;
}

KeysPtr SectorManager::Add()
{	
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->SECTORS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::SectorPtr SectorManager::GetByKey(value_type id)
{		
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->SECTORS->SECTORID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	SectorPtr p(new Sector(manager));
	return p;
}

mud::SectorPtr SectorManager::GetByName(cstring value)
{		
	ValuePtr val(new FieldValue(db::TableImpls::Get()->SECTORS->NAME, value));
	SavableManagerPtr manager = SavableManager::byvalue(val);
	SectorPtr p(new Sector(manager));
	return p;
} 

value_type SectorManager::lookupByName(cstring value)
{	
	ValuePtr val(new FieldValue(db::TableImpls::Get()->SECTORS->NAME, value));
	KeysPtr keys = SavableManager::lookupvalue(val);
	value_type id = keys->getKey(db::TableImpls::Get()->SECTORS->SECTORID)->getIntegerValue();
	return id;
}
