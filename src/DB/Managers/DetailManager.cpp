/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "DetailManager.h"
#include "Detail.h"
#include "TableImpls.h"
#include "FieldImpls.h"
#include "Area.h"
#include "Room.h"
#include "Chunk.h"
#include "StringUtilities.h"

using mud::DetailManager;
using mud::Detail;
using mud::DetailPtr;

std::vector<std::string> DetailManager::List()
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	return GetTable()->tableList(mask);
}

std::vector<std::string> DetailManager::List(mud::AreaPtr parentArea)
{
	Assert(parentArea);
	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	mask->addJoin(db::TableImpls::Get()->DETAILAREA , db::TableImpls::Get()->DETAILS->DETAILID, db::TableImpls::Get()->DETAILAREA->FKDETAILS);
	ValuePtr value(new KeyValue(db::TableImpls::Get()->DETAILAREA->FKAREAS, parentArea->getID()));
	mask->addField(value);
	
	return GetTable()->tableList(mask);
}

std::vector<std::string> DetailManager::List(mud::RoomPtr parentRoom)
{
	Assert(parentRoom);
	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	mask->addJoin(db::TableImpls::Get()->DETAILROOM, db::TableImpls::Get()->DETAILS->DETAILID, db::TableImpls::Get()->DETAILROOM->FKDETAILS);
	ValuePtr value(new KeyValue(db::TableImpls::Get()->DETAILROOM->FKROOMS, parentRoom->getID()));
	mask->addField(value);
	
	return GetTable()->tableList(mask);
}

std::vector<std::string> DetailManager::ReadableList(mud::RoomPtr parentRoom)
{
	Strings result;
	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	mask->addJoin(db::TableImpls::Get()->DETAILROOM, db::TableImpls::Get()->DETAILS->DETAILID, db::TableImpls::Get()->DETAILROOM->FKDETAILS);
	ValuePtr value(new KeyValue(db::TableImpls::Get()->DETAILROOM->FKROOMS, parentRoom->getID()));
	mask->addField(value);
	
	SavableManagersPtr managers = SavableManager::getmulti(mask);	
	
	for(SavableManagerVector::const_iterator it = managers->begin(); it != managers->end(); it++)
	{
		SavableManagerPtr manager = *it;
		Assert(manager);
		
		mud::DetailPtr detail(new Detail(manager));
		result.push_back(detail->getDescription());
	}
	
	return result;
}

std::vector<std::string> DetailManager::List(mud::ChunkPtr parentChunk)
{
	Assert(parentChunk);
	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	mask->addJoin(db::TableImpls::Get()->DETAILCHUNK, db::TableImpls::Get()->DETAILS->DETAILID, db::TableImpls::Get()->DETAILCHUNK->FKDETAILS);
	ValuePtr value(new KeyValue(db::TableImpls::Get()->DETAILCHUNK->FKCHUNKS, parentChunk->getID()));
	mask->addField(value);
	
	return GetTable()->tableList(mask);
}

std::vector<std::string> DetailManager::ReadableList(mud::ChunkPtr parentChunk)
{
	Strings result;
	
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	mask->addJoin(db::TableImpls::Get()->DETAILCHUNK, db::TableImpls::Get()->DETAILS->DETAILID, db::TableImpls::Get()->DETAILCHUNK->FKDETAILS);
	ValuePtr value(new KeyValue(db::TableImpls::Get()->DETAILCHUNK->FKCHUNKS, parentChunk->getID()));
	mask->addField(value);
	
	SavableManagersPtr managers = SavableManager::getmulti(mask);	
	
	for(SavableManagerVector::const_iterator it = managers->begin(); it != managers->end(); it++)
	{
		SavableManagerPtr manager = *it;
		Assert(manager);
		
		mud::DetailPtr detail(new Detail(manager));
		result.push_back(detail->getDescription());
	}
	
	return result;
}

TableImplPtr DetailManager::GetTable()
{
	return db::TableImpls::Get()->DETAILS;
}

KeysPtr DetailManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->DETAILS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::DetailPtr DetailManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->DETAILS->DETAILID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	DetailPtr p(new Detail(manager));
	return p;
}
