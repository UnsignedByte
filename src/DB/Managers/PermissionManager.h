/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#include "Types.h"
#include "FieldImpls.h"
#include "SavableTypes.h"

namespace mud
{
	class PermissionManager
	{
	public:
		bool defaultGrant; /**< The default grant value for commands. */
		bool defaultLog; /**< The default log value for commands. */
	
		TableImplPtr GetTable();
		std::vector<std::string> List();
		
		mud::PermissionPtr GetByKeys(KeysPtr keys);		
		
		value_type lookupByName(cstring value);

	private:
		PermissionManager(void);
		PermissionManager(const PermissionManager& rhs);
		PermissionManager operator=(const PermissionManager& rhs);
		~PermissionManager(void) {};
		
		friend class mud::Managers;
	};
}
