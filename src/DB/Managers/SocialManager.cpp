/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "SocialManager.h"
#include "Social.h"
#include "TableImpls.h"
#include "FieldImpls.h"

using mud::SocialManager;
using mud::Social;
using mud::SocialPtr;

std::vector<std::string> SocialManager::List()
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	return GetTable()->tableList(mask);
}

TableImplPtr SocialManager::GetTable()
{
	return db::TableImpls::Get()->SOCIALS;
}

KeysPtr SocialManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->SOCIALS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::SocialPtr SocialManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->SOCIALS->SOCIALID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	SocialPtr p(new Social(manager));
	return p;
}

mud::SocialPtr SocialManager::GetByName(cstring name)
{
	FieldValuePtr value(new FieldValue(db::TableImpls::Get()->SOCIALS->NAME, name));
	SavableManagerPtr manager = SavableManager::byvalue(value);
	SocialPtr p(new Social(manager));
	return p;
}
