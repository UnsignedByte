/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ColourManager.h"
#include "Colour.h"
#include "TableImpls.h"
#include "FieldImpls.h"

using mud::ColourManager;
using mud::Colour;
using mud::ColourPtr;

std::vector<std::string> ColourManager::List()
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	return GetTable()->tableList(mask);
}

TableImplPtr ColourManager::GetTable()
{
	return db::TableImpls::Get()->COLOURS;
}


KeysPtr ColourManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->COLOURS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::ColourPtr ColourManager::GetByKey(value_type id)
{	
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->COLOURS->COLOURID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	ColourPtr p(new Colour(manager));
	return p;
}

mud::ColourPtr ColourManager::GetByName(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->COLOURS->NAME, value));
	SavableManagerPtr manager = SavableManager::byvalue(val);
	ColourPtr p(new Colour(manager));
	return p;
}

mud::ColourPtr ColourManager::GetByCode(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->COLOURS->CODE, value));
	SavableManagerPtr manager = SavableManager::byvalue(val);
	ColourPtr p(new Colour(manager));
	return p;
}

value_type ColourManager::lookupByCode(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->COLOURS->CODE, value));
	KeysPtr keys = SavableManager::lookupvalue(val);
	value_type id = keys->getKey(db::TableImpls::Get()->COLOURS->COLOURID)->getIntegerValue();
	return id;
}

value_type ColourManager::lookupByName(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->COLOURS->NAME, value));
	KeysPtr keys = SavableManager::lookupvalue(val);
	value_type id = keys->getKey(db::TableImpls::Get()->COLOURS->COLOURID)->getIntegerValue();
	return id;
}
