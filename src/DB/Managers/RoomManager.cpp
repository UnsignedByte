/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "RoomManager.h"
#include "Room.h"
#include "TableImpls.h"
#include "FieldImpls.h"
#include "Cluster.h"

using mud::RoomManager;
using mud::Room;
using mud::RoomPtr;

std::vector<std::string> RoomManager::List(mud::ClusterPtr parentCluster)
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	if(parentCluster)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->ROOMS->FKCLUSTERS, parentCluster->getID()));
		mask->addField(value);
	}
	return GetTable()->tableList(mask);
}

TableImplPtr RoomManager::GetTable()
{
	return db::TableImpls::Get()->ROOMS;
}

KeysPtr RoomManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->ROOMS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::RoomPtr RoomManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ROOMS->ROOMID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	RoomPtr p(new Room(manager));
	return p;
}
