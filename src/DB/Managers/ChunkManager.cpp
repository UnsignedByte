/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ChunkManager.h"
#include "Chunk.h"
#include "TableImpls.h"
#include "FieldImpls.h"
#include "SelectionMask.h"
#include "Room.h"

using mud::ChunkManager;
using mud::Chunk;
using mud::ChunkPtr;

std::vector<std::string> ChunkManager::List(mud::RoomPtr parentRoom)
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	if(parentRoom)
	{
		ValuePtr value(new FieldValue(db::TableImpls::Get()->CHUNKS->FKROOMS, parentRoom->getID()));
		mask->addField(value);
	}
	return GetTable()->tableList(mask);
}

TableImplPtr ChunkManager::GetTable()
{
	return db::TableImpls::Get()->CHUNKS;
}

KeysPtr ChunkManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->CHUNKS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

mud::ChunkPtr ChunkManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->CHUNKS->CHUNKID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	ChunkPtr p(new Chunk(manager));
	return p;
}
