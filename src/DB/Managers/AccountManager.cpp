/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "AccountManager.h"
#include "Account.h"
#include "TableImpls.h"
#include "FieldImpls.h"

using mud::AccountManager;
using mud::Account;
using mud::AccountPtr;

bool AccountManager::IllegalName(const std::string& name)
{
	try
	{
		lookupByName(name);
		return true;
	} catch(RowNotFoundException& e) {}

	return false;
}

std::vector<std::string> AccountManager::List()
{
	SelectionMaskPtr mask(new SelectionMask(GetTable()));
	return GetTable()->tableList(mask);
}

TableImplPtr AccountManager::GetTable()
{
	return db::TableImpls::Get()->ACCOUNTS;
}

KeysPtr AccountManager::Add()
{
	SavableManagerPtr manager = SavableManager::getnew(db::TableImpls::Get()->ACCOUNTS);
	
	bool locked = manager->lock();
	Assert(locked); // new manager, should always be unlocked
	
	manager->save();
	
	manager->unlock();
	return manager->getKeys();
}

AccountPtr AccountManager::GetByKey(value_type id)
{
	KeyValuePtr key(new KeyValue(db::TableImpls::Get()->ACCOUNTS->ACCOUNTID, id));
	SavableManagerPtr manager = SavableManager::bykey(key);
	AccountPtr p(new Account(manager));
	return p;
}

AccountPtr AccountManager::GetByName(cstring value)
{	
	ValuePtr val(new FieldValue(db::TableImpls::Get()->ACCOUNTS->NAME, value));
	SavableManagerPtr manager = SavableManager::byvalue(val);
	AccountPtr p(new Account(manager));
	return p;
}

value_type AccountManager::lookupByName(cstring value)
{
	ValuePtr val(new FieldValue(db::TableImpls::Get()->ACCOUNTS->NAME, value));
	KeysPtr keys = SavableManager::lookupvalue(val);
	value_type id = keys->getKey(db::TableImpls::Get()->ACCOUNTS->ACCOUNTID)->getIntegerValue();
	return id;
}
