/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Managers.h"
#include "AccountManager.h"
#include "AreaManager.h"
#include "ChannelManager.h"
#include "CharacterManager.h"
#include "ChunkManager.h"
#include "ClusterManager.h"
#include "ColourManager.h"
#include "CommandManager.h"
#include "DetailManager.h"
#include "EchoManager.h"
#include "ExitManager.h"
#include "GrantGroupManager.h"
#include "MCharacterManager.h"
#include "PermissionManager.h"
#include "RaceManager.h"
#include "RoomManager.h"
#include "SectorManager.h"
#include "SocialManager.h"
#include "TraceManager.h"

using mud::Managers;

Managers::Managers() :
Account(new AccountManager()),
Area(new AreaManager()),
Channel(new ChannelManager()),
Character(new CharacterManager()),
Chunk(new ChunkManager()),
Cluster(new ClusterManager()),
Colour(new ColourManager()),
Command(new CommandManager()),
Detail(new DetailManager()),
Echo(new EchoManager()),
Exit(new ExitManager()),
GrantGroup(new GrantGroupManager()),
MCharacter(new MCharacterManager()),
Permission(new PermissionManager()),
Race(new RaceManager()),
Room(new RoomManager()),
Sector(new SectorManager()),
Social(new SocialManager()),
Trace(new TraceManager())
{
	
}

Managers::~Managers()
{
	
}
