/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* NOTE: This file was generated automatically. Do not edit. */

#pragma once

/**
 * @file FieldImpls.h
 * This file contains the generated FieldImpl classes.
 *
 * @see FieldImpl
 */

#include "Types.h"
#include "TableImpl.h"

namespace db
{
	/**
	 * This is a generated class that contains all FieldImpls for the Accounts table.
	 */
	class Accounts : public TableImpl
	{
	public:
		KeyImplPtr ACCOUNTID; /**< FieldImplPtr for the accountid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr PASSWORD; /**< FieldImplPtr for the password field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Accounts(): TableImpl("Accounts") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Accounts() { }

		/** Hide the copy constructor. */
		Accounts(const Accounts& rhs);

		/** Hide the assignment operator. */
		Accounts operator=(const Accounts& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Accounts);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Areas table.
	 */
	class Areas : public TableImpl
	{
	public:
		KeyImplPtr AREAID; /**< FieldImplPtr for the areaid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		FieldImplPtr HEIGHT; /**< FieldImplPtr for the height field. */
		FieldImplPtr WIDTH; /**< FieldImplPtr for the width field. */
		FieldImplPtr LENGTH; /**< FieldImplPtr for the length field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Areas(): TableImpl("Areas") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Areas() { }

		/** Hide the copy constructor. */
		Areas(const Areas& rhs);

		/** Hide the assignment operator. */
		Areas operator=(const Areas& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Areas);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Branches table.
	 */
	class Branches : public TableImpl
	{
	public:
		KeyImplPtr BRANCHID; /**< FieldImplPtr for the branchid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		KeyImplPtr FKTREES; /**< FieldImplPtr for the fkTrees field. */
		KeyImplPtr FKSTATSPRIMARY; /**< FieldImplPtr for the fkStatsPrimary field. */
		KeyImplPtr FKSTATSSECONDARY; /**< FieldImplPtr for the fkStatsSecondary field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Branches(): TableImpl("Branches") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Branches() { }

		/** Hide the copy constructor. */
		Branches(const Branches& rhs);

		/** Hide the assignment operator. */
		Branches operator=(const Branches& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Branches);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Channels table.
	 */
	class Channels : public TableImpl
	{
	public:
		KeyImplPtr CHANNELID; /**< FieldImplPtr for the channelid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		FieldImplPtr NEEDLOGIN; /**< FieldImplPtr for the needLogin field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Channels(): TableImpl("Channels") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Channels() { }

		/** Hide the copy constructor. */
		Channels(const Channels& rhs);

		/** Hide the assignment operator. */
		Channels operator=(const Channels& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Channels);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the ChannelLogs table.
	 */
	class ChannelLogs : public TableImpl
	{
	public:
		KeyImplPtr CHANNELLOGID; /**< FieldImplPtr for the channellogid field. */
		KeyImplPtr FKCHANNELS; /**< FieldImplPtr for the fkChannels field. */
		KeyImplPtr FKACCOUNTS; /**< FieldImplPtr for the fkAccounts field. */
		FieldImplPtr TIME; /**< FieldImplPtr for the time field. */
		FieldImplPtr CONTENT; /**< FieldImplPtr for the content field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		ChannelLogs(): TableImpl("ChannelLogs") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~ChannelLogs() { }

		/** Hide the copy constructor. */
		ChannelLogs(const ChannelLogs& rhs);

		/** Hide the assignment operator. */
		ChannelLogs operator=(const ChannelLogs& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(ChannelLogs);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterAccount table.
	 */
	class CharacterAccount : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKACCOUNTS; /**< FieldImplPtr for the fkAccounts field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterAccount(): TableImpl("CharacterAccount") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterAccount() { }

		/** Hide the copy constructor. */
		CharacterAccount(const CharacterAccount& rhs);

		/** Hide the assignment operator. */
		CharacterAccount operator=(const CharacterAccount& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterAccount);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterBranch table.
	 */
	class CharacterBranch : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKBRANCHES; /**< FieldImplPtr for the fkBranches field. */
		FieldImplPtr XP; /**< FieldImplPtr for the xp field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterBranch(): TableImpl("CharacterBranch") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterBranch() { }

		/** Hide the copy constructor. */
		CharacterBranch(const CharacterBranch& rhs);

		/** Hide the assignment operator. */
		CharacterBranch operator=(const CharacterBranch& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterBranch);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterCluster table.
	 */
	class CharacterCluster : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKCLUSTERS; /**< FieldImplPtr for the fkClusters field. */
		FieldImplPtr XP; /**< FieldImplPtr for the xp field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterCluster(): TableImpl("CharacterCluster") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterCluster() { }

		/** Hide the copy constructor. */
		CharacterCluster(const CharacterCluster& rhs);

		/** Hide the assignment operator. */
		CharacterCluster operator=(const CharacterCluster& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterCluster);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterSkill table.
	 */
	class CharacterSkill : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKBRANCHES; /**< FieldImplPtr for the fkBranches field. */
		FieldImplPtr XP; /**< FieldImplPtr for the xp field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterSkill(): TableImpl("CharacterSkill") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterSkill() { }

		/** Hide the copy constructor. */
		CharacterSkill(const CharacterSkill& rhs);

		/** Hide the assignment operator. */
		CharacterSkill operator=(const CharacterSkill& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterSkill);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterStat table.
	 */
	class CharacterStat : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKSTATS; /**< FieldImplPtr for the fkStats field. */
		FieldImplPtr XP; /**< FieldImplPtr for the xp field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterStat(): TableImpl("CharacterStat") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterStat() { }

		/** Hide the copy constructor. */
		CharacterStat(const CharacterStat& rhs);

		/** Hide the assignment operator. */
		CharacterStat operator=(const CharacterStat& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterStat);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the CharacterTree table.
	 */
	class CharacterTree : public TableImpl
	{
	public:
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */
		KeyImplPtr FKTREES; /**< FieldImplPtr for the fkTrees field. */
		FieldImplPtr XP; /**< FieldImplPtr for the xp field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		CharacterTree(): TableImpl("CharacterTree") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~CharacterTree() { }

		/** Hide the copy constructor. */
		CharacterTree(const CharacterTree& rhs);

		/** Hide the assignment operator. */
		CharacterTree operator=(const CharacterTree& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(CharacterTree);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Clusters table.
	 */
	class Clusters : public TableImpl
	{
	public:
		KeyImplPtr CLUSTERID; /**< FieldImplPtr for the clusterid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		KeyImplPtr FKAREAS; /**< FieldImplPtr for the fkAreas field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Clusters(): TableImpl("Clusters") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Clusters() { }

		/** Hide the copy constructor. */
		Clusters(const Clusters& rhs);

		/** Hide the assignment operator. */
		Clusters operator=(const Clusters& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Clusters);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Chunks table.
	 */
	class Chunks : public TableImpl
	{
	public:
		KeyImplPtr CHUNKID; /**< FieldImplPtr for the chunkid field. */
		KeyImplPtr FKROOMS; /**< FieldImplPtr for the fkRooms field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		FieldImplPtr TAGS; /**< FieldImplPtr for the tags field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Chunks(): TableImpl("Chunks") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Chunks() { }

		/** Hide the copy constructor. */
		Chunks(const Chunks& rhs);

		/** Hide the assignment operator. */
		Chunks operator=(const Chunks& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Chunks);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Colours table.
	 */
	class Colours : public TableImpl
	{
	public:
		KeyImplPtr COLOURID; /**< FieldImplPtr for the colourid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr CODE; /**< FieldImplPtr for the code field. */
		FieldImplPtr COLOURSTRING; /**< FieldImplPtr for the colourstring field. */
		FieldImplPtr ANSI; /**< FieldImplPtr for the ansi field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Colours(): TableImpl("Colours") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Colours() { }

		/** Hide the copy constructor. */
		Colours(const Colours& rhs);

		/** Hide the assignment operator. */
		Colours operator=(const Colours& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Colours);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Commands table.
	 */
	class Commands : public TableImpl
	{
	public:
		KeyImplPtr COMMANDID; /**< FieldImplPtr for the commandid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr GRANTGROUP; /**< FieldImplPtr for the grantgroup field. */
		FieldImplPtr HIGHFORCE; /**< FieldImplPtr for the highforce field. */
		FieldImplPtr FORCE; /**< FieldImplPtr for the force field. */
		FieldImplPtr LOWFORCE; /**< FieldImplPtr for the lowforce field. */
		FieldImplPtr HELP; /**< FieldImplPtr for the help field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Commands(): TableImpl("Commands") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Commands() { }

		/** Hide the copy constructor. */
		Commands(const Commands& rhs);

		/** Hide the assignment operator. */
		Commands operator=(const Commands& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Commands);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Details table.
	 */
	class Details : public TableImpl
	{
	public:
		KeyImplPtr DETAILID; /**< FieldImplPtr for the detailid field. */
		FieldImplPtr KEY; /**< FieldImplPtr for the key field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Details(): TableImpl("Details") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Details() { }

		/** Hide the copy constructor. */
		Details(const Details& rhs);

		/** Hide the assignment operator. */
		Details operator=(const Details& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Details);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the DetailArea table.
	 */
	class DetailArea : public TableImpl
	{
	public:
		KeyImplPtr FKDETAILS; /**< FieldImplPtr for the fkDetails field. */
		KeyImplPtr FKAREAS; /**< FieldImplPtr for the fkAreas field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		DetailArea(): TableImpl("DetailArea") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~DetailArea() { }

		/** Hide the copy constructor. */
		DetailArea(const DetailArea& rhs);

		/** Hide the assignment operator. */
		DetailArea operator=(const DetailArea& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(DetailArea);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the DetailRoom table.
	 */
	class DetailRoom : public TableImpl
	{
	public:
		KeyImplPtr FKDETAILS; /**< FieldImplPtr for the fkDetails field. */
		KeyImplPtr FKROOMS; /**< FieldImplPtr for the fkRooms field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		DetailRoom(): TableImpl("DetailRoom") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~DetailRoom() { }

		/** Hide the copy constructor. */
		DetailRoom(const DetailRoom& rhs);

		/** Hide the assignment operator. */
		DetailRoom operator=(const DetailRoom& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(DetailRoom);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the DetailChunk table.
	 */
	class DetailChunk : public TableImpl
	{
	public:
		KeyImplPtr FKDETAILS; /**< FieldImplPtr for the fkDetails field. */
		KeyImplPtr FKCHUNKS; /**< FieldImplPtr for the fkChunks field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		DetailChunk(): TableImpl("DetailChunk") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~DetailChunk() { }

		/** Hide the copy constructor. */
		DetailChunk(const DetailChunk& rhs);

		/** Hide the assignment operator. */
		DetailChunk operator=(const DetailChunk& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(DetailChunk);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the DetailCharacter table.
	 */
	class DetailCharacter : public TableImpl
	{
	public:
		KeyImplPtr FKDETAILS; /**< FieldImplPtr for the fkDetails field. */
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		DetailCharacter(): TableImpl("DetailCharacter") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~DetailCharacter() { }

		/** Hide the copy constructor. */
		DetailCharacter(const DetailCharacter& rhs);

		/** Hide the assignment operator. */
		DetailCharacter operator=(const DetailCharacter& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(DetailCharacter);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the DetailDetail table.
	 */
	class DetailDetail : public TableImpl
	{
	public:
		KeyImplPtr FKDETAILSPRIMARY; /**< FieldImplPtr for the fkDetailsPrimary field. */
		KeyImplPtr FKDETAILSSECONDARY; /**< FieldImplPtr for the fkDetailsSecondary field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		DetailDetail(): TableImpl("DetailDetail") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~DetailDetail() { }

		/** Hide the copy constructor. */
		DetailDetail(const DetailDetail& rhs);

		/** Hide the assignment operator. */
		DetailDetail operator=(const DetailDetail& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(DetailDetail);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Echos table.
	 */
	class Echos : public TableImpl
	{
	public:
		KeyImplPtr ECHOID; /**< FieldImplPtr for the echoid field. */
		FieldImplPtr MESSAGE; /**< FieldImplPtr for the message field. */
		FieldImplPtr VISIBILITY; /**< FieldImplPtr for the visibility field. */
		FieldImplPtr AUDIBILITY; /**< FieldImplPtr for the audibility field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Echos(): TableImpl("Echos") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Echos() { }

		/** Hide the copy constructor. */
		Echos(const Echos& rhs);

		/** Hide the assignment operator. */
		Echos operator=(const Echos& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Echos);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Entities table.
	 */
	class Entities : public TableImpl
	{
	public:
		KeyImplPtr ENTITYID; /**< FieldImplPtr for the entityid field. */
		KeyImplPtr FKRACES; /**< FieldImplPtr for the fkRaces field. */
		KeyImplPtr FKCHUNKS; /**< FieldImplPtr for the fkChunks field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Entities(): TableImpl("Entities") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Entities() { }

		/** Hide the copy constructor. */
		Entities(const Entities& rhs);

		/** Hide the assignment operator. */
		Entities operator=(const Entities& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Entities);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Exits table.
	 */
	class Exits : public TableImpl
	{
	public:
		KeyImplPtr EXITID; /**< FieldImplPtr for the exitid field. */
		KeyImplPtr FKCHUNKSFROM; /**< FieldImplPtr for the fkChunksfrom field. */
		KeyImplPtr FKCHUNKSTO; /**< FieldImplPtr for the fkChunksto field. */
		FieldImplPtr X; /**< FieldImplPtr for the x field. */
		FieldImplPtr Y; /**< FieldImplPtr for the y field. */
		FieldImplPtr Z; /**< FieldImplPtr for the z field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Exits(): TableImpl("Exits") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Exits() { }

		/** Hide the copy constructor. */
		Exits(const Exits& rhs);

		/** Hide the assignment operator. */
		Exits operator=(const Exits& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Exits);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the GrantGroups table.
	 */
	class GrantGroups : public TableImpl
	{
	public:
		KeyImplPtr GRANTGROUPID; /**< FieldImplPtr for the grantgroupid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DEFAULTGRANT; /**< FieldImplPtr for the defaultgrant field. */
		FieldImplPtr DEFAULTLOG; /**< FieldImplPtr for the defaultlog field. */
		FieldImplPtr IMPLIES; /**< FieldImplPtr for the implies field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		GrantGroups(): TableImpl("GrantGroups") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~GrantGroups() { }

		/** Hide the copy constructor. */
		GrantGroups(const GrantGroups& rhs);

		/** Hide the assignment operator. */
		GrantGroups operator=(const GrantGroups& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(GrantGroups);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Logs table.
	 */
	class Logs : public TableImpl
	{
	public:
		KeyImplPtr LOGID; /**< FieldImplPtr for the logid field. */
		FieldImplPtr TYPE; /**< FieldImplPtr for the type field. */
		FieldImplPtr TIME; /**< FieldImplPtr for the time field. */
		FieldImplPtr CONTENT; /**< FieldImplPtr for the content field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Logs(): TableImpl("Logs") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Logs() { }

		/** Hide the copy constructor. */
		Logs(const Logs& rhs);

		/** Hide the assignment operator. */
		Logs operator=(const Logs& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Logs);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Permissions table.
	 */
	class Permissions : public TableImpl
	{
	public:
		KeyImplPtr FKACCOUNTS; /**< FieldImplPtr for the fkAccounts field. */
		KeyImplPtr FKGRANTGROUPS; /**< FieldImplPtr for the fkGrantGroups field. */
		FieldImplPtr GRANT; /**< FieldImplPtr for the grant field. */
		FieldImplPtr LOG; /**< FieldImplPtr for the log field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Permissions(): TableImpl("Permissions") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Permissions() { }

		/** Hide the copy constructor. */
		Permissions(const Permissions& rhs);

		/** Hide the assignment operator. */
		Permissions operator=(const Permissions& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Permissions);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Races table.
	 */
	class Races : public TableImpl
	{
	public:
		KeyImplPtr RACEID; /**< FieldImplPtr for the raceid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Races(): TableImpl("Races") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Races() { }

		/** Hide the copy constructor. */
		Races(const Races& rhs);

		/** Hide the assignment operator. */
		Races operator=(const Races& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Races);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Rooms table.
	 */
	class Rooms : public TableImpl
	{
	public:
		KeyImplPtr ROOMID; /**< FieldImplPtr for the roomid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		KeyImplPtr FKCLUSTERS; /**< FieldImplPtr for the fkClusters field. */
		KeyImplPtr FKSECTORS; /**< FieldImplPtr for the fkSectors field. */
		FieldImplPtr WIDTH; /**< FieldImplPtr for the width field. */
		FieldImplPtr LENGTH; /**< FieldImplPtr for the length field. */
		FieldImplPtr HEIGHT; /**< FieldImplPtr for the height field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Rooms(): TableImpl("Rooms") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Rooms() { }

		/** Hide the copy constructor. */
		Rooms(const Rooms& rhs);

		/** Hide the assignment operator. */
		Rooms operator=(const Rooms& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Rooms);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Sectors table.
	 */
	class Sectors : public TableImpl
	{
	public:
		KeyImplPtr SECTORID; /**< FieldImplPtr for the sectorid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr SYMBOL; /**< FieldImplPtr for the symbol field. */
		FieldImplPtr MOVECOST; /**< FieldImplPtr for the movecost field. */
		FieldImplPtr WATER; /**< FieldImplPtr for the water field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Sectors(): TableImpl("Sectors") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Sectors() { }

		/** Hide the copy constructor. */
		Sectors(const Sectors& rhs);

		/** Hide the assignment operator. */
		Sectors operator=(const Sectors& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Sectors);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Skills table.
	 */
	class Skills : public TableImpl
	{
	public:
		KeyImplPtr SKILLID; /**< FieldImplPtr for the skillid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		KeyImplPtr FKBRANCHES; /**< FieldImplPtr for the fkBranches field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Skills(): TableImpl("Skills") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Skills() { }

		/** Hide the copy constructor. */
		Skills(const Skills& rhs);

		/** Hide the assignment operator. */
		Skills operator=(const Skills& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Skills);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Socials table.
	 */
	class Socials : public TableImpl
	{
	public:
		KeyImplPtr SOCIALID; /**< FieldImplPtr for the socialid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr MESSAGE; /**< FieldImplPtr for the message field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Socials(): TableImpl("Socials") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Socials() { }

		/** Hide the copy constructor. */
		Socials(const Socials& rhs);

		/** Hide the assignment operator. */
		Socials operator=(const Socials& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Socials);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Stats table.
	 */
	class Stats : public TableImpl
	{
	public:
		KeyImplPtr STATID; /**< FieldImplPtr for the statid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		FieldImplPtr SHORTNAME; /**< FieldImplPtr for the shortname field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Stats(): TableImpl("Stats") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Stats() { }

		/** Hide the copy constructor. */
		Stats(const Stats& rhs);

		/** Hide the assignment operator. */
		Stats operator=(const Stats& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Stats);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Traces table.
	 */
	class Traces : public TableImpl
	{
	public:
		KeyImplPtr TRACEID; /**< FieldImplPtr for the traceid field. */
		KeyImplPtr FKACCOUNTS; /**< FieldImplPtr for the fkAccounts field. */
		FieldImplPtr TIME; /**< FieldImplPtr for the time field. */
		FieldImplPtr DESCRIPTION; /**< FieldImplPtr for the description field. */
		FieldImplPtr DIFF; /**< FieldImplPtr for the diff field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Traces(): TableImpl("Traces") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Traces() { }

		/** Hide the copy constructor. */
		Traces(const Traces& rhs);

		/** Hide the assignment operator. */
		Traces operator=(const Traces& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Traces);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the TraceChunk table.
	 */
	class TraceChunk : public TableImpl
	{
	public:
		KeyImplPtr FKTRACES; /**< FieldImplPtr for the fkTraces field. */
		KeyImplPtr FKCHUNKS; /**< FieldImplPtr for the fkChunks field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		TraceChunk(): TableImpl("TraceChunk") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~TraceChunk() { }

		/** Hide the copy constructor. */
		TraceChunk(const TraceChunk& rhs);

		/** Hide the assignment operator. */
		TraceChunk operator=(const TraceChunk& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(TraceChunk);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the TraceDetail table.
	 */
	class TraceDetail : public TableImpl
	{
	public:
		KeyImplPtr FKTRACES; /**< FieldImplPtr for the fkTraces field. */
		KeyImplPtr FKDETAILS; /**< FieldImplPtr for the fkDetails field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		TraceDetail(): TableImpl("TraceDetail") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~TraceDetail() { }

		/** Hide the copy constructor. */
		TraceDetail(const TraceDetail& rhs);

		/** Hide the assignment operator. */
		TraceDetail operator=(const TraceDetail& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(TraceDetail);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the TraceEntity table.
	 */
	class TraceEntity : public TableImpl
	{
	public:
		KeyImplPtr FKTRACES; /**< FieldImplPtr for the fkTraces field. */
		KeyImplPtr FKENTITIES; /**< FieldImplPtr for the fkEntities field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		TraceEntity(): TableImpl("TraceEntity") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~TraceEntity() { }

		/** Hide the copy constructor. */
		TraceEntity(const TraceEntity& rhs);

		/** Hide the assignment operator. */
		TraceEntity operator=(const TraceEntity& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(TraceEntity);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the TraceRoom table.
	 */
	class TraceRoom : public TableImpl
	{
	public:
		KeyImplPtr FKTRACES; /**< FieldImplPtr for the fkTraces field. */
		KeyImplPtr FKROOMS; /**< FieldImplPtr for the fkRooms field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		TraceRoom(): TableImpl("TraceRoom") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~TraceRoom() { }

		/** Hide the copy constructor. */
		TraceRoom(const TraceRoom& rhs);

		/** Hide the assignment operator. */
		TraceRoom operator=(const TraceRoom& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(TraceRoom);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Trees table.
	 */
	class Trees : public TableImpl
	{
	public:
		KeyImplPtr TREEID; /**< FieldImplPtr for the treeid field. */
		FieldImplPtr NAME; /**< FieldImplPtr for the name field. */
		KeyImplPtr FKCLUSTERS; /**< FieldImplPtr for the fkClusters field. */
		KeyImplPtr FKSTATSPRIMARY; /**< FieldImplPtr for the fkStatsPrimary field. */
		KeyImplPtr FKSTATSSECONDARY; /**< FieldImplPtr for the fkStatsSecondary field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Trees(): TableImpl("Trees") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Trees() { }

		/** Hide the copy constructor. */
		Trees(const Trees& rhs);

		/** Hide the assignment operator. */
		Trees operator=(const Trees& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Trees);
	};

	/**
	 * This is a generated class that contains all FieldImpls for the Version table.
	 */
	class Version : public TableImpl
	{
	public:
		KeyImplPtr VERSIONID; /**< FieldImplPtr for the versionid field. */
		FieldImplPtr VERSIONTEXT; /**< FieldImplPtr for the versiontext field. */
		FieldImplPtr MAJOR; /**< FieldImplPtr for the major field. */
		FieldImplPtr MINOR; /**< FieldImplPtr for the minor field. */
		FieldImplPtr MICRO; /**< FieldImplPtr for the micro field. */

	private:
		/** This is a managed class, use <code>TableImpls</code> to get an instance. */
		Version(): TableImpl("Version") { }

		/** This is a managed class, free <code>TableImpls</code> to free the instance. */
		~Version() { }

		/** Hide the copy constructor. */
		Version(const Version& rhs);

		/** Hide the assignment operator. */
		Version operator=(const Version& rhs);

		/** Initialize the table's fields. */
		void Initialize();

		friend class TableImpls;
		friend SmartPtrDelete(Version);
	};

} // end of namespace


