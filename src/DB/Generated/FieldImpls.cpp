/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* NOTE: This file was generated automatically. Do not edit. */

#include "FieldImpls.h"
#include "TableImpl.h"
#include "TableImpls.h"
#include "Tables.h"
#include "FieldImpl.h"
#include "KeyImpl.h"

using namespace db;

void Accounts::Initialize()
{
	ACCOUNTID = KeyImplPtr(new KeyImpl(TableImpls::Get()->ACCOUNTS, TableImpls::Get()->ACCOUNTS, "accountid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->ACCOUNTS, "name", true, true));
	PASSWORD = FieldImplPtr(new FieldImpl(TableImpls::Get()->ACCOUNTS, "password", true, false));

	m_fields.push_back(ACCOUNTID);
	m_fields.push_back(NAME);
	m_fields.push_back(PASSWORD);

	updateFieldCount();
}

void Areas::Initialize()
{
	AREAID = KeyImplPtr(new KeyImpl(TableImpls::Get()->AREAS, TableImpls::Get()->AREAS, "areaid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->AREAS, "name", true, true));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->AREAS, "description", true, false));
	HEIGHT = FieldImplPtr(new FieldImpl(TableImpls::Get()->AREAS, "height", false, false));
	WIDTH = FieldImplPtr(new FieldImpl(TableImpls::Get()->AREAS, "width", false, false));
	LENGTH = FieldImplPtr(new FieldImpl(TableImpls::Get()->AREAS, "length", false, false));

	m_fields.push_back(AREAID);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(HEIGHT);
	m_fields.push_back(WIDTH);
	m_fields.push_back(LENGTH);

	updateFieldCount();
}

void Branches::Initialize()
{
	BRANCHID = KeyImplPtr(new KeyImpl(TableImpls::Get()->BRANCHES, TableImpls::Get()->BRANCHES, "branchid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->BRANCHES, "name", true, true));
	FKTREES = KeyImplPtr(new KeyImpl(TableImpls::Get()->BRANCHES, TableImpls::Get()->TREES, "fkTrees", false, false));
	FKSTATSPRIMARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->BRANCHES, TableImpls::Get()->STATS, "fkStatsPrimary", false, false));
	FKSTATSSECONDARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->BRANCHES, TableImpls::Get()->STATS, "fkStatsSecondary", false, false));

	m_fields.push_back(BRANCHID);
	m_fields.push_back(NAME);
	m_fields.push_back(FKTREES);
	m_fields.push_back(FKSTATSPRIMARY);
	m_fields.push_back(FKSTATSSECONDARY);

	updateFieldCount();
}

void Channels::Initialize()
{
	CHANNELID = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHANNELS, TableImpls::Get()->CHANNELS, "channelid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHANNELS, "name", true, true));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHANNELS, "description", true, false));
	NEEDLOGIN = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHANNELS, "needLogin", false, std::string("1")));

	m_fields.push_back(CHANNELID);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(NEEDLOGIN);

	updateFieldCount();
}

void ChannelLogs::Initialize()
{
	CHANNELLOGID = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHANNELLOGS, TableImpls::Get()->CHANNELLOGS, "channellogid", true, true));
	FKCHANNELS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHANNELLOGS, TableImpls::Get()->CHANNELS, "fkChannels", false, false));
	FKACCOUNTS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHANNELLOGS, TableImpls::Get()->ACCOUNTS, "fkAccounts", false, false));
	TIME = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHANNELLOGS, "time", false, false));
	CONTENT = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHANNELLOGS, "content", true, false));

	m_fields.push_back(CHANNELLOGID);
	m_fields.push_back(FKCHANNELS);
	m_fields.push_back(FKACCOUNTS);
	m_fields.push_back(TIME);
	m_fields.push_back(CONTENT);

	updateFieldCount();
}

void CharacterAccount::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERACCOUNT, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKACCOUNTS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERACCOUNT, TableImpls::Get()->ACCOUNTS, "fkAccounts", true, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKACCOUNTS);

	updateFieldCount();
}

void CharacterBranch::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERBRANCH, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKBRANCHES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERBRANCH, TableImpls::Get()->BRANCHES, "fkBranches", true, false));
	XP = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHARACTERBRANCH, "xp", false, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKBRANCHES);
	m_fields.push_back(XP);

	updateFieldCount();
}

void CharacterCluster::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERCLUSTER, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKCLUSTERS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERCLUSTER, TableImpls::Get()->CLUSTERS, "fkClusters", true, false));
	XP = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHARACTERCLUSTER, "xp", false, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKCLUSTERS);
	m_fields.push_back(XP);

	updateFieldCount();
}

void CharacterSkill::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERSKILL, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKBRANCHES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERSKILL, TableImpls::Get()->BRANCHES, "fkBranches", true, false));
	XP = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHARACTERSKILL, "xp", false, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKBRANCHES);
	m_fields.push_back(XP);

	updateFieldCount();
}

void CharacterStat::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERSTAT, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKSTATS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERSTAT, TableImpls::Get()->STATS, "fkStats", true, false));
	XP = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHARACTERSTAT, "xp", false, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKSTATS);
	m_fields.push_back(XP);

	updateFieldCount();
}

void CharacterTree::Initialize()
{
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERTREE, TableImpls::Get()->ENTITIES, "fkEntities", true, false));
	FKTREES = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHARACTERTREE, TableImpls::Get()->TREES, "fkTrees", true, false));
	XP = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHARACTERTREE, "xp", false, false));

	m_fields.push_back(FKENTITIES);
	m_fields.push_back(FKTREES);
	m_fields.push_back(XP);

	updateFieldCount();
}

void Clusters::Initialize()
{
	CLUSTERID = KeyImplPtr(new KeyImpl(TableImpls::Get()->CLUSTERS, TableImpls::Get()->CLUSTERS, "clusterid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->CLUSTERS, "name", true, true));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->CLUSTERS, "description", true, false));
	FKAREAS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CLUSTERS, TableImpls::Get()->AREAS, "fkAreas", false, false));

	m_fields.push_back(CLUSTERID);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(FKAREAS);

	updateFieldCount();
}

void Chunks::Initialize()
{
	CHUNKID = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHUNKS, TableImpls::Get()->CHUNKS, "chunkid", true, true));
	FKROOMS = KeyImplPtr(new KeyImpl(TableImpls::Get()->CHUNKS, TableImpls::Get()->ROOMS, "fkRooms", false, false));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHUNKS, "name", true, false));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHUNKS, "description", true, false));
	TAGS = FieldImplPtr(new FieldImpl(TableImpls::Get()->CHUNKS, "tags", true, false));

	m_fields.push_back(CHUNKID);
	m_fields.push_back(FKROOMS);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(TAGS);

	updateFieldCount();
}

void Colours::Initialize()
{
	COLOURID = KeyImplPtr(new KeyImpl(TableImpls::Get()->COLOURS, TableImpls::Get()->COLOURS, "colourid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->COLOURS, "name", true, true));
	CODE = FieldImplPtr(new FieldImpl(TableImpls::Get()->COLOURS, "code", true, true));
	COLOURSTRING = FieldImplPtr(new FieldImpl(TableImpls::Get()->COLOURS, "colourstring", true, false));
	ANSI = FieldImplPtr(new FieldImpl(TableImpls::Get()->COLOURS, "ansi", false, false));

	m_fields.push_back(COLOURID);
	m_fields.push_back(NAME);
	m_fields.push_back(CODE);
	m_fields.push_back(COLOURSTRING);
	m_fields.push_back(ANSI);

	updateFieldCount();
}

void Commands::Initialize()
{
	COMMANDID = KeyImplPtr(new KeyImpl(TableImpls::Get()->COMMANDS, TableImpls::Get()->COMMANDS, "commandid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "name", true, true));
	GRANTGROUP = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "grantgroup", false, std::string("1")));
	HIGHFORCE = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "highforce", false, std::string("1")));
	FORCE = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "force", false, std::string("1")));
	LOWFORCE = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "lowforce", false, std::string("0")));
	HELP = FieldImplPtr(new FieldImpl(TableImpls::Get()->COMMANDS, "help", true, false));

	m_fields.push_back(COMMANDID);
	m_fields.push_back(NAME);
	m_fields.push_back(GRANTGROUP);
	m_fields.push_back(HIGHFORCE);
	m_fields.push_back(FORCE);
	m_fields.push_back(LOWFORCE);
	m_fields.push_back(HELP);

	updateFieldCount();
}

void Details::Initialize()
{
	DETAILID = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILS, TableImpls::Get()->DETAILS, "detailid", true, true));
	KEY = FieldImplPtr(new FieldImpl(TableImpls::Get()->DETAILS, "key", true, true));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->DETAILS, "description", true, false));

	m_fields.push_back(DETAILID);
	m_fields.push_back(KEY);
	m_fields.push_back(DESCRIPTION);

	updateFieldCount();
}

void DetailArea::Initialize()
{
	FKDETAILS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILAREA, TableImpls::Get()->DETAILS, "fkDetails", true, false));
	FKAREAS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILAREA, TableImpls::Get()->AREAS, "fkAreas", true, false));

	m_fields.push_back(FKDETAILS);
	m_fields.push_back(FKAREAS);

	updateFieldCount();
}

void DetailRoom::Initialize()
{
	FKDETAILS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILROOM, TableImpls::Get()->DETAILS, "fkDetails", true, false));
	FKROOMS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILROOM, TableImpls::Get()->ROOMS, "fkRooms", true, false));

	m_fields.push_back(FKDETAILS);
	m_fields.push_back(FKROOMS);

	updateFieldCount();
}

void DetailChunk::Initialize()
{
	FKDETAILS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILCHUNK, TableImpls::Get()->DETAILS, "fkDetails", true, false));
	FKCHUNKS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILCHUNK, TableImpls::Get()->CHUNKS, "fkChunks", true, false));

	m_fields.push_back(FKDETAILS);
	m_fields.push_back(FKCHUNKS);

	updateFieldCount();
}

void DetailCharacter::Initialize()
{
	FKDETAILS = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILCHARACTER, TableImpls::Get()->DETAILS, "fkDetails", true, false));
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILCHARACTER, TableImpls::Get()->ENTITIES, "fkEntities", true, false));

	m_fields.push_back(FKDETAILS);
	m_fields.push_back(FKENTITIES);

	updateFieldCount();
}

void DetailDetail::Initialize()
{
	FKDETAILSPRIMARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILDETAIL, TableImpls::Get()->DETAILS, "fkDetailsPrimary", true, false));
	FKDETAILSSECONDARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->DETAILDETAIL, TableImpls::Get()->DETAILS, "fkDetailsSecondary", true, false));

	m_fields.push_back(FKDETAILSPRIMARY);
	m_fields.push_back(FKDETAILSSECONDARY);

	updateFieldCount();
}

void Echos::Initialize()
{
	ECHOID = KeyImplPtr(new KeyImpl(TableImpls::Get()->ECHOS, TableImpls::Get()->ECHOS, "echoid", true, true));
	MESSAGE = FieldImplPtr(new FieldImpl(TableImpls::Get()->ECHOS, "message", true, false));
	VISIBILITY = FieldImplPtr(new FieldImpl(TableImpls::Get()->ECHOS, "visibility", false, false));
	AUDIBILITY = FieldImplPtr(new FieldImpl(TableImpls::Get()->ECHOS, "audibility", false, false));

	m_fields.push_back(ECHOID);
	m_fields.push_back(MESSAGE);
	m_fields.push_back(VISIBILITY);
	m_fields.push_back(AUDIBILITY);

	updateFieldCount();
}

void Entities::Initialize()
{
	ENTITYID = KeyImplPtr(new KeyImpl(TableImpls::Get()->ENTITIES, TableImpls::Get()->ENTITIES, "entityid", true, true));
	FKRACES = KeyImplPtr(new KeyImpl(TableImpls::Get()->ENTITIES, TableImpls::Get()->RACES, "fkRaces", false, false));
	FKCHUNKS = KeyImplPtr(new KeyImpl(TableImpls::Get()->ENTITIES, TableImpls::Get()->CHUNKS, "fkChunks", false, false));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->ENTITIES, "name", true, true));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->ENTITIES, "description", true, false));

	m_fields.push_back(ENTITYID);
	m_fields.push_back(FKRACES);
	m_fields.push_back(FKCHUNKS);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);

	updateFieldCount();
}

void Exits::Initialize()
{
	EXITID = KeyImplPtr(new KeyImpl(TableImpls::Get()->EXITS, TableImpls::Get()->EXITS, "exitid", true, true));
	FKCHUNKSFROM = KeyImplPtr(new KeyImpl(TableImpls::Get()->EXITS, TableImpls::Get()->CHUNKS, "fkChunksfrom", false, false));
	FKCHUNKSTO = KeyImplPtr(new KeyImpl(TableImpls::Get()->EXITS, TableImpls::Get()->CHUNKS, "fkChunksto", false, false));
	X = FieldImplPtr(new FieldImpl(TableImpls::Get()->EXITS, "x", false, false));
	Y = FieldImplPtr(new FieldImpl(TableImpls::Get()->EXITS, "y", false, false));
	Z = FieldImplPtr(new FieldImpl(TableImpls::Get()->EXITS, "z", false, false));

	m_fields.push_back(EXITID);
	m_fields.push_back(FKCHUNKSFROM);
	m_fields.push_back(FKCHUNKSTO);
	m_fields.push_back(X);
	m_fields.push_back(Y);
	m_fields.push_back(Z);

	updateFieldCount();
}

void GrantGroups::Initialize()
{
	GRANTGROUPID = KeyImplPtr(new KeyImpl(TableImpls::Get()->GRANTGROUPS, TableImpls::Get()->GRANTGROUPS, "grantgroupid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->GRANTGROUPS, "name", true, true));
	DEFAULTGRANT = FieldImplPtr(new FieldImpl(TableImpls::Get()->GRANTGROUPS, "defaultgrant", false, false));
	DEFAULTLOG = FieldImplPtr(new FieldImpl(TableImpls::Get()->GRANTGROUPS, "defaultlog", false, false));
	IMPLIES = FieldImplPtr(new FieldImpl(TableImpls::Get()->GRANTGROUPS, "implies", false, false));

	m_fields.push_back(GRANTGROUPID);
	m_fields.push_back(NAME);
	m_fields.push_back(DEFAULTGRANT);
	m_fields.push_back(DEFAULTLOG);
	m_fields.push_back(IMPLIES);

	updateFieldCount();
}

void Logs::Initialize()
{
	LOGID = KeyImplPtr(new KeyImpl(TableImpls::Get()->LOGS, TableImpls::Get()->LOGS, "logid", true, true));
	TYPE = FieldImplPtr(new FieldImpl(TableImpls::Get()->LOGS, "type", true, false));
	TIME = FieldImplPtr(new FieldImpl(TableImpls::Get()->LOGS, "time", false, false));
	CONTENT = FieldImplPtr(new FieldImpl(TableImpls::Get()->LOGS, "content", true, false));

	m_fields.push_back(LOGID);
	m_fields.push_back(TYPE);
	m_fields.push_back(TIME);
	m_fields.push_back(CONTENT);

	updateFieldCount();
}

void Permissions::Initialize()
{
	FKACCOUNTS = KeyImplPtr(new KeyImpl(TableImpls::Get()->PERMISSIONS, TableImpls::Get()->ACCOUNTS, "fkAccounts", true, false));
	FKGRANTGROUPS = KeyImplPtr(new KeyImpl(TableImpls::Get()->PERMISSIONS, TableImpls::Get()->GRANTGROUPS, "fkGrantGroups", true, false));
	GRANT = FieldImplPtr(new FieldImpl(TableImpls::Get()->PERMISSIONS, "grant", false, false));
	LOG = FieldImplPtr(new FieldImpl(TableImpls::Get()->PERMISSIONS, "log", false, false));

	m_fields.push_back(FKACCOUNTS);
	m_fields.push_back(FKGRANTGROUPS);
	m_fields.push_back(GRANT);
	m_fields.push_back(LOG);

	updateFieldCount();
}

void Races::Initialize()
{
	RACEID = KeyImplPtr(new KeyImpl(TableImpls::Get()->RACES, TableImpls::Get()->RACES, "raceid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->RACES, "name", true, true));

	m_fields.push_back(RACEID);
	m_fields.push_back(NAME);

	updateFieldCount();
}

void Rooms::Initialize()
{
	ROOMID = KeyImplPtr(new KeyImpl(TableImpls::Get()->ROOMS, TableImpls::Get()->ROOMS, "roomid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->ROOMS, "name", true, false));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->ROOMS, "description", true, false));
	FKCLUSTERS = KeyImplPtr(new KeyImpl(TableImpls::Get()->ROOMS, TableImpls::Get()->CLUSTERS, "fkClusters", false, false));
	FKSECTORS = KeyImplPtr(new KeyImpl(TableImpls::Get()->ROOMS, TableImpls::Get()->SECTORS, "fkSectors", false, false));
	WIDTH = FieldImplPtr(new FieldImpl(TableImpls::Get()->ROOMS, "width", false, false));
	LENGTH = FieldImplPtr(new FieldImpl(TableImpls::Get()->ROOMS, "length", false, false));
	HEIGHT = FieldImplPtr(new FieldImpl(TableImpls::Get()->ROOMS, "height", false, false));

	m_fields.push_back(ROOMID);
	m_fields.push_back(NAME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(FKCLUSTERS);
	m_fields.push_back(FKSECTORS);
	m_fields.push_back(WIDTH);
	m_fields.push_back(LENGTH);
	m_fields.push_back(HEIGHT);

	updateFieldCount();
}

void Sectors::Initialize()
{
	SECTORID = KeyImplPtr(new KeyImpl(TableImpls::Get()->SECTORS, TableImpls::Get()->SECTORS, "sectorid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->SECTORS, "name", true, true));
	SYMBOL = FieldImplPtr(new FieldImpl(TableImpls::Get()->SECTORS, "symbol", true, false));
	MOVECOST = FieldImplPtr(new FieldImpl(TableImpls::Get()->SECTORS, "movecost", false, false));
	WATER = FieldImplPtr(new FieldImpl(TableImpls::Get()->SECTORS, "water", false, false));

	m_fields.push_back(SECTORID);
	m_fields.push_back(NAME);
	m_fields.push_back(SYMBOL);
	m_fields.push_back(MOVECOST);
	m_fields.push_back(WATER);

	updateFieldCount();
}

void Skills::Initialize()
{
	SKILLID = KeyImplPtr(new KeyImpl(TableImpls::Get()->SKILLS, TableImpls::Get()->SKILLS, "skillid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->SKILLS, "name", true, true));
	FKBRANCHES = KeyImplPtr(new KeyImpl(TableImpls::Get()->SKILLS, TableImpls::Get()->BRANCHES, "fkBranches", false, false));

	m_fields.push_back(SKILLID);
	m_fields.push_back(NAME);
	m_fields.push_back(FKBRANCHES);

	updateFieldCount();
}

void Socials::Initialize()
{
	SOCIALID = KeyImplPtr(new KeyImpl(TableImpls::Get()->SOCIALS, TableImpls::Get()->SOCIALS, "socialid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->SOCIALS, "name", true, true));
	MESSAGE = FieldImplPtr(new FieldImpl(TableImpls::Get()->SOCIALS, "message", true, false));

	m_fields.push_back(SOCIALID);
	m_fields.push_back(NAME);
	m_fields.push_back(MESSAGE);

	updateFieldCount();
}

void Stats::Initialize()
{
	STATID = KeyImplPtr(new KeyImpl(TableImpls::Get()->STATS, TableImpls::Get()->STATS, "statid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->STATS, "name", true, true));
	SHORTNAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->STATS, "shortname", true, true));

	m_fields.push_back(STATID);
	m_fields.push_back(NAME);
	m_fields.push_back(SHORTNAME);

	updateFieldCount();
}

void Traces::Initialize()
{
	TRACEID = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACES, TableImpls::Get()->TRACES, "traceid", true, true));
	FKACCOUNTS = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACES, TableImpls::Get()->ACCOUNTS, "fkAccounts", false, false));
	TIME = FieldImplPtr(new FieldImpl(TableImpls::Get()->TRACES, "time", false, false));
	DESCRIPTION = FieldImplPtr(new FieldImpl(TableImpls::Get()->TRACES, "description", true, false));
	DIFF = FieldImplPtr(new FieldImpl(TableImpls::Get()->TRACES, "diff", true, false));

	m_fields.push_back(TRACEID);
	m_fields.push_back(FKACCOUNTS);
	m_fields.push_back(TIME);
	m_fields.push_back(DESCRIPTION);
	m_fields.push_back(DIFF);

	updateFieldCount();
}

void TraceChunk::Initialize()
{
	FKTRACES = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACECHUNK, TableImpls::Get()->TRACES, "fkTraces", true, false));
	FKCHUNKS = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACECHUNK, TableImpls::Get()->CHUNKS, "fkChunks", true, false));

	m_fields.push_back(FKTRACES);
	m_fields.push_back(FKCHUNKS);

	updateFieldCount();
}

void TraceDetail::Initialize()
{
	FKTRACES = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEDETAIL, TableImpls::Get()->TRACES, "fkTraces", true, false));
	FKDETAILS = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEDETAIL, TableImpls::Get()->DETAILS, "fkDetails", true, false));

	m_fields.push_back(FKTRACES);
	m_fields.push_back(FKDETAILS);

	updateFieldCount();
}

void TraceEntity::Initialize()
{
	FKTRACES = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEENTITY, TableImpls::Get()->TRACES, "fkTraces", true, false));
	FKENTITIES = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEENTITY, TableImpls::Get()->ENTITIES, "fkEntities", true, false));

	m_fields.push_back(FKTRACES);
	m_fields.push_back(FKENTITIES);

	updateFieldCount();
}

void TraceRoom::Initialize()
{
	FKTRACES = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEROOM, TableImpls::Get()->TRACES, "fkTraces", true, false));
	FKROOMS = KeyImplPtr(new KeyImpl(TableImpls::Get()->TRACEROOM, TableImpls::Get()->ROOMS, "fkRooms", true, false));

	m_fields.push_back(FKTRACES);
	m_fields.push_back(FKROOMS);

	updateFieldCount();
}

void Trees::Initialize()
{
	TREEID = KeyImplPtr(new KeyImpl(TableImpls::Get()->TREES, TableImpls::Get()->TREES, "treeid", true, true));
	NAME = FieldImplPtr(new FieldImpl(TableImpls::Get()->TREES, "name", true, true));
	FKCLUSTERS = KeyImplPtr(new KeyImpl(TableImpls::Get()->TREES, TableImpls::Get()->CLUSTERS, "fkClusters", false, false));
	FKSTATSPRIMARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->TREES, TableImpls::Get()->STATS, "fkStatsPrimary", false, false));
	FKSTATSSECONDARY = KeyImplPtr(new KeyImpl(TableImpls::Get()->TREES, TableImpls::Get()->STATS, "fkStatsSecondary", false, false));

	m_fields.push_back(TREEID);
	m_fields.push_back(NAME);
	m_fields.push_back(FKCLUSTERS);
	m_fields.push_back(FKSTATSPRIMARY);
	m_fields.push_back(FKSTATSSECONDARY);

	updateFieldCount();
}

void Version::Initialize()
{
	VERSIONID = KeyImplPtr(new KeyImpl(TableImpls::Get()->VERSION, TableImpls::Get()->VERSION, "versionid", true, true));
	VERSIONTEXT = FieldImplPtr(new FieldImpl(TableImpls::Get()->VERSION, "versiontext", true, false));
	MAJOR = FieldImplPtr(new FieldImpl(TableImpls::Get()->VERSION, "major", false, false));
	MINOR = FieldImplPtr(new FieldImpl(TableImpls::Get()->VERSION, "minor", false, false));
	MICRO = FieldImplPtr(new FieldImpl(TableImpls::Get()->VERSION, "micro", false, false));

	m_fields.push_back(VERSIONID);
	m_fields.push_back(VERSIONTEXT);
	m_fields.push_back(MAJOR);
	m_fields.push_back(MINOR);
	m_fields.push_back(MICRO);

	updateFieldCount();
}

