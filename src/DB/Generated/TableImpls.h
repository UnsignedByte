/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* NOTE: This file was generated automatically. Do not edit. */

#pragma once

/**
 * @file TableImpls.h
 * This file contains the generated db::TableImpls class.
 *
 * @see db::TableImpls
 */

#include "Types.h"
#include "FieldImpls.h"

namespace db
{
	/**
	 * This is a generated class that contains all TableImpls.
	 *
	 * Before using any tables, <code>Initialize</code> should be called.
	 *
	 * @see Initialize
	 */
	class TableImpls : public Singleton<db::TableImpls>
	{
	public:
		/** Initialize all tables. */
		void Initialize();

		/** Returns an iterator to the beginning of m_tables. */
		TableImplVector::const_iterator begin() const { return m_tables.begin(); }

		/** Returns an iterator to the end of m_tables. */
		TableImplVector::const_iterator end() const { return m_tables.end(); }


		SmartPtr<Accounts> ACCOUNTS; /**< TableImpl for the Accounts class. */
		SmartPtr<Areas> AREAS; /**< TableImpl for the Areas class. */
		SmartPtr<Branches> BRANCHES; /**< TableImpl for the Branches class. */
		SmartPtr<Channels> CHANNELS; /**< TableImpl for the Channels class. */
		SmartPtr<ChannelLogs> CHANNELLOGS; /**< TableImpl for the ChannelLogs class. */
		SmartPtr<CharacterAccount> CHARACTERACCOUNT; /**< TableImpl for the CharacterAccount class. */
		SmartPtr<CharacterBranch> CHARACTERBRANCH; /**< TableImpl for the CharacterBranch class. */
		SmartPtr<CharacterCluster> CHARACTERCLUSTER; /**< TableImpl for the CharacterCluster class. */
		SmartPtr<CharacterSkill> CHARACTERSKILL; /**< TableImpl for the CharacterSkill class. */
		SmartPtr<CharacterStat> CHARACTERSTAT; /**< TableImpl for the CharacterStat class. */
		SmartPtr<CharacterTree> CHARACTERTREE; /**< TableImpl for the CharacterTree class. */
		SmartPtr<Clusters> CLUSTERS; /**< TableImpl for the Clusters class. */
		SmartPtr<Chunks> CHUNKS; /**< TableImpl for the Chunks class. */
		SmartPtr<Colours> COLOURS; /**< TableImpl for the Colours class. */
		SmartPtr<Commands> COMMANDS; /**< TableImpl for the Commands class. */
		SmartPtr<Details> DETAILS; /**< TableImpl for the Details class. */
		SmartPtr<DetailArea> DETAILAREA; /**< TableImpl for the DetailArea class. */
		SmartPtr<DetailRoom> DETAILROOM; /**< TableImpl for the DetailRoom class. */
		SmartPtr<DetailChunk> DETAILCHUNK; /**< TableImpl for the DetailChunk class. */
		SmartPtr<DetailCharacter> DETAILCHARACTER; /**< TableImpl for the DetailCharacter class. */
		SmartPtr<DetailDetail> DETAILDETAIL; /**< TableImpl for the DetailDetail class. */
		SmartPtr<Echos> ECHOS; /**< TableImpl for the Echos class. */
		SmartPtr<Entities> ENTITIES; /**< TableImpl for the Entities class. */
		SmartPtr<Exits> EXITS; /**< TableImpl for the Exits class. */
		SmartPtr<GrantGroups> GRANTGROUPS; /**< TableImpl for the GrantGroups class. */
		SmartPtr<Logs> LOGS; /**< TableImpl for the Logs class. */
		SmartPtr<Permissions> PERMISSIONS; /**< TableImpl for the Permissions class. */
		SmartPtr<Races> RACES; /**< TableImpl for the Races class. */
		SmartPtr<Rooms> ROOMS; /**< TableImpl for the Rooms class. */
		SmartPtr<Sectors> SECTORS; /**< TableImpl for the Sectors class. */
		SmartPtr<Skills> SKILLS; /**< TableImpl for the Skills class. */
		SmartPtr<Socials> SOCIALS; /**< TableImpl for the Socials class. */
		SmartPtr<Stats> STATS; /**< TableImpl for the Stats class. */
		SmartPtr<Traces> TRACES; /**< TableImpl for the Traces class. */
		SmartPtr<TraceChunk> TRACECHUNK; /**< TableImpl for the TraceChunk class. */
		SmartPtr<TraceDetail> TRACEDETAIL; /**< TableImpl for the TraceDetail class. */
		SmartPtr<TraceEntity> TRACEENTITY; /**< TableImpl for the TraceEntity class. */
		SmartPtr<TraceRoom> TRACEROOM; /**< TableImpl for the TraceRoom class. */
		SmartPtr<Trees> TREES; /**< TableImpl for the Trees class. */
		SmartPtr<Version> VERSION; /**< TableImpl for the Version class. */

	private:
		/** This is a singleton class, use <code>Get</code> to get an instance. */
		TableImpls();

		/** This is a singleton class, use <code>Free</code> to free the instance. */
		~TableImpls() { }

		/** Hide the copy constructor. */
		TableImpls(const TableImpls& rhs);

		/** Hide the assignment operator. */
		TableImpls operator=(const TableImpls& rhs);

		friend class Singleton<TableImpls>;

		TableImplVector m_tables; /**< All generated tables. */
		bool m_initialized; /**< Whether this manager is initialized yet. */
	};
} // end of namespace

