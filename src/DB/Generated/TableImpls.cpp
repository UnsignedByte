/***************************************************************************
 *   Copyright (C) 2008 by Sverre Rabbelier                                *
 *   sverre@rabbelier.nl                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* NOTE: This file was generated automatically. Do not edit. */

#include "TableImpl.h"
#include "TableImpls.h"
#include "Tables.h"
#include "FieldImpl.h"
#include "FieldImpls.h"
#include "KeyImpl.h"

using namespace db;

TableImpls::TableImpls() :
ACCOUNTS( SmartPtr<db::Accounts>(new db::Accounts()) ),
AREAS( SmartPtr<db::Areas>(new db::Areas()) ),
BRANCHES( SmartPtr<db::Branches>(new db::Branches()) ),
CHANNELS( SmartPtr<db::Channels>(new db::Channels()) ),
CHANNELLOGS( SmartPtr<db::ChannelLogs>(new db::ChannelLogs()) ),
CHARACTERACCOUNT( SmartPtr<db::CharacterAccount>(new db::CharacterAccount()) ),
CHARACTERBRANCH( SmartPtr<db::CharacterBranch>(new db::CharacterBranch()) ),
CHARACTERCLUSTER( SmartPtr<db::CharacterCluster>(new db::CharacterCluster()) ),
CHARACTERSKILL( SmartPtr<db::CharacterSkill>(new db::CharacterSkill()) ),
CHARACTERSTAT( SmartPtr<db::CharacterStat>(new db::CharacterStat()) ),
CHARACTERTREE( SmartPtr<db::CharacterTree>(new db::CharacterTree()) ),
CLUSTERS( SmartPtr<db::Clusters>(new db::Clusters()) ),
CHUNKS( SmartPtr<db::Chunks>(new db::Chunks()) ),
COLOURS( SmartPtr<db::Colours>(new db::Colours()) ),
COMMANDS( SmartPtr<db::Commands>(new db::Commands()) ),
DETAILS( SmartPtr<db::Details>(new db::Details()) ),
DETAILAREA( SmartPtr<db::DetailArea>(new db::DetailArea()) ),
DETAILROOM( SmartPtr<db::DetailRoom>(new db::DetailRoom()) ),
DETAILCHUNK( SmartPtr<db::DetailChunk>(new db::DetailChunk()) ),
DETAILCHARACTER( SmartPtr<db::DetailCharacter>(new db::DetailCharacter()) ),
DETAILDETAIL( SmartPtr<db::DetailDetail>(new db::DetailDetail()) ),
ECHOS( SmartPtr<db::Echos>(new db::Echos()) ),
ENTITIES( SmartPtr<db::Entities>(new db::Entities()) ),
EXITS( SmartPtr<db::Exits>(new db::Exits()) ),
GRANTGROUPS( SmartPtr<db::GrantGroups>(new db::GrantGroups()) ),
LOGS( SmartPtr<db::Logs>(new db::Logs()) ),
PERMISSIONS( SmartPtr<db::Permissions>(new db::Permissions()) ),
RACES( SmartPtr<db::Races>(new db::Races()) ),
ROOMS( SmartPtr<db::Rooms>(new db::Rooms()) ),
SECTORS( SmartPtr<db::Sectors>(new db::Sectors()) ),
SKILLS( SmartPtr<db::Skills>(new db::Skills()) ),
SOCIALS( SmartPtr<db::Socials>(new db::Socials()) ),
STATS( SmartPtr<db::Stats>(new db::Stats()) ),
TRACES( SmartPtr<db::Traces>(new db::Traces()) ),
TRACECHUNK( SmartPtr<db::TraceChunk>(new db::TraceChunk()) ),
TRACEDETAIL( SmartPtr<db::TraceDetail>(new db::TraceDetail()) ),
TRACEENTITY( SmartPtr<db::TraceEntity>(new db::TraceEntity()) ),
TRACEROOM( SmartPtr<db::TraceRoom>(new db::TraceRoom()) ),
TREES( SmartPtr<db::Trees>(new db::Trees()) ),
VERSION( SmartPtr<db::Version>(new db::Version()) )
{
	m_tables.push_back(ACCOUNTS);
	m_tables.push_back(AREAS);
	m_tables.push_back(BRANCHES);
	m_tables.push_back(CHANNELS);
	m_tables.push_back(CHANNELLOGS);
	m_tables.push_back(CHARACTERACCOUNT);
	m_tables.push_back(CHARACTERBRANCH);
	m_tables.push_back(CHARACTERCLUSTER);
	m_tables.push_back(CHARACTERSKILL);
	m_tables.push_back(CHARACTERSTAT);
	m_tables.push_back(CHARACTERTREE);
	m_tables.push_back(CLUSTERS);
	m_tables.push_back(CHUNKS);
	m_tables.push_back(COLOURS);
	m_tables.push_back(COMMANDS);
	m_tables.push_back(DETAILS);
	m_tables.push_back(DETAILAREA);
	m_tables.push_back(DETAILROOM);
	m_tables.push_back(DETAILCHUNK);
	m_tables.push_back(DETAILCHARACTER);
	m_tables.push_back(DETAILDETAIL);
	m_tables.push_back(ECHOS);
	m_tables.push_back(ENTITIES);
	m_tables.push_back(EXITS);
	m_tables.push_back(GRANTGROUPS);
	m_tables.push_back(LOGS);
	m_tables.push_back(PERMISSIONS);
	m_tables.push_back(RACES);
	m_tables.push_back(ROOMS);
	m_tables.push_back(SECTORS);
	m_tables.push_back(SKILLS);
	m_tables.push_back(SOCIALS);
	m_tables.push_back(STATS);
	m_tables.push_back(TRACES);
	m_tables.push_back(TRACECHUNK);
	m_tables.push_back(TRACEDETAIL);
	m_tables.push_back(TRACEENTITY);
	m_tables.push_back(TRACEROOM);
	m_tables.push_back(TREES);
	m_tables.push_back(VERSION);
}

void TableImpls::Initialize()
{
	Assert(!m_initialized);

	m_initialized = true;

	for(TableImplVector::const_iterator it = m_tables.begin(); it != m_tables.end(); it++)
	{
		TableImplPtr table = *it;
		Assert(table);

		table->Initialize();
	}

}

